<?php
	session_start();
	if(!isset($_SESSION['usrname'])){
		header("location:index.php");
	}
?>

<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<link rel="shortcut icon" href="./favicon.ico">

    <title>A prototype webGIS application for management of natural hazards</title>
	
    <!-- Bootstrap core CSS -->
    <link href="startbootstrap-creative-gh-pages/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	
	<!-- Custom Fonts -->
    <link href="startbootstrap-creative-gh-pages/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
	
	<!-- Plugin CSS -->
    <link href="startbootstrap-creative-gh-pages/vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="startbootstrap-creative-gh-pages/css/creative.min.css" rel="stylesheet">
	
	<!-- Google resources -->
	<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<!-- <script src="//maps.googleapis.com/maps/api/js?v=3&key=AIzaSyAsv78e2iZPYgwC_5uZf2Od09Avs7WQHJ8" type="text/javascript"></script> -->
        
    <!-- Ext resources -->
    <link rel="stylesheet" type="text/css" href="src/ext/resources/css/ext-all.css">
    <link rel="stylesheet" type="text/css" href="src/ext/resources/css/xtheme-blue.css">
    <script type="text/javascript" src="src/ext/adapter/ext/ext-base.js"></script>
    <script type="text/javascript" src="src/ext/ext-all.js"></script>
	<script type="text/javascript" src="src/ext/Ext.ux.util.js"></script>
	<script type="text/javascript" src="src/ext/Ext.ux.Exporter/Exporter-all.js"></script>

    <!-- OpenLayers resources -->
    <link rel="stylesheet" type="text/css" href="src/openlayers/theme/default/style.css">

    <!-- GeoExt resources -->
    <link rel="stylesheet" type="text/css" href="src/geoext/resources/css/popup.css">
    <link rel="stylesheet" type="text/css" href="src/geoext/resources/css/layerlegend.css">
    <link rel="stylesheet" type="text/css" href="src/geoext/resources/css/gxtheme-slate.css">
	<link rel="stylesheet" type="text/css" href="src/geoext/resources/css/printpreview.css">

    <!-- gxp resources -->
    <link rel="stylesheet" type="text/css" href="src/gxp/theme/all.css">        

    <!-- app resources -->
    <link rel="stylesheet" type="text/css" href="theme/app/style.css">
	<link rel="stylesheet" type="text/css" href="theme/app/superboxselect.css">
	<link rel="stylesheet" type="text/css" href="theme/app/Spinner.css">
    <link rel="stylesheet" type="text/css" href="theme/app/GroupSummary.css">
	<link rel="stylesheet" type="text/css" href="theme/app/Ext.ux.form.LovCombo.css">
	
	<style>
	   .x-grid3-cell-inner {
            font-family:"segoe ui",tahoma, arial, sans-serif;
        }
        .x-grid-group-hd div {
            font-family:"segoe ui",tahoma, arial, sans-serif;
        }
        .x-grid3-hd-inner {
            font-family:"segoe ui",tahoma, arial, sans-serif;
            font-size:12px;
        }
        .x-grid3-body .x-grid3-td-cost {
            background-color:#f1f2f4;
        }
        .x-grid3-summary-row .x-grid3-td-cost {
            background-color:#e1e2e4;
        }
		.icon-grid {
			background-image:url(src/gxp/theme/img/silk/icon-grid.png) !important;
		}	
		.navbar {
			background-color: #4863A0;
		}
	</style>
       
    </head>
	
<body>
	<nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="http://www.vd.ch/themes/territoire/dangers-naturels/">CDN-VD</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-left">					
					<li>
						<a href="#">						
						<?php
							echo 'Welcome, ', $_SESSION['usrname'],'!';	
						?>
						</a>
					</li>                
				</ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="/Manual.pdf" target='_blank'>User Guide</a>
                    </li>
					<li>
                        <a href="/Method.pdf" target='_blank'>Documentation</a>
                    </li>                                        
                    <li>
                        <a href="logout.php">Logout</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
	
	<div>
		<script>var role = "<?php echo $_SESSION['role']; ?>";</script>
		<script>var usrname = "<?php echo $_SESSION['usrname']; ?>";</script>
		<script>var userid = "<?php echo $_SESSION['userid']; ?>";</script>	
		<script>var dbname = "<?php echo $_SESSION['dbname']; ?>";</script>	
		<script>var displayname = "<?php echo $_SESSION['displayname']; ?>";</script>	
		<script>var email = "<?php echo $_SESSION['email']; ?>";</script>	
		<script type="text/javascript" src="lib/app.js"></script>
			
	 <script>
		 Ext.BLANK_IMAGE_URL = "theme/app/img/blank.gif";
		 OpenLayers.ImgPath = "externals/openlayers/img/";
	 </script>
	</div>
 	
</body>
</html>



