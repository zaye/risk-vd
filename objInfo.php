<?php
	$workspace = $_POST['ws'];
	$task = $_POST['task'];	
	$datastore = $workspace;
	
	$dbconn=pg_connect("host=localhost port=5432 dbname=dbname user=username password=mypassword");	// replace with your db connection information
	
	if (!$dbconn){
		echo "An error occured.\n";
		exit;
	}
	
	if ($task == 'loadSelected') {
		$objType = $_POST['type'];
		
		$query = "SELECT nom, indice FROM ".$workspace.".objects WHERE type = '$objType';"; 
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	if ($task == 'load') {
		$query = "SELECT objects.*, alternatives.nom as alt_nom FROM ".$workspace.".objects, ".$workspace.".objects_alternatives, ".$workspace.".alternatives 
			WHERE objects_alternatives.objet_id = objects.id AND alternatives.id = objects_alternatives.alt_id;"; 		
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	if ($task == 'add') { 
		$temp = $_POST['names'];
		$array = json_decode($temp, true);
		$mapping_index = $array[0];
		
		$objType = $_POST['objType'];
		$layer_title = $_POST['title'];
		$layer_desc = $_POST['desc'];
		$file_name = $_POST['file'];
		$alt_id = $_POST['objAltID'];
		$remarks = $_POST['remarks'];
		
		$query = "INSERT INTO ".$workspace.".objects VALUES (DEFAULT, '$layer_title', '$layer_desc', '$objType', '$remarks', '$file_name', '$mapping_index');"; 
		$query .= "INSERT INTO ".$workspace.".objects_alternatives VALUES ((SELECT id FROM ".$workspace.".objects WHERE indice = '$mapping_index'),(SELECT id FROM ".$workspace.".alternatives WHERE id = '$alt_id'));";	 
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {	
			Echo '{success:true,message: "The new object map has been successfully uploaded!"}'; 
		}	
	}
	
	if ($task == 'delete') {
		$temp = $_POST['objRecords'];		
		$objRecords = json_decode($temp, true);
		$length = count($objRecords);
		
		for ($i = 0; $i < $length; $i++) {	// for each of the selected object layers
			$ID = $objRecords[$i]['id'];			
			$mpIndex = $objRecords[$i]['indice'];
			
			### Step1: remove the records from the objects and its referenced records in the objects_alternatives table
			$query = "DELETE FROM ".$workspace.".objects WHERE id = $ID;";
			
			### Step2: drop the respective layer table using indice column from the db
			$query .= 'DROP TABLE '.$workspace.'."'.$mpIndex.'";';
				
			if (!$rs = pg_query($dbconn,$query)){			
				$message .= 'Failed to delete the record: '.$objRecords[$i]['nom'].' due to the error: '.json_encode(pg_last_error($dbconn));						
			}
			else {
				// if success
				### Step3: remove the published map from the geoserver
					// Initiate cURL session
					$service = "http://localhost:8080/geoserver/"; // replace with your URL
					$request = "rest/workspaces/".$workspace."/datastores/".$datastore."/featuretypes/".$mpIndex.".xml?recurse=true"; // to delete a feature type

					$url = $service . $request;
					$ch = curl_init($url);

					// Optional settings for debugging
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //option to return string
					curl_setopt($ch, CURLOPT_VERBOSE, true);
					curl_setopt($ch, CURLOPT_STDERR, $logfh); // logs curl messages

					//Required DELETE request settings
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
					$passwordStr = "username:mypassword"; // replace with your GeoServer username and password
					curl_setopt($ch, CURLOPT_USERPWD, $passwordStr);

					//POST return code
					$successCode = 200;

					// Execute the curl request
					$buffer = curl_exec($ch); 

					// Check for errors and process results
					$info = curl_getinfo($ch);			
					if ($info['http_code'] != $successCode) {
						$message .= "Unuccessful cURL request to ".$url."\n";						
					}
					else {
						$message .= 'The selected map layer: '.$objRecords[$i]['nom'].' has been deleted!';
					}
					curl_close($ch); // free resources if curl handle will not be reused
					
			}
		}
		Echo '{success: true, message:'.json_encode($message).'}';
	}
	
	if ($task == 'edit') {
		
		// Get the layer name 
		$mpIndex = $_POST['mpIndex'];		
		$ID = $_POST['ID'];	
		$nom = $_POST['name'];
		$desc = $_POST['description'];		
		$type = $_POST['type'];
		$remarques = $_POST['remarks'];
		
			// Initiate cURL session
			$service = "http://localhost:8080/geoserver/"; // replace with your URL
			$request = "rest/workspaces/".$workspace."/datastores/".$datastore."/featuretypes/".$mpIndex.".xml"; // to edit a vector store

			$url = $service . $request;
			$ch = curl_init($url);

			// Optional settings for debugging
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //option to return string
			curl_setopt($ch, CURLOPT_VERBOSE, true);
			curl_setopt($ch, CURLOPT_STDERR, $logfh); // logs curl messages

			//Required PUT request settings
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
			$passwordStr = "username:mypassword"; // replace with your GeoServer username and password
			curl_setopt($ch, CURLOPT_USERPWD, $passwordStr);
			
			//POST data
			curl_setopt($ch, CURLOPT_HTTPHEADER,
				array("Content-type: application/xml"));
			$xmlStr = "<featureType><title>".$nom."</title><keywords><string>features</string><string>".$nom."</string></keywords><enabled>true</enabled></featureType>";
			curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlStr);

			//POST return code
			$successCode = 200;

			// Execute the curl request
			$buffer = curl_exec($ch); 
			
			$info = curl_getinfo($ch);
			curl_close($ch); // free resources if curl handle will not be reused
			
			if ($info['http_code'] != $successCode) {
				Echo '{success:false, message:"There is an error in changing the layer title in geoserver!"}';
			}
			else {
				// if success, update the table
				$query= "UPDATE ".$workspace.".objects SET nom = '$nom', description = '$desc', type = '$type', remarques = '$remarques' WHERE id = $ID;";
				$arr=array();
				
				If (!$rs = pg_query($dbconn,$query)) {
					Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
				}
				else {
					Echo '{success:true,message:"The data has been successfully updated!"}';		
				}				
			}
	}
	
	pg_close($dbconn);
?>