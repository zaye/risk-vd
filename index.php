<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>A prototype webGIS application</title>

    <!-- Bootstrap Core CSS -->
    <link href="startbootstrap-creative-gh-pages/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="startbootstrap-creative-gh-pages/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link href="startbootstrap-creative-gh-pages/vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

    <!-- Theme CSS -->
    <link href="startbootstrap-creative-gh-pages/css/creative.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top">

    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="http://www.vd.ch/themes/territoire/dangers-naturels/">CDN-VD</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="#about">About</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#features">Features</a>
                    </li>                    
                    <li>
                        <a class="page-scroll" href="#contact">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <header>
        <div class="header-content">
            <div class="header-content-inner">
                <h1 id="homeHeading">A webGIS application for risk management of natural hazards in Canton Vaud</h1>
                <hr>
                <p>This application allows to perform a rapid evaluation of risk before and after the consideration of protection measures, and to test the efficiency of measures using a simple cost-benefit analysis tool.</p>				
                <a href="#about" class="btn btn-primary btn-xl page-scroll">Find Out More</a>
            </div>
        </div>
    </header>

    <section class="bg-primary" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">We've got what you need!</h2>
                    <hr class="light">
                    <p class="text-faded">We aim to assist local authorities and community of practitioners in performing a rapid evaluation of natural hazards related risk before and after designing protection measures, and to test the efficiency of measures using a simple cost-benefit analysis. This application is based on the risk concepts applied in EconoMe 4.0 and Valdorisk 1.5. In this application, a spatial component is integrated through a webGIS interface for better risk planning, visualization and evaluation.</p>
					<p class="text-faded">This prototype application is realized based on the open-source geospatial software and technologies, backed up by <b><a class="text-faded" href="http://boundlessgeo.com/">Boundless</a></b> framework and its client-side <b><a class="text-faded" href="http://developers.boundlessgeo.com/">SDK</a></b> development environment. Free and open source components such as PostGIS spatial database, GeoServer and GeoWebCache, GeoExt and OpenLayers are used for the development of the platform.</p>
					<p class="text-faded">This project is funded by Canton Vaud and developed by <b><a class="text-faded" href="http://wp.unil.ch/risk/">Risk Analysis Group</a></b> of the University of Lausanne.</p>
                    <a href="#features" class="page-scroll btn btn-default btn-xl sr-button">Get Started!</a>
                </div>
            </div>
        </div>
    </section>

    <section id="features">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Main Features</h2>
                    <hr class="primary">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-map text-primary sr-icons"></i>
                        <h3>Risk Estimation</h3>
                        <p class="text-muted">Different types of risk (objects and persons) can be calculated for the various combination of hazard and object types.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-pencil text-primary sr-icons"></i>
                        <h3>Planning of Measures</h3>
                        <p class="text-muted">A combination of risk reduction measures can be designed using sketching and editing tools of the webGIS interface.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-money text-primary sr-icons"></i>
                        <h3>Cost Estimation</h3>
                        <p class="text-muted">Annual cost of measures is estimated using different parameters such as investment, operating and maintenance costs.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-bar-chart text-primary sr-icons"></i>
                        <h3>Cost-Benefit Analysis</h3>
                        <p class="text-muted">A cost-benefit report can be generated to compare the risk before and after consideration of measures.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <aside class="bg-dark">
        <div class="container text-center">
            <div class="call-to-action">
                <h2>Try It Now!</h2>					
                <a href="login.php" class="btn btn-default btn-xl sr-button">Sign in!</a>
            </div>
        </div>
    </aside>

    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">Let's Get In Touch!</h2>
                    <hr class="primary">
                    <p>Do you have any feedback or questions? That's great! Give us a call or send us an email and we will get back to you!</p>
                </div>
                <div class="col-lg-4 col-lg-offset-2 text-center">
                    <i class="fa fa-phone fa-3x sr-contact"></i>
                    <p>0216923547</p>
                </div>
                <div class="col-lg-4 text-center">
                    <i class="fa fa-envelope-o fa-3x sr-contact"></i>
                    <p><a href="mailto:zarchi.aye@unil.ch">zarchi.aye@unil.ch</a></p>
                </div>
            </div>
        </div>
    </section>

    <!-- jQuery -->
    <script src="startbootstrap-creative-gh-pages/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="startbootstrap-creative-gh-pages/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script src="startbootstrap-creative-gh-pages/vendor/scrollreveal/scrollreveal.min.js"></script>
    <script src="startbootstrap-creative-gh-pages/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

    <!-- Theme JavaScript -->
    <script src="startbootstrap-creative-gh-pages/js/creative.min.js"></script>

</body>

</html>
