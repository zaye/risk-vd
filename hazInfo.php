<?php
	
	$workspace = $_POST['ws'];
	$task = $_POST['task'];	
	$datastore = $workspace;
	
	$dbconn=pg_connect("host=localhost port=5432 dbname=dbname user=username password=mypassword");	// replace with your db connection information	
	if (!$dbconn){
		echo "An error occured.\n";
		exit;
	}
	
	if ($task == 'loadSelected') {
		$hazType = $_POST['type'];
		$hazProb = $_POST['prob'];
		
		if ($hazProb == 'TFreq') {
			$query = "SELECT nom, indice, ans, temp_de_retour FROM ".$workspace.".hazards WHERE type = '$hazType' AND (temp_de_retour = '$hazProb' OR ans < 30);";
		}
		elseif ($hazProb == 'TEx') {
			$query = "SELECT nom, indice, ans, temp_de_retour FROM ".$workspace.".hazards WHERE type = '$hazType' AND (temp_de_retour = '$hazProb' OR ans > 300);";
		}
		else {
			$query = "SELECT nom, indice, ans, temp_de_retour FROM ".$workspace.".hazards WHERE type = '$hazType' AND temp_de_retour = '$hazProb';";
		}	
		 
		$arr=array();
		array_push($arr, array(nom=>'---',indice=>'None'));
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				array_push($arr,$obj);
			//	$arr[] = $obj;
			}
				
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	if ($task == 'load') {
		$query = "SELECT hazards.*, alternatives.nom as alt_nom FROM ".$workspace.".hazards, ".$workspace.".hazards_alternatives, ".$workspace.".alternatives 
		WHERE hazards_alternatives.processus_id = hazards.id AND alternatives.id = hazards_alternatives.alt_id;"; 		
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	if ($task == 'add') {
		$title = $_POST['title'];
		$desc = $_POST['abstract'];
		$remarks = $_POST['remarks'];
		$shpFileName = $_POST['shpfilename'];
		$tabFileName = $_POST['tabfilename'];
		$userName = $_POST['username'];
		$hazType = $_POST['haz_type'];
		$hazAltID = $_POST['hazAltID'];
		
	/* 	$temp1 = explode(".", $_FILES["file"]["name"]);
		$extension = end($temp1);
		$str = strtolower($temp1[0]);
		$str = preg_replace("/[^a-z0-9_\s-]/", "", $str);
		$str = preg_replace("/[\s-]+/", " ", $str);
		$file_mapping_index = preg_replace("/[\s]/", "_", $str);	
		
		$temp2 = explode(".", $_FILES["table"]["name"]);
		$extension = end($temp2);
		$str = strtolower($temp2[0]);
		$str = preg_replace("/[^a-z0-9_\s-]/", "", $str);
		$str = preg_replace("/[\s-]+/", " ", $str);
		$mapping_index = preg_replace("/[\s]/", "_", $str);	 */
		
	if ($_FILES["table"]["error"] > 0 || $_FILES["file"]["error"] > 0)
		{
			echo '{success:false,message:'.json_encode($_FILES["table"]["error"]).'\n'.json_encode($_FILES["file"]["error"]).'}'; 
		}
	else {
			### STEP 1: extract the uploaded zip and import the shape file (intensity) using shp2pgsql tool
			if (is_uploaded_file($_FILES["file"]["tmp_name"])) {
				// if uploaded via HTTP POST	
				$file_name = $_FILES['file']['name'];
				$actual_name = pathinfo($file_name,PATHINFO_FILENAME);
				$actual_name = preg_replace('/\s+/','_',$actual_name);
				$original_name = $actual_name;
				$extension = pathinfo($file_name,PATHINFO_EXTENSION);

				// rename file name by appending a number to the end, if already exists
				$i = 1;
				while(file_exists(dirname(__FILE__) . '/upload/'.$actual_name.".".$extension))
				{           
					$actual_name = (string)$original_name.$i;
					$file_name = $actual_name.".".$extension;
					$i++;
				}
				
				$targetpath = dirname(__FILE__) . '/upload/'.$actual_name.".".$extension;
				$path_parts = pathinfo($targetpath);
				$withoutExt = dirname(__FILE__) . '/upload/'. $path_parts['filename'];
				$file_mapping_index = $actual_name;
				
				if (file_exists($targetpath))
				{					
					Echo '{success:false,message:"The shape file archive already exists!"}';
				}
				else {
					if (move_uploaded_file($_FILES["file"]["tmp_name"],$targetpath)){
						// if successfully moved to 'upload' folder, import the shape layer using shp2pgsql tool
						$zip = new ZipArchive();
						$res = $zip->open($targetpath);
						if ($res === true) {
							$success = $zip->extractTo($withoutExt.'/'); // extracted (shape) file names should not contain spaces, otherwise shp2pgsql would fail at importing data into the database													
							$getShpFile = glob($withoutExt.'/*.shp');
							$nameShpFile = pathinfo($getShpFile[0],PATHINFO_FILENAME);							
							
							$zip->close();
							
							if ($success === true) {
								$command_str = 'shp2pgsql -d -s 2056 -W "latin1" '.$withoutExt.'/'.$nameShpFile.' '.$workspace.'.'.$file_mapping_index.' | PGPASSWORD=mypassword psql -h localhost -p 5432 -d dbname -U username';
								$tmp = shell_exec($command_str); 
								
								if ($tmp === NULL) echo '{success:false,message:"There was an error in importing shapefile to the database!" }'; // return NULL both when an error occurs or the program produces no output
								else {
									### STEP 2: create tables for different return periods (for GPP-TR)
									if ($hazType == 'GPP-TR'){
										$query = "SELECT distinct t1, t2, t3 FROM $workspace.$file_mapping_index;";
																
										If (!$rs = pg_query($dbconn,$query)) {
											Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
										}
										else {
											$query = "";														
											for ($j = 0; $j < 3; $j++) {				
												$prob[$j] = pg_fetch_result($rs,0,$j);															
												$str = strtolower($title.'_T'.$prob[$j]);															
												$str = preg_replace("/[^a-z0-9_\s-]/", "", $str);
												$str = preg_replace("/[\s-]+/", " ", $str);
												$name[$j] = preg_replace("/[\s]/", "_", $str);	
												$t = $j+1;				
												
												$query .= "CREATE TABLE $workspace.$name[$j] AS TABLE $workspace.$file_mapping_index;";															
												$query .= "ALTER TABLE $workspace.$name[$j] ADD PRIMARY KEY (gid);";
												$query .= "ALTER TABLE $workspace.$name[$j] ADD COLUMN cin_scepro varchar(30) DEFAULT concat('T',$prob[$j]);";
												$query .= "ALTER TABLE $workspace.$name[$j] RENAME COLUMN temps_$t TO cin_lvin_1;";
												$query .= "ALTER TABLE $workspace.$name[$j] ALTER COLUMN geom TYPE geometry(MultiPolygon,2056) USING ST_Force_2D(geom);";
												$query .= "INSERT INTO $workspace.hazards(id, nom, description, type, remarques, nom_origine, indice, temp_de_retour, ans)
															VALUES(default,'$name[$j]','$desc','$hazType','$remarks','$shpFileName','$name[$j]', concat('T',$prob[$j]),$prob[$j]);";															
												$query .= "INSERT INTO $workspace.hazards_alternatives VALUES ((SELECT id FROM ".$workspace.".hazards WHERE indice = '$name[$j]'), $hazAltID);";
											}
											If (!$rs = pg_query($dbconn,$query)) {
												Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
											}
											else {
													### STEP 3: publish the tables to the geoserver														
													
													// Initiate cURL session
													$service = "http://localhost:8080/geoserver/"; // replace with your URL
													$request = "rest/workspaces/".$workspace."/datastores/".$datastore."/featuretypes"; // to add a new featuretype

													$url = $service . $request;
													$ch = curl_init($url);
																
													// Optional settings for debugging
													curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //option to return string
													curl_setopt($ch, CURLOPT_VERBOSE, true);
													curl_setopt($ch, CURLOPT_STDERR, $logfh); // logs curl messages

													//Required POST request settings
													curl_setopt($ch, CURLOPT_POST, True);
													$passwordStr = "username:mypassword"; // replace with your GeoServer username and password
													curl_setopt($ch, CURLOPT_USERPWD, $passwordStr);
																
													//POST data 
													curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: application/xml"));
																
													//POST return code
													$successCode = 201;	$count = 0;
																
													for ($j = 0; $j < 3; $j++) {	
														$xmlStr = "<featureType><name>".$name[$j]."</name><title>".$name[$j]."</title><srs>EPSG:2056</srs></featureType>";
														curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlStr);
														$buffer = curl_exec($ch);					
														$info = curl_getinfo($ch);
														if ($info['http_code'] != $successCode) {
															$count = $count + 1;
															$msgStr .= "# Unsuccessful cURL request to ";
															$msgStr .= $url." [". $info['http_code']. "]\n"; 
														}																
													}
																	
													### STEP 4: associate the existing style 'CIN' to the published layers in GeoServer	
													if ($count == 0) {																											 
														for ($j = 0; $j < 3; $j++) {
															$request = "rest/layers/".$workspace.":".$name[$j].".xml"; 
															$url = $service . $request;
															$ch = curl_init($url);
															
															// Optional settings for debugging
															curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //option to return string
															curl_setopt($ch, CURLOPT_VERBOSE, true);
															curl_setopt($ch, CURLOPT_STDERR, $logfh); // logs curl messages

															//Required PUT request settings
															curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');			
															curl_setopt($ch, CURLOPT_USERPWD, $passwordStr);
																		
															//POST data 
															curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: application/xml"));
															$xmlStr = '<layer><defaultStyle><name>CIN</name><atom:link rel="alternate" href="http://localhost:8080/geoserver/rest/styles/CIN.xml" type="application/xml"/></defaultStyle></layer>';
															curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlStr);
															
															//PUT return code
															$successCode = 200;	$styleCount = 0;														
															
															// Execute the curl request
															$buffer = curl_exec($ch);					
															$info = curl_getinfo($ch);
															if ($info['http_code'] != $successCode) {
																$styleCount = $styleCount + 1;
																$msgStr .= "# Unsuccessful cURL request to ";
																$msgStr .= $url." [". $info['http_code']. "]\n"; 
															}																
														}
														# send the message back to the user 
														if ($styleCount == 0) Echo '{success: true, message:"Data has been imported!"}';			
														else Echo '{success: false, message:'.json_encode($msgStr).'}';																									
													}	
													else Echo '{success: false, message:'.json_encode($msgStr).'}';	
											}
										}
									}
									elseif ($hazType == 'GSS') {
										### STEP 2: create tables for different return periods based on the intensity class of GSS2 (for GSS)
										$query = "ALTER TABLE $workspace.$file_mapping_index ADD COLUMN cin_scepro varchar(30);";
										$query .= "UPDATE $workspace.$file_mapping_index SET cin_scepro =  'T300' WHERE cin_lvin_3 = '1' AND cin_lvin_2 = 'GSS2';";
										$query .= "UPDATE $workspace.$file_mapping_index SET cin_scepro =  'T100' WHERE cin_lvin_3 = '2' AND cin_lvin_2 = 'GSS2';";
										$query .= "UPDATE $workspace.$file_mapping_index SET cin_scepro =  'T30' WHERE cin_lvin_3 = '3' AND cin_lvin_2 = 'GSS2';";
										$query .= "UPDATE $workspace.$file_mapping_index SET cin_scepro =  'TEx' WHERE cin_lvin_3 = '5' AND cin_lvin_2 = 'GSS2';";
										$query .= "SELECT distinct cin_scepro FROM $workspace.$file_mapping_index WHERE cin_scepro != '';";
														
										If (!$rs = pg_query($dbconn,$query)) {
											Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
										}
										else {
											// loop for each retrieved return period to create new tables and add records to the 'hazards' table
											$query = "";
											$row = pg_affected_rows($rs);
																
											for ($j = 0; $j < $row; $j++) {				
												$prob[$j] = pg_fetch_result($rs, $j, 0);															
												$str = strtolower($title.'_'.$prob[$j]);															
												$str = preg_replace("/[^a-z0-9_\s-]/", "", $str);
												$str = preg_replace("/[\s-]+/", " ", $str);
												$name[$j] = preg_replace("/[\s]/", "_", $str);	
																	
												$query .= "CREATE TABLE $workspace.$name[$j] AS
															(SELECT *
															FROM $workspace.$file_mapping_index
															WHERE cin_scepro = '$prob[$j]');";
																			
												$query .= "ALTER TABLE $workspace.$name[$j] ADD PRIMARY KEY (gid);";
																	
												if ($prob[$j] == 'TEx' ) {
													$query .= "INSERT INTO $workspace.hazards(id, nom, description, type, remarques, nom_origine, indice, temp_de_retour, ans) 
													VALUES(default,'$name[$j]','$desc','$hazType','$remarks','$shpFileName','$name[$j]','$prob[$j]',1000);";
												}
												else { 
													$query .= "INSERT INTO $workspace.hazards(id, nom, description, type, remarques, nom_origine, indice, temp_de_retour, ans)
													VALUES(default,'$name[$j]','$desc','$hazType','$remarks','$shpFileName','$name[$j]','$prob[$j]',substring('$prob[$j]' from 2)::int);";
												}															
																	
												$query .= "INSERT INTO $workspace.hazards_alternatives VALUES ((SELECT id FROM ".$workspace.".hazards WHERE indice = '$name[$j]'), $hazAltID);";	 
											}
											
											If (!$rs = pg_query($dbconn,$query)) {
												Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
											}
											else {
												### STEP 3: publish the tables to the geoserver														
																
												// Initiate cURL session
												$service = "http://localhost:8080/geoserver/"; // replace with your URL
												$request = "rest/workspaces/".$workspace."/datastores/".$datastore."/featuretypes"; // to add a new featuretype

												$url = $service . $request;
												$ch = curl_init($url);
																
												// Optional settings for debugging
												curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //option to return string
												curl_setopt($ch, CURLOPT_VERBOSE, true);
												curl_setopt($ch, CURLOPT_STDERR, $logfh); // logs curl messages

												//Required POST request settings
												curl_setopt($ch, CURLOPT_POST, True);
												$passwordStr = "username:mypassword"; // replace with your GeoServer username and password
												curl_setopt($ch, CURLOPT_USERPWD, $passwordStr);
																
												//POST data 
												curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: application/xml"));
																
												//POST return code
												$successCode = 201;	$count = 0;
																
												for ($j = 0; $j < $row; $j++) {	
													$xmlStr = "<featureType><name>".$name[$j]."</name><title>".$name[$j]."</title><srs>EPSG:2056</srs></featureType>";
													curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlStr);
													$buffer = curl_exec($ch);					
													$info = curl_getinfo($ch);
													if ($info['http_code'] != $successCode) {
														$count = $count + 1;
														$msgStr .= "# Unsuccessful cURL request to ";
														$msgStr .= $url." [". $info['http_code']. "]\n"; 
													}																
												}
																		
												### STEP 4: associate the existing style 'CIN' to the published layers in GeoServer	
												if ($count == 0) {																											 
													for ($j = 0; $j < $row; $j++) {
														$request = "rest/layers/".$workspace.":".$name[$j].".xml"; 
														$url = $service . $request;
														$ch = curl_init($url);
															
														// Optional settings for debugging
														curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //option to return string
														curl_setopt($ch, CURLOPT_VERBOSE, true);
														curl_setopt($ch, CURLOPT_STDERR, $logfh); // logs curl messages

														//Required PUT request settings
														curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');			
														curl_setopt($ch, CURLOPT_USERPWD, $passwordStr);
																		
														//POST data 
														curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: application/xml"));
														$xmlStr = '<layer><defaultStyle><name>CIN</name><atom:link rel="alternate" href="http://localhost:8080/geoserver/rest/styles/CIN.xml" type="application/xml"/></defaultStyle></layer>';
														curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlStr);
															
														//PUT return code
														$successCode = 200;	$styleCount = 0;														
															
														// Execute the curl request
														$buffer = curl_exec($ch);					
														$info = curl_getinfo($ch);
														if ($info['http_code'] != $successCode) {
															$styleCount = $styleCount + 1;
															$msgStr .= "# Unsuccessful cURL request to ";
															$msgStr .= $url." [". $info['http_code']. "]\n"; 
														}																
													}
													# send the message back to the user 
													if ($styleCount == 0) Echo '{success: true, message:"Data has been imported!"}';			
													else Echo '{success: false, message:'.json_encode($msgStr).'}';																									
												}	
												else Echo '{success: false, message:'.json_encode($msgStr).'}';																																	
											}
										}
									}
									else {
										### STEP 2: import the scenario table to the database (for AVA, LTO, CPB, INO, GPP)									
										if (is_uploaded_file($_FILES["table"]["tmp_name"])) {
											// if uploaded via HTTP POST			
											$tab_name = $_FILES['table']['name'];										
											$tab_actual_name = pathinfo($tab_name,PATHINFO_FILENAME);
											$tab_actual_name = preg_replace('/\s+/','_', $tab_actual_name);
											$tab_original_name = $tab_actual_name;
											$tab_extension = pathinfo($tab_name,PATHINFO_EXTENSION);

											// rename table name by appending a number to the end, if already exists
											$j = 1;
											while(file_exists(dirname(__FILE__) . '/upload/'.$tab_actual_name.".".$tab_extension))
											{           
												$tab_actual_name = (string)$tab_original_name.$j;
												$tab_name = $tab_actual_name.".".$tab_extension;
												$j++;
											}
											
											$targetpath = dirname(__FILE__) . '/upload/'.$tab_actual_name.".".$tab_extension;										
											$tab_mapping_index = $tab_actual_name;
											
											if (file_exists($targetpath))
											{
												echo '{success:false,message:"The scenario table file already exists!"}';
											}
											else {
												if (move_uploaded_file($_FILES["table"]["tmp_name"],$targetpath)){
													// if successfully moved to 'upload' folder, import the table using ogr2ogr tool (either .csv or .dbf extension)
													$command_str = 'ogr2ogr -f "PostgreSQL" PG:"host=localhost port=5432 user=username dbname=dbname password=mypassword" '.$targetpath.' -nln "'.$workspace.'.'.$tab_mapping_index.'"';
													shell_exec($command_str); 												
													
													if ($tmp === NULL) echo '{success:false,message:"There was an error in importing the scenario table to the database!" }'; // return NULL both when an error occurs or the program produces no output
													else {
														### STEP 3: join both tables and create tables for different return periods	(*check for TFreq*)												
														
														$query = "ALTER TABLE $workspace.$tab_mapping_index ADD COLUMN cin_scepro varchar(30);";
														$query .= "UPDATE $workspace.$tab_mapping_index SET cin_scepro =  sce_lvprob;";
														$query .= "UPDATE $workspace.$tab_mapping_index SET cin_scepro =  'T1' WHERE sce_lvprob = 'T001';";
														$query .= "UPDATE $workspace.$tab_mapping_index SET cin_scepro =  'T2' WHERE sce_lvprob = 'T002';";
														$query .= "UPDATE $workspace.$tab_mapping_index SET cin_scepro =  'T5' WHERE sce_lvprob = 'T005';";
														$query .= "UPDATE $workspace.$tab_mapping_index SET cin_scepro =  'T30' WHERE sce_lvprob = 'T030';";
														$query .= "UPDATE $workspace.$tab_mapping_index SET cin_scepro =  'TEx' WHERE sce_lvprob = 'TEX';";
														$query .= "UPDATE $workspace.$tab_mapping_index SET cin_scepro =  'TFreq' WHERE sce_lvprob = 'G0';";
														$query .= "UPDATE $workspace.$tab_mapping_index SET cin_scepro =  'TFreq' WHERE sce_lvprob = 'G1';";
														if ($hazType != 'GPP'){
															$query .= "SELECT distinct cin_scepro
																FROM $workspace.$tab_mapping_index
																WHERE cin_scepro != ''
																AND cin_scepro != 'X' 
																AND cin_scepro != 'nc';";	
														}																																																						
																	
														If (!$rs = pg_query($dbconn,$query)) {
															Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
														}
														else {
															if ($hazType == 'GPP'){
																$str = strtolower($title);															
																$str = preg_replace("/[^a-z0-9_\s-]/", "", $str);
																$str = preg_replace("/[\s-]+/", " ", $str);
																$name = preg_replace("/[\s]/", "_", $str);	
															//	$hazTypeStr = substr($hazType, 0, -2);
																$query = "";
																$query .= "CREATE TABLE $workspace.$name AS
																		(SELECT distinct $file_mapping_index.*, cin_scepro
																		FROM $workspace.$file_mapping_index, $workspace.$tab_mapping_index
																		WHERE cin_fksce = sce_code
																		AND cin_scepro = 'TFreq');";
																		
																$query .= "ALTER TABLE $workspace.$name ADD PRIMARY KEY (gid);";
																$query .= "INSERT INTO $workspace.hazards(id, nom, description, type, remarques, nom_origine, indice, temp_de_retour, ans)
																	VALUES(default,'$name','$desc','$hazType','$remarks','$shpFileName','$name','TFreq',1);";
																$query .= "INSERT INTO $workspace.hazards_alternatives VALUES ((SELECT id FROM ".$workspace.".hazards WHERE indice = '$name'), $hazAltID);";																
															}
															else {
																// loop for each retrieved return period to create new tables and add records to the 'hazards' table
																$query = "";
																$row = pg_affected_rows($rs);
																
																for ($j = 0; $j < $row; $j++) {				
																	$prob[$j] = pg_fetch_result($rs, $j, 0);															
																	$str = strtolower($title.'_'.$prob[$j]);															
																	$str = preg_replace("/[^a-z0-9_\s-]/", "", $str);
																	$str = preg_replace("/[\s-]+/", " ", $str);
																	$name[$j] = preg_replace("/[\s]/", "_", $str);	
																	
																	$query .= "CREATE TABLE $workspace.$name[$j] AS
																			(SELECT distinct $file_mapping_index.*, cin_scepro
																			FROM $workspace.$file_mapping_index, $workspace.$tab_mapping_index
																			WHERE cin_fksce = sce_code
																			AND cin_scepro = '$prob[$j]');";
																			
																	$query .= "ALTER TABLE $workspace.$name[$j] ADD PRIMARY KEY (gid);";
																	
																	if ($prob[$j] == 'TFreq' ) {
																		$query .= "INSERT INTO $workspace.hazards(id, nom, description, type, remarques, nom_origine, indice, temp_de_retour, ans)
																		VALUES(default,'$name[$j]','$desc','$hazType','$remarks','$shpFileName','$name[$j]','$prob[$j]',10);";
																	}
																	elseif ($prob[$j] == 'TEx' ) {
																		$query .= "INSERT INTO $workspace.hazards(id, nom, description, type, remarques, nom_origine, indice, temp_de_retour, ans) 
																		VALUES(default,'$name[$j]','$desc','$hazType','$remarks','$shpFileName','$name[$j]','$prob[$j]',1000);";
																	}
																	else { 
																		$query .= "INSERT INTO $workspace.hazards(id, nom, description, type, remarques, nom_origine, indice, temp_de_retour, ans)
																		VALUES(default,'$name[$j]','$desc','$hazType','$remarks','$shpFileName','$name[$j]','$prob[$j]',substring('$prob[$j]' from 2)::int);";
																	}															
																	
																	$query .= "INSERT INTO $workspace.hazards_alternatives VALUES ((SELECT id FROM ".$workspace.".hazards WHERE indice = '$name[$j]'), $hazAltID);";	 
																}
															}															
															
														//	echo $query;
															
															If (!$rs = pg_query($dbconn,$query)) {
																Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
															}
															else {
																### STEP 4: publish the tables to the geoserver														
																
																// Initiate cURL session
																$service = "http://localhost:8080/geoserver/"; // replace with your URL
																$request = "rest/workspaces/".$workspace."/datastores/".$datastore."/featuretypes"; // to add a new featuretype

																$url = $service . $request;
																$ch = curl_init($url);
																
																// Optional settings for debugging
																curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //option to return string
																curl_setopt($ch, CURLOPT_VERBOSE, true);
																curl_setopt($ch, CURLOPT_STDERR, $logfh); // logs curl messages

																//Required POST request settings
																curl_setopt($ch, CURLOPT_POST, True);
																$passwordStr = "username:mypassword"; // replace with your GeoServer username and password
																curl_setopt($ch, CURLOPT_USERPWD, $passwordStr);
																
																//POST data 
																curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: application/xml"));
																
																//POST return code
																$successCode = 201;	$count = 0;
																
																if ($hazType == 'GPP'){
																	$xmlStr = "<featureType><name>".$name."</name><title>".$name."</title><srs>EPSG:2056</srs></featureType>";
																	curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlStr);
																	$buffer = curl_exec($ch);					
																	$info = curl_getinfo($ch);
																	
																	# send the message back to the user
																	if ($info['http_code'] != $successCode) {																			
																		$msgStr .= "# Unsuccessful cURL request to ";
																		$msgStr .= $url." [". $info['http_code']. "]\n";
																		Echo '{success:false,message:'.json_encode($msgStr).'}';
																	}
																	else {
																		### associate the existing style 'CIN' to the published layers in GeoServer
																		$request = "rest/layers/".$workspace.":".$name.".xml"; 
																		$url = $service . $request;
																		$ch = curl_init($url);
																				
																		// Optional settings for debugging
																		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //option to return string
																		curl_setopt($ch, CURLOPT_VERBOSE, true);
																		curl_setopt($ch, CURLOPT_STDERR, $logfh); // logs curl messages

																		//Required PUT request settings
																		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');			
																		curl_setopt($ch, CURLOPT_USERPWD, $passwordStr);
																							
																		//POST data 
																		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: application/xml"));
																		$xmlStr = '<layer><defaultStyle><name>CIN</name><atom:link rel="alternate" href="http://localhost:8080/geoserver/rest/styles/CIN.xml" type="application/xml"/></defaultStyle></layer>';
																		curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlStr);
																				
																		//PUT return code
																		$successCode = 200;														
																				
																		// Execute the curl request
																		$buffer = curl_exec($ch);					
																		$info = curl_getinfo($ch);
																		if ($info['http_code'] != $successCode) {
																			$msgStr .= "# Unsuccessful cURL request to ";
																			$msgStr .= $url." [". $info['http_code']. "]\n"; 
																			Echo '{success:false,message:'.json_encode($msgStr).'}';
																		}
																		else {
																			$msgStr .= "Data has been imported!";
																			Echo '{success: true, message:'.json_encode($msgStr).'}';
																		}
																	}
																}
																else {
																	for ($j = 0; $j < $row; $j++) {	
																		$xmlStr = "<featureType><name>".$name[$j]."</name><title>".$name[$j]."</title><srs>EPSG:2056</srs></featureType>";
																		curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlStr);
																		$buffer = curl_exec($ch);					
																		$info = curl_getinfo($ch);
																		if ($info['http_code'] != $successCode) {
																			$count = $count + 1;
																			$msgStr .= "# Unsuccessful cURL request to ";
																			$msgStr .= $url." [". $info['http_code']. "]\n"; 
																		}																
																	}
																		
																	### associate the existing style 'CIN' to the published layers in GeoServer	
																	if ($count == 0) {																											 
																		for ($j = 0; $j < $row; $j++) {
																			$request = "rest/layers/".$workspace.":".$name[$j].".xml"; 
																			$url = $service . $request;
																			$ch = curl_init($url);
																				
																			// Optional settings for debugging
																			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //option to return string
																			curl_setopt($ch, CURLOPT_VERBOSE, true);
																			curl_setopt($ch, CURLOPT_STDERR, $logfh); // logs curl messages

																			//Required PUT request settings
																			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');			
																			curl_setopt($ch, CURLOPT_USERPWD, $passwordStr);
																							
																			//POST data 
																			curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: application/xml"));
																			$xmlStr = '<layer><defaultStyle><name>CIN</name><atom:link rel="alternate" href="http://localhost:8080/geoserver/rest/styles/CIN.xml" type="application/xml"/></defaultStyle></layer>';
																			curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlStr);
																				
																			//PUT return code
																			$successCode = 200;	$styleCount = 0;														
																				
																			// Execute the curl request
																			$buffer = curl_exec($ch);					
																			$info = curl_getinfo($ch);
																			if ($info['http_code'] != $successCode) {
																				$styleCount = $styleCount + 1;
																				$msgStr .= "# Unsuccessful cURL request to ";
																				$msgStr .= $url." [". $info['http_code']. "]\n"; 
																			}																
																		}
																		# send the message back to the user 
																		if ($styleCount == 0) Echo '{success: true, message:"Data has been imported!"}';			
																		else Echo '{success: false, message:'.json_encode($msgStr).'}';																									
																	}	
																	else Echo '{success: false, message:'.json_encode($msgStr).'}';	
																}																
															}
														}		
													}
													
												}	
												else echo '{success:false,message:"There was an error in moving the scenario table file on the server!"}';		
											}
										}
										else echo '{success:false,message:"Possible file upload attack!"}';
									}									
								}
							}
							else echo '{success:false,message:"There was an error in extracting the zip archive on the server!" }';
						} 
						else echo '{success:false,message:"There was an error in opening the zip archive on the server!" }';						
					}	
					else echo '{success:false, message:"There was an error in moving the zip archieve on the server because of error#'.$_FILES['file']['error'].'"}';
				}
			}
			else echo '{success:false,message:"Possible file upload attack!"}';			
		}
		
	}
	
	if ($task == 'delete') {
		$temp = $_POST['hazRecords'];		
		$hazRecords = json_decode($temp, true);
		$length = count($hazRecords);
		
		for ($i = 0; $i < $length; $i++) {	// for each of the selected hazard layers
			$ID = $hazRecords[$i]['id'];			
			$mpIndex = $hazRecords[$i]['indice'];
			
			### Step1: remove the records from the hazards and its referenced records in the hazards_alternatives table
			$query = "DELETE FROM ".$workspace.".hazards WHERE id = $ID;";
			
			### Step2: drop the respective layer table using indice column from the db (including the dependent view)
			$query .= "DROP TABLE ".$workspace.".$mpIndex CASCADE;";
				
			if (!$rs = pg_query($dbconn,$query)){			
				$message .= 'Failed to delete the record: '.$hazRecords[$i]['nom'].' due to the error: '.json_encode(pg_last_error($dbconn));						
			}
			else {
				// if success
				### Step3: remove the published map from the geoserver
					// Initiate cURL session
					$service = "http://localhost:8080/geoserver/"; // replace with your URL
					$request = "rest/workspaces/".$workspace."/datastores/".$datastore."/featuretypes/".$mpIndex.".xml?recurse=true"; // to delete a feature type

					$url = $service . $request;
					$ch = curl_init($url);

					// Optional settings for debugging
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //option to return string
					curl_setopt($ch, CURLOPT_VERBOSE, true);
					curl_setopt($ch, CURLOPT_STDERR, $logfh); // logs curl messages

					//Required DELETE request settings
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
					$passwordStr = "username:mypassword"; // replace with your GeoServer username and password
					curl_setopt($ch, CURLOPT_USERPWD, $passwordStr);

					//POST return code
					$successCode = 200;

					// Execute the curl request
					$buffer = curl_exec($ch); 

					// Check for errors and process results
					$info = curl_getinfo($ch);			
					if ($info['http_code'] != $successCode) {
						$message .= "Unuccessful cURL request to ".$url."\n";						
					}
					else {
						$message .= 'The selected map layer: '.$hazRecords[$i]['nom'].' has been deleted!';
					}
					curl_close($ch); // free resources if curl handle will not be reused
					
			}
		}
		Echo '{success: true, message:'.json_encode($message).'}';
	}
	
	if ($task == 'edit') {
		
		// Get the coverage layer name 
		$mpIndex = $_POST['mpIndex'];		
		$ID = $_POST['ID'];	
		$nom = $_POST['name'];
		$desc = $_POST['description'];
		$ans = (int)$_POST['year'];
		$type = $_POST['haz_type'];
		$remarques = $_POST['remarks'];
		
			// Initiate cURL session
			$service = "http://localhost:8080/geoserver/"; // replace with your URL
			$request = "rest/workspaces/".$workspace."/datastores/".$datastore."/featuretypes/".$mpIndex.".xml"; // to edit a raster store

			$url = $service . $request;
			$ch = curl_init($url);

			// Optional settings for debugging
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //option to return string
			curl_setopt($ch, CURLOPT_VERBOSE, true);
			curl_setopt($ch, CURLOPT_STDERR, $logfh); // logs curl messages

			//Required PUT request settings
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
			$passwordStr = "username:mypassword"; // replace with your GeoServer username and password
			curl_setopt($ch, CURLOPT_USERPWD, $passwordStr);
			
			//POST data
			curl_setopt($ch, CURLOPT_HTTPHEADER,
				array("Content-type: application/xml"));
			$xmlStr = "<featureType><title>".$nom."</title><keywords><string>features</string><string>".$nom."</string></keywords><enabled>true</enabled></featureType>";
			curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlStr);

			//POST return code
			$successCode = 200;

			// Execute the curl request
			$buffer = curl_exec($ch); 
			
			$info = curl_getinfo($ch);
			curl_close($ch); // free resources if curl handle will not be reused
			
			if ($info['http_code'] != $successCode) {
				Echo '{success:false, message:"There is an error in changing the layer title in geoserver!"}';
			}
			else {
				// if success, update the table
				$query= "UPDATE ".$workspace.".hazards SET nom = '$nom', description = '$desc', type = '$type', remarques = '$remarques', ans = $ans WHERE id = $ID;";
				$arr=array();
				
				If (!$rs = pg_query($dbconn,$query)) {
					Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
				}
				else {
					Echo '{success:true,message:"The data has been successfully updated!"}';		
				}				
			}
	}
	
	pg_close($dbconn);
?>