<?php
	$workspace = $_POST['ws'];
	$task = $_POST['task'];	
		
	$dbconn=pg_connect("host=localhost port=5432 dbname=dbname user=username password=mypassword");	// replace with your db connection information
	if (!$dbconn){
		echo "An error occured.\n";
		exit;
		}
	
	if ($task == 'load') {
		$query = "SELECT cost.*, alternatives.nom AS alt_nom, sum (cout_annuel) as cout_annuel  
			FROM ".$workspace.".cost, ".$workspace.".alternatives, ".$workspace.".cost_values
			WHERE alternatives.id = cost.alt_id
			AND cost.id = cost_values.cout_id
			GROUP BY cost.id, alt_nom;";
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	if ($task == 'add') {
	
		// retrieve POST data submitted by form
		$alt_id = $_POST['altID'];
		$alt_name = $_POST['altName'];
		$name_sheet = $_POST['name_sheet'];
		$description_sheet = $_POST['description_sheet'];
		$remarks_sheet = $_POST['remarks_sheet'];
	
		### STEP 1: insert a new record in cost table
		$query = "INSERT INTO ".$workspace.".cost VALUES (DEFAULT, $alt_id, '$name_sheet', '$description_sheet', '$remarks_sheet');"; 
		
		###	STEP 2: query and insert the new records to cost_values table								
		$query .= "INSERT INTO ".$workspace.".cost_values
			(
				cout_id, 
				alt_id,
				mesure_id,
				mode,
				cout_investissement,
				valeur_residuelle,
				cout_exploitation,
				cout_entretien,
				cout_reparation,
				periode_ans,
				taux_interet,
				cout_annuel
			)
		SELECT 
		   (SELECT last_value FROM ".$workspace.".cost_id_seq), $alt_id, t1.mes_id, t2.mode, 0, 0, 0, 0, 0, t2.duree_effet, t2.taux_interet, 0
		FROM
			".$workspace.".alternatives_measures t1,
			".$workspace.".measures t2
		WHERE
			t1.alt_id = $alt_id
			AND t1.mes_id = t2.id;";
	
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {				
			Echo '{success:true,message:"The new cost sheet for selected alternative has been added!"}';
		}
	
	}
	
	if ($task == 'loadCostValues') {
		$costID = $_POST['costID'];
		$query = "SELECT cost_values.*, measures.ouvrage FROM ".$workspace.".cost_values, ".$workspace.".measures WHERE cost_values.cout_id = $costID AND mesure_id = measures.id";
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	// to save the dirty matrix values to the 'cost_values' table 
	if ($_GET['task'] == 'save'){
		$workspace = $_GET['ws'];
		$postdata = file_get_contents("php://input"); 
		$phpArray = json_decode($postdata, true);
		$length = count($phpArray);	
		
		for ($i = 0; $i < $length; $i++) { // loop for each of the dirty records
			$temp = $phpArray[$i]; 			
			$id = $temp['id'];		
			$mode = $temp['mode'];
			$investment_cost = $temp['cout_investissement'];
			$maintenance_cost = $temp['cout_entretien'];
			$repair_cost = $temp['cout_reparation'];
			$operating_cost = $temp['cout_exploitation'];
			$no_of_years = $temp['periode_ans'];
			$residual_value = $temp['valeur_residuelle'];
			$interest_rate = $temp['taux_interet'];
			$annual_cost = floatval($maintenance_cost)+ floatval($operating_cost)+ floatval($repair_cost) + ((floatval($investment_cost) - floatval($residual_value))/floatval($no_of_years))+((floatval($investment_cost) + floatval($residual_value))*floatval($interest_rate)*0.005);
		//	$annual_cost = $temp['cout_annuel'];
			
			$query .= "UPDATE ".$workspace.".cost_values SET mode= '$mode', cout_investissement = $investment_cost, cout_exploitation = $operating_cost, 
				cout_entretien = $maintenance_cost, cout_reparation = $repair_cost, periode_ans = $no_of_years, valeur_residuelle = $residual_value, taux_interet = $interest_rate, cout_annuel = $annual_cost
				WHERE id = $id;";						
		}
		
		If (!$rs = pg_query($dbconn,$query)) {
				Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
			}
		else {			
			Echo '{success:true,message:"The cost values have been updated!"}';
		}	
	}
	
	if ($task == 'loadSelectedMeasures') {
		$costID = $_POST['costID'];
		$query = "SELECT id, ouvrage
				FROM $workspace.measures
				WHERE NOT EXISTS
				(SELECT mesure_id
				FROM $workspace.cost_values
				WHERE cout_id = $costID
				AND mesure_id = measures.id);";
				
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	if ($task == 'insertMeasure'){ // happens when a type of measure is added
		$workspace = $_POST['ws'];
		$costID = $_POST['costID'];
		$altID = $_POST['altID'];
		$measureID = $_POST['measureID'];
				
		$query = "INSERT INTO ".$workspace.".cost_values
			(
				cout_id, 
				alt_id,
				mesure_id,
				mode,
				cout_investissement,
				valeur_residuelle,
				cout_exploitation,
				cout_entretien,
				cout_reparation,
				periode_ans,
				taux_interet,
				cout_annuel
			)
		SELECT 
		   $costID, $altID, $measureID, t2.mode, 0, 0, 0, 0, 0, t2.duree_effet, t2.taux_interet, 0
		FROM ".$workspace.".measures t2
		WHERE $measureID = t2.id;";	
	
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
			Echo '{success:true,message:"The selected measure has been added!"}';
		}
	}
	
	if ($task == 'removeMeasure'){ // happens when a type of measure is deleted
		$workspace = $_POST['ws'];
		$id = $_POST['id'];
				
		$query = "DELETE FROM ".$workspace.".cost_values WHERE id = $id;";	
	
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
			Echo '{success:true,message:"The selected measure type has been deleted!"}';
		}
	}
	
	if ($task == 'editInfo') { // to update data from 'cost' table
		$costID = $_POST['ID'];	
		$name = $_POST['name_sheet'];
		$desc = $_POST['description_sheet'];
		$remarks = $_POST['remarks_sheet'];
		
		$query= "UPDATE ".$workspace.".cost SET nom = '$name', description = '$desc', remarques = '$remarks' WHERE id = $costID;";
				
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
			Echo '{success:true,message:"The data has been updated!"}';
		}
	}
	
	if ($task == 'delete'){ // happens when a cost sheet is deleted
		$workspace = $_POST['ws'];
				
		$temp = $_POST['costIDs'];		
		$records = json_decode($temp, true);
		$length = count($records);
		
		for ($i = 0; $i < $length; $i++) {	// for each of the selected cost sheets
			$ID = $records[$i];			
			$query .= "DELETE FROM ".$workspace.".cost WHERE id = $ID;";			
		}
		
		if (!$rs = pg_query($dbconn,$query)){			
			Echo '{success:"false",message:'.json_encode(pg_last_error($dbconn)).'}';						
		}
		else {
			Echo '{success:"true",message: "The selected cost sheets have been deleted!"}';	
		}		
	}
	
	pg_close($dbconn);
?>
