<?php
	session_start();
	$userid = $_SESSION['userid'];
	$username = $_SESSION['usrname'];
	
	$postdata = file_get_contents("php://input"); 
	$phpArray = json_decode($postdata, true);
	
	// connect to the database
	$dbconn=pg_connect("host=localhost port=5432 dbname=dbname user=username password=mypassword");	// replace with your db connection information
	if (!$dbconn){
		echo "An error occured.\n";
		exit;
	}
	
	// get the input data
	$name = preg_replace('/\s+/','_',$phpArray['prj_name']); // replace whitespaces and spaces with underscore 
	$name = strtolower($name); // change to lowercase
	$description = $phpArray['prj_desc'];
	$projection = $phpArray['prj_projection'];
	$scale = $phpArray['prj_scale'];
	
	// query statements: to create a new database schema, to insert a new record into 'workspace' table, to insert a relationship record into 'users_ws' table
	$query = "CREATE SCHEMA ".$name." AUTHORIZATION postgres;";	
	$query .= "INSERT INTO public.workspace VALUES(DEFAULT,'$userid','$name','$description','$projection','$scale');";
	
	If (!$rs = pg_query($dbconn,$query)) { // run the query
		Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
	}
	else {
		// run the query statements: to create default tables under the newly created schema
	//	$command = 'PGPASSWORD=opengeo psql -h localhost -p 5432 -d geoserver -U opengeo -a -f /vaudapp/schema.sql';
	//	$rs = shell_exec($command_str); 
		
	//	$rs = pg_query($dbconn, "BEGIN; COMMIT;\n" . "SET schema '$name';" . file_get_contents('schema.sql'));
	
	$query = "SET schema '$name';";
	$query .= "CREATE TABLE spatial_prob_default(
				id serial NOT NULL,
				processus character varying(20) NOT NULL,
				probabilite character varying(10) NOT NULL,
				valeur double precision NOT NULL,
				remarques text,
				CONSTRAINT spatial_prob_default_pkey PRIMARY KEY (id)

			);
			
			CREATE TABLE objects_default(
				code integer NOT NULL,
				description text,
				type character varying(30) NOT NULL,
				niveau character varying(30) NOT NULL,
				prix_unitaire double precision NOT NULL DEFAULT 0,
				unite text NOT NULL,
				humains double precision NOT NULL DEFAULT 0,
				humains_exp double precision NOT NULL DEFAULT 1,
				remarques text,
				CONSTRAINT objects_default_pkey PRIMARY KEY (code)

			);
			
			CREATE TABLE objects_default_routes(
				code integer NOT NULL,
				description text,
				type character varying(30) NOT NULL,
				niveau character varying(30) NOT NULL,
				prix_unitaire double precision NOT NULL DEFAULT 0,
				unite text NOT NULL,
				humains double precision NOT NULL DEFAULT 1.76,
				vitesse double precision NOT NULL DEFAULT 0,
				tjm double precision NOT NULL DEFAULT 0,
				longueur double precision NOT NULL DEFAULT 4,
				passage double precision NOT NULL DEFAULT 4,
				remarques text,
				CONSTRAINT objects_default_routes_pkey PRIMARY KEY (code)

			);

			CREATE TABLE vulnerability_default(
				id serial NOT NULL,
				code integer NOT NULL,
				obj_niveau character varying(30) NOT NULL,
				type character varying(30) NOT NULL,
				processus character varying(20) NOT NULL,
				intensite_valeur integer NOT NULL,
				valeur_objet double precision NOT NULL,
				valeur_letalite double precision NOT NULL,
				remarques text,
				CONSTRAINT vul_default_pkey PRIMARY KEY (id)

			);

			CREATE TABLE spatial_probability(
				id serial NOT NULL,
				nom character varying(150) NOT NULL,
				description text,
				type character varying(30) NOT NULL,
				remarques text,
				indice character varying(150) NOT NULL,
				CONSTRAINT spatial_probability_pkey PRIMARY KEY (id)

			);

			CREATE TABLE objects_parameters(
				id serial NOT NULL,
				nom character varying(150) NOT NULL,
				description text,
				type character varying(30) NOT NULL,
				obj_type character varying(30) NOT NULL,
				obj_niveau character varying(30) NOT NULL,
				remarques text,
				indice character varying(150) NOT NULL,
				CONSTRAINT objects_parameters_pkey PRIMARY KEY (id)

			);

			CREATE TABLE vulnerability(
				id serial NOT NULL,
				nom character varying(150) NOT NULL,
				description text,
				type character varying(30) NOT NULL,
				processus_type character varying(20) NOT NULL,
				obj_type character varying(30) NOT NULL,
				obj_niveau character varying(30) NOT NULL,
				remarques text,
				indice character varying(150) NOT NULL,
				CONSTRAINT vulnerability_pkey PRIMARY KEY (id)

			);

			CREATE TABLE alternatives(
				id serial NOT NULL,
				nom character varying(150) NOT NULL,
				description text,
				categorie character varying(20) NOT NULL,
				indice character varying(150) NOT NULL,
				remarques text,
				CONSTRAINT alternatives_pkey PRIMARY KEY (id)

			);

			CREATE TABLE hazards(
				id serial NOT NULL,
				nom character varying(150) NOT NULL,
				description text,
				type character varying(30) NOT NULL,
				temp_de_retour character varying(10) NOT NULL,
				ans integer NOT NULL,
				remarques text,
				nom_origine character varying(150) NOT NULL,
				indice character varying(150) NOT NULL,
				CONSTRAINT haz_id PRIMARY KEY (id)

			);

			CREATE TABLE objects(
				id serial NOT NULL,
				nom character varying(150) NOT NULL,
				description text,
				type character varying(30) NOT NULL,
				remarques text,
				nom_origine character varying(150) NOT NULL,
				indice character varying(150) NOT NULL,
				CONSTRAINT objects_pkey PRIMARY KEY (id)

			);

			CREATE TABLE hazards_alternatives(
				processus_id integer NOT NULL,
				alt_id integer NOT NULL,
				CONSTRAINT haz_alt_id PRIMARY KEY (processus_id,alt_id),
				CONSTRAINT haz_alt_fkey_1 FOREIGN KEY (processus_id)
					  REFERENCES hazards (id) MATCH FULL
					  ON UPDATE CASCADE ON DELETE CASCADE,
				  CONSTRAINT haz_alt_fkey_2 FOREIGN KEY (alt_id)
					  REFERENCES alternatives (id) MATCH FULL
					  ON UPDATE CASCADE ON DELETE CASCADE

			);

			CREATE TABLE objects_alternatives(
				objet_id integer NOT NULL,
				alt_id integer NOT NULL,
				CONSTRAINT obj_alt_pkey PRIMARY KEY (objet_id,alt_id),
				CONSTRAINT obj_alt_fkey1 FOREIGN KEY (objet_id)
					  REFERENCES objects (id) MATCH FULL
					  ON UPDATE CASCADE ON DELETE CASCADE,
				CONSTRAINT obj_alt_fkey2 FOREIGN KEY (alt_id)
					  REFERENCES alternatives (id) MATCH FULL
					  ON UPDATE CASCADE ON DELETE CASCADE

			);

			CREATE TABLE risk_scenarios(
				id serial NOT NULL,
				nom character varying(150) NOT NULL,
				description text,
				alt_id integer NOT NULL,
				option character varying(10) NOT NULL,
				processus_type character varying(10) NOT NULL,
				pra_id integer NOT NULL,
				objet_id integer NOT NULL,
				objet_type character varying(30) NOT NULL,
				objet_niveau character varying(30) NOT NULL,  
				objet_para_id integer,
				vul_niveau character varying(30) NOT NULL,
				vul_id integer,
				indice character varying(150) NOT NULL,
				objet_total double precision NOT NULL,
				humain_total double precision NOT NULL,
				risque_total double precision NOT NULL,
				risque_individuel double precision NOT NULL,
				poly_texte text,
				CONSTRAINT risk_scenarios_pkey PRIMARY KEY (id),
				CONSTRAINT risk_scenarios_fkey_alt FOREIGN KEY (alt_id)
					  REFERENCES alternatives (id) MATCH FULL
					  ON UPDATE NO ACTION ON DELETE NO ACTION,
				CONSTRAINT risk_scenarios_fkey_obj FOREIGN KEY (objet_id)
					  REFERENCES objects (id) MATCH FULL
					  ON UPDATE NO ACTION ON DELETE NO ACTION,
				CONSTRAINT risk_scenarios_fkey_obj_para FOREIGN KEY (objet_para_id)
					  REFERENCES objects_parameters (id) MATCH FULL
					  ON UPDATE NO ACTION ON DELETE NO ACTION,
				CONSTRAINT risk_scenarios_fkey_pra FOREIGN KEY (pra_id)
					  REFERENCES spatial_probability (id) MATCH FULL
					  ON UPDATE NO ACTION ON DELETE NO ACTION,
				CONSTRAINT risk_scenarios_fkey_vul FOREIGN KEY (vul_id)
					  REFERENCES vulnerability (id) MATCH FULL
					  ON UPDATE NO ACTION ON DELETE NO ACTION

			);

			CREATE TABLE risk_scenarios_hazards(
				id serial NOT NULL,
				id_sce integer NOT NULL,
				temp_de_retour character varying(10) NOT NULL,
				indice_processus character varying(150) NOT NULL,
				indice_risque_sce character varying(150) NOT NULL,
				CONSTRAINT risk_scenarios_hazards_pkey PRIMARY KEY (id),
				CONSTRAINT risk_scenarios_hazards_fkey FOREIGN KEY (id_sce)
					REFERENCES risk_scenarios (id) MATCH FULL
					ON UPDATE CASCADE ON DELETE CASCADE

			);

			CREATE TABLE measures(
				id serial NOT NULL,
				ouvrage text NOT NULL,
				duree_effet integer,
				frais_exploitation double precision,
				frais_entretien double precision,
				frais_reparation double precision,
				valeur_residuelle smallint,
				remarques text,
				mode character varying(20) NOT NULL,
				taux_interet integer,
				CONSTRAINT measures_pkey PRIMARY KEY (id)

			);

			CREATE TABLE alternatives_measures(
				alt_id integer NOT NULL,
				mes_id integer NOT NULL,
				CONSTRAINT alt_mes_pkey PRIMARY KEY (alt_id,mes_id),
				CONSTRAINT alt_mes_fkey1 FOREIGN KEY (alt_id)
					  REFERENCES alternatives (id) MATCH FULL
					  ON UPDATE CASCADE ON DELETE CASCADE,
				CONSTRAINT alt_mes_fkey2 FOREIGN KEY (mes_id)
					  REFERENCES measures (id) MATCH FULL
					  ON UPDATE CASCADE ON DELETE CASCADE

			);

			CREATE TABLE cost(
				id serial NOT NULL,
				alt_id integer NOT NULL,
				nom character varying(150) NOT NULL,
				description text,
				remarques text,
				CONSTRAINT cost_pkey PRIMARY KEY (id),
				CONSTRAINT cost_fkey FOREIGN KEY (alt_id)
					REFERENCES alternatives (id) MATCH FULL
					ON UPDATE CASCADE ON DELETE CASCADE

			);

			CREATE TABLE cost_values(
				id serial NOT NULL,
				cout_id integer NOT NULL,
				alt_id integer NOT NULL,
				mesure_id integer NOT NULL,
				mode character varying(20) NOT NULL,
				cout_investissement double precision NOT NULL,
				valeur_residuelle double precision NOT NULL,
				cout_exploitation double precision NOT NULL,
				cout_entretien double precision NOT NULL,
				cout_reparation double precision NOT NULL,
				periode_ans integer NOT NULL,
				taux_interet integer NOT NULL,
				cout_annuel double precision NOT NULL,
				CONSTRAINT cost_values_pkey PRIMARY KEY (id),
				CONSTRAINT cost_values_fkey1 FOREIGN KEY (cout_id)
					  REFERENCES cost (id) MATCH FULL
					  ON UPDATE CASCADE ON DELETE CASCADE,
				CONSTRAINT cost_values_fkey2 FOREIGN KEY (alt_id)
					  REFERENCES alternatives (id) MATCH FULL
					  ON UPDATE CASCADE ON DELETE CASCADE,
				CONSTRAINT cost_values_fkey3 FOREIGN KEY (mesure_id)
					  REFERENCES measures (id) MATCH FULL
					  ON UPDATE CASCADE ON DELETE CASCADE

			);

			CREATE TABLE cost_benefit(
				id serial NOT NULL,
				nom character varying(150),
				description text,
				remarques text,
				CONSTRAINT cost_benefit_pkey PRIMARY KEY (id)

			);

			CREATE TABLE cost_benefit_values(
				cb_id integer NOT NULL,
				cout_id integer NOT NULL,
				risque_sce_id integer NOT NULL,
				CONSTRAINT cost_benefit_values_pkey PRIMARY KEY (cb_id,cout_id,risque_sce_id),
				CONSTRAINT cost_benefit_values_fkey1 FOREIGN KEY (cb_id)
					  REFERENCES cost_benefit (id) MATCH FULL
					  ON UPDATE CASCADE ON DELETE CASCADE,
				CONSTRAINT cost_benefit_values_fkey2 FOREIGN KEY (cout_id)
					  REFERENCES cost (id) MATCH FULL
					  ON UPDATE CASCADE ON DELETE CASCADE,
				CONSTRAINT cost_benefit_values_fkey3 FOREIGN KEY (risque_sce_id)
					  REFERENCES risk_scenarios (id) MATCH FULL
					  ON UPDATE CASCADE ON DELETE CASCADE

			);
			
			INSERT INTO spatial_probability VALUES (DEFAULT, 'default_config','Spatial probability values by default','default','Source: Econome 4.0','spatial_prob_default');
			INSERT INTO objects_parameters VALUES (DEFAULT, 'default_config_buildings_category','Object parameter values by default','default','Buildings','Category','Source: Econome 4.0 + VD','objects_default');
			INSERT INTO objects_parameters VALUES (DEFAULT, 'default_config_buildings_class','Object parameter values by default','default','Buildings','Class','Source: Econome 4.0 + VD','objects_default');
			INSERT INTO objects_parameters VALUES (DEFAULT, 'default_config_routes_code','Object parameter values by default','default','Routes','Code-EconoMe','Source: Econome 4.0 + VD','objects_default_routes');
			INSERT INTO objects_parameters VALUES (DEFAULT, 'default_config_land_use_code','Object parameter values by default','default','Surfaces','Code-EconoMe','Source: Econome 4.0 + VD','objects_default');
			INSERT INTO vulnerability VALUES (DEFAULT, 'default_config_AVA_buildings_category','Vulnerability values by default','default','AVA','Buildings','Category','Source: Econome 4.0 + VD','vulnerability_default');
			INSERT INTO vulnerability VALUES (DEFAULT, 'default_config_LTO_buildings_category','Vulnerability values by default','default','LTO','Buildings','Category','Source: Econome 4.0 + VD','vulnerability_default');
			INSERT INTO vulnerability VALUES (DEFAULT, 'default_config_INO_buildings_category','Vulnerability values by default','default','INO','Buildings','Category','Source: Econome 4.0 + VD','vulnerability_default');
			INSERT INTO vulnerability VALUES (DEFAULT, 'default_config_INOD_buildings_category','Vulnerability values by default','default','INOD','Buildings','Category','Source: Econome 4.0 + VD','vulnerability_default');
			INSERT INTO vulnerability VALUES (DEFAULT, 'default_config_INOS_buildings_category','Vulnerability values by default','default','INOS','Buildings','Category','Source: Econome 4.0 + VD','vulnerability_default');
			INSERT INTO vulnerability VALUES (DEFAULT, 'default_config_CPB_buildings_category','Vulnerability values by default','default','CPB','Buildings','Category','Source: Econome 4.0 + VD','vulnerability_default');
			INSERT INTO vulnerability VALUES (DEFAULT, 'default_config_GSS_buildings_category','Vulnerability values by default','default','GSS','Buildings','Category','Source: Econome 4.0 + VD','vulnerability_default');
			INSERT INTO vulnerability VALUES (DEFAULT, 'default_config_GPP_buildings_category','Vulnerability values by default','default','GPP','Buildings','Category','Source: Econome 4.0 + VD','vulnerability_default');
			INSERT INTO vulnerability VALUES (DEFAULT, 'default_config_GPP_TR_buildings_category','Vulnerability values by default','default','GPP-TR','Buildings','Category','Source: Econome 4.0 + VD','vulnerability_default');
			INSERT INTO vulnerability VALUES (DEFAULT, 'default_config_AVA_buildings_class','Vulnerability values by default','default','AVA','Buildings','Class','Source: Econome 4.0','vulnerability_default');
			INSERT INTO vulnerability VALUES (DEFAULT, 'default_config_LTO_buildings_class','Vulnerability values by default','default','LTO','Buildings','Class','Source: Econome 4.0','vulnerability_default');
			INSERT INTO vulnerability VALUES (DEFAULT, 'default_config_INO_buildings_class','Vulnerability values by default','default','INO','Buildings','Class','Source: Econome 4.0 + VD','vulnerability_default');
			INSERT INTO vulnerability VALUES (DEFAULT, 'default_config_INOD_buildings_class','Vulnerability values by default','default','INOD','Buildings','Class','Source: Econome 4.0','vulnerability_default');
			INSERT INTO vulnerability VALUES (DEFAULT, 'default_config_INOS_buildings_class','Vulnerability values by default','default','INOS','Buildings','Class','Source: Econome 4.0','vulnerability_default');
			INSERT INTO vulnerability VALUES (DEFAULT, 'default_config_CPB_buildings_class','Vulnerability values by default','default','CPB','Buildings','Class','Source: Econome 4.0','vulnerability_default');
			INSERT INTO vulnerability VALUES (DEFAULT, 'default_config_GSS_buildings_class','Vulnerability values by default','default','GSS','Buildings','Class','Source: Econome 4.0','vulnerability_default');
			INSERT INTO vulnerability VALUES (DEFAULT, 'default_config_GPP_buildings_class','Vulnerability values by default','default','GPP','Buildings','Class','Source: Econome 4.0','vulnerability_default');
			INSERT INTO vulnerability VALUES (DEFAULT, 'default_config_GPP_TR_buildings_class','Vulnerability values by default','default','GPP-TR','Buildings','Class','Source: Econome 4.0','vulnerability_default');
			INSERT INTO vulnerability VALUES (DEFAULT, 'default_config_AVA_routes_code','Vulnerability values by default','default','AVA','Routes','Code-EconoMe','Source: Econome 4.0','vulnerability_default');
			INSERT INTO vulnerability VALUES (DEFAULT, 'default_config_LTO_routes_code','Vulnerability values by default','default','LTO','Routes','Code-EconoMe','Source: Econome 4.0','vulnerability_default');
			INSERT INTO vulnerability VALUES (DEFAULT, 'default_config_INO_routes_code','Vulnerability values by default','default','INO','Routes','Code-EconoMe','Source: Econome 4.0 + VD','vulnerability_default');
			INSERT INTO vulnerability VALUES (DEFAULT, 'default_config_INOD_routes_code','Vulnerability values by default','default','INOD','Routes','Code-EconoMe','Source: Econome 4.0','vulnerability_default');
			INSERT INTO vulnerability VALUES (DEFAULT, 'default_config_INOS_routes_code','Vulnerability values by default','default','INOS','Routes','Code-EconoMe','Source: Econome 4.0','vulnerability_default');
			INSERT INTO vulnerability VALUES (DEFAULT, 'default_config_CPB_routes_code','Vulnerability values by default','default','CPB','Routes','Code-EconoMe','Source: Econome 4.0','vulnerability_default');
			INSERT INTO vulnerability VALUES (DEFAULT, 'default_config_GSS_routes_code','Vulnerability values by default','default','GSS','Routes','Code-EconoMe','Source: Econome 4.0','vulnerability_default');
			INSERT INTO vulnerability VALUES (DEFAULT, 'default_config_GPP_routes_code','Vulnerability values by default','default','GPP','Routes','Code-EconoMe','Source: Econome 4.0','vulnerability_default');
			INSERT INTO vulnerability VALUES (DEFAULT, 'default_config_GPP_TR_routes_code','Vulnerability values by default','default','GPP-TR','Routes','Code-EconoMe','Source: Econome 4.0','vulnerability_default');
			INSERT INTO vulnerability VALUES (DEFAULT, 'default_config_AVA_land_use_code','Vulnerability values by default','default','AVA','Surfaces','Code-EconoMe','Source: Econome 4.0','vulnerability_default');
			INSERT INTO vulnerability VALUES (DEFAULT, 'default_config_LTO_land_use_code','Vulnerability values by default','default','LTO','Surfaces','Code-EconoMe','Source: Econome 4.0','vulnerability_default');
			INSERT INTO vulnerability VALUES (DEFAULT, 'default_config_INO_land_use_code','Vulnerability values by default','default','INO','Surfaces','Code-EconoMe','Source: Econome 4.0 + VD','vulnerability_default');
			INSERT INTO vulnerability VALUES (DEFAULT, 'default_config_INOD_land_use_code','Vulnerability values by default','default','INOD','Surfaces','Code-EconoMe','Source: Econome 4.0','vulnerability_default');
			INSERT INTO vulnerability VALUES (DEFAULT, 'default_config_INOS_land_use_code','Vulnerability values by default','default','INOS','Surfaces','Code-EconoMe','Source: Econome 4.0','vulnerability_default');
			INSERT INTO vulnerability VALUES (DEFAULT, 'default_config_CPB_land_use_code','Vulnerability values by default','default','CPB','Surfaces','Code-EconoMe','Source: Econome 4.0','vulnerability_default');
			INSERT INTO vulnerability VALUES (DEFAULT, 'default_config_GSS_land_use_code','Vulnerability values by default','default','GSS','Surfaces','Code-EconoMe','Source: Econome 4.0','vulnerability_default');
			INSERT INTO vulnerability VALUES (DEFAULT, 'default_config_GPP_land_use_code','Vulnerability values by default','default','GPP','Surfaces','Code-EconoMe','Source: Econome 4.0','vulnerability_default');
			INSERT INTO vulnerability VALUES (DEFAULT, 'default_config_GPP_TR_land_use_code','Vulnerability values by default','default','GPP-TR','Surfaces','Code-EconoMe','Source: Econome 4.0','vulnerability_default');
			INSERT INTO alternatives VALUES (DEFAULT, 'Do Nothing','No measures are planned for risk reduction.','descriptive','do_nothing','');
			INSERT INTO cost VALUES (0,1,'Do Nothing','The cost calculation is not carried out. No risk reduction measures are taken.','');
			
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (1, 'AVA', 'TFreq', 0.8, 'Assume as T10');
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (2, 'AVA', 'T30', 0.7, NULL);
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (3, 'AVA', 'T100', 0.6, NULL);
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (4, 'AVA', 'T300', 0.8, NULL);
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (5, 'AVA', 'TEx', 1, 'Assume as T1000');
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (6, 'INOD', 'TFreq', 1, 'Assume as T10');
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (7, 'INOD', 'T30', 0.9, NULL);
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (8, 'INOD', 'T100', 0.7, NULL);
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (9, 'INOD', 'T300', 0.9, NULL);
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (10, 'INOD', 'TEx', 0.9, 'Assume as T1000');
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (11, 'LTO', 'TFreq', 1, 'Assume as T10');
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (12, 'LTO', 'T30', 0.8, NULL);
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (13, 'LTO', 'T100', 0.6, NULL);
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (14, 'LTO', 'T300', 0.8, NULL);
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (15, 'LTO', 'TEx', 1, 'Assume as T1000');
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (16, 'GSS', 'TFreq', 0.05, 'Assume as T10');
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (17, 'GSS', 'T30', 0.1, NULL);
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (18, 'GSS', 'T100', 0.3, NULL);
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (19, 'GSS', 'T300', 0.5, NULL);
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (20, 'GSS', 'TEx', 1, 'Assume as T1000');
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (21, 'GPP', 'TFreq', 1, 'Assume as T1');
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (22, 'GPP', 'T30', 1, NULL);
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (23, 'GPP', 'T100', 1, NULL);
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (24, 'GPP', 'T300', 1, NULL);
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (25, 'GPP', 'TEx', 1, 'Assume as T1000');			
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (26, 'CPB', 'TFreq', 0.005, 'Assume as T10');
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (27, 'CPB', 'T30', 0.01, NULL);
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (28, 'CPB', 'T100', 0.03, NULL);
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (29, 'CPB', 'T300', 0.05, NULL);
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (30, 'CPB', 'TEx', 0.1, 'Assume as T1000');
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (31, 'INOS', 'TFreq', 1, 'Assume as T10');
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (32, 'INOS', 'T30', 1, NULL);
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (33, 'INOS', 'T100', 1, NULL);
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (34, 'INOS', 'T300', 1, NULL);
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (35, 'INOS', 'TEx', 1, 'Assume as T1000');
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (36, 'INO', 'TFreq', 1, 'Assume as T10');
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (37, 'INO', 'T30', 0.95, NULL);
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (38, 'INO', 'T100', 0.85, NULL);
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (39, 'INO', 'T300', 0.95, NULL);
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (40, 'INO', 'TEx', 0.95, 'Assume as T1000');
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (41, 'GPP-TR', 'TFreq', 1, 'Assume as T10');
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (42, 'GPP-TR', 'T30', 1, NULL);
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (43, 'GPP-TR', 'T100', 1, NULL);
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (44, 'GPP-TR', 'T300', 1, NULL);
			INSERT INTO spatial_prob_default (id, processus, probabilite, valeur, remarques) VALUES (45, 'GPP-TR', 'TEx', 1, 'Assume as T1000');

			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (1021, 'Maisons individuelles', 650000, 'unite de logement', 2.24, 0.5, 'Source: Econome 4.0', 'Buildings', 'Category');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (1025, 'Maisons a plusieurs logements', 550000, 'unite de logement', 2.24, 0.5, 'Source: Econome 4.0', 'Buildings', 'Category');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (1060, 'Batiments sans usage dhabitation', 526, 'm3', 0.2, 0.3, 'Source: VD', 'Buildings', 'Category');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (1080, 'Constructions particulieres', 400, 'm3', 0.2, 0.3, 'Source: VD', 'Buildings', 'Category');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (1030, 'Batiments dhabitation avec usage annexe', 583333, 'unite de logement', 2.24, 0.5, 'Source: VD', 'Buildings', 'Category');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (1040, 'Batiments partiellement a usage dhabitation', 629, 'm3', 0.2, 0.4, 'Source: VD', 'Buildings', 'Category');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (1010, 'Habitations provisoires', 1, 'm3', 0, 1, 'To validate', 'Buildings', 'Category');
			
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (1110, 'Maisons individuelles', 650000, 'unite de logement', 2.24, 0.5, 'Code: 1 (Source: Econome 4.0)', 'Buildings', 'Class');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (1121, 'Maisons a deux logements', 550000, 'unite de logement', 2.24, 0.5, 'Code: 87 (Source: Econome 4.0)', 'Buildings', 'Class');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (1122, 'Immeubles a trois logements et plus', 550000, 'unite de logement', 2.24, 0.5, 'Code: 87 (Source: Econome 4.0)', 'Buildings', 'Class');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (1130, 'Habitat communautaire', 550000, 'unite de logement', 2.24, 0.5, 'Code: 87 (Source: Econome 4.0)', 'Buildings', 'Class');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (1211, 'Hotels et restaurants', 492, 'm3', 0.2, 0.3, 'Code: 5 (Source: Econome 4.0)', 'Buildings', 'Class');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (1212, 'Autre batiments dhebergement de tourisme', 492, 'm3', 0.2, 0.3, 'Code: 5 (Source: Econome 4.0)', 'Buildings', 'Class');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (1220, 'Immeubles de bureaux', 810, 'm3', 0.2, 0.3, 'Code: 18 (Source: Econome 4.0)', 'Buildings', 'Class');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (1230, 'Batiment commerciaux', 540, 'm3', 0.5, 0.3, 'Code: 9 (Source: Econome 4.0)', 'Buildings', 'Class');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (1241, 'Gares, aerogares, centraux telephoniques', 530, 'm3', 0.5, 0.3, 'Code: 14 (Source: Econome 4.0)', 'Buildings', 'Class');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (1242, 'Garages et parcs de stationnement couverts', 60000, 'unite', 0.05, 0.3, 'Code: 4 (Source: Econome 4.0)', 'Buildings', 'Class');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (1251, 'Batiment industriels', 280, 'm3', 0.1, 0.3, 'Code: 6 (Source: Econome 4.0)', 'Buildings', 'Class');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (1252, 'Reservoirs, silos et entrepots', 80, 'm3', 0.05, 0.1, 'Code: 3 (Source: Econome 4.0)', 'Buildings', 'Class');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (1261, 'Batiments a usage recreatif ou culturel', 810, 'm3', 0.2, 0.3, 'Code: 18 (Source: Econome 4.0)', 'Buildings', 'Class');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (1262, 'Musees et Bibliotheques', 810, 'm3', 0.2, 0.3, 'Code: 18 (Source: Econome 4.0)', 'Buildings', 'Class');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (1263, 'Batiments pour lenseignement et la recherche', 580, 'm3', 0.2, 0.3, 'Code: 10 (Source: Econome 4.0)', 'Buildings', 'Class');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (1264, 'Hopitaux et etablissements de sante', 850, 'm3', 0.2, 0.8, 'Code: 11 (Source: Econome 4.0)', 'Buildings', 'Class');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (1265, 'Salles de sport', 370, 'm3', 0.2, 0.3, 'Code: 15 (Source: Econome 4.0)', 'Buildings', 'Class');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (1271, 'Batiment dexploitation agricole', 180, 'm3', 0.05, 0.8, 'Code: 2 (Source: Econome 4.0)', 'Buildings', 'Class');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (1272, 'Edifices cultuels et religieux', 720, 'm3', 0.5, 0.3, 'Code: 12 (Source: Econome 4.0)', 'Buildings', 'Class');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (1273, 'Monuments histroiques ou classes', 810, 'm3', 0.2, 0.3, 'Code: 18 (Source: Econome 4.0)', 'Buildings', 'Class');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (1274, 'Autres batiments non classes ailleurs', 1, 'm3', 0.3, 0.1, 'Code: 64 (Source: Econome 4.0)', 'Buildings', 'Class');
			
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (1, 'Unité de logement maison individuelle', 130000, 'are', 0, 0, 'Source: Econome 4.0 + VD', 'Surfaces', 'Code-EconoMe');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (5, 'Hôtel', 150000, 'are', 0, 0, 'Source: Econome 4.0 + VD', 'Surfaces', 'Code-EconoMe');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (6, 'Bâtiment industriel ou artisanal', 85000, 'are', 0, 0, 'Source: Econome 4.0 + VD', 'Surfaces', 'Code-EconoMe');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (9, 'Centre commercial', 160000, 'are', 0, 0, 'Source: Econome 4.0 + VD', 'Surfaces', 'Code-EconoMe');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (12, 'Église', 215000, 'are', 0, 0, 'Source: Econome 4.0 + VD', 'Surfaces', 'Code-EconoMe');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (18, 'Bâtiment public', 240000, 'are', 0, 0, 'Source: Econome 4.0 + VD', 'Surfaces', 'Code-EconoMe');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (21, 'Camping', 350, 'are', 0, 0, 'Source: Econome 4.0 + VD', 'Surfaces', 'Code-EconoMe');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (41, 'Surface exploitée intensivement (terres cultivées, prairies de fauche)', 1600, 'are', 0, 0, 'Source: Econome 4.0', 'Surfaces', 'Code-EconoMe');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (42, 'Parc', 6500, 'are', 0, 0, 'Source: Econome 4.0', 'Surfaces', 'Code-EconoMe');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (43, 'Surface exploitée extensivement (pâturages)', 1400, 'are', 0, 0, 'Source: Econome 4.0', 'Surfaces', 'Code-EconoMe');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (44, 'Forêt protectrice', 1000, 'are', 0, 0, 'Source: Econome 4.0', 'Surfaces', 'Code-EconoMe');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (45, 'Forêt exploitée', 200, 'are', 0, 0, 'Source: Econome 4.0', 'Surfaces', 'Code-EconoMe');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (46, 'Vigne', 6000, 'are', 0, 0, 'Source: Econome 4.0', 'Surfaces', 'Code-EconoMe');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (47, 'Culture maraîchère', 5000, 'are', 0, 0, 'Source: Econome 4.0', 'Surfaces', 'Code-EconoMe');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (48, 'Verger', 1, 'are', 0, 0, 'Source: Econome 4.0', 'Surfaces', 'Code-EconoMe');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (83, 'Installation de golf (extérieure)', 6500, 'are', 0, 0, 'Source: Econome 4.0', 'Surfaces', 'Code-EconoMe');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (84, 'Cimetière', 1, 'are', 0, 0, 'Source: Econome 4.0', 'Surfaces', 'Code-EconoMe');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (85, 'Installation sportive (extérieure)', 1, 'are', 0, 0, 'Source: Econome 4.0', 'Surfaces', 'Code-EconoMe');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (86, 'Autre objet (agriculture, espaces verts, forêt)', 1, 'are', 0, 0, 'Source: Econome 4.0', 'Surfaces', 'Code-EconoMe');
			INSERT INTO objects_default (code, description, prix_unitaire, unite, humains, humains_exp, remarques, type, niveau) VALUES (87, 'Unité de logement dans immeuble résidentiel', 1100000, 'are', 0, 0, 'Source: Econome 4.0 + VD', 'Surfaces', 'Code-EconoMe');
			
			INSERT INTO objects_default_routes (code, description, prix_unitaire, unite, humains, vitesse, tjm, longueur, passage, remarques, type, niveau) VALUES (24, 'Route nationale (largeur 25 m)', 9500, 'CHF/m', DEFAULT, 120, 100000, DEFAULT, DEFAULT, 'Source: Econome 4.0 + VD', 'Routes', 'Code-EconoMe');
			INSERT INTO objects_default_routes (code, description, prix_unitaire, unite, humains, vitesse, tjm, longueur, passage, remarques, type, niveau) VALUES (25, 'Route cantonale (largeur 12 m)', 4100, 'CHF/m', DEFAULT, 80, 3500, DEFAULT, DEFAULT, 'Source: Econome 4.0', 'Routes', 'Code-EconoMe');
			INSERT INTO objects_default_routes (code, description, prix_unitaire, unite, humains, vitesse, tjm, longueur, passage, remarques, type, niveau) VALUES (26, 'Route communale (largeur 8 m)', 2300, 'CHF/m', DEFAULT, 50, 1500, DEFAULT, DEFAULT, 'Source: Econome 4.0', 'Routes', 'Code-EconoMe');
			INSERT INTO objects_default_routes (code, description, prix_unitaire, unite, humains, vitesse, tjm, longueur, passage, remarques, type, niveau) VALUES (31, 'Pont pour un seul véhicule (largeur 4 m)', 16000, 'CHF/m', DEFAULT, 30, 100, DEFAULT, DEFAULT, 'Source: Econome 4.0', 'Routes', 'Code-EconoMe');
			INSERT INTO objects_default_routes (code, description, prix_unitaire, unite, humains, vitesse, tjm, longueur, passage, remarques, type, niveau) VALUES (32, 'Pont route communale (largeur 8 m)', 24000, 'CHF/m', DEFAULT, 50, 1500, DEFAULT, DEFAULT, 'Source: Econome 4.0', 'Routes', 'Code-EconoMe');
			INSERT INTO objects_default_routes (code, description, prix_unitaire, unite, humains, vitesse, tjm, longueur, passage, remarques, type, niveau) VALUES (33, 'Pont route cantonale (largeur 12 m)', 36000, 'CHF/m', DEFAULT, 80, 3500, DEFAULT, DEFAULT, 'Source: Econome 4.0', 'Routes', 'Code-EconoMe');
			INSERT INTO objects_default_routes (code, description, prix_unitaire, unite, humains, vitesse, tjm, longueur, passage, remarques, type, niveau) VALUES (34, 'Pont autoroute (largeur 25 m)', 75000, 'CHF/m', DEFAULT, 100, 100000, DEFAULT, DEFAULT, 'Source: Econome 4.0 + VD', 'Routes', 'Code-EconoMe');
			INSERT INTO objects_default_routes (code, description, prix_unitaire, unite, humains, vitesse, tjm, longueur, passage, remarques, type, niveau) VALUES (55, 'Chemin rural ou forestier (gravier)', 500, 'CHF/m', DEFAULT, 30, 10, DEFAULT, DEFAULT, 'Source: Econome 4.0 + VD', 'Routes', 'Code-EconoMe');
			INSERT INTO objects_default_routes (code, description, prix_unitaire, unite, humains, vitesse, tjm, longueur, passage, remarques, type, niveau) VALUES (56, 'Chemin rural ou forestier (goudronné)', 700, 'CHF/m', DEFAULT, 30, 10, DEFAULT, DEFAULT, 'Source: Econome 4.0 + VD', 'Routes', 'Code-EconoMe');
			INSERT INTO objects_default_routes (code, description, prix_unitaire, unite, humains, vitesse, tjm, longueur, passage, remarques, type, niveau) VALUES (67, 'Autre objet de trafic routier', 1, 'CHF/m', DEFAULT, 30, 100, DEFAULT, DEFAULT, 'Source: Econome 4.0 + VD', 'Routes', 'Code-EconoMe');

			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1010, 'Category', 'Buildings', 'AVA', 1, 1, 1, 'To validate');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1010, 'Category', 'Buildings', 'AVA', 2, 1, 1, 'To validate');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1010, 'Category', 'Buildings', 'AVA', 3, 1, 1, 'To validate');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1021, 'Category', 'Buildings', 'AVA', 1, 0.03, 0.0015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1021, 'Category', 'Buildings', 'AVA', 2, 0.5, 0.15, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1021, 'Category', 'Buildings', 'AVA', 3, 1, 0.4, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1025, 'Category', 'Buildings', 'AVA', 1, 0.03, 0.0015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1025, 'Category', 'Buildings', 'AVA', 2, 0.4, 0.06, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1025, 'Category', 'Buildings', 'AVA', 3, 0.8, 0.16, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1030, 'Category', 'Buildings', 'AVA', 1, 0.03, 0.0015, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1030, 'Category', 'Buildings', 'AVA', 2, 0.4, 0.09, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1030, 'Category', 'Buildings', 'AVA', 3, 0.9, 0.24, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1040, 'Category', 'Buildings', 'AVA', 1, 0.03, 0.0015, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1040, 'Category', 'Buildings', 'AVA', 2, 0.4, 0.09, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1040, 'Category', 'Buildings', 'AVA', 3, 0.8, 0.29, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1060, 'Category', 'Buildings', 'AVA', 1, 0.03, 0.0016, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1060, 'Category', 'Buildings', 'AVA', 2, 0.4, 0.13, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1060, 'Category', 'Buildings', 'AVA', 3, 0.8, 0.36, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1080, 'Category', 'Buildings', 'AVA', 1, 0.03, 0.0017, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1080, 'Category', 'Buildings', 'AVA', 2, 0.5, 0.19, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1080, 'Category', 'Buildings', 'AVA', 3, 0.9, 0.44, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1010, 'Category', 'Buildings', 'LTO', 1, 1, 1, 'To validate');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1010, 'Category', 'Buildings', 'LTO', 2, 1, 1, 'To validate');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1010, 'Category', 'Buildings', 'LTO', 3, 1, 1, 'To validate');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1021, 'Category', 'Buildings', 'LTO', 1, 0.15, 0.0000075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1021, 'Category', 'Buildings', 'LTO', 2, 0.55, 0.0055, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1021, 'Category', 'Buildings', 'LTO', 3, 0.75, 0.075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1025, 'Category', 'Buildings', 'LTO', 1, 0.1, 0.000002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1025, 'Category', 'Buildings', 'LTO', 2, 0.55, 0.00165, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1025, 'Category', 'Buildings', 'LTO', 3, 0.75, 0.0225, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1030, 'Category', 'Buildings', 'LTO', 1, 0.12, 4e-6, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1030, 'Category', 'Buildings', 'LTO', 2, 0.55, 0.003, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1030, 'Category', 'Buildings', 'LTO', 3, 0.75, 0.040, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1040, 'Category', 'Buildings', 'LTO', 1, 0.15, 1.5e-5, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1040, 'Category', 'Buildings', 'LTO', 2, 0.51, 0.005, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1040, 'Category', 'Buildings', 'LTO', 3, 0.68, 0.079, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1060, 'Category', 'Buildings', 'LTO', 1, 0.16, 2.4e-5, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1060, 'Category', 'Buildings', 'LTO', 2, 0.51, 0.014, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1060, 'Category', 'Buildings', 'LTO', 3, 0.70, 0.101, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1080, 'Category', 'Buildings', 'LTO', 1, 0.18, 5e-5, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1080, 'Category', 'Buildings', 'LTO', 2, 0.57, 0.027, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1080, 'Category', 'Buildings', 'LTO', 3, 0.76, 0.156, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1010, 'Category', 'Buildings', 'INOD', 1, 1, 1, 'To validate');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1010, 'Category', 'Buildings', 'INOD', 2, 1, 1, 'To validate');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1010, 'Category', 'Buildings', 'INOD', 3, 1, 1, 'To validate');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1021, 'Category', 'Buildings', 'INOD', 1, 0.15, 7.5e-06, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1021, 'Category', 'Buildings', 'INOD', 2, 0.3, 0.00015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1021, 'Category', 'Buildings', 'INOD', 3, 0.6, 0.018, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1025, 'Category', 'Buildings', 'INOD', 1, 0.1, 2e-06, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1025, 'Category', 'Buildings', 'INOD', 2, 0.25, 3.75e-05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1025, 'Category', 'Buildings', 'INOD', 3, 0.6, 0.0054, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1030, 'Category', 'Buildings', 'INOD', 1, 0.12, 4e-6, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1030, 'Category', 'Buildings', 'INOD', 2, 0.27, 7.5e-5, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1030, 'Category', 'Buildings', 'INOD', 3, 0.6, 0.0096, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1040, 'Category', 'Buildings', 'INOD', 1, 0.15, 1.5e-5, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1040, 'Category', 'Buildings', 'INOD', 2, 0.34, 1.75e-4, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1040, 'Category', 'Buildings', 'INOD', 3, 0.57, 0.0206, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1060, 'Category', 'Buildings', 'INOD', 1, 0.16, 2.4e-5, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1060, 'Category', 'Buildings', 'INOD', 2, 0.34, 4.56e-4, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1060, 'Category', 'Buildings', 'INOD', 3, 0.59, 0.0260, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1080, 'Category', 'Buildings', 'INOD', 1, 0.18, 4.7e-5, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1080, 'Category', 'Buildings', 'INOD', 2, 0.38, 8.67e-4, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1080, 'Category', 'Buildings', 'INOD', 3, 0.65, 0.0408, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1010, 'Category', 'Buildings', 'INOS', 1, 1, 1, 'To validate');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1010, 'Category', 'Buildings', 'INOS', 2, 1, 1, 'To validate');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1010, 'Category', 'Buildings', 'INOS', 3, 1, 1, 'To validate');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1021, 'Category', 'Buildings', 'INOS', 1, 0.02, 2e-07, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1021, 'Category', 'Buildings', 'INOS', 2, 0.2, 2e-05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1021, 'Category', 'Buildings', 'INOS', 3, 0.3, 0.0006, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1025, 'Category', 'Buildings', 'INOS', 1, 0.006, 1.8e-08, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1025, 'Category', 'Buildings', 'INOS', 2, 0.15, 4.5e-06, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1025, 'Category', 'Buildings', 'INOS', 3, 0.25, 0.00015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1030, 'Category', 'Buildings', 'INOS', 1, 0.01, 1e-7, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1030, 'Category', 'Buildings', 'INOS', 2, 0.17, 9.7e-6, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1030, 'Category', 'Buildings', 'INOS', 3, 0.27, 0.0003, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1040, 'Category', 'Buildings', 'INOS', 1, 0.07, 2.1e-6, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1040, 'Category', 'Buildings', 'INOS', 2, 0.28, 9e-6, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1040, 'Category', 'Buildings', 'INOS', 3, 0.39, 3.5e-4, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1060, 'Category', 'Buildings', 'INOS', 1, 0.06, 2.3e-6, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1060, 'Category', 'Buildings', 'INOS', 2, 0.29, 7.63e-5, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1060, 'Category', 'Buildings', 'INOS', 3, 0.42, 0.0004, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1080, 'Category', 'Buildings', 'INOS', 1, 0.07, 3e-7, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1080, 'Category', 'Buildings', 'INOS', 2, 0.33, 2.28e-5, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1080, 'Category', 'Buildings', 'INOS', 3, 0.47, 5.9e-4, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1010, 'Category', 'Buildings', 'CPB', 1, 1, 1, 'To validate');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1010, 'Category', 'Buildings', 'CPB', 2, 1, 1, 'To validate');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1010, 'Category', 'Buildings', 'CPB', 3, 1, 1, 'To validate');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1021, 'Category', 'Buildings', 'CPB', 1, 0.03, 3e-05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1021, 'Category', 'Buildings', 'CPB', 2, 0.3, 0.003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1021, 'Category', 'Buildings', 'CPB', 3, 0.6, 0.18, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1025, 'Category', 'Buildings', 'CPB', 1, 0.01, 1e-05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1025, 'Category', 'Buildings', 'CPB', 2, 0.1, 0.0002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1025, 'Category', 'Buildings', 'CPB', 3, 0.2, 0.02, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1030, 'Category', 'Buildings', 'CPB', 1, 0.02, 1.4e-5, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1030, 'Category', 'Buildings', 'CPB', 2, 0.14, 0.0011, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1030, 'Category', 'Buildings', 'CPB', 3, 0.33, 0.05, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1040, 'Category', 'Buildings', 'CPB', 1, 0.01, 0.00003, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1040, 'Category', 'Buildings', 'CPB', 2, 0.19, 0.0002, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1040, 'Category', 'Buildings', 'CPB', 3, 0.36, 0.10, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1060, 'Category', 'Buildings', 'CPB', 1, 0.01, 0.00011, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1060, 'Category', 'Buildings', 'CPB', 2, 0.25, 0.031, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1060, 'Category', 'Buildings', 'CPB', 3, 0.43, 0.15, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1080, 'Category', 'Buildings', 'CPB', 1, 0.01, 0.0002, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1080, 'Category', 'Buildings', 'CPB', 2, 0.38, 0.071, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1080, 'Category', 'Buildings', 'CPB', 3, 0.6, 0.31, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1010, 'Category', 'Buildings', 'GSS', 1, 1, 1, 'To validate');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1010, 'Category', 'Buildings', 'GSS', 2, 1, 1, 'To validate');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1010, 'Category', 'Buildings', 'GSS', 3, 1, 1, 'To validate');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1021, 'Category', 'Buildings', 'GSS', 1, 0.05, 5e-05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1021, 'Category', 'Buildings', 'GSS', 2, 0.3, 0.003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1021, 'Category', 'Buildings', 'GSS', 3, 0.5, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1025, 'Category', 'Buildings', 'GSS', 1, 0.05, 1.5e-05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1025, 'Category', 'Buildings', 'GSS', 2, 0.3, 0.0009, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1025, 'Category', 'Buildings', 'GSS', 3, 0.5, 0.015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1030, 'Category', 'Buildings', 'GSS', 1, 0.05, 0.00003, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1030, 'Category', 'Buildings', 'GSS', 2, 0.3, 0.001, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1030, 'Category', 'Buildings', 'GSS', 3, 0.5, 0.03, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1040, 'Category', 'Buildings', 'GSS', 1, 0.07, 0.00008, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1040, 'Category', 'Buildings', 'GSS', 2, 0.28, 0.003, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1040, 'Category', 'Buildings', 'GSS', 3, 0.51, 0.06, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1060, 'Category', 'Buildings', 'GSS', 1, 0.07, 0.00011, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1060, 'Category', 'Buildings', 'GSS', 2, 0.29, 0.009, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1060, 'Category', 'Buildings', 'GSS', 3, 0.54, 0.08, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1080, 'Category', 'Buildings', 'GSS', 1, 0.06, 0.00016, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1080, 'Category', 'Buildings', 'GSS', 2, 0.35, 0.019, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1080, 'Category', 'Buildings', 'GSS', 3, 0.63, 0.13, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1010, 'Category', 'Buildings', 'GPP', 1, 1, 0, 'To validate');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1010, 'Category', 'Buildings', 'GPP', 2, 1, 0, 'To validate');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1010, 'Category', 'Buildings', 'GPP', 3, 1, 0, 'To validate');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1021, 'Category', 'Buildings', 'GPP', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1021, 'Category', 'Buildings', 'GPP', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1021, 'Category', 'Buildings', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1025, 'Category', 'Buildings', 'GPP', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1025, 'Category', 'Buildings', 'GPP', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1025, 'Category', 'Buildings', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1030, 'Category', 'Buildings', 'GPP', 1, 0.001, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1030, 'Category', 'Buildings', 'GPP', 2, 0.01, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1030, 'Category', 'Buildings', 'GPP', 3, 1, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1040, 'Category', 'Buildings', 'GPP', 1, 0.002, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1040, 'Category', 'Buildings', 'GPP', 2, 0.021, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1040, 'Category', 'Buildings', 'GPP', 3, 1, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1060, 'Category', 'Buildings', 'GPP', 1, 0.002, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1060, 'Category', 'Buildings', 'GPP', 2, 0.022, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1060, 'Category', 'Buildings', 'GPP', 3, 1, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1080, 'Category', 'Buildings', 'GPP', 1, 0.001, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1080, 'Category', 'Buildings', 'GPP', 2, 0.008, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1080, 'Category', 'Buildings', 'GPP', 3, 1, 0, 'Source: VD');
						
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1110, 'Class', 'Buildings', 'AVA', 1, 0.03, 0.0015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1110, 'Class', 'Buildings', 'AVA', 2, 0.5, 0.15, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1110, 'Class', 'Buildings', 'AVA', 3, 1, 0.4, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1121, 'Class', 'Buildings', 'AVA', 1, 0.03, 0.0015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1121, 'Class', 'Buildings', 'AVA', 2, 0.4, 0.06, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1121, 'Class', 'Buildings', 'AVA', 3, 0.8, 0.16, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1122, 'Class', 'Buildings', 'AVA', 1, 0.03, 0.0015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1122, 'Class', 'Buildings', 'AVA', 2, 0.4, 0.06, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1122, 'Class', 'Buildings', 'AVA', 3, 0.8, 0.16, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1130, 'Class', 'Buildings', 'AVA', 1, 0.03, 0.0015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1130, 'Class', 'Buildings', 'AVA', 2, 0.4, 0.06, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1130, 'Class', 'Buildings', 'AVA', 3, 0.8, 0.16, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1211, 'Class', 'Buildings', 'AVA', 1, 0.03, 0.0015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1211, 'Class', 'Buildings', 'AVA', 2, 0.4, 0.06, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1211, 'Class', 'Buildings', 'AVA', 3, 0.8, 0.16, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1212, 'Class', 'Buildings', 'AVA', 1, 0.03, 0.0015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1212, 'Class', 'Buildings', 'AVA', 2, 0.4, 0.06, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1212, 'Class', 'Buildings', 'AVA', 3, 0.8, 0.16, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1220, 'Class', 'Buildings', 'AVA', 1, 0.03, 0.0015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1220, 'Class', 'Buildings', 'AVA', 2, 0.2, 0.06, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1220, 'Class', 'Buildings', 'AVA', 3, 0.8, 0.32, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1230, 'Class', 'Buildings', 'AVA', 1, 0.03, 0.0015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1230, 'Class', 'Buildings', 'AVA', 2, 0.4, 0.12, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1230, 'Class', 'Buildings', 'AVA', 3, 0.8, 0.32, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1241, 'Class', 'Buildings', 'AVA', 1, 0.03, 0.0015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1241, 'Class', 'Buildings', 'AVA', 2, 0.3, 0.09, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1241, 'Class', 'Buildings', 'AVA', 3, 0.8, 0.32, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1242, 'Class', 'Buildings', 'AVA', 1, 0.03, 0.0015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1242, 'Class', 'Buildings', 'AVA', 2, 0.4, 0.12, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1242, 'Class', 'Buildings', 'AVA', 3, 0.8, 0.32, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1251, 'Class', 'Buildings', 'AVA', 1, 0.03, 0.0015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1251, 'Class', 'Buildings', 'AVA', 2, 0.4, 0.12, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1251, 'Class', 'Buildings', 'AVA', 3, 0.8, 0.32, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1252, 'Class', 'Buildings', 'AVA', 1, 0.05, 0.0025, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1252, 'Class', 'Buildings', 'AVA', 2, 1, 0.5, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1252, 'Class', 'Buildings', 'AVA', 3, 1, 0.9, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1261, 'Class', 'Buildings', 'AVA', 1, 0.03, 0.0015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1261, 'Class', 'Buildings', 'AVA', 2, 0.2, 0.06, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1261, 'Class', 'Buildings', 'AVA', 3, 0.8, 0.32, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1262, 'Class', 'Buildings', 'AVA', 1, 0.03, 0.0015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1262, 'Class', 'Buildings', 'AVA', 2, 0.2, 0.06, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1262, 'Class', 'Buildings', 'AVA', 3, 0.8, 0.32, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1263, 'Class', 'Buildings', 'AVA', 1, 0.03, 0.0015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1263, 'Class', 'Buildings', 'AVA', 2, 0.4, 0.12, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1263, 'Class', 'Buildings', 'AVA', 3, 0.8, 0.32, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1264, 'Class', 'Buildings', 'AVA', 1, 0.03, 0.0015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1264, 'Class', 'Buildings', 'AVA', 2, 0.2, 0.06, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1264, 'Class', 'Buildings', 'AVA', 3, 0.8, 0.32, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1265, 'Class', 'Buildings', 'AVA', 1, 0.03, 0.0015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1265, 'Class', 'Buildings', 'AVA', 2, 0.3, 0.09, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1265, 'Class', 'Buildings', 'AVA', 3, 0.8, 0.32, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1271, 'Class', 'Buildings', 'AVA', 1, 0.03, 0.0015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1271, 'Class', 'Buildings', 'AVA', 2, 0.8, 0.24, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1271, 'Class', 'Buildings', 'AVA', 3, 1, 0.4, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1272, 'Class', 'Buildings', 'AVA', 1, 0.03, 0.0015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1272, 'Class', 'Buildings', 'AVA', 2, 0.2, 0.06, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1272, 'Class', 'Buildings', 'AVA', 3, 0.4, 0.16, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1273, 'Class', 'Buildings', 'AVA', 1, 0.03, 0.0015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1273, 'Class', 'Buildings', 'AVA', 2, 0.2, 0.06, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1273, 'Class', 'Buildings', 'AVA', 3, 0.8, 0.32, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1274, 'Class', 'Buildings', 'AVA', 1, 0.03, 0.0015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1274, 'Class', 'Buildings', 'AVA', 2, 0.5, 0.15, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1274, 'Class', 'Buildings', 'AVA', 3, 1, 0.4, 'Source: Econome 4.0');
			
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1110, 'Class', 'Buildings', 'LTO', 1, 0.15, 0.0000075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1110, 'Class', 'Buildings', 'LTO', 2, 0.55, 0.0055, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1110, 'Class', 'Buildings', 'LTO', 3, 0.75, 0.075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1121, 'Class', 'Buildings', 'LTO', 1, 0.1, 0.000002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1121, 'Class', 'Buildings', 'LTO', 2, 0.55, 0.00165, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1121, 'Class', 'Buildings', 'LTO', 3, 0.75, 0.0225, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1122, 'Class', 'Buildings', 'LTO', 1, 0.1, 0.000002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1122, 'Class', 'Buildings', 'LTO', 2, 0.55, 0.00165, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1122, 'Class', 'Buildings', 'LTO', 3, 0.75, 0.0225, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1130, 'Class', 'Buildings', 'LTO', 1, 0.1, 0.000002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1130, 'Class', 'Buildings', 'LTO', 2, 0.55, 0.00165, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1130, 'Class', 'Buildings', 'LTO', 3, 0.75, 0.0225, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1211, 'Class', 'Buildings', 'LTO', 1, 0.15, 0.000003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1211, 'Class', 'Buildings', 'LTO', 2, 0.5, 0.0015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1211, 'Class', 'Buildings', 'LTO', 3, 0.65, 0.0195, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1212, 'Class', 'Buildings', 'LTO', 1, 0.15, 0.000003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1212, 'Class', 'Buildings', 'LTO', 2, 0.5, 0.0015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1212, 'Class', 'Buildings', 'LTO', 3, 0.65, 0.0195, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1220, 'Class', 'Buildings', 'LTO', 1, 0.15, 0.0000075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1220, 'Class', 'Buildings', 'LTO', 2, 0.5, 0.005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1220, 'Class', 'Buildings', 'LTO', 3, 0.65, 0.065, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1230, 'Class', 'Buildings', 'LTO', 1, 0.15, 0.0000075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1230, 'Class', 'Buildings', 'LTO', 2, 0.5, 0.005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1230, 'Class', 'Buildings', 'LTO', 3, 0.65, 0.065, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1241, 'Class', 'Buildings', 'LTO', 1, 0.15, 0.0000075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1241, 'Class', 'Buildings', 'LTO', 2, 0.5, 0.005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1241, 'Class', 'Buildings', 'LTO', 3, 0.65, 0.065, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1242, 'Class', 'Buildings', 'LTO', 1, 0.3, 0.00015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1242, 'Class', 'Buildings', 'LTO', 2, 0.75, 0.075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1242, 'Class', 'Buildings', 'LTO', 3, 0.95, 0.475, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1251, 'Class', 'Buildings', 'LTO', 1, 0.15, 0.0000075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1251, 'Class', 'Buildings', 'LTO', 2, 0.5, 0.005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1251, 'Class', 'Buildings', 'LTO', 3, 0.65, 0.065, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1252, 'Class', 'Buildings', 'LTO', 1, 0.2, 0.0001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1252, 'Class', 'Buildings', 'LTO', 2, 0.65, 0.065, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1252, 'Class', 'Buildings', 'LTO', 3, 1, 0.2, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1261, 'Class', 'Buildings', 'LTO', 1, 0.15, 0.0000075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1261, 'Class', 'Buildings', 'LTO', 2, 0.5, 0.005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1261, 'Class', 'Buildings', 'LTO', 3, 0.65, 0.065, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1262, 'Class', 'Buildings', 'LTO', 1, 0.15, 0.0000075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1262, 'Class', 'Buildings', 'LTO', 2, 0.5, 0.005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1262, 'Class', 'Buildings', 'LTO', 3, 0.65, 0.065, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1263, 'Class', 'Buildings', 'LTO', 1, 0.1, 0.000002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1263, 'Class', 'Buildings', 'LTO', 2, 0.45, 0.00135, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1263, 'Class', 'Buildings', 'LTO', 3, 0.65, 0.0195, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1264, 'Class', 'Buildings', 'LTO', 1, 0.15, 0.0000075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1264, 'Class', 'Buildings', 'LTO', 2, 0.5, 0.005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1264, 'Class', 'Buildings', 'LTO', 3, 0.65, 0.065, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1265, 'Class', 'Buildings', 'LTO', 1, 0.15, 0.0000075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1265, 'Class', 'Buildings', 'LTO', 2, 0.5, 0.005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1265, 'Class', 'Buildings', 'LTO', 3, 0.65, 0.065, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1271, 'Class', 'Buildings', 'LTO', 1, 0.15, 0.0000075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1271, 'Class', 'Buildings', 'LTO', 2, 0.5, 0.005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1271, 'Class', 'Buildings', 'LTO', 3, 0.65, 0.065, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1272, 'Class', 'Buildings', 'LTO', 1, 0.1, 0.000005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1272, 'Class', 'Buildings', 'LTO', 2, 0.35, 0.0035, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1272, 'Class', 'Buildings', 'LTO', 3, 0.65, 0.065, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1273, 'Class', 'Buildings', 'LTO', 1, 0.15, 0.0000075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1273, 'Class', 'Buildings', 'LTO', 2, 0.5, 0.005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1273, 'Class', 'Buildings', 'LTO', 3, 0.65, 0.065, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1274, 'Class', 'Buildings', 'LTO', 1, 0.15, 0.0000075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1274, 'Class', 'Buildings', 'LTO', 2, 0.5, 0.005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1274, 'Class', 'Buildings', 'LTO', 3, 0.65, 0.065, 'Source: Econome 4.0');
			
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1110, 'Class', 'Buildings', 'INOD', 1, 0.15, 0.0000075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1110, 'Class', 'Buildings', 'INOD', 2, 0.3, 0.00015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1110, 'Class', 'Buildings', 'INOD', 3, 0.6, 0.018, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1121, 'Class', 'Buildings', 'INOD', 1, 0.1, 0.000002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1121, 'Class', 'Buildings', 'INOD', 2, 0.25, 0.0000375, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1121, 'Class', 'Buildings', 'INOD', 3, 0.6, 0.0054, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1122, 'Class', 'Buildings', 'INOD', 1, 0.1, 0.000002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1122, 'Class', 'Buildings', 'INOD', 2, 0.25, 0.0000375, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1122, 'Class', 'Buildings', 'INOD', 3, 0.6, 0.0054, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1130, 'Class', 'Buildings', 'INOD', 1, 0.1, 0.000002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1130, 'Class', 'Buildings', 'INOD', 2, 0.25, 0.0000375, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1130, 'Class', 'Buildings', 'INOD', 3, 0.6, 0.0054, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1211, 'Class', 'Buildings', 'INOD', 1, 0.15, 0.000003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1211, 'Class', 'Buildings', 'INOD', 2, 0.35, 0.0000525, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1211, 'Class', 'Buildings', 'INOD', 3, 0.55, 0.00495, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1212, 'Class', 'Buildings', 'INOD', 1, 0.15, 0.000003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1212, 'Class', 'Buildings', 'INOD', 2, 0.35, 0.0000525, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1212, 'Class', 'Buildings', 'INOD', 3, 0.55, 0.00495, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1220, 'Class', 'Buildings', 'INOD', 1, 0.15, 0.0000075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1220, 'Class', 'Buildings', 'INOD', 2, 0.35, 0.000175, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1220, 'Class', 'Buildings', 'INOD', 3, 0.55, 0.0165, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1230, 'Class', 'Buildings', 'INOD', 1, 0.15, 0.0000075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1230, 'Class', 'Buildings', 'INOD', 2, 0.35, 0.000175, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1230, 'Class', 'Buildings', 'INOD', 3, 0.55, 0.0165, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1241, 'Class', 'Buildings', 'INOD', 1, 0.15, 0.0000075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1241, 'Class', 'Buildings', 'INOD', 2, 0.35, 0.000175, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1241, 'Class', 'Buildings', 'INOD', 3, 0.55, 0.0165, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1242, 'Class', 'Buildings', 'INOD', 1, 0.3, 0.00015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1242, 'Class', 'Buildings', 'INOD', 2, 0.55, 0.00275, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1242, 'Class', 'Buildings', 'INOD', 3, 0.85, 0.1275, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1251, 'Class', 'Buildings', 'INOD', 1, 0.15, 0.0000075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1251, 'Class', 'Buildings', 'INOD', 2, 0.35, 0.000175, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1251, 'Class', 'Buildings', 'INOD', 3, 0.55, 0.0165, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1252, 'Class', 'Buildings', 'INOD', 1, 0.2, 0.0001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1252, 'Class', 'Buildings', 'INOD', 2, 0.35, 0.00175, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1252, 'Class', 'Buildings', 'INOD', 3, 0.85, 0.051, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1261, 'Class', 'Buildings', 'INOD', 1, 0.15, 0.0000075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1261, 'Class', 'Buildings', 'INOD', 2, 0.35, 0.000175, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1261, 'Class', 'Buildings', 'INOD', 3, 0.55, 0.0165, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1262, 'Class', 'Buildings', 'INOD', 1, 0.15, 0.0000075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1262, 'Class', 'Buildings', 'INOD', 2, 0.35, 0.000175, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1262, 'Class', 'Buildings', 'INOD', 3, 0.55, 0.0165, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1263, 'Class', 'Buildings', 'INOD', 1, 0.1, 0.000002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1263, 'Class', 'Buildings', 'INOD', 2, 0.2, 0.00003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1263, 'Class', 'Buildings', 'INOD', 3, 0.5, 0.0045, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1264, 'Class', 'Buildings', 'INOD', 1, 0.15, 0.0000075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1264, 'Class', 'Buildings', 'INOD', 2, 0.35, 0.000175, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1264, 'Class', 'Buildings', 'INOD', 3, 0.55, 0.0165, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1265, 'Class', 'Buildings', 'INOD', 1, 0.15, 0.0000075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1265, 'Class', 'Buildings', 'INOD', 2, 0.35, 0.000175, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1265, 'Class', 'Buildings', 'INOD', 3, 0.55, 0.0165, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1271, 'Class', 'Buildings', 'INOD', 1, 0.15, 0.0000075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1271, 'Class', 'Buildings', 'INOD', 2, 0.35, 0.000175, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1271, 'Class', 'Buildings', 'INOD', 3, 0.55, 0.0165, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1272, 'Class', 'Buildings', 'INOD', 1, 0.1, 0.000005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1272, 'Class', 'Buildings', 'INOD', 2, 0.2, 0.0001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1272, 'Class', 'Buildings', 'INOD', 3, 0.55, 0.0165, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1273, 'Class', 'Buildings', 'INOD', 1, 0.15, 0.0000075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1273, 'Class', 'Buildings', 'INOD', 2, 0.35, 0.000175, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1273, 'Class', 'Buildings', 'INOD', 3, 0.55, 0.0165, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1274, 'Class', 'Buildings', 'INOD', 1, 0.15, 0.0000075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1274, 'Class', 'Buildings', 'INOD', 2, 0.35, 0.000175, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1274, 'Class', 'Buildings', 'INOD', 3, 0.55, 0.0165, 'Source: Econome 4.0');

			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1110, 'Class', 'Buildings', 'INOS', 1, 0.02, 0.0000002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1110, 'Class', 'Buildings', 'INOS', 2, 0.2, 0.00002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1110, 'Class', 'Buildings', 'INOS', 3, 0.3, 0.0006, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1121, 'Class', 'Buildings', 'INOS', 1, 0.006, 0.000000018, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1121, 'Class', 'Buildings', 'INOS', 2, 0.15, 0.0000045, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1121, 'Class', 'Buildings', 'INOS', 3, 0.25, 0.00015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1122, 'Class', 'Buildings', 'INOS', 1, 0.006, 0.000000018, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1122, 'Class', 'Buildings', 'INOS', 2, 0.15, 0.0000045, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1122, 'Class', 'Buildings', 'INOS', 3, 0.25, 0.00015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1130, 'Class', 'Buildings', 'INOS', 1, 0.006, 0.000000018, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1130, 'Class', 'Buildings', 'INOS', 2, 0.15, 0.0000045, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1130, 'Class', 'Buildings', 'INOS', 3, 0.25, 0.00015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1211, 'Class', 'Buildings', 'INOS', 1, 0.1, 0.0000003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1211, 'Class', 'Buildings', 'INOS', 2, 0.3, 0.000009, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1211, 'Class', 'Buildings', 'INOS', 3, 0.4, 0.00024, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1212, 'Class', 'Buildings', 'INOS', 1, 0.1, 0.0000003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1212, 'Class', 'Buildings', 'INOS', 2, 0.3, 0.000009, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1212, 'Class', 'Buildings', 'INOS', 3, 0.4, 0.00024, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1220, 'Class', 'Buildings', 'INOS', 1, 0.1, 0.0000003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1220, 'Class', 'Buildings', 'INOS', 2, 0.3, 0.000009, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1220, 'Class', 'Buildings', 'INOS', 3, 0.4, 0.00024, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1230, 'Class', 'Buildings', 'INOS', 1, 0.1, 0.00001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1230, 'Class', 'Buildings', 'INOS', 2, 0.3, 0.0003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1230, 'Class', 'Buildings', 'INOS', 3, 0.4, 0.0004, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1241, 'Class', 'Buildings', 'INOS', 1, 0.1, 0.0000003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1241, 'Class', 'Buildings', 'INOS', 2, 0.3, 0.000009, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1241, 'Class', 'Buildings', 'INOS', 3, 0.4, 0.00024, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1242, 'Class', 'Buildings', 'INOS', 1, 0.02, 0.0000002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1242, 'Class', 'Buildings', 'INOS', 2, 0.5, 0.00005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1242, 'Class', 'Buildings', 'INOS', 3, 0.6, 0.0018, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1251, 'Class', 'Buildings', 'INOS', 1, 0.1, 0.00001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1251, 'Class', 'Buildings', 'INOS', 2, 0.3, 0.0003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1251, 'Class', 'Buildings', 'INOS', 3, 0.4, 0.0004, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1252, 'Class', 'Buildings', 'INOS', 1, 0.02, 0.0000002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1252, 'Class', 'Buildings', 'INOS', 2, 0.3, 0.00003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1252, 'Class', 'Buildings', 'INOS', 3, 0.6, 0.0006, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1261, 'Class', 'Buildings', 'INOS', 1, 0.1, 0.0000003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1261, 'Class', 'Buildings', 'INOS', 2, 0.3, 0.000009, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1261, 'Class', 'Buildings', 'INOS', 3, 0.4, 0.00024, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1262, 'Class', 'Buildings', 'INOS', 1, 0.1, 0.0000003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1262, 'Class', 'Buildings', 'INOS', 2, 0.3, 0.000009, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1262, 'Class', 'Buildings', 'INOS', 3, 0.4, 0.00024, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1263, 'Class', 'Buildings', 'INOS', 1, 0.006, 0.000000018, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1263, 'Class', 'Buildings', 'INOS', 2, 0.15, 0.0000045, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1263, 'Class', 'Buildings', 'INOS', 3, 0.25, 0.00015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1264, 'Class', 'Buildings', 'INOS', 1, 0.1, 0.00001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1264, 'Class', 'Buildings', 'INOS', 2, 0.3, 0.0003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1264, 'Class', 'Buildings', 'INOS', 3, 0.4, 0.0004, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1265, 'Class', 'Buildings', 'INOS', 1, 0.006, 0.000000018, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1265, 'Class', 'Buildings', 'INOS', 2, 0.3, 0.000009, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1265, 'Class', 'Buildings', 'INOS', 3, 0.4, 0.00024, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1271, 'Class', 'Buildings', 'INOS', 1, 0.05, 0.0000005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1271, 'Class', 'Buildings', 'INOS', 2, 0.3, 0.00003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1271, 'Class', 'Buildings', 'INOS', 3, 0.4, 0.0004, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1272, 'Class', 'Buildings', 'INOS', 1, 0.006, 0.000000000006, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1272, 'Class', 'Buildings', 'INOS', 2, 0.15, 0.0000000015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1272, 'Class', 'Buildings', 'INOS', 3, 0.4, 0.000000004, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1273, 'Class', 'Buildings', 'INOS', 1, 0.1, 0.0000003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1273, 'Class', 'Buildings', 'INOS', 2, 0.3, 0.000009, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1273, 'Class', 'Buildings', 'INOS', 3, 0.4, 0.00024, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1274, 'Class', 'Buildings', 'INOS', 1, 0.1, 0.0000003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1274, 'Class', 'Buildings', 'INOS', 2, 0.3, 0.000009, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1274, 'Class', 'Buildings', 'INOS', 3, 0.4, 0.00024, 'Source: Econome 4.0');
			
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1110, 'Class', 'Buildings', 'CPB', 1, 0.03, 0.00003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1110, 'Class', 'Buildings', 'CPB', 2, 0.3, 0.003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1110, 'Class', 'Buildings', 'CPB', 3, 0.6, 0.18, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1121, 'Class', 'Buildings', 'CPB', 1, 0.01, 0.00001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1121, 'Class', 'Buildings', 'CPB', 2, 0.1, 0.0002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1121, 'Class', 'Buildings', 'CPB', 3, 0.2, 0.02, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1122, 'Class', 'Buildings', 'CPB', 1, 0.01, 0.00001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1122, 'Class', 'Buildings', 'CPB', 2, 0.1, 0.0002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1122, 'Class', 'Buildings', 'CPB', 3, 0.2, 0.02, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1130, 'Class', 'Buildings', 'CPB', 1, 0.01, 0.00001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1130, 'Class', 'Buildings', 'CPB', 2, 0.1, 0.0002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1130, 'Class', 'Buildings', 'CPB', 3, 0.2, 0.02, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1211, 'Class', 'Buildings', 'CPB', 1, 0.01, 0.00001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1211, 'Class', 'Buildings', 'CPB', 2, 0.1, 0.0002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1211, 'Class', 'Buildings', 'CPB', 3, 0.2, 0.02, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1212, 'Class', 'Buildings', 'CPB', 1, 0.01, 0.00001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1212, 'Class', 'Buildings', 'CPB', 2, 0.1, 0.0002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1212, 'Class', 'Buildings', 'CPB', 3, 0.2, 0.02, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1220, 'Class', 'Buildings', 'CPB', 1, 0.01, 0.00001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1220, 'Class', 'Buildings', 'CPB', 2, 0.1, 0.0002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1220, 'Class', 'Buildings', 'CPB', 3, 0.2, 0.02, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1230, 'Class', 'Buildings', 'CPB', 1, 0.01, 0.00001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1230, 'Class', 'Buildings', 'CPB', 2, 0.3, 0.006, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1230, 'Class', 'Buildings', 'CPB', 3, 0.6, 0.06, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1241, 'Class', 'Buildings', 'CPB', 1, 0.01, 0.00001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1241, 'Class', 'Buildings', 'CPB', 2, 0.1, 0.0002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1241, 'Class', 'Buildings', 'CPB', 3, 0.2, 0.02, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1242, 'Class', 'Buildings', 'CPB', 1, 0.03, 0.0003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1242, 'Class', 'Buildings', 'CPB', 2, 0.5, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1242, 'Class', 'Buildings', 'CPB', 3, 0.9, 0.54, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1251, 'Class', 'Buildings', 'CPB', 1, 0.01, 0.00001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1251, 'Class', 'Buildings', 'CPB', 2, 0.3, 0.006, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1251, 'Class', 'Buildings', 'CPB', 3, 0.6, 0.06, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1252, 'Class', 'Buildings', 'CPB', 1, 0.01, 0.001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1252, 'Class', 'Buildings', 'CPB', 2, 0.8, 0.32, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1252, 'Class', 'Buildings', 'CPB', 3, 0.9, 0.54, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1261, 'Class', 'Buildings', 'CPB', 1, 0.01, 0.00001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1261, 'Class', 'Buildings', 'CPB', 2, 0.1, 0.0002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1261, 'Class', 'Buildings', 'CPB', 3, 0.2, 0.02, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1262, 'Class', 'Buildings', 'CPB', 1, 0.01, 0.00001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1262, 'Class', 'Buildings', 'CPB', 2, 0.1, 0.0002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1262, 'Class', 'Buildings', 'CPB', 3, 0.2, 0.02, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1263, 'Class', 'Buildings', 'CPB', 1, 0.01, 0.00001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1263, 'Class', 'Buildings', 'CPB', 2, 0.1, 0.0002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1263, 'Class', 'Buildings', 'CPB', 3, 0.2, 0.02, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1264, 'Class', 'Buildings', 'CPB', 1, 0.01, 0.00001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1264, 'Class', 'Buildings', 'CPB', 2, 0.1, 0.0002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1264, 'Class', 'Buildings', 'CPB', 3, 0.2, 0.02, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1265, 'Class', 'Buildings', 'CPB', 1, 0.01, 0.00001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1265, 'Class', 'Buildings', 'CPB', 2, 0.1, 0.0002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1265, 'Class', 'Buildings', 'CPB', 3, 0.2, 0.02, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1271, 'Class', 'Buildings', 'CPB', 1, 0.01, 0.0001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1271, 'Class', 'Buildings', 'CPB', 2, 0.5, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1271, 'Class', 'Buildings', 'CPB', 3, 0.8, 0.4, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1272, 'Class', 'Buildings', 'CPB', 1, 0.01, 0.00001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1272, 'Class', 'Buildings', 'CPB', 2, 0.1, 0.0002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1272, 'Class', 'Buildings', 'CPB', 3, 0.2, 0.02, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1273, 'Class', 'Buildings', 'CPB', 1, 0.01, 0.00001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1273, 'Class', 'Buildings', 'CPB', 2, 0.1, 0.0002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1273, 'Class', 'Buildings', 'CPB', 3, 0.2, 0.02, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1274, 'Class', 'Buildings', 'CPB', 1, 0.01, 0.00001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1274, 'Class', 'Buildings', 'CPB', 2, 0.3, 0.003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1274, 'Class', 'Buildings', 'CPB', 3, 0.6, 0.36, 'Source: Econome 4.0');
			
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1010, 'Category', 'Buildings', 'INO', 1, 1, 1, 'To validate');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1010, 'Category', 'Buildings', 'INO', 2, 1, 1, 'To validate');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1010, 'Category', 'Buildings', 'INO', 3, 1, 1, 'To validate');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1021, 'Category', 'Buildings', 'INO', 1, 0.09, 0.000004, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1021, 'Category', 'Buildings', 'INO', 2, 0.25, 0.0001, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1021, 'Category', 'Buildings', 'INO', 3, 0.45, 0.0093, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1025, 'Category', 'Buildings', 'INO', 1, 0.05, 0.000001, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1025, 'Category', 'Buildings', 'INO', 2, 0.2, 0.00002, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1025, 'Category', 'Buildings', 'INO', 3, 0.43, 0.0028, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1030, 'Category', 'Buildings', 'INO', 1, 0.06, 0.000002, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1030, 'Category', 'Buildings', 'INO', 2, 0.22, 0.00004, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1030, 'Category', 'Buildings', 'INO', 3, 0.43, 0.0050, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1040, 'Category', 'Buildings', 'INO', 1, 0.11, 0.000009, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1040, 'Category', 'Buildings', 'INO', 2, 0.31, 0.0001, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1040, 'Category', 'Buildings', 'INO', 3, 0.48, 0.0105, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1060, 'Category', 'Buildings', 'INO', 1, 0.11, 0.000013, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1060, 'Category', 'Buildings', 'INO', 2, 0.32, 0.0003, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1060, 'Category', 'Buildings', 'INO', 3, 0.50, 0.0132, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1080, 'Category', 'Buildings', 'INO', 1, 0.12, 0.000023, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1080, 'Category', 'Buildings', 'INO', 2, 0.36, 0.0004, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1080, 'Category', 'Buildings', 'INO', 3, 0.56, 0.0207, 'Source: VD');
			
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1110, 'Class', 'Buildings', 'INO', 1, 0.085, 0.00000385, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1110, 'Class', 'Buildings', 'INO', 2, 0.25, 0.000085, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1110, 'Class', 'Buildings', 'INO', 3, 0.45, 0.0093, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1121, 'Class', 'Buildings', 'INO', 1, 0.053, 0.000001009, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1121, 'Class', 'Buildings', 'INO', 2, 0.2, 0.000021, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1121, 'Class', 'Buildings', 'INO', 3, 0.425, 0.002775, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1122, 'Class', 'Buildings', 'INO', 1, 0.053, 0.000001009, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1122, 'Class', 'Buildings', 'INO', 2, 0.2, 0.000021, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1122, 'Class', 'Buildings', 'INO', 3, 0.425, 0.002775, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1130, 'Class', 'Buildings', 'INO', 1, 0.053, 0.000001009, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1130, 'Class', 'Buildings', 'INO', 2, 0.2, 0.000021, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1130, 'Class', 'Buildings', 'INO', 3, 0.425, 0.002775, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1211, 'Class', 'Buildings', 'INO', 1, 0.125, 0.00000165, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1211, 'Class', 'Buildings', 'INO', 2, 0.325, 0.00003075, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1211, 'Class', 'Buildings', 'INO', 3, 0.475, 0.002595, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1212, 'Class', 'Buildings', 'INO', 1, 0.125, 0.00000165, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1212, 'Class', 'Buildings', 'INO', 2, 0.325, 0.00003075, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1212, 'Class', 'Buildings', 'INO', 3, 0.475, 0.002595, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1220, 'Class', 'Buildings', 'INO', 1, 0.125, 0.0000039, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1220, 'Class', 'Buildings', 'INO', 2, 0.325, 0.000092, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1220, 'Class', 'Buildings', 'INO', 3, 0.475, 0.00837, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1230, 'Class', 'Buildings', 'INO', 1, 0.125, 0.00000875, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1230, 'Class', 'Buildings', 'INO', 2, 0.325, 0.0002375, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1230, 'Class', 'Buildings', 'INO', 3, 0.475, 0.00845, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1241, 'Class', 'Buildings', 'INO', 1, 0.125, 0.0000039, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1241, 'Class', 'Buildings', 'INO', 2, 0.325, 0.000092, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1241, 'Class', 'Buildings', 'INO', 3, 0.475, 0.00837, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1242, 'Class', 'Buildings', 'INO', 1, 0.16, 0.0000751, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1242, 'Class', 'Buildings', 'INO', 2, 0.525, 0.0014, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1242, 'Class', 'Buildings', 'INO', 3, 0.725, 0.06465, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1251, 'Class', 'Buildings', 'INO', 1, 0.125, 0.00000875, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1251, 'Class', 'Buildings', 'INO', 2, 0.325, 0.0002375, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1251, 'Class', 'Buildings', 'INO', 3, 0.475, 0.00845, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1252, 'Class', 'Buildings', 'INO', 1, 0.11, 0.0000501, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1252, 'Class', 'Buildings', 'INO', 2, 0.325, 0.00089, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1252, 'Class', 'Buildings', 'INO', 3, 0.725, 0.0258, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1261, 'Class', 'Buildings', 'INO', 1, 0.125, 0.0000039, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1261, 'Class', 'Buildings', 'INO', 2, 0.325, 0.000092, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1261, 'Class', 'Buildings', 'INO', 3, 0.475, 0.00837, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1262, 'Class', 'Buildings', 'INO', 1, 0.125, 0.0000039, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1262, 'Class', 'Buildings', 'INO', 2, 0.325, 0.000092, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1262, 'Class', 'Buildings', 'INO', 3, 0.475, 0.00837, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1263, 'Class', 'Buildings', 'INO', 1, 0.053, 0.000001009, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1263, 'Class', 'Buildings', 'INO', 2, 0.175, 0.00001725, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1263, 'Class', 'Buildings', 'INO', 3, 0.375, 0.002325, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1264, 'Class', 'Buildings', 'INO', 1, 0.125, 0.00000875, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1264, 'Class', 'Buildings', 'INO', 2, 0.325, 0.0002375, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1264, 'Class', 'Buildings', 'INO', 3, 0.475, 0.00845, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1265, 'Class', 'Buildings', 'INO', 1, 0.078, 0.000003759, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1265, 'Class', 'Buildings', 'INO', 2, 0.325, 0.000092, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1265, 'Class', 'Buildings', 'INO', 3, 0.475, 0.00837, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1271, 'Class', 'Buildings', 'INO', 1, 0.1, 0.000004, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1271, 'Class', 'Buildings', 'INO', 2, 0.325, 0.0001025, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1271, 'Class', 'Buildings', 'INO', 3, 0.475, 0.00845, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1272, 'Class', 'Buildings', 'INO', 1, 0.053, 0.000003, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1272, 'Class', 'Buildings', 'INO', 2, 0.175, 0.00005, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1272, 'Class', 'Buildings', 'INO', 3, 0.475, 0.0083, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1273, 'Class', 'Buildings', 'INO', 1, 0.125, 0.0000039, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1273, 'Class', 'Buildings', 'INO', 2, 0.325, 0.000092, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1273, 'Class', 'Buildings', 'INO', 3, 0.475, 0.00837, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1274, 'Class', 'Buildings', 'INO', 1, 0.125, 0.0000039, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1274, 'Class', 'Buildings', 'INO', 2, 0.325, 0.000092, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1274, 'Class', 'Buildings', 'INO', 3, 0.475, 0.00837, 'Source: VD');
			
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1110, 'Class', 'Buildings', 'GSS', 1, 0.05, 0.00005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1110, 'Class', 'Buildings', 'GSS', 2, 0.3, 0.003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1110, 'Class', 'Buildings', 'GSS', 3, 0.5, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1121, 'Class', 'Buildings', 'GSS', 1, 0.05, 0.000015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1121, 'Class', 'Buildings', 'GSS', 2, 0.3, 0.0009, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1121, 'Class', 'Buildings', 'GSS', 3, 0.5, 0.015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1122, 'Class', 'Buildings', 'GSS', 1, 0.05, 0.000015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1122, 'Class', 'Buildings', 'GSS', 2, 0.3, 0.0009, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1122, 'Class', 'Buildings', 'GSS', 3, 0.5, 0.015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1130, 'Class', 'Buildings', 'GSS', 1, 0.05, 0.000015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1130, 'Class', 'Buildings', 'GSS', 2, 0.3, 0.0009, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1130, 'Class', 'Buildings', 'GSS', 3, 0.5, 0.015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1211, 'Class', 'Buildings', 'GSS', 1, 0.05, 0.000025, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1211, 'Class', 'Buildings', 'GSS', 2, 0.3, 0.0009, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1211, 'Class', 'Buildings', 'GSS', 3, 0.5, 0.015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1212, 'Class', 'Buildings', 'GSS', 1, 0.05, 0.000025, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1212, 'Class', 'Buildings', 'GSS', 2, 0.3, 0.0009, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1212, 'Class', 'Buildings', 'GSS', 3, 0.5, 0.015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1220, 'Class', 'Buildings', 'GSS', 1, 0.05, 0.00005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1220, 'Class', 'Buildings', 'GSS', 2, 0.3, 0.003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1220, 'Class', 'Buildings', 'GSS', 3, 0.5, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1230, 'Class', 'Buildings', 'GSS', 1, 0.1, 0.0001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1230, 'Class', 'Buildings', 'GSS', 2, 0.2, 0.002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1230, 'Class', 'Buildings', 'GSS', 3, 0.5, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1241, 'Class', 'Buildings', 'GSS', 1, 0.1, 0.0001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1241, 'Class', 'Buildings', 'GSS', 2, 0.3, 0.003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1241, 'Class', 'Buildings', 'GSS', 3, 0.5, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1242, 'Class', 'Buildings', 'GSS', 1, 0.1, 0.0005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1242, 'Class', 'Buildings', 'GSS', 2, 0.5, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1242, 'Class', 'Buildings', 'GSS', 3, 0.8, 0.4, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1251, 'Class', 'Buildings', 'GSS', 1, 0.1, 0.0001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1251, 'Class', 'Buildings', 'GSS', 2, 0.2, 0.002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1251, 'Class', 'Buildings', 'GSS', 3, 0.5, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1252, 'Class', 'Buildings', 'GSS', 1, 0.02, 0.0002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1252, 'Class', 'Buildings', 'GSS', 2, 0.5, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1252, 'Class', 'Buildings', 'GSS', 3, 1, 0.2, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1261, 'Class', 'Buildings', 'GSS', 1, 0.05, 0.00005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1261, 'Class', 'Buildings', 'GSS', 2, 0.3, 0.003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1261, 'Class', 'Buildings', 'GSS', 3, 0.5, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1262, 'Class', 'Buildings', 'GSS', 1, 0.05, 0.00005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1262, 'Class', 'Buildings', 'GSS', 2, 0.3, 0.003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1262, 'Class', 'Buildings', 'GSS', 3, 0.5, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1263, 'Class', 'Buildings', 'GSS', 1, 0.07, 0.000021, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1263, 'Class', 'Buildings', 'GSS', 2, 0.3, 0.0009, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1263, 'Class', 'Buildings', 'GSS', 3, 0.5, 0.015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1264, 'Class', 'Buildings', 'GSS', 1, 0.1, 0.0001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1264, 'Class', 'Buildings', 'GSS', 2, 0.2, 0.002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1264, 'Class', 'Buildings', 'GSS', 3, 0.5, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1265, 'Class', 'Buildings', 'GSS', 1, 0.05, 0.00005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1265, 'Class', 'Buildings', 'GSS', 2, 0.3, 0.003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1265, 'Class', 'Buildings', 'GSS', 3, 0.5, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1271, 'Class', 'Buildings', 'GSS', 1, 0.05, 0.00005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1271, 'Class', 'Buildings', 'GSS', 2, 0.2, 0.002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1271, 'Class', 'Buildings', 'GSS', 3, 0.5, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1272, 'Class', 'Buildings', 'GSS', 1, 0.05, 0.00005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1272, 'Class', 'Buildings', 'GSS', 2, 0.2, 0.002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1272, 'Class', 'Buildings', 'GSS', 3, 0.3, 0.03, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1273, 'Class', 'Buildings', 'GSS', 1, 0.05, 0.00005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1273, 'Class', 'Buildings', 'GSS', 2, 0.3, 0.003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1273, 'Class', 'Buildings', 'GSS', 3, 0.5, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1274, 'Class', 'Buildings', 'GSS', 1, 0.05, 0.00005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1274, 'Class', 'Buildings', 'GSS', 2, 0.3, 0.003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1274, 'Class', 'Buildings', 'GSS', 3, 0.5, 0.05, 'Source: Econome 4.0');
			
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1110, 'Class', 'Buildings', 'GPP', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1110, 'Class', 'Buildings', 'GPP', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1110, 'Class', 'Buildings', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1121, 'Class', 'Buildings', 'GPP', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1121, 'Class', 'Buildings', 'GPP', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1121, 'Class', 'Buildings', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1122, 'Class', 'Buildings', 'GPP', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1122, 'Class', 'Buildings', 'GPP', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1122, 'Class', 'Buildings', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1130, 'Class', 'Buildings', 'GPP', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1130, 'Class', 'Buildings', 'GPP', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1130, 'Class', 'Buildings', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1211, 'Class', 'Buildings', 'GPP', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1211, 'Class', 'Buildings', 'GPP', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1211, 'Class', 'Buildings', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1212, 'Class', 'Buildings', 'GPP', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1212, 'Class', 'Buildings', 'GPP', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1212, 'Class', 'Buildings', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1220, 'Class', 'Buildings', 'GPP', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1220, 'Class', 'Buildings', 'GPP', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1220, 'Class', 'Buildings', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1230, 'Class', 'Buildings', 'GPP', 1, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1230, 'Class', 'Buildings', 'GPP', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1230, 'Class', 'Buildings', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1241, 'Class', 'Buildings', 'GPP', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1241, 'Class', 'Buildings', 'GPP', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1241, 'Class', 'Buildings', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1242, 'Class', 'Buildings', 'GPP', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1242, 'Class', 'Buildings', 'GPP', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1242, 'Class', 'Buildings', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1251, 'Class', 'Buildings', 'GPP', 1, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1251, 'Class', 'Buildings', 'GPP', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1251, 'Class', 'Buildings', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1252, 'Class', 'Buildings', 'GPP', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1252, 'Class', 'Buildings', 'GPP', 2, 0.005, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1252, 'Class', 'Buildings', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1261, 'Class', 'Buildings', 'GPP', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1261, 'Class', 'Buildings', 'GPP', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1261, 'Class', 'Buildings', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1262, 'Class', 'Buildings', 'GPP', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1262, 'Class', 'Buildings', 'GPP', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1262, 'Class', 'Buildings', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1263, 'Class', 'Buildings', 'GPP', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1263, 'Class', 'Buildings', 'GPP', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1263, 'Class', 'Buildings', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1264, 'Class', 'Buildings', 'GPP', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1264, 'Class', 'Buildings', 'GPP', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1264, 'Class', 'Buildings', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1265, 'Class', 'Buildings', 'GPP', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1265, 'Class', 'Buildings', 'GPP', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1265, 'Class', 'Buildings', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1271, 'Class', 'Buildings', 'GPP', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1271, 'Class', 'Buildings', 'GPP', 2, 0.005, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1271, 'Class', 'Buildings', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1272, 'Class', 'Buildings', 'GPP', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1272, 'Class', 'Buildings', 'GPP', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1272, 'Class', 'Buildings', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1273, 'Class', 'Buildings', 'GPP', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1273, 'Class', 'Buildings', 'GPP', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1273, 'Class', 'Buildings', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1274, 'Class', 'Buildings', 'GPP', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1274, 'Class', 'Buildings', 'GPP', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1274, 'Class', 'Buildings', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1010, 'Category', 'Buildings', 'GPP-TR', 1, 1, 0, 'To validate');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1010, 'Category', 'Buildings', 'GPP-TR', 2, 1, 0, 'To validate');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1010, 'Category', 'Buildings', 'GPP-TR', 3, 1, 0, 'To validate');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1021, 'Category', 'Buildings', 'GPP-TR', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1021, 'Category', 'Buildings', 'GPP-TR', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1021, 'Category', 'Buildings', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1025, 'Category', 'Buildings', 'GPP-TR', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1025, 'Category', 'Buildings', 'GPP-TR', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1025, 'Category', 'Buildings', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1030, 'Category', 'Buildings', 'GPP-TR', 1, 0.001, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1030, 'Category', 'Buildings', 'GPP-TR', 2, 0.01, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1030, 'Category', 'Buildings', 'GPP-TR', 3, 1, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1040, 'Category', 'Buildings', 'GPP-TR', 1, 0.002, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1040, 'Category', 'Buildings', 'GPP-TR', 2, 0.021, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1040, 'Category', 'Buildings', 'GPP-TR', 3, 1, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1060, 'Category', 'Buildings', 'GPP-TR', 1, 0.002, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1060, 'Category', 'Buildings', 'GPP-TR', 2, 0.022, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1060, 'Category', 'Buildings', 'GPP-TR', 3, 1, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1080, 'Category', 'Buildings', 'GPP-TR', 1, 0.001, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1080, 'Category', 'Buildings', 'GPP-TR', 2, 0.008, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1080, 'Category', 'Buildings', 'GPP-TR', 3, 1, 0, 'Source: VD');
			
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1110, 'Class', 'Buildings', 'GPP-TR', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1110, 'Class', 'Buildings', 'GPP-TR', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1110, 'Class', 'Buildings', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1121, 'Class', 'Buildings', 'GPP-TR', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1121, 'Class', 'Buildings', 'GPP-TR', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1121, 'Class', 'Buildings', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1122, 'Class', 'Buildings', 'GPP-TR', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1122, 'Class', 'Buildings', 'GPP-TR', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1122, 'Class', 'Buildings', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1130, 'Class', 'Buildings', 'GPP-TR', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1130, 'Class', 'Buildings', 'GPP-TR', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1130, 'Class', 'Buildings', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1211, 'Class', 'Buildings', 'GPP-TR', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1211, 'Class', 'Buildings', 'GPP-TR', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1211, 'Class', 'Buildings', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1212, 'Class', 'Buildings', 'GPP-TR', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1212, 'Class', 'Buildings', 'GPP-TR', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1212, 'Class', 'Buildings', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1220, 'Class', 'Buildings', 'GPP-TR', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1220, 'Class', 'Buildings', 'GPP-TR', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1220, 'Class', 'Buildings', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1230, 'Class', 'Buildings', 'GPP-TR', 1, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1230, 'Class', 'Buildings', 'GPP-TR', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1230, 'Class', 'Buildings', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1241, 'Class', 'Buildings', 'GPP-TR', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1241, 'Class', 'Buildings', 'GPP-TR', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1241, 'Class', 'Buildings', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1242, 'Class', 'Buildings', 'GPP-TR', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1242, 'Class', 'Buildings', 'GPP-TR', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1242, 'Class', 'Buildings', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1251, 'Class', 'Buildings', 'GPP-TR', 1, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1251, 'Class', 'Buildings', 'GPP-TR', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1251, 'Class', 'Buildings', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1252, 'Class', 'Buildings', 'GPP-TR', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1252, 'Class', 'Buildings', 'GPP-TR', 2, 0.005, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1252, 'Class', 'Buildings', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1261, 'Class', 'Buildings', 'GPP-TR', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1261, 'Class', 'Buildings', 'GPP-TR', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1261, 'Class', 'Buildings', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1262, 'Class', 'Buildings', 'GPP-TR', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1262, 'Class', 'Buildings', 'GPP-TR', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1262, 'Class', 'Buildings', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1263, 'Class', 'Buildings', 'GPP-TR', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1263, 'Class', 'Buildings', 'GPP-TR', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1263, 'Class', 'Buildings', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1264, 'Class', 'Buildings', 'GPP-TR', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1264, 'Class', 'Buildings', 'GPP-TR', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1264, 'Class', 'Buildings', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1265, 'Class', 'Buildings', 'GPP-TR', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1265, 'Class', 'Buildings', 'GPP-TR', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1265, 'Class', 'Buildings', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1271, 'Class', 'Buildings', 'GPP-TR', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1271, 'Class', 'Buildings', 'GPP-TR', 2, 0.005, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1271, 'Class', 'Buildings', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1272, 'Class', 'Buildings', 'GPP-TR', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1272, 'Class', 'Buildings', 'GPP-TR', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1272, 'Class', 'Buildings', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1273, 'Class', 'Buildings', 'GPP-TR', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1273, 'Class', 'Buildings', 'GPP-TR', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1273, 'Class', 'Buildings', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1274, 'Class', 'Buildings', 'GPP-TR', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1274, 'Class', 'Buildings', 'GPP-TR', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1274, 'Class', 'Buildings', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 24, 'Code-EconoMe', 'Routes', 'AVA', 1, 0.005, 0.00025, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 24, 'Code-EconoMe', 'Routes', 'AVA', 2, 0.1, 0.1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 24, 'Code-EconoMe', 'Routes', 'AVA', 3, 0.2, 0.2, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 25, 'Code-EconoMe', 'Routes', 'AVA', 1, 0.005, 0.00025, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 25, 'Code-EconoMe', 'Routes', 'AVA', 2, 0.1, 0.1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 25, 'Code-EconoMe', 'Routes', 'AVA', 3, 0.2, 0.2, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 26, 'Code-EconoMe', 'Routes', 'AVA', 1, 0.005, 0.00025, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 26, 'Code-EconoMe', 'Routes', 'AVA', 2, 0.1, 0.1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 26, 'Code-EconoMe', 'Routes', 'AVA', 3, 0.2, 0.2, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 31, 'Code-EconoMe', 'Routes', 'AVA', 1, 0.005, 0.00025, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 31, 'Code-EconoMe', 'Routes', 'AVA', 2, 0.7, 0.21, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 31, 'Code-EconoMe', 'Routes', 'AVA', 3, 1, 0.4, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 32, 'Code-EconoMe', 'Routes', 'AVA', 1, 0.005, 0.00025, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 32, 'Code-EconoMe', 'Routes', 'AVA', 2, 0.7, 0.21, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 32, 'Code-EconoMe', 'Routes', 'AVA', 3, 1, 0.4, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 33, 'Code-EconoMe', 'Routes', 'AVA', 1, 0.005, 0.00025, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 33, 'Code-EconoMe', 'Routes', 'AVA', 2, 0.7, 0.21, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 33, 'Code-EconoMe', 'Routes', 'AVA', 3, 1, 0.4, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 34, 'Code-EconoMe', 'Routes', 'AVA', 1, 0.005, 0.00025, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 34, 'Code-EconoMe', 'Routes', 'AVA', 2, 0.7, 0.21, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 34, 'Code-EconoMe', 'Routes', 'AVA', 3, 0.9, 0.36, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 55, 'Code-EconoMe', 'Routes', 'AVA', 1, 0.005, 0.00025, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 55, 'Code-EconoMe', 'Routes', 'AVA', 2, 0.1, 0.1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 55, 'Code-EconoMe', 'Routes', 'AVA', 3, 0.2, 0.2, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 56, 'Code-EconoMe', 'Routes', 'AVA', 1, 0.005, 0.00025, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 56, 'Code-EconoMe', 'Routes', 'AVA', 2, 0.1, 0.1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 56, 'Code-EconoMe', 'Routes', 'AVA', 3, 0.2, 0.2, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 67, 'Code-EconoMe', 'Routes', 'AVA', 1, 0.005, 0.00025, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 67, 'Code-EconoMe', 'Routes', 'AVA', 2, 0.7, 0.35, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 67, 'Code-EconoMe', 'Routes', 'AVA', 3, 0.9, 0.72, 'Source: Econome 4.0');
			
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 24, 'Code-EconoMe', 'Routes', 'CPB', 1, 0.1, 0.1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 24, 'Code-EconoMe', 'Routes', 'CPB', 2, 0.5, 0.8, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 24, 'Code-EconoMe', 'Routes', 'CPB', 3, 1, 1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 25, 'Code-EconoMe', 'Routes', 'CPB', 1, 0.1, 0.1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 25, 'Code-EconoMe', 'Routes', 'CPB', 2, 0.5, 0.8, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 25, 'Code-EconoMe', 'Routes', 'CPB', 3, 1, 1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 26, 'Code-EconoMe', 'Routes', 'CPB', 1, 0.1, 0.1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 26, 'Code-EconoMe', 'Routes', 'CPB', 2, 0.5, 0.8, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 26, 'Code-EconoMe', 'Routes', 'CPB', 3, 1, 1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 31, 'Code-EconoMe', 'Routes', 'CPB', 1, 0.1, 0.1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 31, 'Code-EconoMe', 'Routes', 'CPB', 2, 0.5, 0.8, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 31, 'Code-EconoMe', 'Routes', 'CPB', 3, 1, 1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 32, 'Code-EconoMe', 'Routes', 'CPB', 1, 0.1, 0.1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 32, 'Code-EconoMe', 'Routes', 'CPB', 2, 0.5, 0.8, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 32, 'Code-EconoMe', 'Routes', 'CPB', 3, 1, 1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 33, 'Code-EconoMe', 'Routes', 'CPB', 1, 0.1, 0.1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 33, 'Code-EconoMe', 'Routes', 'CPB', 2, 0.5, 0.8, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 33, 'Code-EconoMe', 'Routes', 'CPB', 3, 1, 1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 34, 'Code-EconoMe', 'Routes', 'CPB', 1, 0.1, 0.1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 34, 'Code-EconoMe', 'Routes', 'CPB', 2, 0.5, 0.8, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 34, 'Code-EconoMe', 'Routes', 'CPB', 3, 1, 1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 55, 'Code-EconoMe', 'Routes', 'CPB', 1, 0.1, 0.1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 55, 'Code-EconoMe', 'Routes', 'CPB', 2, 0.5, 0.8, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 55, 'Code-EconoMe', 'Routes', 'CPB', 3, 1, 1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 56, 'Code-EconoMe', 'Routes', 'CPB', 1, 0.1, 0.1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 56, 'Code-EconoMe', 'Routes', 'CPB', 2, 0.5, 0.8, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 56, 'Code-EconoMe', 'Routes', 'CPB', 3, 1, 1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 67, 'Code-EconoMe', 'Routes', 'CPB', 1, 0.1, 0.1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 67, 'Code-EconoMe', 'Routes', 'CPB', 2, 0.5, 0.8, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 67, 'Code-EconoMe', 'Routes', 'CPB', 3, 1, 1, 'Source: Econome 4.0');
			
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 24, 'Code-EconoMe', 'Routes', 'GPP', 1, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 24, 'Code-EconoMe', 'Routes', 'GPP', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 24, 'Code-EconoMe', 'Routes', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 25, 'Code-EconoMe', 'Routes', 'GPP', 1, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 25, 'Code-EconoMe', 'Routes', 'GPP', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 25, 'Code-EconoMe', 'Routes', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 26, 'Code-EconoMe', 'Routes', 'GPP', 1, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 26, 'Code-EconoMe', 'Routes', 'GPP', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 26, 'Code-EconoMe', 'Routes', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 31, 'Code-EconoMe', 'Routes', 'GPP', 1, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 31, 'Code-EconoMe', 'Routes', 'GPP', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 31, 'Code-EconoMe', 'Routes', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 32, 'Code-EconoMe', 'Routes', 'GPP', 1, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 32, 'Code-EconoMe', 'Routes', 'GPP', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 32, 'Code-EconoMe', 'Routes', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 33, 'Code-EconoMe', 'Routes', 'GPP', 1, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 33, 'Code-EconoMe', 'Routes', 'GPP', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 33, 'Code-EconoMe', 'Routes', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 34, 'Code-EconoMe', 'Routes', 'GPP', 1, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 34, 'Code-EconoMe', 'Routes', 'GPP', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 34, 'Code-EconoMe', 'Routes', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 55, 'Code-EconoMe', 'Routes', 'GPP', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 55, 'Code-EconoMe', 'Routes', 'GPP', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 55, 'Code-EconoMe', 'Routes', 'GPP', 3, 0.8, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 56, 'Code-EconoMe', 'Routes', 'GPP', 1, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 56, 'Code-EconoMe', 'Routes', 'GPP', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 56, 'Code-EconoMe', 'Routes', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 67, 'Code-EconoMe', 'Routes', 'GPP', 1, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 67, 'Code-EconoMe', 'Routes', 'GPP', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 67, 'Code-EconoMe', 'Routes', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 24, 'Code-EconoMe', 'Routes', 'GPP-TR', 1, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 24, 'Code-EconoMe', 'Routes', 'GPP-TR', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 24, 'Code-EconoMe', 'Routes', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 25, 'Code-EconoMe', 'Routes', 'GPP-TR', 1, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 25, 'Code-EconoMe', 'Routes', 'GPP-TR', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 25, 'Code-EconoMe', 'Routes', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 26, 'Code-EconoMe', 'Routes', 'GPP-TR', 1, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 26, 'Code-EconoMe', 'Routes', 'GPP-TR', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 26, 'Code-EconoMe', 'Routes', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 31, 'Code-EconoMe', 'Routes', 'GPP-TR', 1, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 31, 'Code-EconoMe', 'Routes', 'GPP-TR', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 31, 'Code-EconoMe', 'Routes', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 32, 'Code-EconoMe', 'Routes', 'GPP-TR', 1, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 32, 'Code-EconoMe', 'Routes', 'GPP-TR', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 32, 'Code-EconoMe', 'Routes', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 33, 'Code-EconoMe', 'Routes', 'GPP-TR', 1, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 33, 'Code-EconoMe', 'Routes', 'GPP-TR', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 33, 'Code-EconoMe', 'Routes', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 34, 'Code-EconoMe', 'Routes', 'GPP-TR', 1, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 34, 'Code-EconoMe', 'Routes', 'GPP-TR', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 34, 'Code-EconoMe', 'Routes', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 55, 'Code-EconoMe', 'Routes', 'GPP-TR', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 55, 'Code-EconoMe', 'Routes', 'GPP-TR', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 55, 'Code-EconoMe', 'Routes', 'GPP-TR', 3, 0.8, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 56, 'Code-EconoMe', 'Routes', 'GPP-TR', 1, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 56, 'Code-EconoMe', 'Routes', 'GPP-TR', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 56, 'Code-EconoMe', 'Routes', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 67, 'Code-EconoMe', 'Routes', 'GPP-TR', 1, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 67, 'Code-EconoMe', 'Routes', 'GPP-TR', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 67, 'Code-EconoMe', 'Routes', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 24, 'Code-EconoMe', 'Routes', 'LTO', 1, 0.05, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 24, 'Code-EconoMe', 'Routes', 'LTO', 2, 0.25, 0.1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 24, 'Code-EconoMe', 'Routes', 'LTO', 3, 0.45, 0.3015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 25, 'Code-EconoMe', 'Routes', 'LTO', 1, 0.05, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 25, 'Code-EconoMe', 'Routes', 'LTO', 2, 0.35, 0.5005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 25, 'Code-EconoMe', 'Routes', 'LTO', 3, 0.65, 0.7995, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 26, 'Code-EconoMe', 'Routes', 'LTO', 1, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 26, 'Code-EconoMe', 'Routes', 'LTO', 2, 0.65, 0.702, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 26, 'Code-EconoMe', 'Routes', 'LTO', 3, 0.95, 0.9025, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 31, 'Code-EconoMe', 'Routes', 'LTO', 1, 0.07, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 31, 'Code-EconoMe', 'Routes', 'LTO', 2, 0.65, 0.702, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 31, 'Code-EconoMe', 'Routes', 'LTO', 3, 1, 0.9, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 32, 'Code-EconoMe', 'Routes', 'LTO', 1, 0.07, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 32, 'Code-EconoMe', 'Routes', 'LTO', 2, 0.65, 0.702, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 32, 'Code-EconoMe', 'Routes', 'LTO', 3, 0.95, 0.9025, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 33, 'Code-EconoMe', 'Routes', 'LTO', 1, 0.025, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 33, 'Code-EconoMe', 'Routes', 'LTO', 2, 0.25, 0.7, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 33, 'Code-EconoMe', 'Routes', 'LTO', 3, 0.95, 0.798, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 34, 'Code-EconoMe', 'Routes', 'LTO', 1, 0.05, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 34, 'Code-EconoMe', 'Routes', 'LTO', 2, 0.25, 0.1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 34, 'Code-EconoMe', 'Routes', 'LTO', 3, 0.45, 0.3015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 55, 'Code-EconoMe', 'Routes', 'LTO', 1, 0.35, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 55, 'Code-EconoMe', 'Routes', 'LTO', 2, 0.95, 0.703, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 55, 'Code-EconoMe', 'Routes', 'LTO', 3, 1, 0.9, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 56, 'Code-EconoMe', 'Routes', 'LTO', 1, 0.25, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 56, 'Code-EconoMe', 'Routes', 'LTO', 2, 0.65, 0.702, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 56, 'Code-EconoMe', 'Routes', 'LTO', 3, 1, 0.9, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 67, 'Code-EconoMe', 'Routes', 'LTO', 1, 0.2, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 67, 'Code-EconoMe', 'Routes', 'LTO', 2, 0.65, 0.702, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 67, 'Code-EconoMe', 'Routes', 'LTO', 3, 1, 1, 'Source: Econome 4.0');
			
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 24, 'Code-EconoMe', 'Routes', 'GSS', 1, 0.1, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 24, 'Code-EconoMe', 'Routes', 'GSS', 2, 0.2, 0.1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 24, 'Code-EconoMe', 'Routes', 'GSS', 3, 0.3, 0.3, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 25, 'Code-EconoMe', 'Routes', 'GSS', 1, 0.1, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 25, 'Code-EconoMe', 'Routes', 'GSS', 2, 0.2, 0.5, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 25, 'Code-EconoMe', 'Routes', 'GSS', 3, 0.5, 0.8, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 26, 'Code-EconoMe', 'Routes', 'GSS', 1, 0.1, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 26, 'Code-EconoMe', 'Routes', 'GSS', 2, 0.5, 0.7, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 26, 'Code-EconoMe', 'Routes', 'GSS', 3, 0.8, 0.904, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 31, 'Code-EconoMe', 'Routes', 'GSS', 1, 0.1, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 31, 'Code-EconoMe', 'Routes', 'GSS', 2, 0.5, 0.7, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 31, 'Code-EconoMe', 'Routes', 'GSS', 3, 1, 0.9, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 32, 'Code-EconoMe', 'Routes', 'GSS', 1, 0.1, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 32, 'Code-EconoMe', 'Routes', 'GSS', 2, 0.5, 0.7, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 32, 'Code-EconoMe', 'Routes', 'GSS', 3, 0.8, 0.904, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 33, 'Code-EconoMe', 'Routes', 'GSS', 1, 0.1, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 33, 'Code-EconoMe', 'Routes', 'GSS', 2, 0.2, 0.7, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 33, 'Code-EconoMe', 'Routes', 'GSS', 3, 0.8, 0.8, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 34, 'Code-EconoMe', 'Routes', 'GSS', 1, 0.1, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 34, 'Code-EconoMe', 'Routes', 'GSS', 2, 0.2, 0.1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 34, 'Code-EconoMe', 'Routes', 'GSS', 3, 0.3, 0.3, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 55, 'Code-EconoMe', 'Routes', 'GSS', 1, 0.1, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 55, 'Code-EconoMe', 'Routes', 'GSS', 2, 0.6, 0.702, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 55, 'Code-EconoMe', 'Routes', 'GSS', 3, 0.9, 0.9, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 56, 'Code-EconoMe', 'Routes', 'GSS', 1, 0.1, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 56, 'Code-EconoMe', 'Routes', 'GSS', 2, 0.5, 0.7, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 56, 'Code-EconoMe', 'Routes', 'GSS', 3, 0.8, 0.904, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 67, 'Code-EconoMe', 'Routes', 'GSS', 1, 0.1, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 67, 'Code-EconoMe', 'Routes', 'GSS', 2, 0.5, 0.7, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 67, 'Code-EconoMe', 'Routes', 'GSS', 3, 0.8, 1, 'Source: Econome 4.0');
			
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 24, 'Code-EconoMe', 'Routes', 'INOS', 1, 0, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 24, 'Code-EconoMe', 'Routes', 'INOS', 2, 0.01, 0.0000000001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 24, 'Code-EconoMe', 'Routes', 'INOS', 3, 0.1, 0.0001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 25, 'Code-EconoMe', 'Routes', 'INOS', 1, 0, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 25, 'Code-EconoMe', 'Routes', 'INOS', 2, 0.01, 0.0000000001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 25, 'Code-EconoMe', 'Routes', 'INOS', 3, 0.1, 0.001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 26, 'Code-EconoMe', 'Routes', 'INOS', 1, 0, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 26, 'Code-EconoMe', 'Routes', 'INOS', 2, 0.1, 0.000000001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 26, 'Code-EconoMe', 'Routes', 'INOS', 3, 0.3, 0.03, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 31, 'Code-EconoMe', 'Routes', 'INOS', 1, 0, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 31, 'Code-EconoMe', 'Routes', 'INOS', 2, 0.01, 0.0000000001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 31, 'Code-EconoMe', 'Routes', 'INOS', 3, 0.3, 0.000000003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 32, 'Code-EconoMe', 'Routes', 'INOS', 1, 0, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 32, 'Code-EconoMe', 'Routes', 'INOS', 2, 0.01, 0.0000000001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 32, 'Code-EconoMe', 'Routes', 'INOS', 3, 0.1, 0.000000001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 33, 'Code-EconoMe', 'Routes', 'INOS', 1, 0, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 33, 'Code-EconoMe', 'Routes', 'INOS', 2, 0.001, 0.00000000001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 33, 'Code-EconoMe', 'Routes', 'INOS', 3, 0.01, 0.0000000001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 34, 'Code-EconoMe', 'Routes', 'INOS', 1, 0, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 34, 'Code-EconoMe', 'Routes', 'INOS', 2, 0.001, 0.00000000001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 34, 'Code-EconoMe', 'Routes', 'INOS', 3, 0.01, 0.0000000001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 55, 'Code-EconoMe', 'Routes', 'INOS', 1, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 55, 'Code-EconoMe', 'Routes', 'INOS', 2, 0.6, 0.000000006, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 55, 'Code-EconoMe', 'Routes', 'INOS', 3, 1, 0.3, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 56, 'Code-EconoMe', 'Routes', 'INOS', 1, 0, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 56, 'Code-EconoMe', 'Routes', 'INOS', 2, 0.5, 0.000000005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 56, 'Code-EconoMe', 'Routes', 'INOS', 3, 1, 0.3, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 67, 'Code-EconoMe', 'Routes', 'INOS', 1, 0.1, 0.00001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 67, 'Code-EconoMe', 'Routes', 'INOS', 2, 0.3, 0.0003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 67, 'Code-EconoMe', 'Routes', 'INOS', 3, 0.6, 0.18, 'Source: Econome 4.0');
			
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 24, 'Code-EconoMe', 'Routes', 'INOD', 1, 0.05, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 24, 'Code-EconoMe', 'Routes', 'INOD', 2, 0.05, 0.00025, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 24, 'Code-EconoMe', 'Routes', 'INOD', 3, 0.35, 0.0315, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 25, 'Code-EconoMe', 'Routes', 'INOD', 1, 0.05, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 25, 'Code-EconoMe', 'Routes', 'INOD', 2, 0.1, 0.0025, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 25, 'Code-EconoMe', 'Routes', 'INOD', 3, 0.45, 0.108, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 26, 'Code-EconoMe', 'Routes', 'INOD', 1, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 26, 'Code-EconoMe', 'Routes', 'INOD', 2, 0.2, 0.007, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 26, 'Code-EconoMe', 'Routes', 'INOD', 3, 0.75, 0.2025, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 31, 'Code-EconoMe', 'Routes', 'INOD', 1, 0.07, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 31, 'Code-EconoMe', 'Routes', 'INOD', 2, 0.15, 0.00525, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 31, 'Code-EconoMe', 'Routes', 'INOD', 3, 0.75, 0.2025, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 32, 'Code-EconoMe', 'Routes', 'INOD', 1, 0.07, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 32, 'Code-EconoMe', 'Routes', 'INOD', 2, 0.15, 0.00525, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 32, 'Code-EconoMe', 'Routes', 'INOD', 3, 0.65, 0.1755, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 33, 'Code-EconoMe', 'Routes', 'INOD', 1, 0.025, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 33, 'Code-EconoMe', 'Routes', 'INOD', 2, 0.05, 0.00175, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 33, 'Code-EconoMe', 'Routes', 'INOD', 3, 0.65, 0.156, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 34, 'Code-EconoMe', 'Routes', 'INOD', 1, 0.05, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 34, 'Code-EconoMe', 'Routes', 'INOD', 2, 0.05, 0.00025, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 34, 'Code-EconoMe', 'Routes', 'INOD', 3, 0.3, 0.027, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 55, 'Code-EconoMe', 'Routes', 'INOD', 1, 0.35, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 55, 'Code-EconoMe', 'Routes', 'INOD', 2, 0.65, 0.02275, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 55, 'Code-EconoMe', 'Routes', 'INOD', 3, 1, 0.27, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 56, 'Code-EconoMe', 'Routes', 'INOD', 1, 0.25, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 56, 'Code-EconoMe', 'Routes', 'INOD', 2, 0.55, 0.01925, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 56, 'Code-EconoMe', 'Routes', 'INOD', 3, 1, 0.27, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 67, 'Code-EconoMe', 'Routes', 'INOD', 1, 0.2, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 67, 'Code-EconoMe', 'Routes', 'INOD', 2, 0.35, 0.01225, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 67, 'Code-EconoMe', 'Routes', 'INOD', 3, 0.85, 0.255, 'Source: Econome 4.0');

			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 24, 'Code-EconoMe', 'Routes', 'INO', 1, 0.025, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 24, 'Code-EconoMe', 'Routes', 'INO', 2, 0.03, 0.00012500005, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 24, 'Code-EconoMe', 'Routes', 'INO', 3, 0.225, 0.0158, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 25, 'Code-EconoMe', 'Routes', 'INO', 1, 0.025, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 25, 'Code-EconoMe', 'Routes', 'INO', 2, 0.055, 0.00012500005, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 25, 'Code-EconoMe', 'Routes', 'INO', 3, 0.275, 0.0545, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 26, 'Code-EconoMe', 'Routes', 'INO', 1, 0.05, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 26, 'Code-EconoMe', 'Routes', 'INO', 2, 0.15, 0.0035000005, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 26, 'Code-EconoMe', 'Routes', 'INO', 3, 0.525, 0.11625, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 31, 'Code-EconoMe', 'Routes', 'INO', 1, 0.035, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 31, 'Code-EconoMe', 'Routes', 'INO', 2, 0.08, 0.00262500005, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 31, 'Code-EconoMe', 'Routes', 'INO', 3, 0.525, 0.1012500015, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 32, 'Code-EconoMe', 'Routes', 'INO', 1, 0.035, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 32, 'Code-EconoMe', 'Routes', 'INO', 2, 0.08, 0.00262500005, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 32, 'Code-EconoMe', 'Routes', 'INO', 3, 0.375, 0.0877500005, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 33, 'Code-EconoMe', 'Routes', 'INO', 1, 0.0125, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 33, 'Code-EconoMe', 'Routes', 'INO', 2, 0.0255, 0.000875000005, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 33, 'Code-EconoMe', 'Routes', 'INO', 3, 0.33, 0.07800000005, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 34, 'Code-EconoMe', 'Routes', 'INO', 1, 0.025, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 34, 'Code-EconoMe', 'Routes', 'INO', 2, 0.0255, 0.0001250000050, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 34, 'Code-EconoMe', 'Routes', 'INO', 3, 0.155, 0.01350000005, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 55, 'Code-EconoMe', 'Routes', 'INO', 1, 0.225, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 55, 'Code-EconoMe', 'Routes', 'INO', 2, 0.625, 0.011375003, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 55, 'Code-EconoMe', 'Routes', 'INO', 3, 1, 0.285, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 56, 'Code-EconoMe', 'Routes', 'INO', 1, 0.125, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 56, 'Code-EconoMe', 'Routes', 'INO', 2, 0.525, 0.0096250025, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 56, 'Code-EconoMe', 'Routes', 'INO', 3, 1, 0.285, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 67, 'Code-EconoMe', 'Routes', 'INO', 1, 0.15, 0.000005, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 67, 'Code-EconoMe', 'Routes', 'INO', 2, 0.325, 0.006275, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 67, 'Code-EconoMe', 'Routes', 'INO', 3, 0.725, 0.2175, 'Source: VD');
			
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1, 'Code-EconoMe', 'Surfaces', 'AVA', 1, 0.03, 0.0015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1, 'Code-EconoMe', 'Surfaces', 'AVA', 2, 0.5, 0.15, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1, 'Code-EconoMe', 'Surfaces', 'AVA', 3, 1, 0.4, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 5, 'Code-EconoMe', 'Surfaces', 'AVA', 1, 0.03, 0.0015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 5, 'Code-EconoMe', 'Surfaces', 'AVA', 2, 0.4, 0.06, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 5, 'Code-EconoMe', 'Surfaces', 'AVA', 3, 0.8, 0.16, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 6, 'Code-EconoMe', 'Surfaces', 'AVA', 1, 0.03, 0.0015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 6, 'Code-EconoMe', 'Surfaces', 'AVA', 2, 0.4, 0.12, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 6, 'Code-EconoMe', 'Surfaces', 'AVA', 3, 0.8, 0.32, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 9, 'Code-EconoMe', 'Surfaces', 'AVA', 1, 0.03, 0.0015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 9, 'Code-EconoMe', 'Surfaces', 'AVA', 2, 0.4, 0.12, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 9, 'Code-EconoMe', 'Surfaces', 'AVA', 3, 0.8, 0.32, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 12, 'Code-EconoMe', 'Surfaces', 'AVA', 1, 0.03, 0.0015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 12, 'Code-EconoMe', 'Surfaces', 'AVA', 2, 0.2, 0.06, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 12, 'Code-EconoMe', 'Surfaces', 'AVA', 3, 0.4, 0.16, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 18, 'Code-EconoMe', 'Surfaces', 'AVA', 1, 0.03, 0.0015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 18, 'Code-EconoMe', 'Surfaces', 'AVA', 2, 0.2, 0.06, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 18, 'Code-EconoMe', 'Surfaces', 'AVA', 3, 0.4, 0.32, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 21, 'Code-EconoMe', 'Surfaces', 'AVA', 1, 0.001, 0.0002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 21, 'Code-EconoMe', 'Surfaces', 'AVA', 2, 0.2, 0.1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 21, 'Code-EconoMe', 'Surfaces', 'AVA', 3, 0.6, 0.48, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 41, 'Code-EconoMe', 'Surfaces', 'AVA', 1, 0, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 41, 'Code-EconoMe', 'Surfaces', 'AVA', 2, 0.05, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 41, 'Code-EconoMe', 'Surfaces', 'AVA', 3, 0.3, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 42, 'Code-EconoMe', 'Surfaces', 'AVA', 1, 0.05, 0.01, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 42, 'Code-EconoMe', 'Surfaces', 'AVA', 2, 0.1, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 42, 'Code-EconoMe', 'Surfaces', 'AVA', 3, 0.3, 0.24, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 43, 'Code-EconoMe', 'Surfaces', 'AVA', 1, 0, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 43, 'Code-EconoMe', 'Surfaces', 'AVA', 2, 0.03, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 43, 'Code-EconoMe', 'Surfaces', 'AVA', 3, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 44, 'Code-EconoMe', 'Surfaces', 'AVA', 1, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 44, 'Code-EconoMe', 'Surfaces', 'AVA', 2, 0.6, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 44, 'Code-EconoMe', 'Surfaces', 'AVA', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 45, 'Code-EconoMe', 'Surfaces', 'AVA', 1, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 45, 'Code-EconoMe', 'Surfaces', 'AVA', 2, 0.6, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 45, 'Code-EconoMe', 'Surfaces', 'AVA', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 46, 'Code-EconoMe', 'Surfaces', 'AVA', 1, 0.05, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 46, 'Code-EconoMe', 'Surfaces', 'AVA', 2, 0.2, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 46, 'Code-EconoMe', 'Surfaces', 'AVA', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 47, 'Code-EconoMe', 'Surfaces', 'AVA', 1, 0.05, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 47, 'Code-EconoMe', 'Surfaces', 'AVA', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 47, 'Code-EconoMe', 'Surfaces', 'AVA', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 48, 'Code-EconoMe', 'Surfaces', 'AVA', 1, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 48, 'Code-EconoMe', 'Surfaces', 'AVA', 2, 0.6, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 48, 'Code-EconoMe', 'Surfaces', 'AVA', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 83, 'Code-EconoMe', 'Surfaces', 'AVA', 1, 0.05, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 83, 'Code-EconoMe', 'Surfaces', 'AVA', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 83, 'Code-EconoMe', 'Surfaces', 'AVA', 3, 0.3, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 84, 'Code-EconoMe', 'Surfaces', 'AVA', 1, 0.05, 0.01, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 84, 'Code-EconoMe', 'Surfaces', 'AVA', 2, 0.1, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 84, 'Code-EconoMe', 'Surfaces', 'AVA', 3, 0.3, 0.24, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 85, 'Code-EconoMe', 'Surfaces', 'AVA', 1, 0.05, 0.01, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 85, 'Code-EconoMe', 'Surfaces', 'AVA', 2, 0.1, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 85, 'Code-EconoMe', 'Surfaces', 'AVA', 3, 0.3, 0.24, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 86, 'Code-EconoMe', 'Surfaces', 'AVA', 1, 0.05, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 86, 'Code-EconoMe', 'Surfaces', 'AVA', 2, 0.2, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 86, 'Code-EconoMe', 'Surfaces', 'AVA', 3, 0.5, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 87, 'Code-EconoMe', 'Surfaces', 'AVA', 1, 0.03, 0.0015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 87, 'Code-EconoMe', 'Surfaces', 'AVA', 2, 0.4, 0.06, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 87, 'Code-EconoMe', 'Surfaces', 'AVA', 3, 0.8, 0.16, 'Source: Econome 4.0');
			
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1, 'Code-EconoMe', 'Surfaces', 'CPB', 1, 0.03, 0.00003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1, 'Code-EconoMe', 'Surfaces', 'CPB', 2, 0.3, 0.003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1, 'Code-EconoMe', 'Surfaces', 'CPB', 3, 0.6, 0.18, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 5, 'Code-EconoMe', 'Surfaces', 'CPB', 1, 0.01, 0.00001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 5, 'Code-EconoMe', 'Surfaces', 'CPB', 2, 0.1, 0.0002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 5, 'Code-EconoMe', 'Surfaces', 'CPB', 3, 0.2, 0.02, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 6, 'Code-EconoMe', 'Surfaces', 'CPB', 1, 0.01, 0.00001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 6, 'Code-EconoMe', 'Surfaces', 'CPB', 2, 0.3, 0.006, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 6, 'Code-EconoMe', 'Surfaces', 'CPB', 3, 0.6, 0.06, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 9, 'Code-EconoMe', 'Surfaces', 'CPB', 1, 0.01, 0.00001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 9, 'Code-EconoMe', 'Surfaces', 'CPB', 2, 0.3, 0.006, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 9, 'Code-EconoMe', 'Surfaces', 'CPB', 3, 0.6, 0.06, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 12, 'Code-EconoMe', 'Surfaces', 'CPB', 1, 0.01, 0.00001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 12, 'Code-EconoMe', 'Surfaces', 'CPB', 2, 0.1, 0.0002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 12, 'Code-EconoMe', 'Surfaces', 'CPB', 3, 0.2, 0.02, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 18, 'Code-EconoMe', 'Surfaces', 'CPB', 1, 0.01, 0.00001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 18, 'Code-EconoMe', 'Surfaces', 'CPB', 2, 0.1, 0.0002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 18, 'Code-EconoMe', 'Surfaces', 'CPB', 3, 0.2, 0.02, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 21, 'Code-EconoMe', 'Surfaces', 'CPB', 1, 0.5, 0.005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 21, 'Code-EconoMe', 'Surfaces', 'CPB', 2, 0.8, 0.08, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 21, 'Code-EconoMe', 'Surfaces', 'CPB', 3, 1, 0.6, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 41, 'Code-EconoMe', 'Surfaces', 'CPB', 1, 0.2, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 41, 'Code-EconoMe', 'Surfaces', 'CPB', 2, 0.5, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 41, 'Code-EconoMe', 'Surfaces', 'CPB', 3, 0.7, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 42, 'Code-EconoMe', 'Surfaces', 'CPB', 1, 0.5, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 42, 'Code-EconoMe', 'Surfaces', 'CPB', 2, 1, 0.8, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 42, 'Code-EconoMe', 'Surfaces', 'CPB', 3, 1, 1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 43, 'Code-EconoMe', 'Surfaces', 'CPB', 1, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 43, 'Code-EconoMe', 'Surfaces', 'CPB', 2, 0.3, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 43, 'Code-EconoMe', 'Surfaces', 'CPB', 3, 0.5, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 44, 'Code-EconoMe', 'Surfaces', 'CPB', 1, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 44, 'Code-EconoMe', 'Surfaces', 'CPB', 2, 0.3, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 44, 'Code-EconoMe', 'Surfaces', 'CPB', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 45, 'Code-EconoMe', 'Surfaces', 'CPB', 1, 0.2, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 45, 'Code-EconoMe', 'Surfaces', 'CPB', 2, 0.5, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 45, 'Code-EconoMe', 'Surfaces', 'CPB', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 46, 'Code-EconoMe', 'Surfaces', 'CPB', 1, 0.5, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 46, 'Code-EconoMe', 'Surfaces', 'CPB', 2, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 46, 'Code-EconoMe', 'Surfaces', 'CPB', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 47, 'Code-EconoMe', 'Surfaces', 'CPB', 1, 0.6, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 47, 'Code-EconoMe', 'Surfaces', 'CPB', 2, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 47, 'Code-EconoMe', 'Surfaces', 'CPB', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 48, 'Code-EconoMe', 'Surfaces', 'CPB', 1, 0.2, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 48, 'Code-EconoMe', 'Surfaces', 'CPB', 2, 0.5, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 48, 'Code-EconoMe', 'Surfaces', 'CPB', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 83, 'Code-EconoMe', 'Surfaces', 'CPB', 1, 0.5, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 83, 'Code-EconoMe', 'Surfaces', 'CPB', 2, 1, 0.8, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 83, 'Code-EconoMe', 'Surfaces', 'CPB', 3, 1, 1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 84, 'Code-EconoMe', 'Surfaces', 'CPB', 1, 0.5, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 84, 'Code-EconoMe', 'Surfaces', 'CPB', 2, 1, 0.8, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 84, 'Code-EconoMe', 'Surfaces', 'CPB', 3, 1, 1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 85, 'Code-EconoMe', 'Surfaces', 'CPB', 1, 0.5, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 85, 'Code-EconoMe', 'Surfaces', 'CPB', 2, 1, 0.8, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 85, 'Code-EconoMe', 'Surfaces', 'CPB', 3, 1, 1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 86, 'Code-EconoMe', 'Surfaces', 'CPB', 1, 0.5, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 86, 'Code-EconoMe', 'Surfaces', 'CPB', 2, 0.8, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 86, 'Code-EconoMe', 'Surfaces', 'CPB', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 87, 'Code-EconoMe', 'Surfaces', 'CPB', 1, 0.01, 0.00001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 87, 'Code-EconoMe', 'Surfaces', 'CPB', 2, 0.1, 0.0002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 87, 'Code-EconoMe', 'Surfaces', 'CPB', 3, 0.2, 0.02, 'Source: Econome 4.0');			
			
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1, 'Code-EconoMe', 'Surfaces', 'GPP', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1, 'Code-EconoMe', 'Surfaces', 'GPP', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1, 'Code-EconoMe', 'Surfaces', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 5, 'Code-EconoMe', 'Surfaces', 'GPP', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 5, 'Code-EconoMe', 'Surfaces', 'GPP', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 5, 'Code-EconoMe', 'Surfaces', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 6, 'Code-EconoMe', 'Surfaces', 'GPP', 1, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 6, 'Code-EconoMe', 'Surfaces', 'GPP', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 6, 'Code-EconoMe', 'Surfaces', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 9, 'Code-EconoMe', 'Surfaces', 'GPP', 1, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 9, 'Code-EconoMe', 'Surfaces', 'GPP', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 9, 'Code-EconoMe', 'Surfaces', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 12, 'Code-EconoMe', 'Surfaces', 'GPP', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 12, 'Code-EconoMe', 'Surfaces', 'GPP', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 12, 'Code-EconoMe', 'Surfaces', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 18, 'Code-EconoMe', 'Surfaces', 'GPP', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 18, 'Code-EconoMe', 'Surfaces', 'GPP', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 18, 'Code-EconoMe', 'Surfaces', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 21, 'Code-EconoMe', 'Surfaces', 'GPP', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 21, 'Code-EconoMe', 'Surfaces', 'GPP', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 21, 'Code-EconoMe', 'Surfaces', 'GPP', 3, 0.5, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 41, 'Code-EconoMe', 'Surfaces', 'GPP', 1, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 41, 'Code-EconoMe', 'Surfaces', 'GPP', 2, 0.2, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 41, 'Code-EconoMe', 'Surfaces', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 42, 'Code-EconoMe', 'Surfaces', 'GPP', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 42, 'Code-EconoMe', 'Surfaces', 'GPP', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 42, 'Code-EconoMe', 'Surfaces', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 43, 'Code-EconoMe', 'Surfaces', 'GPP', 1, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 43, 'Code-EconoMe', 'Surfaces', 'GPP', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 43, 'Code-EconoMe', 'Surfaces', 'GPP', 3, 0.5, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 44, 'Code-EconoMe', 'Surfaces', 'GPP', 1, 0, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 44, 'Code-EconoMe', 'Surfaces', 'GPP', 2, 0, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 44, 'Code-EconoMe', 'Surfaces', 'GPP', 3, 0.4, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 45, 'Code-EconoMe', 'Surfaces', 'GPP', 1, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 45, 'Code-EconoMe', 'Surfaces', 'GPP', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 45, 'Code-EconoMe', 'Surfaces', 'GPP', 3, 0.6, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 46, 'Code-EconoMe', 'Surfaces', 'GPP', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 46, 'Code-EconoMe', 'Surfaces', 'GPP', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 46, 'Code-EconoMe', 'Surfaces', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 47, 'Code-EconoMe', 'Surfaces', 'GPP', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 47, 'Code-EconoMe', 'Surfaces', 'GPP', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 47, 'Code-EconoMe', 'Surfaces', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 48, 'Code-EconoMe', 'Surfaces', 'GPP', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 48, 'Code-EconoMe', 'Surfaces', 'GPP', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 48, 'Code-EconoMe', 'Surfaces', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 83, 'Code-EconoMe', 'Surfaces', 'GPP', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 83, 'Code-EconoMe', 'Surfaces', 'GPP', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 83, 'Code-EconoMe', 'Surfaces', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 84, 'Code-EconoMe', 'Surfaces', 'GPP', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 84, 'Code-EconoMe', 'Surfaces', 'GPP', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 84, 'Code-EconoMe', 'Surfaces', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 85, 'Code-EconoMe', 'Surfaces', 'GPP', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 85, 'Code-EconoMe', 'Surfaces', 'GPP', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 85, 'Code-EconoMe', 'Surfaces', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 86, 'Code-EconoMe', 'Surfaces', 'GPP', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 86, 'Code-EconoMe', 'Surfaces', 'GPP', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 86, 'Code-EconoMe', 'Surfaces', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 87, 'Code-EconoMe', 'Surfaces', 'GPP', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 87, 'Code-EconoMe', 'Surfaces', 'GPP', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 87, 'Code-EconoMe', 'Surfaces', 'GPP', 3, 1, 0, 'Source: Econome 4.0');
			
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 5, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 5, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 5, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 6, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 1, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 6, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 6, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 9, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 1, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 9, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 9, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 12, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 12, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 12, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 18, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 18, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 18, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 21, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 21, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 21, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 3, 0.5, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 41, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 1, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 41, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 2, 0.2, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 41, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 42, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 42, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 42, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 43, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 1, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 43, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 43, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 3, 0.5, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 44, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 1, 0, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 44, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 2, 0, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 44, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 3, 0.4, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 45, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 1, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 45, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 45, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 3, 0.6, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 46, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 46, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 46, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 47, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 47, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 47, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 48, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 48, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 48, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 83, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 83, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 83, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 84, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 84, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 84, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 85, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 85, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 85, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 86, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 86, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 86, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 87, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 87, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 2, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 87, 'Code-EconoMe', 'Surfaces', 'GPP-TR', 3, 1, 0, 'Source: Econome 4.0');
			
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1, 'Code-EconoMe', 'Surfaces', 'LTO', 1, 0.15, 0.0000075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1, 'Code-EconoMe', 'Surfaces', 'LTO', 2, 0.55, 0.005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1, 'Code-EconoMe', 'Surfaces', 'LTO', 3, 0.75, 0.075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 5, 'Code-EconoMe', 'Surfaces', 'LTO', 1, 0.15, 0.000003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 5, 'Code-EconoMe', 'Surfaces', 'LTO', 2, 0.5, 0.0015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 5, 'Code-EconoMe', 'Surfaces', 'LTO', 3, 0.65, 0.0195, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 6, 'Code-EconoMe', 'Surfaces', 'LTO', 1, 0.15, 0.0000075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 6, 'Code-EconoMe', 'Surfaces', 'LTO', 2, 0.5, 0.005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 6, 'Code-EconoMe', 'Surfaces', 'LTO', 3, 0.65, 0.065, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 9, 'Code-EconoMe', 'Surfaces', 'LTO', 1, 0.15, 0.0000075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 9, 'Code-EconoMe', 'Surfaces', 'LTO', 2, 0.5, 0.005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 9, 'Code-EconoMe', 'Surfaces', 'LTO', 3, 0.65, 0.065, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 12, 'Code-EconoMe', 'Surfaces', 'LTO', 1, 0.1, 0.000005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 12, 'Code-EconoMe', 'Surfaces', 'LTO', 2, 0.35, 0.0035, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 12, 'Code-EconoMe', 'Surfaces', 'LTO', 3, 0.65, 0.065, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 18, 'Code-EconoMe', 'Surfaces', 'LTO', 1, 0.15, 0.0000075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 18, 'Code-EconoMe', 'Surfaces', 'LTO', 2, 0.5, 0.005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 18, 'Code-EconoMe', 'Surfaces', 'LTO', 3, 0.65, 0.065, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 21, 'Code-EconoMe', 'Surfaces', 'LTO', 1, 0.45, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 21, 'Code-EconoMe', 'Surfaces', 'LTO', 2, 1, 0.7, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 21, 'Code-EconoMe', 'Surfaces', 'LTO', 3, 1, 1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 41, 'Code-EconoMe', 'Surfaces', 'LTO', 1, 0.05, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 41, 'Code-EconoMe', 'Surfaces', 'LTO', 2, 0.2, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 41, 'Code-EconoMe', 'Surfaces', 'LTO', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 42, 'Code-EconoMe', 'Surfaces', 'LTO', 1, 0.15, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 42, 'Code-EconoMe', 'Surfaces', 'LTO', 2, 0.65, 0.455, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 42, 'Code-EconoMe', 'Surfaces', 'LTO', 3, 1, 1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 43, 'Code-EconoMe', 'Surfaces', 'LTO', 1, 0.05, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 43, 'Code-EconoMe', 'Surfaces', 'LTO', 2, 0.18, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 43, 'Code-EconoMe', 'Surfaces', 'LTO', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 44, 'Code-EconoMe', 'Surfaces', 'LTO', 1, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 44, 'Code-EconoMe', 'Surfaces', 'LTO', 2, 0.65, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 44, 'Code-EconoMe', 'Surfaces', 'LTO', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 45, 'Code-EconoMe', 'Surfaces', 'LTO', 1, 0.15, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 45, 'Code-EconoMe', 'Surfaces', 'LTO', 2, 0.75, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 45, 'Code-EconoMe', 'Surfaces', 'LTO', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 46, 'Code-EconoMe', 'Surfaces', 'LTO', 1, 0.15, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 46, 'Code-EconoMe', 'Surfaces', 'LTO', 2, 0.5, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 46, 'Code-EconoMe', 'Surfaces', 'LTO', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 47, 'Code-EconoMe', 'Surfaces', 'LTO', 1, 0.05, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 47, 'Code-EconoMe', 'Surfaces', 'LTO', 2, 0.25, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 47, 'Code-EconoMe', 'Surfaces', 'LTO', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 48, 'Code-EconoMe', 'Surfaces', 'LTO', 1, 0, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 48, 'Code-EconoMe', 'Surfaces', 'LTO', 2, 0.75, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 48, 'Code-EconoMe', 'Surfaces', 'LTO', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 83, 'Code-EconoMe', 'Surfaces', 'LTO', 1, 0.2, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 83, 'Code-EconoMe', 'Surfaces', 'LTO', 2, 0.65, 0.455, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 83, 'Code-EconoMe', 'Surfaces', 'LTO', 3, 1, 1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 84, 'Code-EconoMe', 'Surfaces', 'LTO', 1, 0.2, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 84, 'Code-EconoMe', 'Surfaces', 'LTO', 2, 0.65, 0.455, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 84, 'Code-EconoMe', 'Surfaces', 'LTO', 3, 1, 1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 85, 'Code-EconoMe', 'Surfaces', 'LTO', 1, 0.2, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 85, 'Code-EconoMe', 'Surfaces', 'LTO', 2, 0.65, 0.455, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 85, 'Code-EconoMe', 'Surfaces', 'LTO', 3, 1, 1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 86, 'Code-EconoMe', 'Surfaces', 'LTO', 1, 0.15, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 86, 'Code-EconoMe', 'Surfaces', 'LTO', 2, 0.65, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 86, 'Code-EconoMe', 'Surfaces', 'LTO', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 87, 'Code-EconoMe', 'Surfaces', 'LTO', 1, 0.1, 0.000002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 87, 'Code-EconoMe', 'Surfaces', 'LTO', 2, 0.55, 0.00165, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 87, 'Code-EconoMe', 'Surfaces', 'LTO', 3, 0.75, 0.0225, 'Source: Econome 4.0');
			
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1, 'Code-EconoMe', 'Surfaces', 'GSS', 1, 0.05, 0.00005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1, 'Code-EconoMe', 'Surfaces', 'GSS', 2, 0.3, 0.003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1, 'Code-EconoMe', 'Surfaces', 'GSS', 3, 0.5, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 5, 'Code-EconoMe', 'Surfaces', 'GSS', 1, 0.05, 0.00003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 5, 'Code-EconoMe', 'Surfaces', 'GSS', 2, 0.3, 0.0009, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 5, 'Code-EconoMe', 'Surfaces', 'GSS', 3, 0.5, 0.015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 6, 'Code-EconoMe', 'Surfaces', 'GSS', 1, 0.1, 0.0001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 6, 'Code-EconoMe', 'Surfaces', 'GSS', 2, 0.2, 0.002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 6, 'Code-EconoMe', 'Surfaces', 'GSS', 3, 0.5, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 9, 'Code-EconoMe', 'Surfaces', 'GSS', 1, 0.1, 0.0001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 9, 'Code-EconoMe', 'Surfaces', 'GSS', 2, 0.2, 0.002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 9, 'Code-EconoMe', 'Surfaces', 'GSS', 3, 0.5, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 12, 'Code-EconoMe', 'Surfaces', 'GSS', 1, 0.05, 0.00005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 12, 'Code-EconoMe', 'Surfaces', 'GSS', 2, 0.2, 0.002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 12, 'Code-EconoMe', 'Surfaces', 'GSS', 3, 0.3, 0.03, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 18, 'Code-EconoMe', 'Surfaces', 'GSS', 1, 0.05, 0.00005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 18, 'Code-EconoMe', 'Surfaces', 'GSS', 2, 0.3, 0.003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 18, 'Code-EconoMe', 'Surfaces', 'GSS', 3, 0.5, 0.05, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 21, 'Code-EconoMe', 'Surfaces', 'GSS', 1, 0.3, 0.015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 21, 'Code-EconoMe', 'Surfaces', 'GSS', 2, 0.8, 0.56, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 21, 'Code-EconoMe', 'Surfaces', 'GSS', 3, 0.8, 0.8, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 41, 'Code-EconoMe', 'Surfaces', 'GSS', 1, 0.03, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 41, 'Code-EconoMe', 'Surfaces', 'GSS', 2, 0.05, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 41, 'Code-EconoMe', 'Surfaces', 'GSS', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 42, 'Code-EconoMe', 'Surfaces', 'GSS', 1, 0.3, 0.015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 42, 'Code-EconoMe', 'Surfaces', 'GSS', 2, 0.5, 0.35, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 42, 'Code-EconoMe', 'Surfaces', 'GSS', 3, 1, 1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 43, 'Code-EconoMe', 'Surfaces', 'GSS', 1, 0.01, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 43, 'Code-EconoMe', 'Surfaces', 'GSS', 2, 0.03, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 43, 'Code-EconoMe', 'Surfaces', 'GSS', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 44, 'Code-EconoMe', 'Surfaces', 'GSS', 1, 0.2, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 44, 'Code-EconoMe', 'Surfaces', 'GSS', 2, 0.5, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 44, 'Code-EconoMe', 'Surfaces', 'GSS', 3, 0.9, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 45, 'Code-EconoMe', 'Surfaces', 'GSS', 1, 0.3, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 45, 'Code-EconoMe', 'Surfaces', 'GSS', 2, 0.6, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 45, 'Code-EconoMe', 'Surfaces', 'GSS', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 46, 'Code-EconoMe', 'Surfaces', 'GSS', 1, 0.2, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 46, 'Code-EconoMe', 'Surfaces', 'GSS', 2, 0.35, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 46, 'Code-EconoMe', 'Surfaces', 'GSS', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 47, 'Code-EconoMe', 'Surfaces', 'GSS', 1, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 47, 'Code-EconoMe', 'Surfaces', 'GSS', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 47, 'Code-EconoMe', 'Surfaces', 'GSS', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 48, 'Code-EconoMe', 'Surfaces', 'GSS', 1, 0.3, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 48, 'Code-EconoMe', 'Surfaces', 'GSS', 2, 0.6, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 48, 'Code-EconoMe', 'Surfaces', 'GSS', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 83, 'Code-EconoMe', 'Surfaces', 'GSS', 1, 0.3, 0.015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 83, 'Code-EconoMe', 'Surfaces', 'GSS', 2, 0.5, 0.35, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 83, 'Code-EconoMe', 'Surfaces', 'GSS', 3, 1, 1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 84, 'Code-EconoMe', 'Surfaces', 'GSS', 1, 0.3, 0.015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 84, 'Code-EconoMe', 'Surfaces', 'GSS', 2, 0.5, 0.35, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 84, 'Code-EconoMe', 'Surfaces', 'GSS', 3, 1, 1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 85, 'Code-EconoMe', 'Surfaces', 'GSS', 1, 0.3, 0.015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 85, 'Code-EconoMe', 'Surfaces', 'GSS', 2, 0.5, 0.35, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 85, 'Code-EconoMe', 'Surfaces', 'GSS', 3, 1, 1, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 86, 'Code-EconoMe', 'Surfaces', 'GSS', 1, 0.3, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 86, 'Code-EconoMe', 'Surfaces', 'GSS', 2, 0.5, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 86, 'Code-EconoMe', 'Surfaces', 'GSS', 3, 1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 87, 'Code-EconoMe', 'Surfaces', 'GSS', 1, 0.05, 0.00002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 87, 'Code-EconoMe', 'Surfaces', 'GSS', 2, 0.3, 0.0009, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 87, 'Code-EconoMe', 'Surfaces', 'GSS', 3, 0.5, 0.015, 'Source: Econome 4.0');
			
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1, 'Code-EconoMe', 'Surfaces', 'INOS', 1, 0.02, 0.0000002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1, 'Code-EconoMe', 'Surfaces', 'INOS', 2, 0.2, 0.00002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1, 'Code-EconoMe', 'Surfaces', 'INOS', 3, 0.3, 0.0006, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 5, 'Code-EconoMe', 'Surfaces', 'INOS', 1, 0.1, 0.0000003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 5, 'Code-EconoMe', 'Surfaces', 'INOS', 2, 0.3, 0.000009, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 5, 'Code-EconoMe', 'Surfaces', 'INOS', 3, 0.4, 0.00024, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 6, 'Code-EconoMe', 'Surfaces', 'INOS', 1, 0.1, 0.00001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 6, 'Code-EconoMe', 'Surfaces', 'INOS', 2, 0.3, 0.0003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 6, 'Code-EconoMe', 'Surfaces', 'INOS', 3, 0.4, 0.0004, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 9, 'Code-EconoMe', 'Surfaces', 'INOS', 1, 0.1, 0.00001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 9, 'Code-EconoMe', 'Surfaces', 'INOS', 2, 0.3, 0.0003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 9, 'Code-EconoMe', 'Surfaces', 'INOS', 3, 0.4, 0.0004, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 12, 'Code-EconoMe', 'Surfaces', 'INOS', 1, 0.006, 0.000000000006, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 12, 'Code-EconoMe', 'Surfaces', 'INOS', 2, 0.15, 0.000000002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 12, 'Code-EconoMe', 'Surfaces', 'INOS', 3, 0.4, 0.000000004, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 18, 'Code-EconoMe', 'Surfaces', 'INOS', 1, 0.15, 0.0000003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 18, 'Code-EconoMe', 'Surfaces', 'INOS', 2, 0.35, 0.000009, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 18, 'Code-EconoMe', 'Surfaces', 'INOS', 3, 0.55, 0.00024, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 21, 'Code-EconoMe', 'Surfaces', 'INOS', 1, 0.5, 0.0000000005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 21, 'Code-EconoMe', 'Surfaces', 'INOS', 2, 0.8, 0.0008, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 21, 'Code-EconoMe', 'Surfaces', 'INOS', 3, 1, 0.3, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 41, 'Code-EconoMe', 'Surfaces', 'INOS', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 41, 'Code-EconoMe', 'Surfaces', 'INOS', 2, 0.05, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 41, 'Code-EconoMe', 'Surfaces', 'INOS', 3, 0.5, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 42, 'Code-EconoMe', 'Surfaces', 'INOS', 1, 0.2, 0.0000000002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 42, 'Code-EconoMe', 'Surfaces', 'INOS', 2, 0.3, 0.0003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 42, 'Code-EconoMe', 'Surfaces', 'INOS', 3, 0.6, 0.18, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 43, 'Code-EconoMe', 'Surfaces', 'INOS', 1, 0.001, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 43, 'Code-EconoMe', 'Surfaces', 'INOS', 2, 0.03, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 43, 'Code-EconoMe', 'Surfaces', 'INOS', 3, 0.5, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 44, 'Code-EconoMe', 'Surfaces', 'INOS', 1, 0, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 44, 'Code-EconoMe', 'Surfaces', 'INOS', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 44, 'Code-EconoMe', 'Surfaces', 'INOS', 3, 0.4, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 45, 'Code-EconoMe', 'Surfaces', 'INOS', 1, 0, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 45, 'Code-EconoMe', 'Surfaces', 'INOS', 2, 0.2, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 45, 'Code-EconoMe', 'Surfaces', 'INOS', 3, 0.4, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 46, 'Code-EconoMe', 'Surfaces', 'INOS', 1, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 46, 'Code-EconoMe', 'Surfaces', 'INOS', 2, 0.2, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 46, 'Code-EconoMe', 'Surfaces', 'INOS', 3, 0.4, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 47, 'Code-EconoMe', 'Surfaces', 'INOS', 1, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 47, 'Code-EconoMe', 'Surfaces', 'INOS', 2, 0.2, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 47, 'Code-EconoMe', 'Surfaces', 'INOS', 3, 0.5, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 48, 'Code-EconoMe', 'Surfaces', 'INOS', 1, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 48, 'Code-EconoMe', 'Surfaces', 'INOS', 2, 0.2, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 48, 'Code-EconoMe', 'Surfaces', 'INOS', 3, 0.5, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 83, 'Code-EconoMe', 'Surfaces', 'INOS', 1, 0.2, 0.0000000002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 83, 'Code-EconoMe', 'Surfaces', 'INOS', 2, 0.3, 0.0003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 83, 'Code-EconoMe', 'Surfaces', 'INOS', 3, 0.6, 0.18, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 84, 'Code-EconoMe', 'Surfaces', 'INOS', 1, 0.2, 0.0000000002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 84, 'Code-EconoMe', 'Surfaces', 'INOS', 2, 0.3, 0.0003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 84, 'Code-EconoMe', 'Surfaces', 'INOS', 3, 0.6, 0.18, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 85, 'Code-EconoMe', 'Surfaces', 'INOS', 1, 0.2, 0.0000000002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 85, 'Code-EconoMe', 'Surfaces', 'INOS', 2, 0.3, 0.0003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 85, 'Code-EconoMe', 'Surfaces', 'INOS', 3, 0.6, 0.18, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 86, 'Code-EconoMe', 'Surfaces', 'INOS', 1, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 86, 'Code-EconoMe', 'Surfaces', 'INOS', 2, 0.2, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 86, 'Code-EconoMe', 'Surfaces', 'INOS', 3, 0.5, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 87, 'Code-EconoMe', 'Surfaces', 'INOS', 1, 0.006, 0.000000018, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 87, 'Code-EconoMe', 'Surfaces', 'INOS', 2, 0.15, 0.0000045, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 87, 'Code-EconoMe', 'Surfaces', 'INOS', 3, 0.25, 0.00015, 'Source: Econome 4.0');
			
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1, 'Code-EconoMe', 'Surfaces', 'INOD', 1, 0.15, 0.0000075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1, 'Code-EconoMe', 'Surfaces', 'INOD', 2, 0.3, 0.00015, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1, 'Code-EconoMe', 'Surfaces', 'INOD', 3, 0.6, 0.018, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 5, 'Code-EconoMe', 'Surfaces', 'INOD', 1, 0.15, 0.000003, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 5, 'Code-EconoMe', 'Surfaces', 'INOD', 2, 0.35, 0.0000525, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 5, 'Code-EconoMe', 'Surfaces', 'INOD', 3, 0.55, 0.00495, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 6, 'Code-EconoMe', 'Surfaces', 'INOD', 1, 0.15, 0.0000075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 6, 'Code-EconoMe', 'Surfaces', 'INOD', 2, 0.35, 0.000175, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 6, 'Code-EconoMe', 'Surfaces', 'INOD', 3, 0.55, 0.017, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 9, 'Code-EconoMe', 'Surfaces', 'INOD', 1, 0.15, 0.0000075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 9, 'Code-EconoMe', 'Surfaces', 'INOD', 2, 0.35, 0.000175, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 9, 'Code-EconoMe', 'Surfaces', 'INOD', 3, 0.55, 0.017, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 12, 'Code-EconoMe', 'Surfaces', 'INOD', 1, 0.1, 0.000005, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 12, 'Code-EconoMe', 'Surfaces', 'INOD', 2, 0.2, 0.0001, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 12, 'Code-EconoMe', 'Surfaces', 'INOD', 3, 0.55, 0.0165, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 18, 'Code-EconoMe', 'Surfaces', 'INOD', 1, 0.15, 0.0000075, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 18, 'Code-EconoMe', 'Surfaces', 'INOD', 2, 0.35, 0.000175, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 18, 'Code-EconoMe', 'Surfaces', 'INOD', 3, 0.55, 0.0165, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 21, 'Code-EconoMe', 'Surfaces', 'INOD', 1, 0.45, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 21, 'Code-EconoMe', 'Surfaces', 'INOD', 2, 0.85, 0.02975, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 21, 'Code-EconoMe', 'Surfaces', 'INOD', 3, 1, 0.3, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 41, 'Code-EconoMe', 'Surfaces', 'INOD', 1, 0.05, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 41, 'Code-EconoMe', 'Surfaces', 'INOD', 2, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 41, 'Code-EconoMe', 'Surfaces', 'INOD', 3, 0.85, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 42, 'Code-EconoMe', 'Surfaces', 'INOD', 1, 0.15, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 42, 'Code-EconoMe', 'Surfaces', 'INOD', 2, 0.35, 0.122675, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 42, 'Code-EconoMe', 'Surfaces', 'INOD', 3, 0.85, 0.5525, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 43, 'Code-EconoMe', 'Surfaces', 'INOD', 1, 0.05, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 43, 'Code-EconoMe', 'Surfaces', 'INOD', 2, 0.05, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 43, 'Code-EconoMe', 'Surfaces', 'INOD', 3, 0.85, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 44, 'Code-EconoMe', 'Surfaces', 'INOD', 1, 0.1, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 44, 'Code-EconoMe', 'Surfaces', 'INOD', 2, 0.2, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 44, 'Code-EconoMe', 'Surfaces', 'INOD', 3, 0.8, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 45, 'Code-EconoMe', 'Surfaces', 'INOD', 1, 0.15, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 45, 'Code-EconoMe', 'Surfaces', 'INOD', 2, 0.3, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 45, 'Code-EconoMe', 'Surfaces', 'INOD', 3, 0.8, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 46, 'Code-EconoMe', 'Surfaces', 'INOD', 1, 0.15, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 46, 'Code-EconoMe', 'Surfaces', 'INOD', 2, 0.25, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 46, 'Code-EconoMe', 'Surfaces', 'INOD', 3, 0.8, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 47, 'Code-EconoMe', 'Surfaces', 'INOD', 1, 0.05, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 47, 'Code-EconoMe', 'Surfaces', 'INOD', 2, 0.15, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 47, 'Code-EconoMe', 'Surfaces', 'INOD', 3, 0.75, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 48, 'Code-EconoMe', 'Surfaces', 'INOD', 1, 0, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 48, 'Code-EconoMe', 'Surfaces', 'INOD', 2, 0.3, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 48, 'Code-EconoMe', 'Surfaces', 'INOD', 3, 0.85, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 83, 'Code-EconoMe', 'Surfaces', 'INOD', 1, 0.2, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 83, 'Code-EconoMe', 'Surfaces', 'INOD', 2, 0.35, 0.122675, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 83, 'Code-EconoMe', 'Surfaces', 'INOD', 3, 0.85, 0.5525, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 84, 'Code-EconoMe', 'Surfaces', 'INOD', 1, 0.2, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 84, 'Code-EconoMe', 'Surfaces', 'INOD', 2, 0.35, 0.122675, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 84, 'Code-EconoMe', 'Surfaces', 'INOD', 3, 0.85, 0.5525, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 85, 'Code-EconoMe', 'Surfaces', 'INOD', 1, 0.2, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 85, 'Code-EconoMe', 'Surfaces', 'INOD', 2, 0.35, 0.122675, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 85, 'Code-EconoMe', 'Surfaces', 'INOD', 3, 0.85, 0.5525, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 86, 'Code-EconoMe', 'Surfaces', 'INOD', 1, 0.15, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 86, 'Code-EconoMe', 'Surfaces', 'INOD', 2, 0.3, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 86, 'Code-EconoMe', 'Surfaces', 'INOD', 3, 0.85, 0, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 87, 'Code-EconoMe', 'Surfaces', 'INOD', 1, 0.1, 0.000002, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 87, 'Code-EconoMe', 'Surfaces', 'INOD', 2, 0.25, 0.0000375, 'Source: Econome 4.0');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 87, 'Code-EconoMe', 'Surfaces', 'INOD', 3, 0.6, 0.0054, 'Source: Econome 4.0');
			
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1, 'Code-EconoMe', 'Surfaces', 'INO', 1, 0.085, 0.00000385, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1, 'Code-EconoMe', 'Surfaces', 'INO', 2, 0.25, 0.000085, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 1, 'Code-EconoMe', 'Surfaces', 'INO', 3, 0.45, 0.0093, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 5, 'Code-EconoMe', 'Surfaces', 'INO', 1, 0.125, 0.00000165, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 5, 'Code-EconoMe', 'Surfaces', 'INO', 2, 0.325, 0.00003075, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 5, 'Code-EconoMe', 'Surfaces', 'INO', 3, 0.475, 0.002595, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 6, 'Code-EconoMe', 'Surfaces', 'INO', 1, 0.125, 0.00000875, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 6, 'Code-EconoMe', 'Surfaces', 'INO', 2, 0.325, 0.0002375, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 6, 'Code-EconoMe', 'Surfaces', 'INO', 3, 0.475, 0.00845, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 9, 'Code-EconoMe', 'Surfaces', 'INO', 1, 0.125, 0.00000875, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 9, 'Code-EconoMe', 'Surfaces', 'INO', 2, 0.325, 0.0002375, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 9, 'Code-EconoMe', 'Surfaces', 'INO', 3, 0.475, 0.00845, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 12, 'Code-EconoMe', 'Surfaces', 'INO', 1, 0.053, 0.0000025, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 12, 'Code-EconoMe', 'Surfaces', 'INO', 2, 0.175, 0.000050001, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 12, 'Code-EconoMe', 'Surfaces', 'INO', 3, 0.475, 0.00825, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 18, 'Code-EconoMe', 'Surfaces', 'INO', 1, 0.15, 0.0000039, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 18, 'Code-EconoMe', 'Surfaces', 'INO', 2, 0.35, 0.000092, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 18, 'Code-EconoMe', 'Surfaces', 'INO', 3, 0.55, 0.00837, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 21, 'Code-EconoMe', 'Surfaces', 'INO', 1, 0.475, 0.00000000025, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 21, 'Code-EconoMe', 'Surfaces', 'INO', 2, 0.825, 0.015275, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 21, 'Code-EconoMe', 'Surfaces', 'INO', 3, 1, 0.3, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 41, 'Code-EconoMe', 'Surfaces', 'INO', 1, 0.0255, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 41, 'Code-EconoMe', 'Surfaces', 'INO', 2, 0.075, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 41, 'Code-EconoMe', 'Surfaces', 'INO', 3, 0.675, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 42, 'Code-EconoMe', 'Surfaces', 'INO', 1, 0.175, 0.0000000001, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 42, 'Code-EconoMe', 'Surfaces', 'INO', 2, 0.325, 0.0614875, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 42, 'Code-EconoMe', 'Surfaces', 'INO', 3, 0.725, 0.36625, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 43, 'Code-EconoMe', 'Surfaces', 'INO', 1, 0.0255, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 43, 'Code-EconoMe', 'Surfaces', 'INO', 2, 0.04, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 43, 'Code-EconoMe', 'Surfaces', 'INO', 3, 0.675, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 44, 'Code-EconoMe', 'Surfaces', 'INO', 1, 0.05, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 44, 'Code-EconoMe', 'Surfaces', 'INO', 2, 0.15, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 44, 'Code-EconoMe', 'Surfaces', 'INO', 3, 0.6, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 45, 'Code-EconoMe', 'Surfaces', 'INO', 1, 0.075, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 45, 'Code-EconoMe', 'Surfaces', 'INO', 2, 0.25, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 45, 'Code-EconoMe', 'Surfaces', 'INO', 3, 0.6, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 46, 'Code-EconoMe', 'Surfaces', 'INO', 1, 0.125, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 46, 'Code-EconoMe', 'Surfaces', 'INO', 2, 0.225, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 46, 'Code-EconoMe', 'Surfaces', 'INO', 3, 0.6, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 47, 'Code-EconoMe', 'Surfaces', 'INO', 1, 0.075, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 47, 'Code-EconoMe', 'Surfaces', 'INO', 2, 0.175, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 47, 'Code-EconoMe', 'Surfaces', 'INO', 3, 0.625, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 48, 'Code-EconoMe', 'Surfaces', 'INO', 1, 0.05, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 48, 'Code-EconoMe', 'Surfaces', 'INO', 2, 0.2, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 48, 'Code-EconoMe', 'Surfaces', 'INO', 3, 0.675, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 83, 'Code-EconoMe', 'Surfaces', 'INO', 1, 0.2, 0.0000000001, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 83, 'Code-EconoMe', 'Surfaces', 'INO', 2, 0.325, 0.0614875, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 83, 'Code-EconoMe', 'Surfaces', 'INO', 3, 0.725, 0.36625, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 84, 'Code-EconoMe', 'Surfaces', 'INO', 1, 0.2, 0.0000000001, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 84, 'Code-EconoMe', 'Surfaces', 'INO', 2, 0.325, 0.0614875, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 84, 'Code-EconoMe', 'Surfaces', 'INO', 3, 0.75, 0.36625, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 85, 'Code-EconoMe', 'Surfaces', 'INO', 1, 0.2, 0.0000000001, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 85, 'Code-EconoMe', 'Surfaces', 'INO', 2, 0.325, 0.0614875, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 85, 'Code-EconoMe', 'Surfaces', 'INO', 3, 0.725, 0.36625, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 86, 'Code-EconoMe', 'Surfaces', 'INO', 1, 0.125, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 86, 'Code-EconoMe', 'Surfaces', 'INO', 2, 0.25, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 86, 'Code-EconoMe', 'Surfaces', 'INO', 3, 0.675, 0, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 87, 'Code-EconoMe', 'Surfaces', 'INO', 1, 0.053, 0.00000101, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 87, 'Code-EconoMe', 'Surfaces', 'INO', 2, 0.2, 0.000021, 'Source: VD');
			INSERT INTO vulnerability_default (id, code, obj_niveau, type, processus, intensite_valeur, valeur_objet, valeur_letalite, remarques) VALUES (DEFAULT, 87, 'Code-EconoMe', 'Surfaces', 'INO', 3, 0.425, 0.002775, 'Source: VD');
			
			INSERT INTO measures (id, ouvrage, duree_effet, frais_exploitation, frais_entretien, frais_reparation, valeur_residuelle, remarques, mode, taux_interet) VALUES (1, 'Galerie', 80, 0, 0.75, 0.75, 0, 'source: Econome 2.0', 'Auto', 2);
			INSERT INTO measures (id, ouvrage, duree_effet, frais_exploitation, frais_entretien, frais_reparation, valeur_residuelle, remarques, mode, taux_interet) VALUES (2, 'Tunnel', 80, 0.5, 1, 1, 0, 'source: Econome 2.0', 'Auto', 2);
			INSERT INTO measures (id, ouvrage, duree_effet, frais_exploitation, frais_entretien, frais_reparation, valeur_residuelle, remarques, mode, taux_interet) VALUES (3, 'Galerie de protection contre les crues', 100, 0.5, 0.25, 0.25, 0, 'source: Econome 2.0', 'Auto', 2);
			INSERT INTO measures (id, ouvrage, duree_effet, frais_exploitation, frais_entretien, frais_reparation, valeur_residuelle, remarques, mode, taux_interet) VALUES (4, 'Ouvrage de stabilisation de la neige', 80, 0, 0.5, 0.5, 0, 'source: Econome 2.0', 'Auto', 2);
			INSERT INTO measures (id, ouvrage, duree_effet, frais_exploitation, frais_entretien, frais_reparation, valeur_residuelle, remarques, mode, taux_interet) VALUES (5, 'Installation de minage', 20, 5, 2, 2, 0, 'source: Econome 2.0', 'Auto', 2);
			INSERT INTO measures (id, ouvrage, duree_effet, frais_exploitation, frais_entretien, frais_reparation, valeur_residuelle, remarques, mode, taux_interet) VALUES (6, 'Filet de protection contre les chutes de pierres', 50, 0, 1, 1, 0, 'source: Econome 2.0', 'Auto', 2);
			INSERT INTO measures (id, ouvrage, duree_effet, frais_exploitation, frais_entretien, frais_reparation, valeur_residuelle, remarques, mode, taux_interet) VALUES (7, 'Digue en terre', 100, 0, 0.25, 0.25, 0, 'source: Econome 2.0', 'Auto', 2);
			INSERT INTO measures (id, ouvrage, duree_effet, frais_exploitation, frais_entretien, frais_reparation, valeur_residuelle, remarques, mode, taux_interet) VALUES (8, 'Ouvrage temporaire/ouvrage en bois', 30, 0, 1, 1, 0, 'source: Econome 2.0', 'Auto', 2);
			INSERT INTO measures (id, ouvrage, duree_effet, frais_exploitation, frais_entretien, frais_reparation, valeur_residuelle, remarques, mode, taux_interet) VALUES (9, 'Ouvrage de stabilisation des pentes', 50, 0, 0.5, 0.5, 0, 'source: Econome 2.0', 'Auto', 2);
			INSERT INTO measures (id, ouvrage, duree_effet, frais_exploitation, frais_entretien, frais_reparation, valeur_residuelle, remarques, mode, taux_interet) VALUES (10, 'Reboisements', 30, 0, 1, 1, 0, 'source: Econome 2.0', 'Auto', 2);
			INSERT INTO measures (id, ouvrage, duree_effet, frais_exploitation, frais_entretien, frais_reparation, valeur_residuelle, remarques, mode, taux_interet) VALUES (11, 'Barrage de correction torrentielle en bois', 30, 0, 1, 1, 0, 'source: Econome 2.0', 'Auto', 2);
			INSERT INTO measures (id, ouvrage, duree_effet, frais_exploitation, frais_entretien, frais_reparation, valeur_residuelle, remarques, mode, taux_interet) VALUES (12, 'Barrage de correction torrentielle en beton', 50, 0, 1, 1, 0, 'source: Econome 2.0', 'Auto', 2);
			INSERT INTO measures (id, ouvrage, duree_effet, frais_exploitation, frais_entretien, frais_reparation, valeur_residuelle, remarques, mode, taux_interet) VALUES (13, 'Filets de protection contre les laves torrentielles', 30, 1, 1.5, 1.5, 0, 'source: Econome 2.0', 'Auto', 2);
			INSERT INTO measures (id, ouvrage, duree_effet, frais_exploitation, frais_entretien, frais_reparation, valeur_residuelle, remarques, mode, taux_interet) VALUES (14, 'Rateliers en bois', 50, 2, 0.5, 0.5, 0, 'source: Econome 2.0', 'Auto', 2);
			INSERT INTO measures (id, ouvrage, duree_effet, frais_exploitation, frais_entretien, frais_reparation, valeur_residuelle, remarques, mode, taux_interet) VALUES (15, 'Barrage sur rivere de plaine et depotoir a alluvions en beton', 80, 1, 0.5, 0.5, 0, 'source: Econome 2.0', 'Auto', 2);
			INSERT INTO measures (id, ouvrage, duree_effet, frais_exploitation, frais_entretien, frais_reparation, valeur_residuelle, remarques, mode, taux_interet) VALUES (16, 'Maintient de la foret protectrice', 0, NULL, NULL, NULL, 0, 'source: Econome 2.0 et Valdorisk + Validation', 'Manuel', 0);
			INSERT INTO measures (id, ouvrage, duree_effet, frais_exploitation, frais_entretien, frais_reparation, valeur_residuelle, remarques, mode, taux_interet) VALUES (17, 'Mesures a objet', 1, NULL, NULL, NULL, 0, 'source: Econome 2.0 et Valdorisk + Validation (at least year should be 1 not to become infinity)', 'Manuel', 0);
			INSERT INTO measures (id, ouvrage, duree_effet, frais_exploitation, frais_entretien, frais_reparation, valeur_residuelle, remarques, mode, taux_interet) VALUES (18, 'Autre', 20, 0, 0, 0, 0, 'source: Econome 2.0', 'Auto', 2);
			";
	
		$rs = pg_query($dbconn,$query); 

		// if success, create a corresponding workspace/store on the GeoServer to link with postgis store
		if ($rs){
						
			// Initiate cURL session
			$service = "http://localhost:8080/geoserver/"; // replace with your URL
			$request = "rest/workspaces"; // to add a new workspace
			$url = $service . $request;
			$ch = curl_init($url);

			// Optional settings for debugging
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //option to return string
			curl_setopt($ch, CURLOPT_VERBOSE, true);
			curl_setopt($ch, CURLOPT_STDERR, $logfh); // logs curl messages

			//Required POST request settings
			curl_setopt($ch, CURLOPT_POST, True);
			$passwordStr = "username:mypassword"; // replace with your GeoServer username and password
			curl_setopt($ch, CURLOPT_USERPWD, $passwordStr);

			//POST data for a new workspace
			curl_setopt($ch, CURLOPT_HTTPHEADER,
			array("Content-type: application/xml"));
			$xmlStr = "<workspace><name>".$name."</name></workspace>";
			curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlStr);

			//POST return code
			$successCode = 201;

			// Execute the curl request
			$buffer = curl_exec($ch); 

			// Check for errors and process results
			$info = curl_getinfo($ch);
			if ($info['http_code'] != $successCode) {
				$msgStr = "# Unsuccessful cURL request to ";
				$msgStr .= $url." [". $info['http_code']. "]\n";
				Echo '{"success":false,msg:"Only the database schema has been created"}';						
			} 
			else {
				// if success, create a new postgis datastore under the created workspace
				// Initiate cURL session
				$service = "http://localhost:8080/geoserver/"; // replace with your URL
				$request = "rest/workspaces/".$name."/datastores"; // to add a new datastore under the specific workspace
				$url = $service . $request;
				$ch = curl_init($url);

				// Optional settings for debugging
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //option to return string
				curl_setopt($ch, CURLOPT_VERBOSE, true);
				curl_setopt($ch, CURLOPT_STDERR, $logfh); // logs curl messages

				//Required POST request settings
				curl_setopt($ch, CURLOPT_POST, True);
				$passwordStr = "username:mypassword"; // replace with your GeoServer username and password
				curl_setopt($ch, CURLOPT_USERPWD, $passwordStr);

				//POST data for a new postgis datastore
				curl_setopt($ch, CURLOPT_HTTPHEADER,
				array("Content-type: application/xml"));
				$xmlStr = "<dataStore><name>".$name."</name><connectionParameters><host>localhost</host><port>5432</port><database>geoserver</database><schema>".$name."</schema><user>admin</user><passwd>eekei2Am</passwd><dbtype>postgis</dbtype></connectionParameters></dataStore>";
				curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlStr);

				//POST return code
				$successCode = 201;

				// Execute the curl request
				$buffer = curl_exec($ch); 
							
				// Check for errors and process results
				$info = curl_getinfo($ch);
				if ($info['http_code'] != $successCode) {
					$msgStr = "# Unsuccessful cURL request to ";
					$msgStr .= $url." [". $info['http_code']. "]\n";
					Echo '{"success":false,msg:"Only the database schema and GeoServer workspace has been created!"}';
				} else {
					$msgStr = "# Successful cURL request to ".$url."\n";
					Echo '{"success":true,msg:"The schema, workspace and datastore has been created!"}';
				}								
			}
			
			curl_close($ch); // free resources if curl handle will not be reused					
		}
		else {
			Echo '{success:false,msg:'.json_encode(pg_last_error($dbconn)).'}';
		}
	}
	
	pg_close($dbconn);
?>           