/**
 * Copyright (c) 2008-2011 The Open Planning Project
 * 
 * Published under the GPL license.
 * See https://github.com/opengeo/gxp/raw/master/license.txt for the full text
 * of the license.
 */
 
 /**
 * @require widgets/form/RadioFieldSet.js
 */
 
/** api: (define)
 *  module = gxp
 *  class = LayerUploadPanelHazard
 *  base_link = `Ext.FormPanel <http://extjs.com/deploy/dev/docs/?class=Ext.FormPanel>`_
 */
Ext.namespace("gxp");

/** api: constructor
 *  .. class:: LayerUploadPanelHazard(config)
 *   
 *      A panel for uploading new layer data to GeoServer.
 */
gxp.LayerUploadPanelHazard = Ext.extend(Ext.FormPanel, {
    
    /** i18n */
    titleLabel: "Title",
    titleEmptyText: "Layer title",
    abstractLabel: "Description",
    abstractEmptyText: "Layer description (optional)",
	remarkLabel: "Remarks",
    remarkEmptyText: "Layer remarks (optional)",
    fileLabel: "Layer",
    fieldEmptyText: "Browse for intensity layer archive...",
	tableLabel: "Table",
    tableEmptyText: "Browse for scenario table...",
    uploadText: "Upload",
    waitMsgText: "Uploading your data...",
    invalidFileExtensionText: "File extension must be one of: ",   
	resetText: "Reset",
	hazTypeEmptyText: "Type of hazards",
	hazAltComboEmptyText: "Select the alternative",
    
    /** private: property[fileUpload]
     *  ``Boolean``
     */
    fileUpload: true,
    
    /** api: config[validFileExtensions]
     *  ``Array``
     *  List of valid file extensions.  These will be used in validating the 
     *  file input value.  Default is ``[".zip"]``.
     */
    validFileExtensions: [".zip"],  
	
	/** api: config[validFileTableExtensions]
     *  ``Array``
     *  List of valid file extensions.  These will be used in validating the 
     *  file input value.  Default is ``[".dbf"]``.
     */
    validFileTableExtensions: [".dbf"], 
    
    /** private: method[constructor]
     */
    constructor: function(config) {
        // Allow for a custom method to handle upload responses.
       /*  config.errorReader = {
            read: config.handleUploadResponse || this.handleUploadResponse.createDelegate(this)
        }; */
        gxp.LayerUploadPanelHazard.superclass.constructor.call(this, config);
    },   
        
    /** private: method[initComponent]
     */
    initComponent: function() {			
			
        this.items = [{
            xtype: "textfield",
            name: "title",
            fieldLabel: this.titleLabel,
            emptyText: this.titleEmptyText,
            allowBlank: false
        }, {
            xtype: "textarea",
            name: "abstract",
            fieldLabel: this.abstractLabel,
            emptyText: this.abstractEmptyText,
            allowBlank: true
        }, {
            xtype: "textarea",
            name: "remarks",
            fieldLabel: this.remarkLabel,
            emptyText: this.remarkEmptyText,
            allowBlank: true
        }, {
            xtype: "fileuploadfield",
            itemId: "file",
            anchor: "90%",
            emptyText: this.fieldEmptyText,
            fieldLabel: this.fileLabel,
            name: "file",
            buttonText: "",
            buttonCfg: {
                iconCls: "gxp-icon-filebrowse"
            },
            listeners: {
                "fileselected": function(cmp, value) {
                    // remove the path from the filename - avoids C:/fakepath etc.
                    cmp.setValue(value.split(/[/\\]/).pop());
                }
            },
            validator: this.fileNameValidator.createDelegate(this)
        }, {
            xtype: "fileuploadfield",
            itemId: "table",
            anchor: "90%",
            emptyText: this.tableEmptyText,
            fieldLabel: this.tableLabel,
            name: "table",
            buttonText: "",
            buttonCfg: {
                iconCls: "gxp-icon-filebrowse"
            },
            listeners: {
                "fileselected": function(cmp, value) {
                    // remove the path from the filename - avoids C:/fakepath etc.
                    cmp.setValue(value.split(/[/\\]/).pop());
                }
            },
            validator: this.tableNameValidator.createDelegate(this)
        },{
			xtype: "fieldset",
			title: "Properties of the uploaded hazard layer",
			autoHeight: true,
			collapsible: true,
			defaults: {
				anchor: '97%' // leave room for error icon
			},
			items: [{
				fieldLabel: 'Type',
				name: 'haz_type',
				hiddenName: 'haz_type',
				xtype: "combo",
				mode: "local",
				editable: false,
				store: new Ext.data.ArrayStore({
					id: 0,
					fields: ['myValue','displayText'],
					data: [['AVA','Avalanches'], ['LTO','Laves torrentielles'],['INO','Inondation'],['INOD','Inondation dynamique'], ['INOS','Inondation statique'],['CPB', 'Chutes de pierre et de blocs'],['GPP', 'Glissements de terrain permanents'],['GPP-TR', 'Glissements de terrain permanents (TR)'],['GSS', 'Glissements de terrain spontanés']]  // data is local
				}),
				valueField: 'myValue',
				displayField: 'displayText',
				emptyText: this.hazTypeEmptyText,
				triggerAction: 'all',
				allowBlank: false,
				listeners: {
					select: function(combo, record, index) {
						if (combo.getValue() == 'GPP-TR' || combo.getValue() == 'GSS') {
							this.items.items[4].setVisible(false);
							this.items.items[4].disable();
						}
						else {
							this.items.items[4].setVisible(true);
							this.items.items[4].enable();
						}
					},
					scope: this
				}
			},{
				fieldLabel: 'Alternative',	
				emptyText: this.hazAltComboEmptyText,
				name: 'hazAltName',	
				hiddenName: 'hazAltID',
				allowBlank: false,
				editable: false,
				xtype: 'combo',					
				mode:'local',
				triggerAction: 'all',
				displayField: 'nom',
				valueField: 'id',
				store: new Ext.data.JsonStore({ 
					url: 'altInfo.php',	
					autoLoad: true,
					root: 'rows',
					fields : ['nom', 'id'],
					baseParams: {
						ws: workspace,
						task: 'load'
					}
				})							
			}]
		}];				
		
		       
        this.buttons = [{
            text: this.uploadText,
			icon:  'src/gxp/theme/img/silk/folder.png',
            handler: function() {
                var form = this.getForm();				
                if (form.isValid()) {
                    var fields = form.getFieldValues();					
                  	form.submit({
						url: 'hazInfo.php',
						waitMsg: this.waitMsgText,
                        waitMsgTarget: true,
						timeout: 900, // <<-- Timeout value specified here in seconds
						params: {
							task: 'add',
							ws: workspace,
							shpfilename: fields.file,
							tabfilename: fields.table,
							username: usrname
						},
						success: function(form, action) {
							if (action.result.success) {
								Ext.Msg.alert('Success', action.result.message); // show the success message	
								form.items.itemAt(4).setVisible(true);
								form.items.itemAt(4).enable();
								form.reset(); 
								Ext.getCmp('hazGrid').getStore().reload({
									callback: function(){
										Ext.getCmp('hazGrid').getView().refresh();
									}
								});	
							}
							else Ext.Msg.alert('Failed', action.result.message);																			
						},
						// If you don't pass success:true, it will always go here
						failure: function(form, action) {
							Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
						},
						scope: this
                    });			
                }
            },
            scope: this
        },{
			text: this.resetText,
			icon:  'src/gxp/theme/img/silk/arrow_refresh.png', 
			handler: function() {
				var form = this.getForm();				
				form.reset();
				form.items.itemAt(4).setVisible(true);
				form.items.itemAt(4).enable();
			},
            scope: this
		}];
		
        gxp.LayerUploadPanelHazard.superclass.initComponent.call(this);

    },
	
	/** private: method[fileNameValidator]
     *  :arg name: ``String`` The chosen filename.
     *  :returns: ``Boolean | String``  True if valid, message otherwise.
     */
    fileNameValidator: function(name) {
        var valid = false;
        var ext, len = name.length;
        for (var i=0, ii=this.validFileExtensions.length; i<ii; ++i) {
            ext = this.validFileExtensions[i];
            if (name.slice(-ext.length).toLowerCase() === ext) {
                valid = true;
                break;
            }
        }
        return valid || this.invalidFileExtensionText + '<br/>' + this.validFileExtensions.join(", ");
    },
	
	/** private: method[tableNameValidator]
     *  :arg name: ``String`` The chosen file tablename.
     *  :returns: ``Boolean | String``  True if valid, message otherwise.
     */
    tableNameValidator: function(name) {
        var valid = false;
        var ext, len = name.length;
        for (var i=0, ii=this.validFileTableExtensions.length; i<ii; ++i) {
            ext = this.validFileTableExtensions[i];
            if (name.slice(-ext.length).toLowerCase() === ext) {
                valid = true;
                break;
            }
        }
        return valid || this.invalidFileExtensionText + '<br/>' + this.validFileTableExtensions.join(", ");
    }

});

/** api: xtype = gxp_LayerUploadPanelHazard */
Ext.reg("gxp_LayerUploadPanelHazard", gxp.LayerUploadPanelHazard);
