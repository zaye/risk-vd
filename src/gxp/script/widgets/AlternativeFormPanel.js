/**
 * Copyright (c) 2008-2011 The Open Planning Project
 * 
 * Published under the GPL license.
 * See https://github.com/opengeo/gxp/raw/master/license.txt for the full text
 * of the license.
 */
 
 /**
 * @require widgets/form/RadioFieldSet.js
 */
 
/** api: (define)
 *  module = gxp
 *  class = AlternativeFormPanel
 *  base_link = `Ext.FormPanel <http://extjs.com/deploy/dev/docs/?class=Ext.FormPanel>`_
 */
Ext.namespace("gxp");

/** api: constructor
 *  .. class:: AlternativeFormPanel(config)
 *   
 *      A panel for uploading new alternative layer data to GeoServer.
 */
gxp.AlternativeFormPanel = Ext.extend(Ext.FormPanel, {
    
    /** i18n */
    titleLabel: "Name",
    titleEmptyText: "Name of the scenario",
    abstractLabel: "Description",
    abstractEmptyText: "Description of the scenario",
    fileLabel: "Data",
    fieldEmptyText: "Browse for data archive...",
    uploadText: "Add",
	uploadFailedText: "Upload failed",
    processingUploadText: "Processing upload...",
    waitMsgText: "Uploading your data...",
    invalidFileExtensionText: "File extension must be one of: ",
    optionsText: "Upload Options",
    workspaceLabel: "Workspace",
    workspaceEmptyText: "Default workspace",
    dataStoreLabel: "Store",
    dataStoreEmptyText: "Choose a store",
//  dataStoreNewText: "Create new store",
    crsLabel: "CRS",
    crsEmptyText: "Coordinate Reference System ID",
    invalidCrsText: "CRS identifier should be an EPSG code (e.g. EPSG:4326)",
	resetText: "Reset",	
    
    /** private: property[fileUpload]
     *  ``Boolean``
     */
    fileUpload: true,
    
    /** api: config[validFileExtensions]
     *  ``Array``
     *  List of valid file extensions.  These will be used in validating the 
     *  file input value.  Default is ``[".zip", ".tif", ".tiff", ".gz", ".tar.bz2", 
     *  ".tar", ".tgz", ".tbz2"]``.
     */
    validFileExtensions: [".zip", ".gz", ".tar.bz2", ".tar", ".tgz", ".tbz2"],
    
    /** api: config[url]
     *  ``String``
     *  URL for GeoServer RESTConfig root.  E.g. "http://example.com/geoserver/rest".
     */
    
    /** private: property[defaultDataStore]
     *  ``string``
     */
    defaultDataStore: null,
    
    /** private: method[constructor]
     */
    constructor: function(config) {
        // Allow for a custom method to handle upload responses.
        config.errorReader = {
            read: config.handleUploadResponse || this.handleUploadResponse.createDelegate(this)
        };
        gxp.AlternativeFormPanel.superclass.constructor.call(this, config);
    },
    
    /** private: property[selectedWorkspace]
     *  {Ext.data.Record}
     */
    selectedWorkspace: null,
    
    /** private: method[initComponent]
     */
    initComponent: function() {
			
        this.items = [{
			xtype:'fieldset',
			title: 'Scenario Information',
			collapsible: true,
			defaultType: 'textfield',
			defaults: {
				anchor: "95%",					
				msgTarget: "side"
			},
			collapsed: false,
			items :[{
				xtype: "textfield",
				name: "title",
				fieldLabel: this.titleLabel,
				emptyText: this.titleEmptyText,
				allowBlank: false
			},{
				xtype: "textarea",
				name: "abstract",
				fieldLabel: this.abstractLabel,
				emptyText: this.abstractEmptyText,
				allowBlank: true
			},{
				fieldLabel: 'Option',
				name: 'option',
				xtype: 'combo',
				ref: "../option",
				mode: "local",
				emptyText: 'Select the option',
				editable: false,
				store: new Ext.data.ArrayStore({
					id: 0,
					fields: [
						'myId',  // numeric value is the key
						'displayText'
					],
					data: [['sketch', 'Sketch (mapping)'], ['descriptive', 'Non-sketch (descriptive)'], ['upload', 'Upload']]  // data is local
				}),
				valueField: 'myId',
				displayField: 'displayText',
				triggerAction: 'all',
				listeners: {
					select: function(combo, record, index) {
						if (combo.getValue() == 'sketch') {
							this.sketch.setVisible(true); // this.sketch.enable();
							this.optionsFieldset.setVisible(false); // this.optionsFieldset.disable();  
							this.sketch.items.items[0].items.items[0].setValue(app.mapPanel.map.getExtent().transform(new OpenLayers.Projection("EPSG:900913"), new OpenLayers.Projection("EPSG:4326")).left);
							this.sketch.items.items[0].items.items[1].setValue(app.mapPanel.map.getExtent().transform(new OpenLayers.Projection("EPSG:900913"), new OpenLayers.Projection("EPSG:4326")).bottom);
							this.sketch.items.items[1].items.items[0].setValue(app.mapPanel.map.getExtent().transform(new OpenLayers.Projection("EPSG:900913"), new OpenLayers.Projection("EPSG:4326")).right);
							this.sketch.items.items[1].items.items[1].setValue(app.mapPanel.map.getExtent().transform(new OpenLayers.Projection("EPSG:900913"), new OpenLayers.Projection("EPSG:4326")).top);
							
						}
						else if (combo.getValue() == 'upload') {
							this.sketch.setVisible(false); // this.sketch.disable();
							this.optionsFieldset.setVisible(true); // this.optionsFieldset.enable();
						}
						else {
							this.sketch.setVisible(false);
							this.optionsFieldset.setVisible(false);
						}			
					},
					scope: this
				}
			},{
				fieldLabel: 'Measures',
				name: 'measures',
				hiddenName: 'measures',
				xtype: 'lovcombo',
				mode: "local",
				emptyText: 'Select the measures',
				editable: false,
				allowBlank: false,
				hideOnSelect:false,
				store: new Ext.data.ArrayStore({
					id: 0,
					fields: [
						'myId',  // numeric value is the key
						'displayText'
					],
					data: [[1, 'Galerie'], [2, 'Tunnel'],[3,'Galerie de protection contre les crues'],[4,'Ouvrage de stabilisation de la neige'],
					[5,'Installation de minage'],[6,'Filet de protection contre les chutes de pierres'],[7,'Digue en terre'],[8,'Ouvrage temporaire/ouvrage en bois'],
					[9,'Ouvrage de stabilisation des pentes'],[10,'Reboisements'],[11,'Barrage de correction torrentielle en bois'],[12,'Barrage de correction torrentielle en beton'],
					[13,'Filets de protection contre les laves torrentielles'],[14,'Rateliers en bois'],[15,'Barrage sur rivere de plaine et depotoir a alluvions en beton'],
					[16,'Maintient de la foret protectrice'],[17,"Mesures a l'objet"],[18,'Autre']]  // data is local
				}),
				valueField: 'myId',
				displayField: 'displayText',
				triggerAction: 'all'
			},{
				fieldLabel: 'Remarks',
				xtype: 'textarea',
				emptyText: 'If any other remarks (optional)',
				name: 'remarks',
				allowBlank: true
			}]
		}, {
			xtype:'fieldset',
			ref: 'sketch',
			title: 'Preliminary Sketch option',
			labelWidth: 120,
			hidden: this.option != 'sketch',
			defaults: {
				anchor: "95%",
				msgTarget: "side"
			},
			collapsed: false,
			items:[{
				fieldLabel: 'BBox(minx,miny)',					
				xtype : 'compositefield',					
				items: [{
					xtype: 'numberfield',
					name: 'LatLon_minx',
					flex: 1,
					emptyText: 'LatLon value for minx'
				},{
					xtype: 'numberfield',
					name: 'LatLon_miny',
					flex: 1,
					emptyText: 'LatLon value for miny'
				}]
			},{
				fieldLabel: 'BBox(maxx,maxy)',
				xtype : 'compositefield',
				items: [{
					xtype: 'numberfield',
					name: 'LatLon_maxx',
					flex: 1,
					emptyText: 'LatLon value for maxx'
				},{
					xtype: 'numberfield',
					name: 'LatLon_maxy',
					flex: 1,
					emptyText: 'LatLon value for maxy'
				}]					
			}],
			listeners: {
				hide: function(p) {
					p.cascade(function () {
						this.disable();
					});
				},
				show: function(p) {
					p.cascade(function () {
						this.enable();
					});
				}
			}
		}, {
            xtype: "fieldset",
            ref: "optionsFieldset",
            title: this.optionsText,
            collapsed: false,
            hidden: this.option != 'upload',
        //  hideMode: "offsets",
            defaults: {
                anchor: "95%"
            },
            items: [
                this.createWorkspacesCombo(),
                this.createDataStoresCombo(),
                {
                    xtype: "textfield",
                    name: "nativeCRS",
                    // anchor: "90%",
                    fieldLabel: this.crsLabel,
                    emptyText: this.crsEmptyText,
                    allowBlank: true,
                    regex: /^epsg:\d+$/i,
                    regexText: this.invalidCrsText
                },
				{
					xtype: "fileuploadfield",
					itemId: "file",
					anchor: "90%",
					emptyText: this.fieldEmptyText,
					fieldLabel: this.fileLabel,
					name: "file",
					buttonText: "",
					buttonCfg: {
						iconCls: "gxp-icon-filebrowse"
					},
					listeners: {
						"fileselected": function(cmp, value) {
							// remove the path from the filename - avoids C:/fakepath etc.
							cmp.setValue(value.split(/[/\\]/).pop());
						}
					},
					validator: this.fileNameValidator.createDelegate(this)
				}
            ],
            listeners: {
                collapse: function(fieldset) {
                    // reset all combos
                    fieldset.items.each(function(item) {
                        item.reset();
                    });
                },
				hide: function(p) {
					p.cascade(function () {
						this.disable();
					});
				},
				show: function(p) {
					p.cascade(function () {
						this.enable();
					});
				}
            }
        }];		
		       
        this.buttons = [{
            text: this.uploadText,
			iconCls: 'add',
            handler: function() {
                var form = this.getForm();		
				var fields = form.getFieldValues();
                if (form.isValid()) {
					if (fields.option != 'sketch' && fields.option != 'descriptive'){
						var	jsonData = {'import': {}};
						if (fields.workspace) {
							jsonData["import"].targetWorkspace = {workspace: {name: fields.workspace}};
						}
						if (fields.store) {
							jsonData["import"].targetStore = {dataStore: {name: fields.store}}
						} else if (this.defaultDataStore) {
							jsonData["import"].targetStore = {dataStore: {name: this.defaultDataStore}}                        
						}
					/* 	if (Ext.isEmpty(fields.store) && this.defaultDataStore) {
							jsonData["import"].targetStore = {dataStore: {name: this.defaultDataStore}};
						} else if (!Ext.isEmpty(fields.store) && fields.store !== this.dataStoreNewText) {
							jsonData["import"].targetStore = {dataStore: {name: fields.store}};                        
						} */
						Ext.Ajax.request({
							url: this.getUploadUrl(),
							method: "POST",
							jsonData: jsonData,
							success: function(response) {
								this._import = response.getResponseHeader("Location");
								this.optionsFieldset.expand();
								form.submit({
									url: this._import + "/tasks?expand=all",
									waitMsg: this.waitMsgText,
									waitMsgTarget: true,
								//  reset: true,
									scope: this
								});								
							},
							scope: this
						});	
					}
					else {
						Ext.Ajax.request({
							url:'altInfo.php',
							params: {
								task: 'add',
								ws: workspace,
								name: fields.title,
								description: fields["abstract"],								
								option: fields.option,
								measures: fields.measures,
								remarks: fields.remarks,								
								LatLon_minx: this.sketch.items.items[0].items.items[0].getValue(),
								LatLon_miny: this.sketch.items.items[0].items.items[1].getValue(),
								LatLon_maxx: this.sketch.items.items[1].items.items[0].getValue(),
								LatLon_maxy: this.sketch.items.items[1].items.items[1].getValue()
							},
							success: function(response, opts) {
									form.reset();									
									this.optionsFieldset.setVisible(false);  
									this.sketch.setVisible(false); 
									
									var obj = Ext.decode(response.responseText);
									Ext.Msg.alert('Information', obj.message);
									Ext.getCmp('altInfoGrid').getStore().reload({ //refresh the grid with newly added layer
										callback: function(){
											Ext.getCmp('altInfoGrid').getView().refresh();
										}
									});
									// add the layer to the map	
									var lyr_name = obj.mpIndex;
									var names = {};
									names[workspace + ":" + lyr_name] = true; 
									var source = app.tools.addlyrs.target.layerSources["local"]; // need to reassign the source in case other sources such as google are being added by users
									app.tools.addlyrs.setSelectedSource(source);
									app.tools.addlyrs.selectedSource.store.load({
										callback: function(records, options, success) {
											var gridPanel, sel;
												if (app.tools.addlyrs.capGrid && app.tools.addlyrs.capGrid.isVisible()) {
													gridPanel = app.tools.addlyrs.capGrid.get(0).get(0);
													sel = gridPanel.getSelectionModel();
													sel.clearSelections();
												}
												// select newly added layers
												var newRecords = [];
												var last = 0;
												app.tools.addlyrs.selectedSource.store.each(function(record, index) {
													if (record.get("name") in names) {
														last = index;
														newRecords.push(record);
													}
												});
												if (gridPanel) {
													// this needs to be deferred because the 
													// grid view has not refreshed yet
													window.setTimeout(function() {
														sel.selectRecords(newRecords);
														gridPanel.getView().focusRow(last);
													}, 100);
												} else {
													app.tools.addlyrs.addLayers(newRecords, true);
												}                                                                                    
										},
										scope: this                                            
									});
								},
							failure: function(response, opts) {
									console.log('server-side failure with status code ' + response.status);
							},
							scope: this
						});	
 					}
                }
            },
            scope: this
        },{
			text: this.resetText,
			icon:  'src/gxp/theme/img/silk/arrow_refresh.png', 
			handler: function() {
				var form = this.getForm();				
				form.reset();				
				this.optionsFieldset.setVisible(false);  
				this.sketch.setVisible(false); 
			},
            scope: this
		}];
        
        this.addEvents(
            /**
             * Event: workspaceselected
             * Fires when a workspace is selected.
             *
             * Listener arguments:
             * panel - {<gxp.AlternativeFormPanel} This form panel.
             * record - {Ext.data.Record} The selected workspace record.
             */
            "workspaceselected",

            /**
             * Event: datastoreselected
             * Fires when a datastore is selected.
             *
             * Listener arguments:
             * panel - {<gxp.AlternativeFormPanel} This form panel.
             * record - {Ext.data.Record} The selected datastore record.
             */
            "datastoreselected",

            /**
             * Event: uploadcomplete
             * Fires upon successful upload.
             *
             * Listener arguments:
             * panel - {<gxp.AlternativeFormPanel} This form panel.
             * details - {Object} An object with an "import" property,
             *     representing a summary of the import result as provided by
             *     GeoServer's Importer API.
             */
            "uploadcomplete"
        );
        
        this.getDefaultDataStore('default');

        gxp.AlternativeFormPanel.superclass.initComponent.call(this);

    },
    
    /** private: method[fileNameValidator]
     *  :arg name: ``String`` The chosen filename.
     *  :returns: ``Boolean | String``  True if valid, message otherwise.
     */
    fileNameValidator: function(name) {
        var valid = false;
        var ext, len = name.length;
        for (var i=0, ii=this.validFileExtensions.length; i<ii; ++i) {
            ext = this.validFileExtensions[i];
            if (name.slice(-ext.length).toLowerCase() === ext) {
                valid = true;
                break;
            }
        }
        return valid || this.invalidFileExtensionText + '<br/>' + this.validFileExtensions.join(", ");
    },
	

    /** private: method[createWorkspacesCombo]
     *  :returns: ``Object`` Combo config.
     */
    createWorkspacesCombo: function() {
        return {
            xtype: "combo",
            name: "workspace",
            ref: "../workspace",
            fieldLabel: this.workspaceLabel,
            emptyText: this.workspaceEmptyText,
            store: new Ext.data.JsonStore({
                url: this.getWorkspacesUrl(),  
                autoLoad: true,
                root: "workspaces.workspace",
                fields: ["name", "href"],
				listeners: {
					load: function(store, records, options) // restrict workspace values to the currently selected workspace
					{ 
						var temp = [];
						store.each(function(record){
							if (record.data.name != workspace) temp.push(record);
						});
						store.remove(temp);						
						this.workspace.setValue(workspace);
					},
					scope: this
				}
            }),
            displayField: "name",
            valueField: "name",
            mode: "local",
            allowBlank: true,
            triggerAction: "all",
			forceSelection: true,
			lastQuery: '', //so that the first time dropdown will filter!!!!
            editable: false,
            listeners: {
                select: function(combo, record, index) {
                    this.getDefaultDataStore(record.get('name'));
                    this.fireEvent("workspaceselected", this, record);
                },
                scope: this
            }
        };
    },
    
    /** private: method[createDataStoresCombo]
     *  :returns: ``Ext.form.ComboBox``
     */
    createDataStoresCombo: function() {
        // this store will be loaded whenever a workspace is selected
        var store = new Ext.data.JsonStore({
            autoLoad: false,
            root: "dataStores.dataStore",
            fields: ["name", "href"]
        });
        this.on({
            workspaceselected: function(panel, record) {
                combo.reset();
                var workspaceUrl = record.get("href");
                store.removeAll();
                store.proxy = new Ext.data.HttpProxy({
                    url: workspaceUrl.split(".json").shift() + "/datastores.json"
                });
			//	store.proxy.on('loadexception', addDefault, this);
                store.load();
            },
            scope: this
        });
		
		/* 	var addDefault = function() {
            var defaultData = {
                name: this.dataStoreNewText
            };
            var r = new store.recordType(defaultData);
            store.insert(0, r);
            store.proxy && store.proxy.un('loadexception', addDefault, this);
        };

        store.on('load', addDefault, this); */

        var combo = new Ext.form.ComboBox({
            name: "store",
            ref: "../dataStore",
            fieldLabel: this.dataStoreLabel,
            emptyText: this.dataStoreEmptyText,
            store: store,
            displayField: "name",
            valueField: "name",
            mode: "local",
            allowBlank: true,
            triggerAction: "all",
			forceSelection: true,
            editable: false,
            listeners: {
                select: function(combo, record, index) {
                    this.fireEvent("datastoreselected", this, record);
                },
                scope: this
            }
        });
        
        return combo;
    },

    getDefaultDataStore: function(workspace) {
        Ext.Ajax.request({
            url: this.url + '/workspaces/' + workspace + '/datastores/default.json',
            callback: function(options, success, response) {
                this.defaultDataStore = null;
                if (response.status === 200) {
                    var json = Ext.decode(response.responseText);
					if (workspace === 'default' && json.dataStore && json.dataStore.workspace) {
                        this.workspace.setValue(json.dataStore.workspace.name);
                        var store = this.workspace.store;
                        var data = {
                            name: json.dataStore.workspace.name,
                            href: json.dataStore.workspace.href
                        };
                        var r = new store.recordType(data);
                        this.fireEvent("workspaceselected", this, r);
                    }
                    //TODO Revisit this logic - currently we assume that stores
                    // with the substring "file" in the type are file based,
                    // and for file-based data stores we want to crate a new
                    // store.
                    if (json.dataStore && json.dataStore.enabled === true && !/file/i.test(json.dataStore.type)) {
                        this.defaultDataStore = json.dataStore.name;                        
                        this.dataStore.setValue(this.defaultDataStore);
                    }
                }
            },
            scope: this
        });
    },

    /** private: method[getUploadUrl]
     */
    getUploadUrl: function() {
        return this.url + "/imports";
    },
    
    /** private: method[getWorkspacesUrl]
     */
    getWorkspacesUrl: function() {
        return this.url + "/workspaces.json";
    },
    
    /** private: method[handleUploadResponse]
     *  TODO: if response includes errors object, this can be removed
     *  Though it should only be removed if the server always returns text/html!
     */
    handleUploadResponse: function(response) {
        var obj = this.parseResponseText(response.responseText),
            records, tasks, task, msg, i,
			formData = this.getForm().getFieldValues(),
            success = !!obj;
        if (obj) {
            if (typeof obj === "string") {
                success = false;
                msg = obj;
            } else {
                tasks = obj.tasks || [obj.task];
                if (tasks.length === 0) {
                    success = false;
                    msg = "Upload contains no suitable files.";
                } else {
                    for (i=tasks.length-1; i>=0; --i) {
                        task = tasks[i];
                        if (!task) {
                            success = false;
                            msg = "Unknown upload error";
                        } else if (task.state === 'NO_FORMAT') {
                            success = false;
                            msg = "Upload contains no suitable files.";
                        } else if (task.state === 'NO_CRS' && !formData.nativeCRS) {
                            success = false;
                            msg = "Coordinate Reference System (CRS) of source file " + task.data.file + " could not be determined. Please specify manually.";
                        }
                    }
                }
            }
        }
        if (!success) {
            // mark the file field as invlid
            records = [{data: {id: "file", msg: msg || this.uploadFailedText}}];
        } else {
            var itemModified = !!(formData.title || formData["abstract"] || formData.nativeCRS);
            // do not do this for coverages see https://github.com/boundlessgeo/suite/issues/184
            if (itemModified && tasks[0].target.dataStore) {
                this.waitMsg = new Ext.LoadMask((this.ownerCt || this).getEl(), {msg: this.processingUploadText});
                this.waitMsg.show();
                // for now we only support a single task
                var payload = {
                    title: formData.title || undefined,
                    "abstract": formData["abstract"] || undefined,
                    srs: formData.nativeCRS || undefined
                };
                Ext.Ajax.request({
                    method: "PUT",
                    url: tasks[0].layer.href,
                    jsonData: payload,
                    success: this.finishUpload,
                    failure: function(response) {
                        if (this.waitMsg) {
                            this.waitMsg.hide();
                        }
                        var errors = [];
                        try {
                            var json = Ext.decode(response.responseText);
                            if (json.errors) {
                                for (var i=0, ii=json.errors.length; i<ii; ++i) {
                                    errors.push({
                                        id: ~json.errors[i].indexOf('SRS') ? 'nativeCRS' : 'file',
                                        msg: json.errors[i]
                                    });
                                }
                            }
                        } catch(e) {
                            errors.push({
                                id: "file",
                                msg: response.responseText
                            });
                        }
                        this.getForm().markInvalid(errors);
                    },
                    scope: this
                });
            } else {
                this.finishUpload();
            }
        }
        // always return unsuccessful - we manually reset the form in callbacks
        return {success: false, records: records};
    },
    
    finishUpload: function() {
        Ext.Ajax.request({
            method: "POST",
            url: this._import,
            failure: this.handleFailure,
            success: this.handleUploadSuccess,
            scope: this
        });
    },
    
    /** private: parseResponseText
     *  :arg text: ``String``
     *  :returns:  ``Object``
     *
     *  Parse the response text.  Assuming a JSON string but allowing for a 
     *  string wrapped in a <pre> element (given non text/html response type).
     */
    parseResponseText: function(text) {
        var obj;
        try {
            obj = Ext.decode(text);
        } catch (err) {
            // if response type was text/plain, the text will be wrapped in a <pre>
            var match = text.match(/^\s*<pre[^>]*>(.*)<\/pre>\s*/);
            if (match) {
                try {
                    obj = Ext.decode(match[1]);
                } catch (err) {
                    obj = match[1];
                }
            }
        }
        return obj;
    },
    
    /** private: method[handleUploadSuccess]
     */
    handleUploadSuccess: function(response) {
        Ext.Ajax.request({
            method: "GET",
            url: this._import + '?expand=all',
            failure: this.handleFailure,
            success: function(response) {
                if (this.waitMsg) {
                    this.waitMsg.hide();
                }
            //  this.getForm().reset();
                var details = Ext.decode(response.responseText);
				this.fireEvent("uploadcomplete", this, details);
                delete this._import;
                // wait 250 ms for GeoServer to settle after the upload
                //window.setTimeout(function() {
                //  this.fireEvent("uploadcomplete", this, details);
                //  delete this._import;
                //}.bind(this), 250);
            },
            scope: this
        });
    },
    
    /** private: method[handleFailure]
     */
    handleFailure: function(response) {
        // see http://stackoverflow.com/questions/10046972/msie-returns-status-code-of-1223-for-ajax-request
        if (response && response.status === 1223) {
            this.handleUploadSuccess(response);
        } else {
            if (this.waitMsg) {
                this.waitMsg.hide();
            }
            this.getForm().markInvalid([{file: this.uploadFailedText}]);
        }
    }

});

/** api: xtype = gxp_AlternativeFormPanel */
Ext.reg("gxp_AlternativeFormPanel", gxp.AlternativeFormPanel);
