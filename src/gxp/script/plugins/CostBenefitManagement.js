/**
 * @require plugins/Tool.js
 */
 
Ext.ns("gxp.plugins");

gxp.plugins.CostBenefitManagement = Ext.extend(gxp.plugins.Tool, {

  ptype: "myapp_costbenefit",
  
  /** private: method[constructor]
     */
    constructor: function() {
        this.initCostBenMgtWin();
        gxp.plugins.CostBenefitManagement.superclass.constructor.apply(this, arguments);        
    },
	/** api: method[showCostBenMgt]
     * Shows the respective window for the selected tree node.
     */ 
    showCostBenMgt: function() {
		this.CostBenInfo_store.reload({ 
			params: 
				{ws: workspace,
				task: 'load'} // reload the grid with the selected workspace 
		});
		this.costBenInfoWin.show();
	},
	/** api: private method[initCostBenMgtWin]
     * Initialize the window for the cost-benefit grid panel. 
     */
	initCostBenMgtWin: function() {
		this.costBenInfoWin = new Ext.Window({
			title: 'Cost benefit report information',
			layout: 'fit',		
			plain:false,
			maximizable: true,
			collapsible: true,
			closeable: true,
			closeAction:'hide',
			buttonAlign:'right',
			items: this.initCostBenInfoGrid()
		});
	},
	
	/** api: private method[initCostBenInfoGrid]
     * initialization of the grid panel to manage the cost benefit reports 
     */
	 
	initCostBenInfoGrid: function(){		
		
		var xg = Ext.grid;		
		var costBenAddStore = new Ext.data.GroupingStore({
				url: 'costBenInfo.php',
				baseParams: {
					task: 'loadGrid',
					ws: workspace
				},
				sortInfo:{field: 'id', direction: "ASC"},
				groupField:'groupe',
				autoLoad: true,
				reader: new Ext.data.JsonReader({
					totalProperty : 'totalCount',
					root          : 'rows',
					successProperty: 'success',
					idProperty    : 'id',
					fields: [					
						{name : 'id', type : 'int'},
						{name : 'cout_id', type : 'int'},
						{name : 'alt_id', type : 'int'},
						{name : 'nom', type : 'String'},
						{name : 'cout_nom', type : 'String'},
						{name : 'alt_nom', type : 'String'},
						{name : 'description', type : 'String'}, 
						{name : 'processus_type', type : 'String'},
						{name : 'objet_type', type : 'String'},						
						{name : 'indice', type : 'String'},
						{name : 'risque_total', type : 'float'},
						{name : 'cout_total', type : 'float'},
						{name : 'groupe', convert : function (v, rec){
							return rec.alt_nom + ', ' + rec.processus_type + ', ' + rec.objet_type;
							} 
						}						
					] 
				})
		});	
		
		var customRender = Ext.util.Format.frMoney = function(v) 	// plug Swiss Franc currency renderer into formatter
			{
				v = Ext.util.Format.number(v, '0,000.00');																				 
				return (v + ' CHF');
			};
			
		var costBenAddGrid = new Ext.grid.GridPanel({
			store: costBenAddStore,
			colModel: new Ext.grid.ColumnModel({
				defaults: {	
					sortable: true,
					menuDisabled: true
				},
				columns: [
					//	new Ext.grid.RowNumberer(),
					{header: "Group", dataIndex: 'groupe', hidden: true},
					{header: "ID", dataIndex: 'id', hidden: true},
					{header: "Cost_ID", dataIndex: 'cout_id', hidden: true},
					{header: "Alt_ID", dataIndex: 'alt_id', hidden: true},
					{header: "Risk Name", dataIndex: 'nom'},
					{header: "Cost Name", dataIndex: 'cout_nom'},
					{header: "Alternative Name", dataIndex: 'alt_nom', hidden: true},
					{header: "Description", dataIndex: 'description', hidden: true},					
					{header: "Hazard Type", dataIndex: 'processus_type'},	
					{header: "Object Type", dataIndex: 'objet_type'},
					{header: "Mapping Index", dataIndex: 'indice', hidden: true},
					{header: "Risk Total (per year)", dataIndex: 'risque_total', align: 'right', renderer: customRender},
					{header: "Cost Total (per year)", dataIndex: 'cout_total', align: 'right', renderer: customRender}
				]
			}),	
			view: new Ext.grid.GroupingView({				
				forceFit: true,
				groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
			}),						
			frame:true,
			stripeRows: true,
			loadMask: true,			
			height: 300,
			buttons: [{
				text: 'Generate New',
				iconCls: 'add',
				handler: function(){
					// check whether the risk scnearios are selected
					var records = costBenAddGrid.getSelectionModel().getSelections();
					if ( records.length < 2) {
						Ext.MessageBox.show({
							title: 'Help Support!',
							msg: 'Please select at least two risk scenarios for comparison of risk (before/after) to generate a new cost-benefit report!',
							buttons: Ext.MessageBox.OK,
							animEl: 'mb9',
							icon: Ext.MessageBox.INFO
						});
					}
					else {
						// show the costBenAddForm to enter the data
						var costBenAddForm = new Ext.form.FormPanel({
							labelWidth: 75, // label settings here cascade unless overridden
							url:'costBenInfo.php',
							frame:true,			
							bodyStyle:'padding:5px 5px 0',
							width: 450,	
							autoHeight:true,		
							items: [{
								xtype:'fieldset',
								title: 'Report Information',
								collapsible: false,
								defaultType: 'textfield',
								defaults: {
									anchor: "90%",					
									msgTarget: "side"
								},
								collapsed: false,
								items :[{
									fieldLabel: 'Name',
									name: 'name_report',
									emptyText: 'Name of the report..',
									allowBlank: false
								},{
									fieldLabel: 'Description',
									xtype: 'textarea',
									name: 'description_report',
									emptyText: 'Description of the report..',
									allowBlank: true
								},{
									fieldLabel: 'Remarks',
									xtype: 'textarea',
									name: 'remarks_report',
									emptyText: 'If any other remarks (optional)..',
									allowBlank: true
								}]	
							}],
							buttons: [{
								text: 'Add',
								iconCls:  'add',
								handler: function(){
									var form = costBenAddForm.getForm();
									var recs = [];
									for (var i=0;i<records.length;i++) {
										recs.push(records[i].data);								
									}
									
									if (form.isValid()) {						
										form.submit({
											url: 'costBenInfo.php',
											params: {
												task: 'add',
												ws: workspace,
												records: Ext.encode(recs)
											},
											waitMsg : 'Please wait...',
											success: function(form, action) {
												if (action.result.success) {
													Ext.Msg.alert('Success', action.result.message); // show the success message	
													form.reset(); // reset the form													
													Ext.getCmp('costBenInfoGrid').getStore().reload({ // refresh the grid view
														callback: function(){
															Ext.getCmp('costBenInfoGrid').getView().refresh();
														}
													});
												}
												else Ext.Msg.alert('Failed', action.result.message);
											},
											// If you don't pass success:true, it will always go here
											failure: function(form, action) {
												Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
											}
										});	
									}
									else {
										Ext.Msg.alert('Validation Error', 'The form is not valid!' );
									}
								}
							},{
								text: 'Reset',
								icon:  'src/gxp/theme/img/silk/arrow_refresh.png', 
								handler: function(){
									// reset the form 
									costBenAddForm.getForm().reset(); 
								}
							}]		
						});

						new Ext.Window({
							title: 'Generate a new cost-benefit report ...',
							layout: 'form',
							autoHeight: true,
							plain:false,
							modal: true,
							resizable: true,
							closeable: true,
							closeAction:'hide',
							buttonAlign:'right',
							items: [costBenAddForm]
						}).show();
					}	
				}
			}]						 
		});		
		
		var costBenAddWin = new Ext.Window({
			title: 'Select to generate a new cost-benefit report',
			layout: 'fit',
			width: 600,
			height: 400,
			plain:false,
			closeable: true,
			closeAction:'hide',
			maximizable: true,
			collapsible: true,
			buttonAlign:'right',
			items: [costBenAddGrid]
		});
		
		var xg = Ext.grid;		
		this.CostBenInfo_store = new Ext.data.Store({
			url: 'costBenInfo.php',
			sortInfo:{field: 'id', direction: "ASC"},
			reader: new Ext.data.JsonReader({
				totalProperty : 'totalCount',
				root          : 'rows',
				successProperty: 'success',
				idProperty    : 'id',
				fields: [					
					{name : 'id', type : 'int'},
					{name : 'nom', type : 'String'},
					{name : 'description', type : 'String'}, 
					{name : 'remarques', type : 'String'}
				] 
			})
		});	
		
		var costBenInfoGrid = new xg.GridPanel({
			id: 'costBenInfoGrid',
			store: this.CostBenInfo_store,
			colModel: new Ext.grid.ColumnModel({
				columns: [
					{header: "ID", dataIndex: 'id', hidden: true},
					{header: "Name", dataIndex: 'nom'},
					{header: "Description", dataIndex: 'description'},
					{header: "Remarks", dataIndex: 'remarques'},
					{
						xtype: 'actioncolumn',
						items: [
							{
								icon: 'src/gxp/theme/img/silk/table_edit.png',  
								tooltip: 'Edit/View Data (in summary)',
								handler: function(grid, rowIndex, colIndex) {
									var rec = costBenInfoGrid.getStore().getAt(rowIndex);
									var customRender = function(v) 	// plug Swiss Franc currency renderer into formatter
									{									
										v = Ext.util.Format.number(v, '0,000.00');																				 
										return (v + ' CHF');
									};
									Ext.ux.grid.GroupSummary.Calculations['totalCost'] = function(v, record, field){
										return v + parseFloat(record.get('risque_total'));
									};
									
									var costBenEditGrid = new Ext.grid.EditorGridPanel({
										id: 'costBenEditGrid',										
										store: new Ext.data.GroupingStore({
											url: 'costBenInfo.php',
											baseParams: {
												task: 'loadCostBenValues',
												ws: workspace,
												cbID: rec.get('id')												
											},
											sortInfo:{field: 'risque_sce_id', direction: "ASC"},
											groupField:'cout_nom',
											reader: new Ext.data.JsonReader({
												totalProperty : 'totalCount',
												root          : 'rows',
												successProperty: 'success',
												idProperty    : 'id',
												fields: [ 					
													{name : 'cout_id', type : 'int'},
													{name : 'cout_nom', type : 'String'},
													{name : 'risque_sce_id', type : 'int'},
													{name : 'alt_nom', type : 'String'},
													{name : 'risque_sce_nom', type : 'String'},
													{name : 'processus_type', type : 'String'},
													{name : 'objet_type', type : 'String'},
													{name : 'risque_total', type : 'float'},
													{name : 'indice', type : 'String'}
												]
											}),
											autoLoad: true
										}),
										colModel: new Ext.grid.ColumnModel({
											defaults: {
												width: 120,
												sortable: true
											},
											columns: [
												{header: "Cost ID", sortable: true, dataIndex: 'cout_id', hidden: true},
												{header: "Cost Sheet", sortable: true, dataIndex: 'cout_nom'},
												{header: "Risk Scenario ID", sortable: true, dataIndex: 'risque_sce_id', hidden: true},
												{header: "Alternative", width: 100, sortable: true, dataIndex: 'alt_nom'},
												{header: "Risk Scenario", width: 100, sortable: true, dataIndex: 'risque_sce_nom'},
												{header: "Hazard Type", width: 100, sortable: true, dataIndex: 'processus_type'},
												{header: "Object Type", width: 100, sortable: true, dataIndex: 'objet_type'},
												{header: "Risk Total", width: 100, sortable: true, dataIndex: 'risque_total', summaryType: 'totalCost', summaryRenderer: customRender, renderer: customRender},
												{header: "Mapping Index", sortable: true, dataIndex: 'indice', hidden: true},
												{
													xtype: 'actioncolumn',
													width: 20,
													items: [
														{
															icon: 'src/gxp/theme/img/silk/map.png',  
															tooltip: 'Visualize the selected risk scenario',								
															handler: function(grid, rowIndex, colIndex) {
															// to load the layer to the map 
																	var rec = grid.getStore().getAt(rowIndex);
																	var lyr_name = rec.get('indice');
																	var names = {};
																	names[workspace + ":" + lyr_name] = true; 
																	
																	var source = app.tools.addlyrs.target.layerSources["local"]; // need to reassign the source in case other sources such as google are being added by users
																	app.tools.addlyrs.setSelectedSource(source);
																	var newRecords = [];
																	var last = 0;
																	app.tools.addlyrs.selectedSource.store.each(function(record, index) {
																		if (record.get("name") in names) {
																			last = index;
																			newRecords.push(record);
																		}
																	});										
																	app.tools.addlyrs.addLayers(newRecords, false);	
															}
														},
														{
															icon: 'src/gxp/theme/img/silk/compress.png',  
															tooltip: 'Download the selected risk scenario',								
															handler: function(grid, rowIndex, colIndex) {
															// to download the layer 
																	var rec = grid.getStore().getAt(rowIndex);
																	var layerName = workspace + ':' + rec.get('indice');																	
																	var url = 'http://changes-itn.unil.ch:8080/geoserver/wfs?';
																	url += 'service=WFS&request=GetFeature&version=1.0.0&typeName=' + layerName +
																		'&format_options=filename:'+ layerName +'.zip' +
																		'&outputFormat=SHAPE-ZIP';
															
																	window.open(url);
															}
														}
													]
												}	
											]
										}),
										view: new Ext.grid.GroupingView({
											forceFit: true,
											showGroupName: false,
											enableNoGroups: false,
											enableGroupingMenu: false,
											hideGroupedColumn: true
										}),
										plugins: new Ext.ux.grid.GroupSummary(),
										sm: new Ext.grid.RowSelectionModel({singleSelect:false}),												
										height: 300,													
										frame: true,
										stripeRows : true,
										footer: true,
										region: 'center',
										split: true,
										tbar : []
										/* buttons: [
										{
											text: 'Add', 
											iconCls: 'add',
											handler: function(){
												var pairWin = new Ext.Window({
													title: 'Selection of the pair (cost/risk scenarios)',
													autoHeight:true,
													layout: 'form',		
													modal: true,
													plain:false,
													closable: true,
													resizable : false,
													buttonAlign:'right',													
													items: new Ext.form.FormPanel({
														frame:true,			
														bodyStyle:'padding:5px 5px 0',
														width: 350,	
														autoHeight:true,	
														defaults: {
															anchor: "95%",					
															msgTarget: "side"
														},
														items: [{												
															fieldLabel: 'Cost sheet',
															name: 'cost',
															hiddenName: 'costID',
															xtype: 'combo',																	
															mode: "local",
															editable: false,
															allowBlank: false,
															store: new Ext.data.JsonStore({ 
																url: 'costBenInfo.php',
																baseParams: {																
																	ws: workspace,
																	task: 'loadAllCostNames'
																},
																root: 'rows',
																autoLoad: true,
																fields : ['id','nom']																		
															}),						
															valueField: 'id',
															displayField: 'nom',																	
															triggerAction: 'all',
															listeners: {
																select: function(combo, record, index) {																	
																	var form = pairWin.items.items[0];
																	form.items.items[1].clearValue();
																	form.items.items[1].getStore().reload({ params: 
																		{ 	ws: workspace,													
																			task: 'loadSelectedRiskSceNames',
																			selCostID: combo.getValue(),
																			cbID: rec.get('id')
																		} 
																	});
																}
															}
														},{
															fieldLabel: 'Risk scenarios',
															name: 'riskSce',
															hiddenName: 'riskSceID',
															xtype: 'lovcombo',
															mode: "local",
															editable: false,
															allowBlank: false,
															hideOnSelect:false,
															store: new Ext.data.JsonStore({ 
																url: 'costBenInfo.php',
																baseParams: {
																	ws: workspace,
																	task: 'loadSelectedRiskSceNames',
																	cbID: rec.get('id')
																},
																root: 'rows',
															//	autoLoad: true,
																fields : ['id','nom']																		
															}),
															valueField: 'id',
															displayField: 'nom',
															triggerAction: 'all'
														}]
													}),
													buttons: [{
														text: 'Add',
														iconCls: 'add',
														handler: function() {
															var form = pairWin.items.items[0].form;
															if (form.isValid()){
																// add the new records to the 'cost_benefit_values' table
																form.submit({
																	url:'costBenInfo.php',
																	params: {
																		task: 'insertRiskSce',
																		ws: workspace,
																		cbID: rec.get('id')
																	},
																	waitMsg : 'Please wait...',
																	success: function(form, action) {
																		Ext.Msg.alert('Success', action.result.message);
																		pairWin.close();
																		costBenEditGrid.getStore().reload({ params: 
																			{ ws: workspace,
																			task: 'loadCostBenValues'} // reload the store with the selected workspace 
																		});
																	},
																	failure: function(form, action) {
																		Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
																	}
																});	
															}
															else {
																Ext.Msg.alert('Validation Error', 'The form is not valid!' );
															}
														}
													}]
												});
												
												pairWin.show();												
											}
										},{
											text: 'Remove',
											iconCls: 'delete',
											handler: function(){
												// remove the selected records from the 'cost_benefit_values' table
												Ext.Msg.show({
													title:'Remove the risk scenarios?',
													msg: 'Are you sure to remove the selected risk scenarios from the report?',
													buttons: Ext.Msg.YESNOCANCEL,
													fn: function(buttonId, text, opt){
														if (buttonId == 'yes'){
															var records = costBenEditGrid.getSelectionModel().getSelections();
															var temp =[];
															for (var i=0;i<records.length;i++) {									
																temp.push(records[i].data);
															}
															Ext.Ajax.request({   
																url: 'costBenInfo.php',
																params: {
																	task: 'removeRiskSce',
																	ws: workspace,
																	records: Ext.encode(temp),
																	cbID: rec.get('id')
																},
																success: function(response, opts){
																	var obj = Ext.decode(response.responseText);	
																	if (obj.success == 'true') {
																		Ext.Msg.alert('Success', 'The selected risk scenarios have been removed from the report!');																		
																		// reload the store and refresh
																		costBenEditGrid.getStore().reload({
																			callback: function(){
																				costBenEditGrid.getView().refresh();
																			}
																		});
																	}
																	else {
																		Ext.Msg.alert('Failed', obj.message);
																	}										
																},
																failure: function(response, opts){
																	Ext.Msg.alert('Failed', response.message);
																}
															});
														}
													}
												});	
											}
										}] */
									});		
									
									// grid to export 
									var costBenEditExportGrid = new Ext.grid.EditorGridPanel({
										store: new Ext.data.GroupingStore({
											url: 'costBenInfo.php',
											baseParams: {
												task: 'loadCostBenValues',
												ws: workspace,
												cbID: rec.get('id')												
											},
											sortInfo:{field: 'risque_sce_id', direction: "ASC"},
											groupField:'cout_nom',
											reader: new Ext.data.JsonReader({
												totalProperty : 'totalCount',
												root          : 'rows',
												successProperty: 'success',
												idProperty    : 'id',
												fields: [ 					
													{name : 'cout_id', type : 'int'},
													{name : 'cout_nom', type : 'String'},
													{name : 'risque_sce_id', type : 'int'},
													{name : 'alt_nom', type : 'String'},
													{name : 'risque_sce_nom', type : 'String'},
													{name : 'processus_type', type : 'String'},
													{name : 'objet_type', type : 'String'},
													{name : 'risque_total', type : 'float'},
													{name : 'indice', type : 'String'}
												]
											}),
											autoLoad: true
										}),
										colModel: new Ext.grid.ColumnModel({
											defaults: {
												width: 120,
												sortable: true
											},
											columns: [
												{header: "Cost ID", sortable: true, dataIndex: 'cout_id', hidden: true},
												{header: "Cost Sheet", sortable: true, dataIndex: 'cout_nom'},
												{header: "Risk Scenario ID", sortable: true, dataIndex: 'risque_sce_id', hidden: true},
												{header: "Alternative", width: 100, sortable: true, dataIndex: 'alt_nom'},
												{header: "Risk Scenario", width: 100, sortable: true, dataIndex: 'risque_sce_nom'},
												{header: "Hazard Type", width: 100, sortable: true, dataIndex: 'processus_type'},
												{header: "Object Type", width: 100, sortable: true, dataIndex: 'objet_type'},
												{header: "Risk Total", width: 100, sortable: true, dataIndex: 'risque_total', summaryType: 'totalCost', summaryRenderer: customRender, renderer: customRender},
												{header: "Mapping Index", sortable: true, dataIndex: 'indice', hidden: true}
											]
										})	
									});
									
									//Create the Download button and add it to the top toolbar
									var exportButton = new Ext.ux.Exporter.Button({
									  component: costBenEditExportGrid,
									  text     : "Download as .xls"
									});
									
									costBenEditGrid.getTopToolbar().add(exportButton);
									
									var costBenEditWin = new Ext.Window({
										title: 'Edit/View the report information:',										
										width: 500,
										height: 400,
										minWidth: 300,
										minHeight: 200,
										layout: 'border',
										plain:true,
										maximizable: true,
										collapsible: true,
										closeable: true,
										closeAction:'hide',
										buttonAlign:'right',
										items: [ new Ext.form.FormPanel({
											labelWidth: 75, // label settings here cascade unless overridden
											itemId: 'costBenEditForm',
											region: 'north',
											frame:true,			
											width: 350,	
											autoHeight:true,	
											collapsible: true,
											isCollapsed: false,
											collapseMode: 'mini',
											defaults: {
												anchor: '100%'
											},
											items: [{
												fieldLabel: 'Name',
												xtype: 'textfield',
												name: 'name_report',
												allowBlank: false,
												value: rec.get('nom')
											},{
												fieldLabel: 'Description',
												xtype: 'textarea',
												name: 'description_report',
												allowBlank: true,
												value: rec.get('description')
											},{
												fieldLabel: 'Remarks',
												xtype: 'textarea',
												name: 'remarks_report',
												allowBlank: true,
												value: rec.get('remarques')
											}],
											buttons: [{
												text: 'Update',
												icon:  'src/gxp/theme/img/silk/table_save.png',
												handler: function(){
													var form = costBenEditWin.getComponent('costBenEditForm').getForm();				
													if (form.isValid()) {
														form.submit({
															url: 'costBenInfo.php',
															params: {
																task: 'update',
																ws: workspace,
																ID: rec.get('id')
															},
															waitMsg : 'Please wait...',
															success: function(form, action) {
																Ext.Msg.alert('Success', action.result.message);
																Ext.getCmp('costBenInfoGrid').getStore().reload({ // refresh the grid view
																	callback: function(){
																		Ext.getCmp('costBenInfoGrid').getView().refresh();
																	}
																});
															},
															failure: function(form, action) {
																Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
															}
														});
													}
													else {
														Ext.Msg.alert('Validation Error', 'The form is not valid!' );
													}
												}
											}]
										}), costBenEditGrid
										]
									});	
									
									costBenEditWin.show();
								}
							},
							{
								icon: 'src/gxp/theme/img/silk/table.png',  
								tooltip: 'View Data (in details)',
								handler: function(grid, rowIndex, colIndex) {
									var rec = grid.getStore().getAt(rowIndex);
									var customRender = function(v) 	// plug Swiss Franc currency renderer into formatter
									{									
										v = Ext.util.Format.number(v, '0,000.00');																				 
										return (v + ' CHF');
									};
									var customExpRender = function(v) 	// plug exponential renderer into formatter
									{									
										return v.toExponential(2);
									};
									
									var costBenComDataGrid = new Ext.grid.GridPanel({
										store: new Ext.data.GroupingStore({
											url: 'costBenInfo.php',
											baseParams: {
												task: 'loadCostBenComValues',
												ws: workspace,
												cbID: rec.get('id')												
											},
											sortInfo:{field: 'risque_sce_pro_id', direction: "ASC"},
											groupField:'group',
											reader: new Ext.data.JsonReader({
												totalProperty : 'totalCount',
												root          : 'rows',
												successProperty: 'success',
												idProperty    : 'id',
												fields: [ 'risque_sce_pro_id',					
													{name : 'cout_id', type : 'int'},
													{name : 'cout_nom', type : 'String'},
													{name : 'risque_sce_id', type : 'int'},
													{name : 'alt_nom', type : 'String'},
													{name : 'risque_sce_nom', type : 'String'},
													{name : 'processus_type', type : 'String'},
													{name : 'objet_type', type : 'String'},
													{name : 'temp_de_retour', type : 'String'},
													{name : 'risque_obj', type : 'float'},
													{name : 'risque_humain', type : 'float'},
													{name : 'risque_total', type : 'float'},
													{name : 'risque_individuel', type : 'float'},
													{name : 'group', convert : function (v, rec){
														return rec.alt_nom + ', ' + rec.processus_type + ', ' + rec.objet_type;
													}
													}
												]
											}),
											autoLoad: true
										}),								
										colModel: new Ext.grid.ColumnModel({
											defaults: {
											width: 120,
											sortable: true
										},
										columns: [
											{header: "Group", dataIndex: 'group', hidden: true},
											{header: "Cost ID", dataIndex: 'cout_id', hidden: true},
											{header: "Cost Sheet", dataIndex: 'cout_nom', hidden: true},
											{header: "Risk Scenario ID", dataIndex: 'risque_sce_id', hidden: true},
											{header: "Alternative", dataIndex: 'alt_nom'},
											{header: "Risk Scenario", dataIndex: 'risque_sce_nom'},
											{header: "Hazard Type", dataIndex: 'processus_type'},
											{header: "Object Type", dataIndex: 'objet_type'},
											{header: "Return Period", dataIndex: 'temp_de_retour'},
											{header: "Object (CHF/year)", dataIndex: 'risque_obj', renderer: customRender, align: 'right', summaryType: 'sum', summaryRenderer: customRender},
											{header: "Human (CHF/year)", dataIndex: 'risque_humain', renderer: customRender, align: 'right', summaryType: 'sum', summaryRenderer: customRender},
											{header: "Risk Total (CHF/year)", dataIndex: 'risque_total', renderer: customRender, align: 'right', summaryType: 'sum', summaryRenderer: customRender},
											{header: "Individual (person/year)", dataIndex: 'risque_individuel', renderer: customExpRender, align: 'right', summaryType: 'sum', summaryRenderer: customExpRender}
										]
										}),
										view: new Ext.grid.GroupingView({				
											forceFit: true,
											groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
										}),	
										plugins: new Ext.ux.grid.GroupSummary(),
										sm: new Ext.grid.RowSelectionModel({singleSelect:true}),	
										frame: true,
										tbar: []
									});
									
									//Create the Download button and add it to the top toolbar
									var exportButton = new Ext.ux.Exporter.Button({
									  component: costBenComDataGrid,
									  text     : "Download as .xls"
									});
									
									costBenComDataGrid.getTopToolbar().add(exportButton);
									
									// a window to show the panel 
									var costBenComDataWin = new Ext.Window({
										title: 'View the detailed report: ' + rec.get('nom'),
										width: 500,
										height: 400,
										layout: 'fit',
										plain: true,
										maximizable: true,
										collapsible: true,
										closeable: true,
										buttonAlign:'right',
										items: [costBenComDataGrid]
									});
									
									costBenComDataWin.show();
								}
							},	
							{								
								icon: 'src/gxp/theme/img/silk/chart_bar.png',  
								tooltip: 'Visualize the comparison values (Table/Chart)',								
								handler: function(grid, rowIndex, colIndex) {
									var rec = grid.getStore().getAt(rowIndex);								
									var costBenChartStore = new Ext.data.JsonStore({
										url: 'costBenInfo.php',
										sortInfo:{field: 'cout_id', direction: "ASC"},
										root: 'rows',
										fields: ['cout_id','cout_nom','alt_nom','cout_annuel','risque_total','benefice','rapport','risque_individuel'],												
										autoLoad: true,
										baseParams: {
											task: 'loadChartStoreGrid',
											ws: workspace,
											id: rec.get('id')
										}
									});
									var customRender = function(v) 	// plug Swiss Franc currency renderer into formatter
									{										
										v = Ext.util.Format.number(v, '0,000.00');																				 
										return (v + ' CHF');
									};
									
									// grid to show
									var costBenDataGrid = new Ext.grid.GridPanel({
										store: costBenChartStore,								
										colModel: new Ext.grid.ColumnModel({
											defaults: {
											width: 120,
											sortable: true
										},
										columns: [
											{header: "Cost ID", dataIndex: 'cout_id', hidden: true},
											{header: "Cost Sheet", dataIndex: 'cout_nom'},
											{header: "Alternative", dataIndex: 'alt_nom'},
											{header: "Annual Cost (CHF/year)", dataIndex: 'cout_annuel', align: 'right', renderer: customRender},
											{header: "Risk Total (CHF/year)", dataIndex: 'risque_total', align: 'right', renderer: customRender},
											{header: "Benefit (CHF/year)", dataIndex: 'benefice', align: 'right', renderer: customRender},
											{header: "Cost-Benefit Ratio", dataIndex: 'rapport', align: 'right', renderer: function(v, params, record){												
												return Ext.util.Format.number(record.get('benefice')/record.get('cout_annuel'), '0.00'); 
											}}
										]
										}),
										viewConfig: {
											forceFit: true
										},
										sm: new Ext.grid.RowSelectionModel({singleSelect:true}),	
										frame: true,
										tbar: []
									});
									
									//Create the Download button and add it to the top toolbar
									var exportButton = new Ext.ux.Exporter.Button({
									  component: costBenDataGrid,
									  text     : "Download as .xls"
									});
									
									costBenDataGrid.getTopToolbar().add(exportButton);
									
									var costBenChartPanel = new Ext.Panel({										
										frame:true,												
										layout: 'vbox',	
										layoutConfig: {
											align: 'stretch',
											pack: 'start'
										},
										items: [{
											flex: 1,
											layout: 'fit',
											border: true,
											frame: true,
											title: 'Stacked Column Chart: Comparison of annual risk and cost',
											items: [{
												xtype: 'stackedcolumnchart',
												store: costBenChartStore,
												url: 'src/ext/resources/charts.swf',
												xField: 'cout_nom',												
												yAxis: new Ext.chart.NumericAxis({
													stackingEnabled: true,
													labelRenderer : Ext.util.Format.numberRenderer('0,0')
												}),
												series: [{
													displayName: 'Risk Total (CHF/year)',
													yField: 'risque_total',
													style: {
														mode: 'stretch'
													}
												},{
													displayName: 'Annual Cost (CHF/year)',
													yField: 'cout_annuel',
													style: {
														mode: 'stretch',
														color: 0xA52A2A
													}
												}],
												extraStyle: {															
													legend: {																		
														display: 'bottom',
														padding: 5,
														font: {
															family: 'Tahoma',
															size: 10
														}
													}
												}
											}]			
										},{	
											flex: 1,
											layout: 'fit',
											border: true,
											items: [costBenDataGrid]
										}]
									});	
									
									// a window to show the panel 
									var costBenChartWin = new Ext.Window({
										title: 'Visualization of the report: ' + rec.get('nom'),
										width: 450,
										height: 500,
										layout: 'fit',
										plain: true,
										maximizable: true,
										collapsible: true,
										closeable: true,
										buttonAlign:'right',
										items: [costBenChartPanel]
									});
													
									costBenChartWin.show();
								}
							}]
					}],
				defaults: {
					sortable: true,
					menuDisabled: false,
					width: 5
				}
				}),
			viewConfig: {
				forceFit: true
			},
			frame:true,
			stripeRows: true,
			width: 700,
			height: 450,
			title: 'Available Cost Benefit Reports',
			iconCls: 'icon-grid',		
			fbar: ['->', {
				text:'Add New',
				iconCls: 'add',
				handler : function(){ // show a new window to create a new cost benefit report						
					costBenAddStore.reload({ params: 
						{ 	ws: workspace,
							task: 'loadGrid'
						} 
					});
					costBenAddWin.show(); 					
				},
				scope: this
			},{
				text:'Delete',
				iconCls: 'delete',	
				handler : function(){
					// delete the selected cost benefit reports 
					Ext.Msg.show({
						title:'Delete the cost-benefit reports?',
						msg: 'Are you sure to delete the selected reports? This action is undoable!',
						buttons: Ext.Msg.YESNOCANCEL,
						fn: function(buttonId, text, opt){
							if (buttonId == 'yes'){
								var records = Ext.getCmp('costBenInfoGrid').getSelectionModel().getSelections();
								var ids = [];
								for (var i=0;i<records.length;i++) {
									ids.push(records[i].get('id'));
								}
								Ext.Ajax.request({   
									url: 'costBenInfo.php',
									params: {
										task: 'delete',
										ws: workspace,
										costBenIDs: Ext.encode(ids)
									},
									success: function(response, opts){
										var obj = Ext.decode(response.responseText);	
										if (obj.success == 'true') {
											Ext.Msg.alert('Success', obj.message);
											// reload the store and refresh
											Ext.getCmp('costBenInfoGrid').getStore().reload({ 
												callback: function(){
													Ext.getCmp('costBenInfoGrid').getView().refresh();
												}
											});
										}
										else {
											Ext.Msg.alert('Failed', obj.message);
										}	
									},
									failure: function(response, opts){
										Ext.Msg.alert('Failed', response.message);
									}
								});
							}
						}
					});	
				}
			}]
		});
		
		return costBenInfoGrid;	
	}
});

Ext.preg(gxp.plugins.CostBenefitManagement.prototype.ptype, gxp.plugins.CostBenefitManagement);		