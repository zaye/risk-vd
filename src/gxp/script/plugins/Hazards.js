/**
 * Copyright (c) 2011-2016 
 * 
 * Published under the GPL license.
 */
 
/**
 * @require plugins/Tool.js
 * @require widgets/LayerUploadPanelHazard.js
 */
 
 /** api: (define)
 *  module = gxp.plugins
 *  class = Hazards
 */
 
 /** api: (extends)
 *  plugins/Tool.js
 */
 
Ext.ns("gxp.plugins");

/** api: constructor
 *  .. class:: Hazards(config)
 *
 *    Plugin for the hazard component of the risk analysis application.
 *    
 */
 
 gxp.plugins.Hazards = Ext.extend(gxp.plugins.Tool, {

	 /** api: ptype = myapp_hazards */
	ptype: "myapp_hazards",
	
	/** private: method[constructor]
     */
    constructor: function() {
        this.initialization();
        gxp.plugins.Hazards.superclass.constructor.apply(this, arguments);        
    },
  
	/** api: method[show]
     * Shows the respective grid window for the selected tree node.
     */ 
    showCmp: function(n) {
		if (n.attributes.id == 'PrAInfo')  {
			this.PrAStore.reload({ 
				params: 
					{ws: workspace,
					task: 'load'} // reload the grid with the selected workspace 
				});
			this.PrAInfoWin.show();
		}
		else {
			this.hazStore.reload({ 
				params: 
					{ws: workspace,
					task: 'load'} // reload the grid with the selected workspace 
				});
			this.hazInfoWin.show();
		}
	},

	/** api: private method[initialization]
     * Initialize the different grid windows to show more than one window at a time. 
     */
	initialization: function() {
		
		this.hazInfoWin = new Ext.Window({
			title: 'Hazard layers information',
			layout: 'fit',
			plain:false,
			maximizable: true,
			collapsible: true,
			closeable: true,
			closeAction:'hide',
			buttonAlign:'right',
			items: this.initHazGrid()
		});
		
		this.PrAInfoWin = new Ext.Window({
			title: 'Configuration of the spatial probability (PrA) values ',
			layout: 'fit',
			plain:false,
			maximizable: true,
			collapsible: true,
			closeable: true,
			closeAction:'hide',
			buttonAlign:'right',
			items: this.initPrAGrid()
		});
	},
	
	/** api: private method[initHazGrid]
     * initialization of the grid panel to show the uploaded hazard maps
     */
	 
	initHazGrid: function(){
		this.hazStore = new Ext.data.GroupingStore({
				url: 'hazInfo.php',
				sortInfo:{field: 'group', direction: "ASC"},
				groupField:'group',
				reader: new Ext.data.JsonReader({
					totalProperty : 'totalCount',
					root          : 'rows',
					successProperty: 'success',
					idProperty    : 'id',
					fields: [
						{name : 'id', type : 'int'},
						{name : 'nom', type : 'String'},
						{name : 'description', type : 'String'},
						{name : 'type', type : 'String'},
						{name : 'alt_nom', type : 'String'},
						{name : 'temp_de_retour', type : 'String'},
						{name : 'ans', type : 'int'},
						{name : 'remarques', type : 'String'},
						{name : 'nom_origine', type : 'String'},
						{name : 'indice', type : 'String'},
						{name : 'group', convert : function (v, rec){
							return rec.type + ', ' + rec.alt_nom;
						}}						
					] 
				})
		});	
		
		var hazGrid = new Ext.grid.GridPanel({
			id: 'hazGrid',
			store: this.hazStore,
			colModel: new Ext.grid.ColumnModel({
				columns: [					
					{header: "Group", dataIndex: 'group', hidden: true},
					{header: "ID", dataIndex: 'id', hidden: true},
					{header: "Type", dataIndex: 'type'},					
					{header: "Layer Name", dataIndex: 'nom'},
					{header: "Description", dataIndex: 'description'},
					{header: "Return Period", dataIndex: 'temp_de_retour'},
					{header: "Return Period (year)", dataIndex: 'ans', hidden: true},
					{header: "For Alternative", dataIndex: 'alt_nom'},
					{header: "Remarks", dataIndex: 'remarques'},			
					{header: "Mapping Index", dataIndex: 'indice', hidden: true},
					{header: "Original Name", dataIndex: 'nom_origine', hidden: true},
					{
						xtype: 'actioncolumn',
						items: [
						{
								icon: 'src/gxp/theme/img/silk/map.png',  
								tooltip: 'Visualize the hazard map',								
								handler: function(grid, rowIndex, colIndex) {
									// to load the layer to the map 
										var rec = grid.getStore().getAt(rowIndex);
										var lyr_name = workspace + ':' + rec.get('indice');
										var names = {};
										names[lyr_name] = true;
										
										var source = app.tools.addlyrs.target.layerSources["local"]; // need to reassign the source in case other sources such as google are being added by users
										app.tools.addlyrs.setSelectedSource(source);
										app.tools.addlyrs.selectedSource.store.load(); // refresh the store
										var newRecords = [];
										var last = 0;
										app.tools.addlyrs.selectedSource.store.each(function(record, index) {
											if (record.get("name") in names) {
												last = index;
												newRecords.push(record);
											}
										});										
										app.tools.addlyrs.addLayers(newRecords, false);	
								}
							},
							{
								icon: 'src/gxp/theme/img/silk/table_edit.png',  
								tooltip: 'Edit Data',			
								handler: function(grid, rowIndex, colIndex) {									
									var rec = grid.getStore().getAt(rowIndex);
									
									var hazEditWin = new Ext.Window({
										title: 'Edit the hazard map information..',
										layout: 'form',
										autoHeight: true,
										plain:false,
										modal: true,
										resizable: false,
										closeable: true,
										closeAction:'hide',
										buttonAlign:'right',
										items: new Ext.form.FormPanel({
											labelWidth: 100, // label settings here cascade unless overridden
											itemId: 'hazEditForm',
											frame:true,			
											bodyStyle:'padding:5px 5px 0',
											width: 350,	
											autoHeight:true,		
											defaults: {
												anchor: '100%'
											},
											items: [{
												fieldLabel: 'Name/Title',
												xtype: 'textfield',
												name: 'name',
												allowBlank: false,
												value: rec.get('nom')
											},{
												fieldLabel: 'Hazard Type',
												name: 'type',
												hiddenName: 'haz_type',
												xtype: "combo",
												mode: "local",
												editable: false,
												store: new Ext.data.ArrayStore({
													id: 0,
													fields: ['myValue','displayText'],
													data: [['AVA', 'Avalanches'],['LTO', 'Laves torrentielles'],['INO','Inondation'],['INOD','Inondation dynamique'],['INOS','Inondation statique'], ['CPB', 'Chutes de pierre et de blocs'],['GPP', 'Glissements de terrain permanents'],['GPP-TR', 'Glissements de terrain permanents (TR)'],['GSS', 'Glissements de terrain spontanés']]  // data is local
												}),
												valueField: 'myValue',
												displayField: 'displayText',
												value: rec.get('type'),
												triggerAction: 'all',
												allowBlank: false
											},{
												fieldLabel: 'Return Period',
												xtype: 'numberfield',
												name: 'year',
												allowBlank: false,
												value: rec.get('ans')
											},{
												fieldLabel: 'Description',
												xtype: 'textarea',
												name: 'description',
												value: rec.get('description')
											},{
												fieldLabel: 'Remarks',
												xtype: 'textarea',
												name: 'remarks',
												value: rec.get('remarques')
											}],
											buttons: [{
												text: 'Update',
												icon:  'src/gxp/theme/img/silk/table_save.png',
												handler: function(){
													var form = hazEditWin.getComponent('hazEditForm').getForm();				
													if (form.isValid()) {
														form.submit({
															url: 'hazInfo.php',
															params: {
																task: 'edit',
																ws: workspace,
																mpIndex: rec.get('indice'),
																ID: rec.get('id')
															},
															waitMsg : 'Please wait...',
															success: function(form, action) {
																Ext.Msg.alert('Success', action.result.message);
																hazEditWin.close();
																Ext.getCmp('hazGrid').getStore().reload({ // refresh the grid view
																	callback: function(){
																	Ext.getCmp('hazGrid').getView().refresh();
																	}
																});
															},
															failure: function(form, action) {
																Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
															}
														});
													}
													else {
														Ext.Msg.alert('Validation Error', 'The form is not valid!' );
													}
												}
											},{
												text: 'Cancel',
												icon:  'src/gxp/theme/img/decline.png',
												handler: function(){
													hazEditWin.close();
												}
											}]
										})											
									});
									
									hazEditWin.show();
								}
							}
						]		
					}
				],
				defaults: {
					sortable: true,
					menuDisabled: false,
					width: 5
				}
			}),			
			view: new Ext.grid.GroupingView({				
				forceFit: true,
				groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
			}),						
			frame:true,
			width: 700,
			height: 450,
			title: 'Avaiable Hazard Maps (Layers)',
			iconCls: 'icon-grid',
			fbar: ['->', {
				text:'Add New',
				iconCls: 'add',
				handler : function(){ // show a new window to upload the shape layer and associated scenario table 					
						var hazForm = new gxp.LayerUploadPanelHazard({
							width: 450,
							frame: true,
							autoHeight: true,
							bodyStyle: "padding: 10px 10px 0 10px;",
							labelWidth: 75,
							defaults: {
								anchor: "95%",
								allowBlank: false,
								msgTarget: "side"
							}
						});		
						
						var hazDataUpload = new Ext.Window({
							title: "Upload Hazard Map",
							layout: 'fit',
							autoHeight: true,
							plain:false,
							modal: true,
							maximizable: false,
							collapsible: false,
							closeable: true,
							closeAction:'hide',
							buttonAlign:'right',
							items: [hazForm]
						});
						
						hazDataUpload.show();
					},
				scope: this
			},{
				text:'Delete',
				iconCls: 'delete',	
				handler : function(){
					// delete the record from store, db, geoserver
					Ext.Msg.show({
						title:'Delete the hazard maps?',
						msg: 'Are you sure to delete the selected hazard maps? This action is undoable, please consider and try to remove first any referenced scenarios if these maps are being used in risk calculation!',
						buttons: Ext.Msg.YESNOCANCEL,
						fn: function(buttonId, text, opt){
							if (buttonId == 'yes'){
								var records = Ext.getCmp('hazGrid').getSelectionModel().getSelections();
								var temp =[];
								for (var i=0;i<records.length;i++) {									
									temp.push(records[i].data);
								}
								Ext.Ajax.request({   
									url: 'hazInfo.php',
									params: {
										task: 'delete',
										ws: workspace,
										hazRecords: Ext.encode(temp)
									},
									success: function(response, opts){
										var obj = Ext.decode(response.responseText);										
										Ext.Msg.alert('Information', obj.message);	
										// reload the store and refresh
										Ext.getCmp('hazGrid').getStore().reload({ 
												callback: function(){
													Ext.getCmp('hazGrid').getView().refresh();
												}
											});
									},
									failure: function(response, opts){
										var obj = Ext.decode(response.responseText);										
										Ext.Msg.alert('Failed', obj.message);
									}
								});
							}
						}
					});
				}
			}]
		});
		
		return hazGrid;
	},
	
	/** api: private method[initPrAGrid]
     * initialization of the grid panel to show the default and available PrA configurations
     */
	initPrAGrid: function(){
		this.PrAStore = new Ext.data.GroupingStore({
				url: 'PrAInfo.php',
				sortInfo:{field: 'nom', direction: "ASC"},
				groupField:'type',
				reader: new Ext.data.JsonReader({
					totalProperty : 'totalCount',
					root          : 'rows',
					successProperty: 'success',
					idProperty    : 'id',
					fields: [
						{name : 'id', type : 'int'},						
						{name : 'nom', type : 'String'},
						{name : 'description', type : 'String'},
						{name : 'type', type : 'String'},
						{name : 'remarques', type : 'String'},
						{name : 'indice', type : 'String'}
					] 
				})
		});	
		
		var PrAInfoGrid = new Ext.grid.GridPanel({
			id: 'PrAInfoGrid',
			store: this.PrAStore,
			colModel: new Ext.grid.ColumnModel({
				columns: [
					{header: "ID", dataIndex: 'id', hidden: true},					
					{header: "Name", dataIndex: 'nom'},
					{header: "Description", dataIndex: 'description'},	
					{header: "Option", dataIndex: 'type'},
					{header: "Remarks", dataIndex: 'remarques'},		
					{header: "Mapping Index", dataIndex: 'indice', hidden: true},
					{
						xtype: 'actioncolumn',
						items: [
							{
								icon: 'src/gxp/theme/img/silk/table_edit.png',  								
								tooltip: 'Edit/View Data',								
								handler: function(grid, rowIndex, colIndex) {									
									var rec = grid.getStore().getAt(rowIndex);	
									var PrAEditGrid = new Ext.grid.EditorGridPanel({
										id: 'PrAEditGrid',										
										title: '',
										store: new Ext.data.GroupingStore({
											url: 'PrAInfo.php',
											baseParams: {
												task: 'loadPrAValues',
												ws: workspace,
												mpIndex: rec.get('indice')												
											},
											sortInfo:{field:'id', direction: "ASC"},
											groupField:'processus',
											reader: new Ext.data.JsonReader({
												totalProperty : 'totalCount',
												root          : 'rows',
												successProperty: 'success',
												idProperty    : 'id',
												fields: [ 					
													{name : 'id', type : 'int'},
													{name : 'processus', type : 'String'},
													{name : 'probabilite', type : 'String'}, 
												//	{name : 'frequence', type : 'number'},
													{name : 'valeur', type : 'number'}, 													
													{name : 'remarques', type : 'String'}													
												]
											}),
											autoLoad: true
										}),
										colModel: new Ext.grid.ColumnModel({
											defaults: {
												width: 120,
												sortable: true
											},
											columns: [
												{header: "ID", sortable: true, dataIndex: 'id', hidden: true},
												{header: "Hazards", sortable: true, dataIndex: 'processus'},
												{header: "Return Period", sortable: true, dataIndex: 'probabilite'},
											//	{header: "Frequency", sortable: true, dataIndex: 'frequence', editable: rec.get('id') != 1? true: false, editor:new Ext.form.NumberField({allowDecimals: true, decimalPrecision:4, allowBlank: false, allowNegative: false})},
												{header: "PrA Value", sortable: true, dataIndex: 'valeur', editable: rec.get('id') != 1? true: false, editor:new Ext.form.NumberField({allowDecimals: true, decimalPrecision:4, minValue: 0, maxValue: 1, allowBlank: false, allowNegative: false})},
												{header: "Remarks", sortable: true, dataIndex: 'remarques', editable: rec.get('id') != 1? true: false, editor:new Ext.form.TextField()}
											]
										}),
										view: new Ext.grid.GroupingView({
											forceFit: true,
											showGroupName: false,
											enableNoGroups: false,
											enableGroupingMenu: false,
											hideGroupedColumn: false
										}),									
										sm: new Ext.grid.RowSelectionModel({singleSelect:true}),												
										height: 300,													
										frame: true,
										stripeRows : true,
										footer: true,
										region: 'center',
										buttons: [
											{
												text: 'Save Values',
												iconCls: 'save',
												hidden: rec.get('id') == 1? true: false,
												handler: function(){
													// to POST only the dirty records to update the values
													var records = [];
													var PrAEditStore = PrAEditGrid.getStore();
													for (var i= 0;i < PrAEditStore.data.length;i++) {
														recs = PrAEditStore.data.items[i];
														if (recs.dirty == true){
															records.push((recs.data));
														//	console.dir(recs.data);
														}
													}
													
													if (records.length != 0) {
														Ext.Ajax.request({
															url:'PrAInfo.php',
															params: {
																task: 'save',
																ws: workspace,
																mpIndex: rec.get('indice')
															},
															jsonData: Ext.util.JSON.encode(records),
															success: function(response, opts) {
																PrAEditStore.commitChanges(); // unmark the dirty records and commit all changes to the records
																var obj = Ext.decode(response.responseText);										
																Ext.Msg.alert('Success', obj.message);						
															}
														});
													}
													else { Ext.Msg.alert('Information', 'There are no new values to be saved!');
													}
												}
											}
										]
										
									});	
									
									var PrAEditWin = new Ext.Window({
										title: 'Edit/View the PrA values of this configuration:' + rec.get('nom'),
										width: 500,
										height: 400,
										minWidth: 300,
										minHeight: 200,
										layout: 'border',
										plain:true,
										maximizable: true,
										collapsible: true,
										closeable: true,
										closeAction:'hide',
										buttonAlign:'right',
										items: [new Ext.form.FormPanel({
											labelWidth: 75, // label settings here cascade unless overridden
											itemId: 'PrAEditForm',
											region: 'north',
											frame:true,			
											autoHeight:true,
											collapsible: true,
											isCollapsed: false,
											collapseMode: 'mini',
											defaults: {
												anchor: '100%'
											},
											items: [{
												fieldLabel: 'Name',
												xtype: 'textfield',
												name: 'name',
												allowBlank: false,
												value: rec.get('nom')
											},{
												fieldLabel: 'Description',
												xtype: 'textarea',
												name: 'description',
												allowBlank: true,
												value: rec.get('description')
											},{
												fieldLabel: 'Remarks',
												xtype: 'textarea',
												name: 'remarks',
												allowBlank: true,
												value: rec.get('remarques')
											}],
											buttons: [{
												text: 'Update',
												hidden: rec.get('id') == 1? true: false,
												icon:  'src/gxp/theme/img/silk/table_save.png',
												handler: function(){
													var form = PrAEditWin.getComponent('PrAEditForm').getForm();				
													if (form.isValid()) {
														form.submit({
															url: 'PrAInfo.php',
															params: {
																task: 'edit',
																ws: workspace,
																ID: rec.get('id')
															},
															waitMsg : 'Please wait...',
															success: function(form, action) {
																Ext.Msg.alert('Success', action.result.message);
																Ext.getCmp('PrAInfoGrid').getStore().reload({ // refresh the grid view
																	callback: function(){
																		Ext.getCmp('PrAInfoGrid').getView().refresh();
																	}
																});
															},
															failure: function(form, action) {
																Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
															}
														});
													}
													else {
														Ext.Msg.alert('Validation Error', 'The form is not valid!' );
													}
												}
											}]	
										}), PrAEditGrid]
									});
									
									PrAEditWin.show();	
								}
							}
						]		
					}
				],
				defaults: {
					sortable: true,
					menuDisabled: false,
					width: 5
				}
			}),			
			view: new Ext.grid.GroupingView({				
				forceFit: true,
				groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
			}),						
			frame:true,
			width: 700,
			height: 450,
			title: 'Defined PrA parameter configurations',
			iconCls: 'icon-grid',
			fbar  : ['->', {
				text:'Add New',
				iconCls: 'add',
				handler : function(){
					
					var PrAForm = new Ext.form.FormPanel({
						width: 450,
						frame: true,
						autoHeight: true,
						bodyStyle: "padding: 10px 10px 0 10px;",
						labelWidth: 70,					
						items: [{
							xtype:'fieldset',
							title: 'Configuration information',
							collapsible: false,
							defaults: {
								anchor: "95%",					
								msgTarget: "side"
							},
							collapsed: false,
							items :[{
								fieldLabel: 'Name',
								xtype: 'textfield',
								name: 'name',
								allowBlank: false
							},{
								fieldLabel: 'Description',
								xtype: 'textarea',
								name: 'description',
								allowBlank: true
							},{
								fieldLabel: 'Remarks',
								xtype: 'textarea',
								name: 'remarks',
								allowBlank: true
							}]
						}],
						buttons: [{
							text: 'Add',
							icon:  'src/gxp/theme/img/silk/table_add.png',
							handler: function(){
								// create a new PrA table and populate with default values
								var form = PrAForm.getForm();		
								var fields = form.getFieldValues();
								if (form.isValid()) {			
									form.submit({
										url: 'PrAInfo.php',
										params: {
											task: 'add',
											ws: workspace
										},
										waitMsg : 'Please wait...',
										success: function(form, action) {
											Ext.Msg.alert('Information', action.result.message); 	
											form.reset(); // reset the form
											Ext.getCmp('PrAInfoGrid').getStore().reload({ // refresh the grid view
												callback: function(){
													Ext.getCmp('PrAInfoGrid').getView().refresh();
												}
											});	
										},
										// If you don't pass success:true, it will always go here
										failure: function(form, action) {
											Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
										}	
									});	
								}	
							}
						}, {
							text: 'Reset',
							icon:  'src/gxp/theme/img/decline.png', 
							handler: function(){
								// reset the form
								PrAForm.getForm().reset();
							}
						}]
					});	
					
					new Ext.Window({
						title: 'Add a new PrA configuration ...',
						layout: 'form',
						autoHeight: true,
						plain:false,
						modal: true,
						resizable: false,
						closeable: true,
						closeAction:'hide',
						buttonAlign:'right',
						items: [PrAForm]
					}).show();
						
				},
				scope: this
			},{
				text:'Delete',
				iconCls: 'delete',
				handler : function(){
					// if yes, delete the selected records from store and db
					Ext.Msg.show({
						title:'Delete the selected configurations?',
						msg: 'Are you sure to delete the selected configurations? This action is undoable, please consider removing first any referenced scenarios if these configurations are being used in risk calculation!!',
						buttons: Ext.Msg.YESNOCANCEL,
						fn: function(buttonId, text, opt){
							if (buttonId == 'yes'){
								var records = Ext.getCmp('PrAInfoGrid').getSelectionModel().getSelections();
								var ids = [];
								for (var i=0;i<records.length;i++) {
									if (records[i].get('id') != 1){ // if default PrA table, exclude it from the list of selected records
										ids.push(records[i].data);
									}
									else {										
										Ext.Msg.alert('Information', 'The default configuration is not allowed to delete, please exclude it from the list of selected records and try again!');
										return;
									}
								}
								Ext.Ajax.request({   
									url: 'PrAInfo.php',
									params: {
										task: 'delete',
										ws: workspace,
										IDs: Ext.encode(ids)
									},
									success: function(response, opts){
										Ext.Msg.alert('Success', 'The selected PrA configurations have been successfully deleted!');
										// reload the store and refresh
										Ext.getCmp('PrAInfoGrid').getStore().reload({ 
												callback: function(){
													Ext.getCmp('PrAInfoGrid').getView().refresh();
												}
											});
									},
									failure: function(response, opts){
										Ext.Msg.alert('Failed', response.message);
									}
								});
							}
						}
					});	
				}
			}]
		});
		return PrAInfoGrid;
	}

});

Ext.preg(gxp.plugins.Hazards.prototype.ptype, gxp.plugins.Hazards); 