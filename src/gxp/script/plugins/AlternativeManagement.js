/**
 * @require plugins/Tool.js
 * @require widgets/AlternativeFormPanel.js
 */
 
Ext.ns("gxp.plugins");

gxp.plugins.AlternativeManagement = Ext.extend(gxp.plugins.Tool, {

  ptype: "myapp_alternativemanagement",
  
  /** private: method[constructor]
     */
    constructor: function() {
        this.initAltMgtWin();
        gxp.plugins.AlternativeManagement.superclass.constructor.apply(this, arguments);        
    },
	
	/** api: method[showAltMgt]
     * Shows the respective window of the selected tree node.
     */ 
    showAltMgt: function() {
		this.AltInfo_store.reload({ 
			params: 
				{ws: workspace,
				task: 'loadGrid'} // reload the grid with the selected workspace 
		});
		this.altInfoWin.show();
	},
	
	/** api: private method[initAltMgtWin]
     * Initialize the window to show more than one window at a time. 
     */
	initAltMgtWin: function() {
		this.altInfoWin = new Ext.Window({
			title: 'Alternative scenarios information',
			layout: 'fit',		
			plain:false,
			maximizable: true,
			collapsible: true,
			closeable: true,
			closeAction:'hide',
			buttonAlign:'right',
			items: this.initAltInfoGrid()
		});
	},
	
	/** api: private method[initAltInfoGrid]
     * initialization of the grid panel to management the alternative scenarios
     */
	 
	initAltInfoGrid: function(){	
		
		var xg = Ext.grid;		
		this.AltInfo_store = new Ext.data.GroupingStore({
				url: 'altInfo.php',
				sortInfo:{field: 'id', direction: "ASC"},
				groupField:'categorie',
				reader: new Ext.data.JsonReader({
					totalProperty : 'totalCount',
					root          : 'rows',
					successProperty: 'success',
					idProperty    : 'id',
					fields: [					
						{name : 'id', type : 'int'},
						{name : 'nom', type : 'String'},
						{name : 'description', type : 'String'}, 
						{name : 'categorie', type : 'String'}, 
						{name : 'mesures_nom', type : 'String'}, 
						{name : 'mesures_id', type : 'String'}, 
						{name : 'indice', type : 'String'},
						{name : 'remarques', type : 'String'}
					] 
				})
		});		
					
        var altInfoGrid = new xg.GridPanel({
			id: 'altInfoGrid',			
			view: new Ext.grid.GroupingView({
				markDirty: false
			}),
			store: this.AltInfo_store,
			colModel: new Ext.grid.ColumnModel({
				columns: [
					{header: "ID", dataIndex: 'id', hidden: true},
					{header: "Name", dataIndex: 'nom'},
					{header: "Description", dataIndex: 'description'},
					{header: "Option", dataIndex: 'categorie', hidden: true},					
					{header: "Measures", dataIndex: 'mesures_nom'},	
					{header: "Measures ID", dataIndex: 'mesures_id', hidden: true},	
					{header: "Remarks", dataIndex: 'remarques'},
					{header: "Mapping Index", dataIndex: 'indice', hidden: true},
					{
						xtype: 'actioncolumn',
						items: [						
							{
								icon: 'src/gxp/theme/img/silk/map.png',  
								tooltip: 'Visualize',
								handler: function(grid, rowIndex, colIndex) {
									var rec = altInfoGrid.getStore().getAt(rowIndex);
									if (rec.get('categorie') == "descriptive"){
										Ext.Msg.alert('Information', 'The selected alternative is a descriptive scenario and cannot be visualized on the map!');
									}
									else {
										// add the layer to the map	
										var lyr_name = rec.get('indice');
										var names = {};
										names[workspace + ":" + lyr_name] = true;
										
										var source = app.tools.addlyrs.target.layerSources["local"]; // need to reassign the source in case other sources such as google are being added by users
										app.tools.addlyrs.setSelectedSource(source);
										var newRecords = [];
										var last = 0;
										app.tools.addlyrs.selectedSource.store.each(function(record, index) {
											if (record.get("name") in names) {
												last = index;
												newRecords.push(record);
											}
										});										
										app.tools.addlyrs.addLayers(newRecords, false);	
									}
								},
								getClass: function(v, meta, rec) {                              
                                    return 'x-action-col-icon';
                                }
							},
							{
								icon: 'src/gxp/theme/img/silk/table_edit.png',  
								tooltip: 'Edit Data',
								handler: function(grid, rowIndex, colIndex) {									
									var rec = altInfoGrid.getStore().getAt(rowIndex);
								//	alert("Save this alternative: " + rec.get('alternative_name') + rec.get('alternative_description'));	
									
									var altEditWin = new Ext.Window({
										title: 'Edit the alternative scenario..',
										layout: 'form',
										autoHeight: true,
										plain:false,
										modal: true,
										resizable: false,
										closeable: true,
										closeAction:'hide',
										buttonAlign:'right',
										items: new Ext.form.FormPanel({
											labelWidth: 75, // label settings here cascade unless overridden
											itemId: 'altEditForm',
											frame:true,			
											bodyStyle:'padding:5px 5px 0',
											width: 350,	
											autoHeight:true,		
											defaults: {
												anchor: '100%'
											},
											items: [{
												fieldLabel: 'Name',
												xtype: 'textfield',
												name: 'name',
												allowBlank: false,
												value: rec.get('nom')
											},{
												fieldLabel: 'Description',
												xtype: 'textarea',
												name: 'description',
												allowBlank: true,
												value: rec.get('description')
											},{
												fieldLabel: 'Measures',
												name: 'measures',
												hiddenName: 'measureIDs',
												xtype: 'lovcombo',
												mode: "local",
												editable: false,
												allowBlank: false,
												hideOnSelect:false,
												value: rec.get('mesures_id'),
												store: new Ext.data.ArrayStore({
														id: 0,
														fields: [
															'myId',  // numeric value is the key
															'displayText'
														],
														data: [[1, 'Galerie'], [2, 'Tunnel'],[3,'Galerie de protection contre les crues'],[4,'Ouvrage de stabilisation de la neige'],
														[5,'Installation de minage'],[6,'Filet de protection contre les chutes de pierres'],[7,'Digue en terre'],[8,'Ouvrage temporaire/ouvrage en bois'],
														[9,'Ouvrage de stabilisation des pentes'],[10,'Reboisements'],[11,'Barrage de correction torrentielle en bois'],[12,'Barrage de correction torrentielle en beton'],
														[13,'Filets de protection contre les laves torrentielles'],[14,'Rateliers en bois'],[15,'Barrage sur rivere de plaine et depotoir a alluvions en beton'],
														[16,'Maintient de la foret protectrice'],[17,"Mesures a l'objet"],[18,'Autre']]  // data is local
												}),
												valueField: 'myId',
												displayField: 'displayText',
												triggerAction: 'all'
											},{
												fieldLabel: 'Remarks',
												xtype: 'textarea',
												name: 'remarks',
												allowBlank: true,
												value: rec.get('remarques')
											}],
											buttons: [{
												text: 'Update',
												icon:  'src/gxp/theme/img/silk/table_save.png',
												handler: function(){
													var form = altEditWin.getComponent('altEditForm').getForm();				
													if (form.isValid()) {
														form.submit({
															url: 'altInfo.php',
															params: {
																task: 'edit',
																ws: workspace,
																mpIndex: rec.get('indice'),
																ID: rec.get('id'),
																option: rec.get('categorie')
															},
															waitMsg : 'Please wait...',
															success: function(form, action) {
																Ext.Msg.alert('Success', action.result.message);
																altEditWin.close();
																Ext.getCmp('altInfoGrid').getStore().reload({ // refresh the grid view
																	callback: function(){
																	Ext.getCmp('altInfoGrid').getView().refresh();
																	}
																});
															},
															failure: function(form, action) {
																Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
															}
														});
													}
													else {
														Ext.Msg.alert('Validation Error', 'The form is not valid!' );
													}
												}
											},{
												text: 'Cancel',
												icon:  'src/gxp/theme/img/decline.png',
												handler: function(){
													altEditWin.close();
												}
											}]
										})											
									});
									
									altEditWin.show();
								}
							}
						]
					}
				],
				defaults: {
					sortable: true,
					menuDisabled: false,
					width: 5
				}
			}),			
			view: new Ext.grid.GroupingView({				
				forceFit: true,
				groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
			}),						
			frame:true,
			stripeRows: true,
			width: 700,
			height: 450,
			title: 'Available Alternative Scenarios',
			iconCls: 'icon-grid',
			fbar: ['->', {
				text:'Add New',
				iconCls: 'add',
				handler : function(){ // show a new window to create a new scenario (with sketch, descriptive or upload option) 					
						var alt_form = new gxp.AlternativeFormPanel({
							url: "/geoserver/rest",
							width: 450,
							frame: true,
							autoHeight: true,
							bodyStyle: "padding: 10px 10px 0 10px;",
							labelWidth: 65,
							defaults: {
								anchor: "95%",
								allowBlank: false,
								msgTarget: "side"
							},
							listeners: {
								uploadcomplete: function(form, detail) {
									var formData = form.getForm().getFieldValues();
									var mp_index = [];									
									var layers = detail["import"].tasks;
                                    var item, names = {}, resource, layer;
                                    for (var i=0, len=layers.length; i<len; ++i) {
                                        item = layers[i];
                                        if (item.state === "ERROR") {
                                            Ext.Msg.alert(item.layer.originalName, item.errorMessage);
                                            return;
                                        }
                                        var ws;
                                        if (item.target.dataStore) {
                                            ws = item.target.dataStore.workspace.name;
                                        } else if (item.target.coverageStore) {
                                            ws = item.target.coverageStore.workspace.name;
                                        }
                                        names[ws + ":" + item.layer.name] = true;
										mp_index.push(ws + ":" + item.layer.name);
                                    }
									
									var source = app.tools.addlyrs.target.layerSources["local"]; // need to reassign the source in case other sources such as google are being added by users
									app.tools.addlyrs.setSelectedSource(source);
									app.tools.addlyrs.selectedSource.store.load({
										params: {"_dc": Math.random()},
										callback: function(records, options, success) {
											var gridPanel, sel;
											if (app.tools.addlyrs.capGrid && app.tools.addlyrs.capGrid.isVisible()) {
													gridPanel = app.tools.addlyrs.capGrid.get(0).get(0);
													sel = gridPanel.getSelectionModel();
													sel.clearSelections();
											}
											// select newly added layers
											var newRecords = [];
											var last = 0;
											app.tools.addlyrs.selectedSource.store.each(function(record, index) {
												if (record.get("name") in names) {
													last = index;
													newRecords.push(record);
												}
											});
											if (gridPanel) {
													// this needs to be deferred because the 
													// grid view has not refreshed yet
													window.setTimeout(function() {
														sel.selectRecords(newRecords);
														gridPanel.getView().focusRow(last);
													}, 100);
											} else {
													app.tools.addlyrs.addLayers(newRecords, true);
												}                                                                                    
											},
										scope: this                                            
									});
									
									//saving meta data to alternative table in the selected workspace 
									Ext.Ajax.request({
										url:'altInfo.php',
										params: {
											task: 'add',
											ws: workspace,
											names: Ext.encode(mp_index),
											name: formData.title,
											description: formData["abstract"],
											option: formData.option,
											measures: formData.measures,
											remarks: formData.remarks													
										},
										timeout: 300000, // <<-- Timeout value specified here in milliseconds
										success: function(response, opts) {
												form.getForm().reset();
												form.optionsFieldset.setVisible(false);  
												form.sketch.setVisible(false); 
													
												var obj = Ext.decode(response.responseText);
												Ext.Msg.alert('Information', obj.message);
												Ext.getCmp('altInfoGrid').getStore().reload({ //refresh the grid with newly added layer
													callback: function(){
													Ext.getCmp('altInfoGrid').getView().refresh();
													}
												});
											},
										failure: function(response, opts) {
												console.log('server-side failure with status code ' + response.status);
											}
									});
								}	
							}	
						});
		
						var altAddWin = new Ext.Window({
							title: 'Add a new alternative scenario..',
							layout: 'form',
							autoHeight: true,
							plain:false,
							modal: true,
							resizable: false,
							closeable: true,
							closeAction:'hide',
							buttonAlign:'right',
							items: [alt_form]
						});
						
						altAddWin.show();
					},
				scope: this
			},{
				text:'Delete',
				iconCls: 'delete',	
				handler : function(){
					// delete the records from store, db, geoserver
					Ext.Msg.show({
						title:'Delete the alternatives?',
						msg: 'Are you sure to delete the selected alternatives? This action is undoable. It will remove associated cost sheets and hazard/object layers if being referenced!!!',
						buttons: Ext.Msg.YESNOCANCEL,
						fn: function(buttonId, text, opt){
							if (buttonId == 'yes'){
								var records = Ext.getCmp('altInfoGrid').getSelectionModel().getSelections();
								var temp =[];
								for (var i=0;i<records.length;i++) {									
									temp.push(records[i].data);
								}
								Ext.Ajax.request({   
									url: 'altInfo.php',
									params: {
										task: 'delete',
										ws: workspace,
										altRecords: Ext.encode(temp)
									},
									success: function(response, opts){
										var obj = Ext.decode(response.responseText);	
										if (obj.success == 'true') {
											Ext.Msg.alert('Success', obj.message);
											// reload the store and refresh
											Ext.getCmp('altInfoGrid').getStore().reload({ 
												callback: function(){
													Ext.getCmp('altInfoGrid').getView().refresh();
												}
											});
										}
										else {
											Ext.Msg.alert('Failed', obj.message);
										}										
									},
									failure: function(response, opts){
										Ext.Msg.alert('Failed', response.message);
									}
								});
							}
						}
					});					
				}
			}]
		});
		return altInfoGrid;
	}
});

Ext.preg(gxp.plugins.AlternativeManagement.prototype.ptype, gxp.plugins.AlternativeManagement);