/**
 * @require plugins/Tool.js
 */
 
Ext.ns("gxp.plugins");

gxp.plugins.CostManagement = Ext.extend(gxp.plugins.Tool, {

  ptype: "myapp_cost",
  
  /** private: method[constructor]
     */
    constructor: function() {
        this.initCostMgtWin();
        gxp.plugins.CostManagement.superclass.constructor.apply(this, arguments);        
    },
	/** api: method[showCostMgt]
     * Shows the respective window for the selected tree node.
     */ 
    showCostMgt: function() {
		this.CostInfo_store.reload({ 
			params: 
				{ws: workspace,
				task: 'load'} // reload the grid with the selected workspace 
		});
		this.costInfoWin.show();
	},
	/** api: private method[initCostMgtWin]
     * Initialize the window to show more than one window at a time. 
     */
	initCostMgtWin: function() {
		this.costInfoWin = new Ext.Window({
			title: 'Cost sheets information',
			layout: 'fit',		
			plain:false,
			maximizable: true,
			collapsible: true,
			closeable: true,
			closeAction:'hide',
			buttonAlign:'right',
			items: this.initCostInfoGrid()
		});
	},
	
	/** api: private method[initCostInfoGrid]
     * initialization of the grid panel to manage the cost calculation sheet
     */
	 
	initCostInfoGrid: function(){
	
	var cost_form = new Ext.form.FormPanel({
			labelWidth: 75, // label settings here cascade unless overridden
			url:'costInfo.php',
			frame:true,			
			bodyStyle:'padding:5px 5px 0',
			width: 485,	
			autoHeight:true,		
			items: [{
				xtype:'fieldset',
				title: 'Sheet Information',
				collapsible: false,
				defaultType: 'textfield',
				defaults: {
					anchor: "95%",					
					msgTarget: "side"
				},
				collapsed: false,
				items :[{
						fieldLabel: 'Name',
						name: 'name_sheet',
						emptyText: 'Name of the sheet..',
						allowBlank: false
					},{
						fieldLabel: 'Description',
						xtype: 'textarea',
						name: 'description_sheet',
						emptyText: 'Description of the sheet..',
						allowBlank: true
					},{
						fieldLabel: 'Alternative',
						id: 'cost_alt',
						name: 'altName',
						submitValue : true,
						hiddenName:'altID', 
						xtype: 'combo',
						mode: "local",
						editable: false,
						allowBlank: false,
						store: new Ext.data.JsonStore({ 
							url: 'altInfo.php',							
							autoLoad: true,
							root: 'rows',
							fields : ['id','nom']
						}),
						valueField: 'id',
						displayField: 'nom',
						emptyText: 'select the alternative..',
						triggerAction: 'all'	
					},{
						fieldLabel: 'Remarks',
						xtype: 'textarea',
						name: 'remarks_sheet',
						emptyText: 'If any other remarks (optional)..',
						allowBlank: true
					}]	
				}],
			buttons: [{
				text: 'Add',
				icon:  'src/gxp/theme/img/silk/table_add.png',
				handler: function(){
					var form = cost_form.getForm();
					if (form.isValid()) {						
						form.submit({
							url: 'costInfo.php',
							params: {
								task: 'add',
								ws: workspace
							},
							waitMsg : 'Please wait...',
							success: function(form, action) {
								Ext.Msg.alert('Success', action.result.message);	
								form.reset(); 
								Ext.getCmp('costInfoGrid').getStore().reload({ // refresh the grid view
									callback: function(){
									Ext.getCmp('costInfoGrid').getView().refresh();
									}
								});	
							},
							// If you don't pass success:true, it will always go here
							failure: function(form, action) {
								Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
							}
						});	
					}
					else {
						Ext.Msg.alert('Validation Error', 'The form is not valid!' );
					}
				}
			},{
				text: 'Cancel',
				icon:  'src/gxp/theme/img/decline.png', 
				handler: function(){
					// reset the form and close the dialog box 
					cost_form.getForm().reset();				
					costAddWin.hide();
				}
			}]		
	});	
	
	var costAddWin = new Ext.Window({
			title: 'Add a new sheet for cost estimation ...',
			layout: 'form',
			autoHeight: true,
			plain:false,
			modal: true,
			resizable: false,
			closeable: true,
			closeAction:'hide',
			buttonAlign:'right',
			items: [cost_form]
		});
		
	var xg = Ext.grid;		
	this.CostInfo_store = new Ext.data.GroupingStore({
		url: 'costInfo.php',
		sortInfo:{field: 'id', direction: "ASC"},
		groupField:'alt_id',
		reader: new Ext.data.JsonReader({
			totalProperty : 'totalCount',
			root          : 'rows',
			successProperty: 'success',
			idProperty    : 'id',
			fields: [					
				{name : 'id', type : 'int'},
				{name : 'alt_id', type : 'int'},
				{name : 'alt_nom', type : 'String'},
				{name : 'nom', type : 'String'},
				{name : 'description', type : 'String'}, 
				{name : 'remarques', type : 'String'},
				{name : 'cout_annuel', type : 'float'}
			] 
		})
	});	
	
	var costInfoGrid = new xg.GridPanel({
			id: 'costInfoGrid',			
			view: new Ext.grid.GroupingView({
				markDirty: false
			}),
			store: this.CostInfo_store,
			colModel: new Ext.grid.ColumnModel({
				columns: [
					{header: "Cost ID", dataIndex: 'id', hidden: true},
					{header: "Alt ID", dataIndex: 'alt_id', hidden: true},
					{header: "Name", dataIndex: 'nom'},
					{header: "Description", dataIndex: 'description'},
					{header: "Alternative Scenario", dataIndex: 'alt_nom'},					
					{header: "Remarks", dataIndex: 'remarques'},
					{header: "Annual Cost", dataIndex: 'cout_annuel', renderer: function(v) {	// plug Swiss Franc currency renderer into formatter
						v = (Math.round((v-0)*100))/100;
						v = (v == Math.floor(v)) ? v + ".00" : ((v*10 == Math.floor(v*10)) ? v + "0" : v);
						return (v + ' CHF');
					}},
					{
						xtype: 'actioncolumn',
						items: [
							{
								icon: 'src/gxp/theme/img/silk/calculator_edit.png',  
								tooltip: 'Edit/Calculate Values',
								handler: function(grid, rowIndex, colIndex) {
									var rec = costInfoGrid.getStore().getAt(rowIndex);	
									
									var summary = new Ext.ux.grid.GroupSummary();								
									
									Ext.util.Format.frMoney = function(v) 	// plug Swiss Franc currency renderer into formatter
									{
										v = (Math.round((v-0)*100))/100;
										v = (v == Math.floor(v)) ? v + ".00" : ((v*10 == Math.floor(v*10)) ? v + "0" : v);
										return (v + ' CHF');
									};
									
									Ext.ux.grid.GroupSummary.Calculations['totalCost'] = function(v, record, field){
										if (record.data.mesure_id == 16) {
											return v + parseFloat(record.get('cout_entretien'));															
										}
										else {
											return v + parseFloat(record.get('cout_entretien'))+ parseFloat(record.get('cout_exploitation'))+ parseFloat(record.get('cout_reparation')) + ((parseFloat(record.get('cout_investissement')) - parseFloat(record.get('valeur_residuelle')))/parseFloat(record.get('periode_ans')))+((parseFloat(record.get('cout_investissement')) + parseFloat(record.get('valeur_residuelle')))*parseFloat(record.get('taux_interet'))*0.005);
										}
									};
										
									var customRender = Ext.util.Format.frMoney;									
									var KbRender = function(v, params, record){
										if (record.data.mode == 'Auto') {
											switch (record.data.mesure_id) {
												case 1:
												case 4:
												case 6:
												case 7:
												case 8:
												case 9:
												case 10:
												case 11:
												case 12:
												case 18:
													var temp = parseFloat(record.get('cout_investissement')) * 0;
													record.set('cout_exploitation',	temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;
												case 2:
												case 3:
													var temp = parseFloat(record.get('cout_investissement')) * 0.005;
													record.set('cout_exploitation',	temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;
												case 5:
													var temp = parseFloat(record.get('cout_investissement')) * 0.05;
													record.set('cout_exploitation',	temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;
												case 13:
												case 15:
													var temp = parseFloat(record.get('cout_investissement')) * 0.01;
													record.set('cout_exploitation',	temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;
												case 14:
													var temp = parseFloat(record.get('cout_investissement')) * 0.02;
													record.set('cout_exploitation',	temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;												
											}
										}
										else {
											if (record.data.mesure_id == 16) {
												params.css = "disable-cell" ; //this changes the cell background-color 
											//	return '<span style="color:#000000; font-weight =bold">' + customRender(v) + '</span>';
											}
											else return customRender(v);
										}								
									};	
									
									var KuRender = function(v, params, record){
										if (record.data.mode == 'Auto') {
											switch (record.data.mesure_id) {
												case 1:
													var temp = parseFloat(record.get('cout_investissement')) * 0.015 * 0.5;
													record.set('cout_entretien', temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;
												case 2:
												case 6:
												case 8:
												case 10:
												case 11:
												case 12:
													var temp = parseFloat(record.get('cout_investissement')) * 0.02 * 0.5;
													record.set('cout_entretien', temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;
												case 3:
												case 7:
													var temp = parseFloat(record.get('cout_investissement')) * 0.005 * 0.5;
													record.set('cout_entretien', temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;
												case 4:
												case 9:
												case 14:
												case 15:
													var temp = parseFloat(record.get('cout_investissement')) * 0.01 * 0.5;
													record.set('cout_entretien', temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;
												case 5:
													var temp = parseFloat(record.get('cout_investissement')) * 0.04 * 0.5;
													record.set('cout_entretien', temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;
												case 13:
													var temp = parseFloat(record.get('cout_investissement')) * 0.03 * 0.5;
													record.set('cout_entretien', temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;
												case 18:
													var temp = parseFloat(record.get('cout_investissement')) * 0 * 0.5;
													record.set('cout_entretien', temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;
											}
										}
										else {
											return customRender(v);
											// if (record.data.mesure_id == 16) {
												// record.set('cout_entretien',v);
												// return customRender(v);		
											// }
											// else return customRender(v);
										}																
									};	
									
									var KrRender = function(v, params, record){
										if (record.data.mode == 'Auto') {
											switch (record.data.mesure_id) {
												case 1:
													var temp = parseFloat(record.get('cout_investissement')) * 0.015 * 0.5;
													record.set('cout_reparation', temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;
												case 2:
												case 6:
												case 8:
												case 10:
												case 11:
												case 12:
													var temp = parseFloat(record.get('cout_investissement')) * 0.02 * 0.5;
													record.set('cout_reparation', temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;
												case 3:
												case 7:
													var temp = parseFloat(record.get('cout_investissement')) * 0.005 * 0.5;
													record.set('cout_reparation', temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;
												case 4:
												case 9:
												case 14:
												case 15:												
													var temp = parseFloat(record.get('cout_investissement')) * 0.01 * 0.5;
													record.set('cout_reparation', temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;
												case 5:
													var temp = parseFloat(record.get('cout_investissement')) * 0.04 * 0.5;
													record.set('cout_reparation', temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;
												case 13:
													var temp = parseFloat(record.get('cout_investissement')) * 0.03 * 0.5;
													record.set('cout_reparation', temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;
												case 18:
													var temp = parseFloat(record.get('cout_investissement')) * 0 * 0.5;
													record.set('cout_reparation', temp);
													params.css = "disable-cell" ; //this changes the cell background-color 
													return '<span style="color:#000000; font-weight =bold">' + customRender(temp) + '</span>';
													break;
											}
										}
										else {
											if (record.data.mesure_id == 16) {
												params.css = "disable-cell" ; //this changes the cell background-color 
											//	return '<span style="color:#000000; font-weight =bold">' + customRender(v) + '</span>';
											}
											else return customRender(v);											
										}	
									};	
									
									var costEditGrid = new Ext.grid.EditorGridPanel({
										id: 'costEditGrid',										
										title: 'Calculation of Cost for a certain alternative scenario',
										store: new Ext.data.GroupingStore({
											url: 'costInfo.php',
											baseParams: {
												task: 'loadCostValues',
												ws: workspace,
												costID: rec.get('id')												
											},
											sortInfo:{field: 'mesure_id', direction: "ASC"},
											groupField:'alt_id',
											reader: new Ext.data.JsonReader({
												totalProperty : 'totalCount',
												root          : 'rows',
												successProperty: 'success',
												idProperty    : 'id',
												fields: [ 					
													{name : 'id', type : 'int'},
													{name : 'cout_id', type : 'int'},
													{name : 'alt_id', type : 'int'},
													{name : 'mesure_id', type : 'int'},
													{name : 'ouvrage', type : 'String'},
													{name : 'mode', type : 'String'},
													{name : 'cout_investissement', type : 'String'}, 
													{name : 'cout_exploitation', type : 'String'}, 
													{name : 'cout_entretien', type : 'String'},
													{name : 'cout_reparation', type : 'String'},
													{name : 'periode_ans', type : 'int'},
													{name : 'valeur_residuelle', type : 'String'},
													{name : 'taux_interet', type : 'int'},
													{name : 'cout_annuel', type : 'String'}
												]
											}),
											autoLoad: true
										}),
										colModel: new Ext.grid.ColumnModel({
											defaults: {
												width: 120,
												sortable: true
											},
											columns: [
												{header: "ID", sortable: true, dataIndex: 'id', hidden: true},
												{header: "Cost ID", sortable: true, dataIndex: 'cout_id', hidden: true},
												{header: "Alt ID", sortable: true, dataIndex: 'alt_id', hidden: true},
												{header: "Measure ID", sortable: true, dataIndex: 'mesure_id', hidden: true},
												{header: "Type of the measure", width: 100, sortable: true, dataIndex: 'ouvrage'},
												{header: "Mode", width: 40, sortable: true, dataIndex: 'mode', editable: true, 
													editor: new Ext.form.ComboBox({
													store: new Ext.data.ArrayStore({
														fields: ["value", "text"],
														data: [
															['Auto', "Auto"],
															['Manuel', "Manual"]
														]
													}),
													valueField: "value",
													displayField: "text",
													mode: "local",
													triggerAction: 'all',
													allowBlank: false
												}),
													renderer: function(v, params, record){
													if (record.data.mesure_id == 16 || record.data.mesure_id ==17) {
														params.css = "disable-cell" ; //this changes the cell background-color 
														return '<span style="color:#000000; font-weight =bold">' + v + '</span>';
													}
													else return v;
												}},												
												{header: "Investment Cost", width: 40, sortable: true,   dataIndex: 'cout_investissement', renderer: function(v, params, record){
													if (record.data.mesure_id == 16) {
														params.css = "disable-cell" ; //this changes the cell background-color 
													//	return '<span style="color:#000000; font-weight =bold">' + customRender(v) + '</span>';
													}
													else return customRender(v);
												}, editable: true, editor: new Ext.form.NumberField()},
												{header: "Operating Cost", width: 40, sortable: true,   dataIndex: 'cout_exploitation', renderer: KbRender, editable: true, editor: new Ext.form.NumberField()},
												{header: "Maintenance Cost", width: 40, sortable: true,   dataIndex: 'cout_entretien', renderer: KuRender, editable: true, editor: new Ext.form.NumberField()},
												{header: "Repair Cost", width: 40, sortable: true,   dataIndex: 'cout_reparation', renderer: KrRender, editable: true, editor: new Ext.form.NumberField()},
												{header: "No. of effective years", width: 40, sortable: true, dataIndex: 'periode_ans', editable: true, editor: new Ext.form.NumberField({allowNegative: false,allowBlank: false}), renderer: function(v,params, record){
													if (record.data.mode == 'Auto') {
														switch (record.data.mesure_id) {
															case 1:
															case 2:
															case 4:
															case 15:
																record.set('periode_ans',80);
																params.css = "disable-cell" ; //this changes the cell background-color 
																return '<span style="color:#000000; font-weight =bold">' + 80 + ' years' + '</span>';																
																break;
															case 3:
															case 7:
																record.set('periode_ans',100);
																params.css = "disable-cell" ; //this changes the cell background-color 
																return '<span style="color:#000000; font-weight =bold">' + 100 + ' years' + '</span>';
																break;
															case 5:
															case 18:
																record.set('periode_ans',20);
																params.css = "disable-cell" ; //this changes the cell background-color 
																return '<span style="color:#000000; font-weight =bold">' + 20 + ' years' + '</span>';
																break;
															case 6:
															case 9:
															case 12:
															case 14:
																record.set('periode_ans',50);
																params.css = "disable-cell" ; //this changes the cell background-color 
																return '<span style="color:#000000; font-weight =bold">' + 50 + ' years' + '</span>';
																break;
															case 8:
															case 10:
															case 11:
															case 13:
																record.set('periode_ans',30);
																params.css = "disable-cell" ; //this changes the cell background-color 
																return '<span style="color:#000000; font-weight =bold">' + 30 + ' years' + '</span>';
																break;															
														}														
													}
													else {														
														if (record.data.mesure_id == 16) {
															params.css = "disable-cell" ; //this changes the cell background-color 
														//	return '<span style="color:#000000; font-weight =bold">' + v + ' years' + '</span>';
														}
														else return v +' years';
													}
												}},
												{header: "Residual Value (after N years)", width: 40, sortable: true, dataIndex: 'valeur_residuelle', editable: true, editor: new Ext.form.NumberField(), renderer: function(v, params, record){
													if (record.data.mode == 'Auto') {
														record.set('valeur_residuelle',0);
														params.css = "disable-cell" ; //this changes the cell background-color 
														return '<span style="color:#000000; font-weight =bold">' + customRender(0) + '</span>';
													}
													else {
														if (record.data.mesure_id == 16) {
															params.css = "disable-cell" ; //this changes the cell background-color 
														//	return '<span style="color:#000000; font-weight =bold">' + customRender(v) + '</span>';
														}
														else return customRender(v);
													}
												}},
												{header: "Interest Rate (%)", width: 40, sortable: true, dataIndex: 'taux_interet', editable: true, editor: new Ext.form.NumberField({allowNegative: false,allowBlank: false}), renderer: function(v, params, record){
													if (record.data.mode == 'Auto') {
														record.set('taux_interet',2);
														params.css = "disable-cell" ; //this changes the cell background-color 
														return '<span style="color:#000000; font-weight =bold">' + '2 %' + '</span>';
													}
													else {
														if (record.data.mesure_id == 16) {
															params.css = "disable-cell" ; //this changes the cell background-color 
														//	return '<span style="color:#000000; font-weight =bold">' + v +' %' + '</span>';
														}
														else return v +' %';
													}
												}},
												{header: "Annual Cost Total", width: 40, sortable: true, dataIndex: 'cout_annuel', summaryType: 'totalCost', summaryRenderer: customRender, renderer: function(v, params, record){
														if (record.data.mesure_id == 16) {
															var total = parseFloat(record.get('cout_entretien'));															
														}
														else {
															var total = parseFloat(record.get('cout_entretien'))+ parseFloat(record.get('cout_exploitation'))+ parseFloat(record.get('cout_reparation')) + ((parseFloat(record.get('cout_investissement')) - parseFloat(record.get('valeur_residuelle')))/parseFloat(record.get('periode_ans')))+((parseFloat(record.get('cout_investissement')) + parseFloat(record.get('valeur_residuelle')))*parseFloat(record.get('taux_interet'))*0.005);
														}	
													//	var total = parseFloat(record.data.cout_entretien)+ parseFloat(record.data.cout_exploitation)+ parseFloat(record.data.cout_reparation) + ((parseFloat(record.data.cout_investissement) - parseFloat(record.data.valeur_residuelle))/parseFloat(record.data.periode_ans))+((parseFloat(record.data.cout_investissement) + parseFloat(record.data.valeur_residuelle))*parseFloat(record.data.taux_interet)*0.005);
													//	record.set('cout_annuel',total);
														params.css = "disable-cell" ; //this changes the cell background-color 
														return '<span style="color:#000000; font-weight =bold">' + customRender(total) + '</span>';
													}
												}
											]	
										}),
										view: new Ext.grid.GroupingView({
											forceFit: true,
											showGroupName: false,
											enableNoGroups: false,
											enableGroupingMenu: false,
											hideGroupedColumn: true
										}),
										// viewConfig: {														
											// forceFit: true
										// },										
										plugins: summary,									
										sm: new Ext.grid.RowSelectionModel({singleSelect:true}),												
										height: 300,													
										frame: true,
										stripeRows : true,
										footer: true,
										region: 'center',
										split: true,
										listeners: {
											beforeedit: function(e){
												if (e.record.get('mesure_id') == 17) {
													// disable editing for 'mode' combo box, in case of measure no. 16 (all other columns are allowed) 
													if (e.column == 5) return false;
													else return true;
												}
												else if (e.record.get('mesure_id') == 16) {
													// disable editing for 'mode' combo box, in case of measure no. 17 (only maintenance cost is allowed)
													if (e.column == 8) return true;
													else return false;
												}
												else {
													// enable editing for 'mode' combo box, in case of all other measures
													if (e.column == 5) return true;
													
													// if the selected cell is not 'mode' column 
													if (e.record.get('mode') == 'Auto') {
														if (e.column == 6) { 
															return true; // enable editing for investment cost column only
														}
														else {
															return false; // disable editing for other columns
														}
													}
													else {
														return true;	
													}			
												}
																																	
											},											
											'rowcontextmenu' : function(costEditGrid, index, event) {
												// when the user right-click on the row
												var record = costEditGrid.getStore().getAt(index);
												event.stopEvent();
												var menu = new Ext.menu.Menu({
													items: [{
														text: 'Add Measure',
														handler: function() {
														// add a measure to the table with default values and refresh the grid
														var mesWin = new Ext.Window({
															title: 'Selection of the measure',
															autoHeight:true,
															width: 240,	
															layout: 'hbox',		
															modal: true,
															plain:false,
															closable: true,
															resizable : false,
															buttonAlign:'center',													
															items: [{			
																name: 'measure',
																hiddenName: 'measureID',
																xtype: 'combo',			
																margins: {top:5, right:5, bottom:5, left:5},
																mode: "local",
																editable: false,
																allowBlank: false,
																store: new Ext.data.JsonStore({ 
																	url: 'costInfo.php',
																	baseParams: {																
																		ws: workspace,
																		task: 'loadSelectedMeasures',
																		costID: rec.get('id')
																	},
																	root: 'rows',
																	autoLoad: true,
																	fields : ['id','ouvrage']																		
																}),						
																valueField: 'id',
																displayField: 'ouvrage',																	
																triggerAction: 'all'
															}],
															buttons: [{
																text: 'Add',
																iconCls: 'add',
																handler: function() {
																	var combo = mesWin.items.items[0];
																	if (combo.isValid()){
																		// add the new records to the 'cost_values' table
																		Ext.Ajax.request({
																			url:'costInfo.php',
																			params: {
																				task: 'insertMeasure',
																				ws: workspace,
																				costID: rec.get('id'),
																				altID: rec.get('alt_id'),
																				measureID: combo.getValue()
																			},																						
																			success: function(response, opts) {																				
																				var obj = Ext.decode(response.responseText);										
																				Ext.Msg.alert('Success', obj.message);	// show the success message	
																				mesWin.close();
																				costEditGrid.getStore().reload({ params: 
																					{ ws: workspace,
																					task: 'loadCostValues'} // reload the store with the selected workspace 
																				});
																			}
																		});
																	}
																	else {
																		Ext.Msg.alert('Validation Error', 'The form is not valid!' );
																	}
																}
															}]
														});
														
														mesWin.show();		
														
														}
													},{
														text: 'Delete Measure',
														handler: function() {
														// if yes, remove the measure row from the store and delete it from the tables
															Ext.Msg.show({
																title:'Delete Measure?',
																msg: 'Are you sure to delete this measure: "'+ record.get('ouvrage') + '" from the sheet?',
																buttons: Ext.Msg.YESNOCANCEL,
																fn: function(buttonId, text, opt){
																	if (buttonId == 'yes'){																		
																		// Ajax request to delete the row
																		Ext.Ajax.request({
																			url:'costInfo.php',
																			params: {
																				task: 'removeMeasure',
																				ws: workspace,
																				id: record.get('id')
																			},																						
																			success: function(response, opts) {
																				costEditGrid.getStore().remove(record); // remove the record from the store
																				Ext.getCmp('costInfoGrid').getStore().reload({ // refresh the cost sheet grid view
																					callback: function(){
																						Ext.getCmp('costInfoGrid').getView().refresh();
																					}
																				});
																				var obj = Ext.decode(response.responseText);										
																				Ext.Msg.alert('Success', obj.message);	// show the success message					
																			}
																		});
																	}
																},
																animEl: 'elId',
																icon: Ext.MessageBox.QUESTION
															});
														}
													}]
												}).showAt(event.getXY());													
											}
										},
										buttons: [
										{
											text: 'Add Row', 
											hidden: true,
											iconCls: 'add',
											handler: function(){
												// add an empty default record to the 'cost_values' table
												Ext.Ajax.request({
														url:'costInfo.php',
														params: {
															task: 'insertMeasure',
															ws: workspace,
															costID: rec.get('cout_id'),
															altID: rec.get('alt_id')
														},
														success: function(response, opts) {
															var obj = Ext.decode(response.responseText);										
															Ext.Msg.alert('Success', obj.message);						
															costEditGrid.getStore().reload({ params: 
																{ ws: workspace,
																 task: 'loadCostValues'} // reload the store with the selected workspace 
															});
														}
												});
											}
										},{
											text: 'Remove',
											iconCls: 'delete',
											hidden: true,
											handler: function(){
												// remove the selected row from the 'cost_values' table
											}
										},{
											text: 'Save Values',
											iconCls: 'save',
											handler: function(){
												// to POST only the dirty records to update the values
												var records = [];
												var costEditStore = costEditGrid.getStore();
												for (var i= 0;i < costEditStore.data.length;i++) {
													recs = costEditStore.data.items[i];
													if (recs.dirty == true){
														records.push((recs.data));
													//	console.dir(recs.data);
													}
												}
												
												if (records.length != 0) {
													Ext.Ajax.request({
														url:'costInfo.php',
														params: {
															task: 'save',
															ws: workspace
														},
														jsonData: Ext.util.JSON.encode(records),
														success: function(response, opts) {
															costEditStore.commitChanges(); // unmark the dirty records and commit all changes to the records
															var obj = Ext.decode(response.responseText);										
															Ext.Msg.alert('Success', obj.message);	
															Ext.getCmp('costInfoGrid').getStore().reload({ // refresh the cost sheet grid view
																callback: function(){
																	Ext.getCmp('costInfoGrid').getView().refresh();
																}
															});
														}
													});
												}
												else { Ext.Msg.alert('Information', 'There are no new values to be saved!');
												}
											}
										}]
									});	
									
									var costEditWin = new Ext.Window({
										title: 'Edit/Calculate the cost estimation sheet:',										
										width: 500,
										height: 400,
										minWidth: 300,
										minHeight: 200,
										layout: 'border',
										plain:true,
										maximizable: true,
										collapsible: true,
										closeable: true,
										closeAction:'hide',
										buttonAlign:'right',
										items: [ new Ext.form.FormPanel({
											labelWidth: 75, // label settings here cascade unless overridden
											itemId: 'costEditForm',
											region: 'north',
											frame:true,			
											width: 350,	
											autoHeight:true,	
											collapsible: true,
											isCollapsed: false,
											collapseMode: 'mini',
											defaults: {
												anchor: '100%'
											},
											items: [{
												fieldLabel: 'Name',
												xtype: 'textfield',
												name: 'name_sheet',
												allowBlank: false,
												value: rec.get('nom')
											},{
												fieldLabel: 'Description',
												xtype: 'textarea',
												name: 'description_sheet',
												allowBlank: true,
												value: rec.get('description')
											},{
												fieldLabel: 'Remarks',
												xtype: 'textarea',
												name: 'remarks_sheet',
												allowBlank: true,
												value: rec.get('remarques')
											}],
											buttons: [{
												text: 'Update',
												icon:  'src/gxp/theme/img/silk/table_save.png',
												handler: function(){
													var form = costEditWin.getComponent('costEditForm').getForm();				
													if (form.isValid()) {
														form.submit({
															url: 'costInfo.php',
															params: {
																task: 'editInfo',
																ws: workspace,
																ID: rec.get('id')
															},
															waitMsg : 'Please wait...',
															success: function(form, action) {
																Ext.Msg.alert('Success', action.result.message);
																Ext.getCmp('costInfoGrid').getStore().reload({ // refresh the grid view
																	callback: function(){
																		Ext.getCmp('costInfoGrid').getView().refresh();
																	}
																});
															},
															failure: function(form, action) {
																Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
															}
														});
													}
													else {
														Ext.Msg.alert('Validation Error', 'The form is not valid!' );
													}
												}
											}]
										}), costEditGrid
									]
									});	
									
									costEditWin.show();
								},
								getClass: function(v, meta, rec) {                              
                                    return 'x-action-col-icon';
                                }
							}
						]	
					}
				],
				defaults: {
					sortable: true,
					menuDisabled: false,
					width: 5
				}
			}),			
			view: new Ext.grid.GroupingView({				
				forceFit: true,
				groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
			}),	
			frame:true,
			stripeRows: true,
			width: 700,
			height: 450,
			title: 'Available Cost Estimation Sheets',
			iconCls: 'icon-grid',
			fbar: ['->', {
				text:'Add New',
				iconCls: 'add',
				handler : function(){ // show a new window to create a new cost estimation sheet
						Ext.getCmp('cost_alt').getStore().reload({ params: 
							{ ws: workspace,
							 task: 'loadGrid'} // reload the alternative store with the selected workspace 
						});
						costAddWin.show();
					},
				scope: this
			},{
				text:'Delete',
				iconCls: 'delete',	
				handler : function(){
					// delete the selected cost sheets 
					Ext.Msg.show({
						title:'Delete the cost sheets?',
						msg: 'Are you sure to delete the selected sheets? This action is undoable and would remove the associated data if being referenced in the cost-benefit report!',
						buttons: Ext.Msg.YESNOCANCEL,
						fn: function(buttonId, text, opt){
							if (buttonId == 'yes'){
								var records = Ext.getCmp('costInfoGrid').getSelectionModel().getSelections();
								var ids = [];
								for (var i=0;i<records.length;i++) {
									ids.push(records[i].get('id'));
								}
								Ext.Ajax.request({   
									url: 'costInfo.php',
									params: {
										task: 'delete',
										ws: workspace,
										costIDs: Ext.encode(ids)
									},
									success: function(response, opts){
										var obj = Ext.decode(response.responseText);	
										if (obj.success == 'true') {
											Ext.Msg.alert('Success', obj.message);
											// reload the store and refresh
											Ext.getCmp('costInfoGrid').getStore().reload({ 
												callback: function(){
													Ext.getCmp('costInfoGrid').getView().refresh();
												}
											});
										}
										else {
											Ext.Msg.alert('Failed', obj.message);
										}	
									},
									failure: function(response, opts){
										Ext.Msg.alert('Failed', response.message);
									}
								});
							}
						}
					});	
				}
			}
			]
	});
		return costInfoGrid;		
	}
});

Ext.preg(gxp.plugins.CostManagement.prototype.ptype, gxp.plugins.CostManagement);	
