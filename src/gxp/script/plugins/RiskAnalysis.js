/**
 * Copyright (c) 2011-2016 
 * 
 * Published under the GPL license.
 */
 
/**
 * @require plugins/Tool.js
 */
 
 /** api: (define)
 *  module = gxp.plugins
 *  class = RiskAnalysis
 */
 
 /** api: (extends)
 *  plugins/Tool.js
 */
 
Ext.ns("gxp.plugins");

/** api: constructor
 *  .. class:: RiskAnalysis(config)
 *
 *    Plugin for the Risk Analysis component of the application.
 *    
 */
 
  gxp.plugins.RiskAnalysis = Ext.extend(gxp.plugins.Tool, {

	 /** api: ptype = myapp_riskanalysis */
	ptype: "myapp_riskanalysis",
	
	/** private: method[constructor]
     */
    constructor: function() {
        this.initialization();
        gxp.plugins.RiskAnalysis.superclass.constructor.apply(this, arguments);        
    },
	
	/** api: method[show]
     * Shows the respective grid window for the selected tree node.
     */ 
    show: function(n) {
		if (n.attributes.id == 'riskInfo') {
			this.riskStore.reload({ 
				params: 
					{ws: workspace,
					task: 'load'} // reload the grid with the selected workspace 
				});
			this.riskInfoWin.show();	
		}
		else {
			this.colRiskStore.reload({ 
				params: 
					{ws: workspace,
					task: 'load'} // reload the grid with the selected workspace 
				});
			this.colRiskInfoWin.show();
		}
			
	},
	
	/** api: private method[initialization]
     * Initialize the grid window to show more than one window at a time. 
     */
	initialization: function() {
		this.riskInfoWin = new Ext.Window({
			title: 'Risk scenarios information',
			layout: 'fit',
			plain:false,
			maximizable: true,
			collapsible: true,
			closeable: true,
			closeAction:'hide',
			buttonAlign:'right',
			items: this.initRiskGrid()
		});
		
		this.colRiskInfoWin = new Ext.Window({
			title: 'Collective risk scenarios information',
			layout: 'fit',
			plain:false,
			maximizable: true,
			collapsible: true,
			closeable: true,
			closeAction:'hide',
			buttonAlign:'right',
			items: this.initColRiskGrid()
		});
	},
	
	/** api: private method[initRiskGrid]
     * initialization of the grid panel to show the calculated risk scenarios per return periods
     */
	 
	initRiskGrid: function(){
		
		this.riskStore = new Ext.data.GroupingStore({
				url: 'riskScenarioInfo.php',
				sortInfo:{field: 'temp_de_retour', direction: "ASC"},
				groupField:'nom',
				reader: new Ext.data.JsonReader({
					totalProperty : 'totalCount',
					root          : 'rows',
					successProperty: 'success',
					idProperty    : 'idd',
					fields: [
						{name : 'id', type : 'int'},
						{name : 'nom', type : 'String'},
						{name : 'description', type : 'String'},
						{name : 'alt_nom', type : 'String'},
						{name : 'temp_de_retour', type : 'String'},
						{name : 'processus_nom', type : 'String'},
						{name : 'processus_type', type : 'String'},
						{name : 'probabilite_nom', type : 'String'},
						{name : 'objet_nom', type : 'String'}, 
						{name : 'objet_type', type : 'String'},
						{name : 'objet_para_nom', type : 'String'},
						{name : 'vul_nom', type : 'String'},
						{name : 'indice', type : 'String'},
						{name : 'indice_risque_sce', type : 'String'}
					] 
				})
		});	
	
		var riskGrid = new Ext.grid.GridPanel({
			id: 'riskGrid',
			store: this.riskStore,
			colModel: new Ext.grid.ColumnModel({
				columns: [					
					{header: "ID", dataIndex: 'id', hidden: true},
					{header: "Name", dataIndex: 'nom',hidden: true},
					{header: "Description", dataIndex: 'description', hidden: true},
					{header: "Alternative", dataIndex: 'alt_nom'},
					{header: "Return Period", dataIndex: 'temp_de_retour'},
					{header: "Hazard Map", dataIndex: 'processus_nom', hidden: true},
					{header: "Hazard Type", dataIndex: 'processus_type'},	
					{header: "PrA Configuration", dataIndex: 'probabilite_nom'},
					{header: "Object Map", dataIndex: 'objet_nom'},
					{header: "Object Type", dataIndex: 'objet_type'},
					{header: "Object Parameter Configuration", dataIndex: 'objet_para_nom'},
					{header: "Vulnerability Configuration", dataIndex: 'vul_nom'},
					{header: "Mapping Index", dataIndex: 'indice', hidden: true},
					{header: "Mapping Index (return periods)", dataIndex: 'indice_risque_sce', hidden: true},
					{
						xtype: 'actioncolumn',
						items: [
							{
								icon: 'src/gxp/theme/img/silk/map.png',  
								tooltip: 'Visualize the risk scenario of the corresponding return period',								
								handler: function(grid, rowIndex, colIndex) {
								// to load the layer to the map 
										var rec = grid.getStore().getAt(rowIndex);
										var lyr_name = rec.get('indice_risque_sce');
										var names = {};
										names[workspace + ":" + lyr_name] = true; 
										
										var source = app.tools.addlyrs.target.layerSources["local"]; // need to reassign the source in case other sources such as google are being added by users
										app.tools.addlyrs.setSelectedSource(source);
										var newRecords = [];
										var last = 0;
										app.tools.addlyrs.selectedSource.store.each(function(record, index) {
											if (record.get("name") in names) {
												last = index;
												newRecords.push(record);
											}
										});										
										app.tools.addlyrs.addLayers(newRecords, false);	
								}
							},{
								icon: 'src/gxp/theme/img/silk/layers.png',  
								tooltip: 'Visualize the risk total scenario for all return periods',								
								handler: function(grid, rowIndex, colIndex) {
								// to load the layer to the map 
										var rec = grid.getStore().getAt(rowIndex);
										var lyr_name = rec.get('indice');
										var names = {};
										names[workspace + ":" + lyr_name] = true; 
										
										var source = app.tools.addlyrs.target.layerSources["local"]; // need to reassign the source in case other sources such as google are being added by users
										app.tools.addlyrs.setSelectedSource(source);
										var newRecords = [];
										var last = 0;
										app.tools.addlyrs.selectedSource.store.each(function(record, index) {
											if (record.get("name") in names) {
												last = index;
												newRecords.push(record);
											}
										});										
										app.tools.addlyrs.addLayers(newRecords, false);	
								}
							}
						]
					}
				],
				defaults: {
					sortable: true,
					menuDisabled: false,
					width: 50
				}
			}),	
			view: new Ext.grid.GroupingView({				
				forceFit: true,
				groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
			}),						
			frame:true,
			stripeRows: true,
			width: 700,
			height: 450,
			title: 'Calculated Risk Scenarios',
			iconCls: 'icon-grid'
		});
		
		return riskGrid;
	},
	
	/** api: private method[initColRiskGrid]
     * initialization of the grid panel to show the calculated collective risk scenarios 
     */
	 
	initColRiskGrid: function(){
		this.colRiskStore = new Ext.data.GroupingStore({
				url: 'colRiskScenarioInfo.php',
				sortInfo:{field: 'id', direction: "ASC"},
				groupField:'processus_type',
				reader: new Ext.data.JsonReader({
					totalProperty : 'totalCount',
					root          : 'rows',
					successProperty: 'success',
					idProperty    : 'idd',
					fields: [
						{name : 'id', type : 'int'},
						{name : 'nom', type : 'String'},
						{name : 'description', type : 'String'},
						{name : 'alt_nom', type : 'String'},
						{name : 'option', type : 'String'},
						{name : 'processus_type', type : 'String'},
						{name : 'probabilite_nom', type : 'String'},
						{name : 'objet_nom', type : 'String'}, 
						{name : 'objet_type', type : 'String'},
						{name : 'objet_niveau', type : 'String'},
						{name : 'objet_para_nom', type : 'String'},
						{name : 'vul_niveau', type : 'String'},
						{name : 'vul_nom', type : 'String'},
						{name : 'indice', type : 'String'},
						{name : 'poly_texte', type : 'String'}
					] 
				})
		});	
		
		var colRiskGrid = new Ext.grid.GridPanel({
			id: 'colRiskGrid',
			store: this.colRiskStore,
			colModel: new Ext.grid.ColumnModel({
				columns: [					
					{header: "ID", dataIndex: 'id', hidden: true},
					{header: "Name", dataIndex: 'nom'},
					{header: "Description", dataIndex: 'description'},
					{header: "Alternative", dataIndex: 'alt_nom'},
					{header: "Hazard Type", dataIndex: 'processus_type', hidden: true},	
					{header: "Object Type", dataIndex: 'objet_type'},
					{header: "Object Level", dataIndex: 'objet_niveau', hidden: true},					
					{header: "Mapping Index", dataIndex: 'indice', hidden: true},
					{header: "Polygon Area", dataIndex: 'poly_texte', hidden: true},
					{
						xtype: 'actioncolumn',
						items: [
							{
								icon: 'src/gxp/theme/img/silk/information.png',  
								tooltip: 'View parameters',								
								handler: function(grid, rowIndex, colIndex) {
									var rec = grid.getStore().getAt(rowIndex);
									new Ext.Window({
										title: "Parameter configuration",
										layout: 'fit',										
										plain:false,
										autoHeight: true,
										modal: true,
										maximizable: false,
										collapsible: false,
										closeable: true,
										closeAction:'hide',
										items: [{
											title: rec.get('nom'),
											autoScroll: true,
											xtype: 'grid',
											autoHeight: true,
											store: new Ext.data.ArrayStore({
												fields : [{ name:'parameters'}, {name:'values'}],
												data: [['Alternative', rec.get('alt_nom')],
													   ['Option', rec.get('option')],
													   ['Hazard Type', rec.get('processus_type')],
													   ['PrA Configuration', rec.get('probabilite_nom')],
													   ['Object Type', rec.get('objet_type')],
													   ['Object Name', rec.get('objet_nom')],
													   ['Object Level', rec.get('objet_niveau')],
													   ['Object Configuration', rec.get('objet_para_nom')],
													   ['Vulnerability Level', rec.get('vul_niveau')],
													   ['Vulnerability Configuration', rec.get('vul_nom')],
													   ['Area', rec.get('poly_texte')]
													]
											}),					
											colModel: new Ext.grid.ColumnModel({
												defaults: {	
													width: 150,
													sortable: false,
													menuDisabled: true
												},
												columns: [
													{header: "Parameter", dataIndex: 'parameters', renderer: function(v, params, record){														
														params.css = "disable-cell" ; //this changes the cell background-color 
														return v;
													}},
													{header: "Value", dataIndex: 'values'}
												]
											}),
											viewConfig: {												
												forceFit: false
											},
											selModel: new Ext.grid.RowSelectionModel({singleSelect: true}),
											frame: true,
											stripeRows: false
										}]											
									}).show();							
									
								}
							},
							{
								icon: 'src/gxp/theme/img/silk/map.png',  
								tooltip: 'Visualize the collective risk scenario in the map',								
								handler: function(grid, rowIndex, colIndex) {
								// to load the layer to the map 
										var rec = grid.getStore().getAt(rowIndex);
										var lyr_name = rec.get('indice');
										var names = {};
										names[workspace + ":" + lyr_name] = true; 
										
										var source = app.tools.addlyrs.target.layerSources["local"]; // need to reassign the source in case other sources such as google are being added by users
										app.tools.addlyrs.setSelectedSource(source);
										var newRecords = [];
										var last = 0;
										app.tools.addlyrs.selectedSource.store.each(function(record, index) {
											if (record.get("name") in names) {
												last = index;
												newRecords.push(record);
											}
										});										
										app.tools.addlyrs.addLayers(newRecords, false);	
								}
							},{
								icon: 'src/gxp/theme/img/silk/shape_handles.png',  
								tooltip: 'Visualize the defined polygon zone in the map',	
								handler: function(grid, rowIndex, colIndex) {
									// extract coordinates and show the feature polygon 
									var rec = grid.getStore().getAt(rowIndex);
									if (rec.get('option') == 'L') {
										Ext.Msg.alert('Information', 'This scenario is a layer-based scenario and therefore, the defined zone cannot be visualized!');
									}
									else {
										var polyStr = rec.get('poly_texte');
										var feature = new OpenLayers.Format.WKT().read(polyStr);
										var layer = new OpenLayers.Layer.Vector("Area_layer_" + rec.get('nom'));										
										app.mapPanel.map.addLayer(layer);										
										layer.addFeatures([feature]);										
									}									
								}
							},{								
								icon: 'src/gxp/theme/img/silk/chart_bar.png',  
								tooltip: 'Visualize the collective risk scenario (Table/Chart)',								
								handler: function(grid, rowIndex, colIndex) {
									var rec = grid.getStore().getAt(rowIndex);								
									var colChartStore = new Ext.data.JsonStore({
										url: 'colRiskScenarioInfo.php',
										sortInfo:{field: 'id', direction: "ASC"},
										root: 'rows',
										fields: ['id','id_sce','temp_de_retour', 'indice_risque_sce', 'risque_obj','risque_humain','risque_total','risque_humain_morts','risque_individuel'],												
										autoLoad: true,
										baseParams: {
											task: 'loadChartStoreGrid',
											ws: workspace,
											mpIndex: rec.get('indice'),
											id: rec.get('id'),
											objetType: rec.get('objet_type')
										}
									});
									var customRender = function(v) 	// plug Swiss Franc currency renderer into formatter
									{									
										v = Ext.util.Format.number(v, '0,000.00');																				 
										return (v + ' CHF');
									};
									
									// grid to show
									var colDataGrid = new Ext.grid.GridPanel({
										store: colChartStore,								
										colModel: new Ext.grid.ColumnModel({
											defaults: {
											width: 120,
											sortable: true
										},
										columns: [
											{header: "ID", dataIndex: 'id', hidden: true},
											{header: "Scenario ID", dataIndex: 'id_sce', hidden: true},
											{header: "Return Period", dataIndex: 'temp_de_retour'},
											{header: "Object (CHF/year)", dataIndex: 'risque_obj', renderer: customRender, align: 'right'},
											{header: "Human (CHF/year)", dataIndex: 'risque_humain', renderer: customRender, align: 'right'},
											{header: "Total (CHF/year)", dataIndex: 'risque_total', renderer: customRender, align: 'right'},
											{header: "Human (persons/year)", dataIndex: 'risque_humain_morts', align: 'right'},											
											{
												xtype: 'actioncolumn',
												header: "Map",
												align: 'center',
												items: [
													{
														icon: 'src/gxp/theme/img/silk/map.png',  
														tooltip: 'Visualize the selected risk scenario in the map',	
														/* getClass: function(value, meta, record) {
															if (record.get('risque_total') == 0 && record.get('risque_individuel') == 0) {
																return 'x-hide-visibility';
															}
														}, */
														handler: function(grid, rowIndex, colIndex) {
															// to load the layer to the map 
															var rec = grid.getStore().getAt(rowIndex);
															var lyr_name = rec.get('indice_risque_sce');
															var names = {};
															names[workspace + ":" + lyr_name] = true; 
															
															var source = app.tools.addlyrs.target.layerSources["local"]; // need to reassign the source in case other sources such as google are being added by users
															app.tools.addlyrs.setSelectedSource(source);
															var newRecords = [];
															var last = 0;
															app.tools.addlyrs.selectedSource.store.each(function(record, index) {
																if (record.get("name") in names) {
																	last = index;
																	newRecords.push(record);
																}
															});										
															app.tools.addlyrs.addLayers(newRecords, false);	
														}
													}
												]	
											}		
										]
										}),
										viewConfig: {
											forceFit: true
										},
										sm: new Ext.grid.RowSelectionModel({singleSelect:true}),	
										frame: true,
										tbar : []																	
									});	
									
									// grid to export
									var colDataExportGrid = new Ext.grid.GridPanel({
										store: colChartStore,								
										colModel: new Ext.grid.ColumnModel({
											defaults: {
											width: 120,
											sortable: true
										},
										columns: [
											{header: "ID", dataIndex: 'id', hidden: true},
											{header: "Scenario ID", dataIndex: 'id_sce', hidden: true},
											{header: "Return Period", dataIndex: 'temp_de_retour'},
											{header: "Object (CHF/year)", dataIndex: 'risque_obj', renderer: customRender, align: 'right'},
											{header: "Human (CHF/year)", dataIndex: 'risque_humain', renderer: customRender, align: 'right'},
											{header: "Total (CHF/year)", dataIndex: 'risque_total', renderer: customRender, align: 'right'},
											{header: "Human (persons/year)", dataIndex: 'risque_humain_morts', align: 'right'}								
										]
										}),
										viewConfig: {
											forceFit: true
										},
										sm: new Ext.grid.RowSelectionModel({singleSelect:true}),	
										frame: true,
										tbar : []																	
									});	
									
									//Create the Download button and add it to the top toolbar
									var exportButton = new Ext.ux.Exporter.Button({
									  component: colDataExportGrid,
									  text     : "Download as .xls"
									});
									
									colDataGrid.getTopToolbar().add(exportButton);
									
									var colChartPanel = new Ext.Panel({										
										frame:true,												
										layout: 'vbox',	
										layoutConfig: {
											align: 'stretch',
											pack: 'start'
										},
										items: [{
											xtype: 'compositefield',
											itemId: 'xyAxis',
											combineErrors: false,
											items: [{
												xtype: 'button',
												icon: 'src/gxp/theme/img/silk/arrow_refresh.png',
												tooltip: 'Reload the curve with the selected value',
												handler: function(){	
													var yAxis = colChartPanel.getComponent('xyAxis').items.items[0].getValue();	
													var yAxisTitle = colChartPanel.getComponent('xyAxis').items.items[0].getRawValue();
													colChartPanel.getComponent('colRiskChart').items.items[0].setYField(yAxis);	
													colChartPanel.getComponent('colRiskChart').items.items[0].setYAxis(new Ext.chart.NumericAxis({
														title: yAxisTitle,
														labelRenderer: (yAxis == 'risque_humain_morts' || yAxis == 'risque_individuel')? Ext.util.Format.numberRenderer('0,0.000000'): Ext.util.Format.numberRenderer('0,0.00')
													}));

												}
											},{
												name: 'vul_type',
												xtype: "combo",
												mode: "local",
												editable: false,
												store: new Ext.data.ArrayStore({
													id: 0,
													fields: ['myValue','displayText'],
													data: [['risque_obj', 'Object (CHF/year)'], ['risque_humain', 'Human (CHF/year)'],['risque_total', 'Total (CHF/year)'],
														['risque_humain_morts', 'Human (persons/year)']]  // data is local
												}),
												valueField: 'myValue',
												value: 'risque_obj',
												displayField: 'displayText',
												triggerAction: 'all'
											}]
										},{	
											flex: 1,
											layout: 'fit',
											border: true,
											frame: true,	
											itemId: 'colRiskChart',
											title: 'Bar Chart: Calculated risk per return periods',
											items: [{
												xtype: 'columnchart',
												store: colChartStore,												
												yField: 'risque_obj',
												url: 'src/ext/resources/charts.swf',
												xField: 'temp_de_retour',
												xAxis: new Ext.chart.CategoryAxis({
													title: 'Return Periods'
												}),
												yAxis: new Ext.chart.NumericAxis({
													title: 'Object (CHF/year)',
													labelRenderer : Ext.util.Format.numberRenderer('0,0.00')
												}),
												/* series: [{
													type: 'column',
													displayName: 'Object (CHF/year)',
													yField: 'risque_obj',
													style: {
														image:'bar.gif',
														mode: 'stretch',
														color:0x99BBE8
													}
												}], */
												chartStyle: {
													padding: 10,
													animationEnabled: true,
													font: {
														name: 'Tahoma',
														color: 0x444444,
														size: 11
													},
													dataTip: {
														padding: 5,
														border: {
															color: 0x99bbe8,
															size:1
														},
														background: {
															color: 0xDAE7F6,
															alpha: .9
														},
														font: {
															name: 'Tahoma',
															color: 0x15428B,
															size: 10,
															bold: true
														},
														xAxis: {
															color: 0x69aBc8,
															majorTicks: {color: 0x69aBc8, length: 4},
															minorTicks: {color: 0x69aBc8, length: 2},
															majorGridLines: {size: 1, color: 0xeeeeee}
														},
														yAxis: {
															color: 0x69aBc8,
															majorTicks: {color: 0x69aBc8, length: 4},
															minorTicks: {color: 0x69aBc8, length: 2},
															majorGridLines: {size: 1, color: 0xdfe8f6}
														}
													}
												},
												extraStyle: {
													yAxis: {
														titleRotation: -90
													}
													/* legend: {																		
														display: 'bottom',
														padding: 5,
														font: {
															family: 'Tahoma',
															size: 10
														}
													} */
												}  
											}]
										},{	
											flex: 1,
											layout: 'fit',
											border: true,
											items: [colDataGrid]
										}]
									});	
									
									// a window to show the panel 
									var colChartWin = new Ext.Window({
										title: 'Visualization of the calculated risk scenario: ' + rec.get('nom'),
										width: 450,
										height: 500,
										layout: 'fit',
										plain: true,
										maximizable: true,
										collapsible: true,
										closeable: true,
										buttonAlign:'right',
										items: [colChartPanel]
									});
													
									colChartWin.show();
								}
							},{
								icon: 'src/gxp/theme/img/silk/map_magnify.png',  								
								tooltip: 'Visualize the collective risk scenario (aggregated version) in the map',		
								getClass: function(value, meta, record) {
									if (record.get('objet_type') != 'Routes') {
										return 'x-hide-visibility';
									}
								},
								handler: function(grid, rowIndex, colIndex) {
									// to load the layer to the map 
									var rec = grid.getStore().getAt(rowIndex);
									var lyr_name = rec.get('indice') + '_sum';
									var names = {};
									names[workspace + ":" + lyr_name] = true; 
										
									var source = app.tools.addlyrs.target.layerSources["local"]; // need to reassign the source in case other sources such as google are being added by users
									app.tools.addlyrs.setSelectedSource(source);
									var newRecords = [];
									var last = 0;
									app.tools.addlyrs.selectedSource.store.each(function(record, index) {
										if (record.get("name") in names) {
											last = index;
											newRecords.push(record);
										}
									});										
									app.tools.addlyrs.addLayers(newRecords, false);											
								}
							}
						]
					}]
			}),
			view: new Ext.grid.GroupingView({				
				forceFit: true,
				groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
			}),						
			frame:true,
			stripeRows: true,
			width: 700,
			height: 450,
			title: 'Calculated Collective Risk Scenarios',
			iconCls: 'icon-grid',
			fbar: ['->', {
				text:'New Scenario',
				iconCls: 'add',			
				hidden: true,
				handler : function(){ // show a new window to run the risk calculation and output the results to geoserver (and db)					
					
					},
				scope: this
			},{
				text:'Delete',
				iconCls: 'delete',	
				handler : function(){
					// delete the records from store, db, geoserver
					Ext.Msg.show({
						title:'Delete the risk scenarios?',
						msg: 'Are you sure to delete the selected scenarios? This action is undoable and will remove a group of calculated sub-scenarios for return periods!',
						buttons: Ext.Msg.YESNOCANCEL,
						fn: function(buttonId, text, opt){
							if (buttonId == 'yes'){
								var records = Ext.getCmp('colRiskGrid').getSelectionModel().getSelections();
								var temp =[];
								for (var i=0;i<records.length;i++) {									
									temp.push(records[i].data);
								}
								Ext.Ajax.request({   
									url: 'colRiskScenarioInfo.php',
									params: {
										task: 'delete',
										ws: workspace,
										scenarioRecords: Ext.encode(temp)
									},
									success: function(response, opts){
										var obj = Ext.decode(response.responseText);										
										Ext.Msg.alert('Information', obj.message);	
										// reload the store and refresh
										Ext.getCmp('colRiskGrid').getStore().reload({ 
												callback: function(){
													Ext.getCmp('colRiskGrid').getView().refresh();
												}
											});
									},
									failure: function(response, opts){
										var obj = Ext.decode(response.responseText);
										Ext.Msg.alert('Failed', obj.message);
									}
								});
							}
						}
					});
				}
			}]
		});
		
		return colRiskGrid;
	}
	});

Ext.preg(gxp.plugins.RiskAnalysis.prototype.ptype, gxp.plugins.RiskAnalysis);