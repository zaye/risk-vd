/**
 * Copyright (c) 2011-2016 
 * 
 * Published under the GPL license.
 */

/**
 * @requires plugins/Tool.js
 * @require OpenLayers/WPSClient.js
 */

/** api: (define)
 *  module = gxp.plugins
 *  class = ExportSelectedFeatures
 */

/** api: (extends)
 *  plugins/Tool.js
 */
Ext.namespace("gxp.plugins");

/** api: constructor
 *  .. class:: ExportSelectedFeatures(config)
 *
 *    Plugin for exporting selected features from a selected layer of the map.
 *    
 */
gxp.plugins.ExportSelectedFeatures = Ext.extend(gxp.plugins.Tool, {
    
    /** api: ptype = myapp_ExportSelectedFeatures */
    ptype: "myapp_ExportSelectedFeatures",
    
    /** api: config[exportMenuText]
     *  ``String``
     *  Text for remove menu item (i18n).
     */
    exportMenuText: "Export selected features",

    /** api: config[exportActionTip]
     *  ``String``
     *  Text for remove action tooltip (i18n).
     */
    exportActionTip: "Export selected features",
	
	/** api: config[outputFormat]
     *  ``String`` The output format to use for the download. Defaults to a zipped up shape file.
     */ 
	outputFormat: "SHAPE-ZIP",
	
	/** api: config[filename]
     *  ``String`` The ZIP file's name
     */
    filename: 'selectedfeatures',
    
    /** api: method[addActions]
     */
    addActions: function() {
		// Create a WPSClient instance for use with the local GeoServer
        this.wpsClient = new OpenLayers.WPSClient({
            servers: {
                local: '/geoserver/wps'
            }
        });
		
		var featureManager = this.target.tools[this.featureManager];		
        var actions = gxp.plugins.ExportSelectedFeatures.superclass.addActions.apply(this, [{
            menuText: this.exportMenuText,
            iconCls: "gxp-icon-exportselectedfeatures",
            disabled: true,
            tooltip: this.exportActionTip,
            handler: function() {	
			
				var url = featureManager.schema.url;				
				var features = featureManager.featureLayer.selectedFeatures;
				var typeName = featureManager.schema.reader.raw.featureTypes[0].typeName;
				var fids = '';
							
				for (var i=0; i<features.length; i++) {
					fids += features[i].fid + ','; 
				}			
							
				this.exportForm = new Ext.form.FormPanel({
					width: 350,
					frame: true,
					autoHeight: true,
					bodyStyle: "padding: 10px 10px 0 10px;",
					labelWidth: 65,
					defaults: {
						anchor: "95%",
						allowBlank: false,
						msgTarget: "side"
					},
					items: [{
						xtype: "textfield",
						name: "title",
						fieldLabel: 'Name',
						emptyText: 'Name of the layer or folder',
						allowBlank: false
					}, {
						xtype: "textarea",
						name: "abstract",
						fieldLabel: 'Description',
						emptyText: 'Description of the layer',
						allowBlank: true
					}],
					buttons: [
					/* {
						text: 'Add',
						hidden: true,
						icon:  'src/gxp/theme/img/silk/map_add.png',
						handler: function(){
							var filename = this.exportForm.getForm().getFieldValues().title; 	
							this.outputFormat = 'json';								
							var url = 'http://localhost:8080/geoserver/wfs?service=WFS&amp;request=GetFeature&amp;version=1.0.0&amp;typeName=' + workspace + ':' + typeName +
							'&amp;featureID=' + fids + '&amp;outputFormat=' + this.outputFormat;
							
							Ext.Ajax.request({   
								url: 'exportSelectedFeatures.php',
								params: {							
									ws: workspace,
									fname: filename,
									xlink: url
								},
								success: function(response, opts){									
									var obj = Ext.decode(response.responseText);										
									var names = {}; 
									var layerName = workspace + ':' + filename;
									names[layerName] = true; 
									
									if (obj.message == layerName){
										// add the newly exported layer to the map
										var source = app.tools.addlyrs.target.layerSources["local"]; // need to reassign the source in case other sources such as google are being added by users										
										app.tools.addlyrs.setSelectedSource(source);
										
										app.tools.addlyrs.selectedSource.store.load({
											callback: function(records, options, success) {
												var newRecords = [];
												var last = 0;
												app.tools.addlyrs.selectedSource.store.each(function(record, index) {
													if (record.get("name") in names) {
														last = index;
														newRecords.push(record);
													}
												});	
												app.tools.addlyrs.addLayers(newRecords, false);		
											},
											scope: this                                            
										});									
									}
									else {
										Ext.Msg.alert('Failed', obj.message);
									}
								},
								failure: function(response, opts){
									Ext.Msg.alert('Failed', 'The server-side failure with status code ' + response.status);		
								}
							});
							exportWin.close();
						},
							scope: this
					}, */
					{
						text: 'Download',
						icon:  'src/gxp/theme/img/silk/compress.png',
						handler: function(){
							this.filename = this.exportForm.getForm().getFieldValues().title; 	
							url += 'service=WFS&request=GetFeature&version=1.0.0&typeName=' + workspace + ':' + typeName +
								'&format_options=filename:'+ this.filename +'.zip' +
								'&featureID=' + fids + '&outputFormat=SHAPE-ZIP';
					
							window.open(url);
							exportWin.close();
						},
							scope: this
					}]	
				});	
				
				var exportWin = new Ext.Window({
					title: "Export selected features ...",
					layout: 'fit',
					autoHeight: true,
					plain:false,
					modal: true,
					maximizable: false,
					collapsible: false,
					closeable: true,
					closeAction:'hide',
					buttonAlign:'right',
					items: [this.exportForm],
					tools:[
							{
								id:'help',
								qtip: 'Get Help',
								handler: function(event, toolEl, panel){
									Ext.Msg.show({
										title:'Help Information',
										msg: 'Enter the name and click the download button to export the selected features (in .zip format) of the currently selected map layer.' + "<br><br>" + 'The downloaded zip folder includes the shape files which could be opened in ESRI ArcGIS or QGIS software.',
										buttons: Ext.Msg.OK,															   
										animEl: 'elId',
										icon: Ext.MessageBox.INFO
									});
							}
						}]
				}).show();					
            },
            scope: this
        }]);
		
		var ExportSelectedFeaturesAction = actions[0];

		featureManager.featureLayer.events.on({
            "featureselected": function(evt) {
                var disabled = false;
				actions[0].setDisabled(disabled);               
            },
            "featureunselected": function(evt) {
                if (evt.feature.layer.selectedFeatures.length == 0) {
                    actions[0].disable();                   
                }
            },
            scope: this
        });
		
        this.target.on("layerselectionchange", function(record) {
            ExportSelectedFeaturesAction.setDisabled(
                this.target.mapPanel.layers.getCount() <= 1 || !record || !record.selectedFeatures
            );
        }, this);
        
        return actions;
    },
	
	/** Helper function for adding process result to the map */
    addResult: function(outputs) {
		var layerName = outputs.result;
        console.log(outputs.result);
		// this.target.mapPanel.map.addLayer(layerName); // to do: add a vector layer record by finding in the store
    }
        
});

Ext.preg(gxp.plugins.ExportSelectedFeatures.prototype.ptype, gxp.plugins.ExportSelectedFeatures);