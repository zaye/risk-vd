/**
 * Copyright (c) 2011-2016 
 * 
 * Published under the GPL license.
 */
 
/**
 * @require plugins/Tool.js
 * @require widgets/LayerUploadPanelEaR.js
 */
 
 /** api: (define)
 *  module = gxp.plugins
 *  class = Objects
 */
 
 /** api: (extends)
 *  plugins/Tool.js
 */
 
Ext.ns("gxp.plugins");

/** api: constructor
 *  .. class:: Objects(config)
 *
 *    Plugin for the elements-at-risk component of the risk analysis application.
 *    
 */
 
 gxp.plugins.Objects = Ext.extend(gxp.plugins.Tool, {

	 /** api: ptype = myapp_objects */
	ptype: "myapp_objects",
	
	/** private: method[constructor]
     */
    constructor: function() {
        this.initialization();
        gxp.plugins.Objects.superclass.constructor.apply(this, arguments);        
    },
  
	/** api: method[show]
     * Shows the respective grid window for the selected tree node.
     */ 
    show: function(n) {
		if (n.attributes.id == 'objParaInfo')  {
			this.objParaStore.reload({ 
				params: 
					{ws: workspace,
					task: 'load'} // reload the grid with the selected workspace 
				});
			this.objParaInfoWin.show();
		}
		else {
			this.objStore.reload({ 
				params: 
					{ws: workspace,
					task: 'load'} // reload the grid with the selected workspace 
				});
			this.objInfoWin.show();
		}
	},

	/** api: private method[initialization]
     * Initialize the different grid windows to show more than one window at a time. 
     */
	initialization: function() {
		
		this.objInfoWin = new Ext.Window({
			title: 'Object layers information',
			layout: 'fit',
			plain:false,
			maximizable: true,
			collapsible: true,
			closeable: true,
			closeAction:'hide',
			buttonAlign:'right',
			items: this.initObjGrid()
		});
		
		this.objParaInfoWin = new Ext.Window({
			title: 'Configuration of the object parameter values',
			layout: 'fit',
			plain:false,
			maximizable: true,
			collapsible: true,
			closeable: true,
			closeAction:'hide',
			buttonAlign:'right',
			items: this.initObjParaGrid()
		});
	},
	
	/** api: private method[initObjGrid]
     * initialization of the grid panel to show the uploaded object maps
     */
	 
	initObjGrid: function(){
		this.objStore = new Ext.data.GroupingStore({
				url: 'objInfo.php',
				sortInfo:{field: 'type', direction: "ASC"},
				groupField:'type',
				reader: new Ext.data.JsonReader({
					totalProperty : 'totalCount',
					root          : 'rows',
					successProperty: 'success',
					idProperty    : 'id',
					fields: [
						{name : 'id', type : 'int'},
						{name : 'nom', type : 'String'},
						{name : 'alt_nom', type : 'String'},
						{name : 'description', type : 'String'},
						{name : 'type', type : 'String'},
						{name : 'remarques', type : 'String'},
						{name : 'nom_origine', type : 'String'},
						{name : 'indice', type : 'String'}
					] 
				})
		});	
		
		var objGrid = new Ext.grid.GridPanel({
			id: 'objGrid',
			store: this.objStore,
			colModel: new Ext.grid.ColumnModel({
				columns: [					
					{header: "ID", dataIndex: 'id', hidden: true},
					{header: "Type", dataIndex: 'type', hidden: true},
					{header: "Layer Name", dataIndex: 'nom'},
					{header: "For Alternative", dataIndex: 'alt_nom'},
					{header: "Description", dataIndex: 'description'},
					{header: "Remarks", dataIndex: 'remarques'},			
					{header: "Mapping Index", dataIndex: 'indice', hidden: true},
					{header: "Original Name", dataIndex: 'nom_origine', hidden: true},
					{
						xtype: 'actioncolumn',
						items: [
						{
								icon: 'src/gxp/theme/img/silk/map.png',  
								tooltip: 'Visualize the object map',								
								handler: function(grid, rowIndex, colIndex) {
									// to load the layer to the map 
										var rec = grid.getStore().getAt(rowIndex);
										var lyr_name = workspace + ':' + rec.get('indice');								
										var names = {};
										names[lyr_name] = true;
										
										var source = app.tools.addlyrs.target.layerSources["local"]; // need to reassign the source in case other sources such as google are being added by users
										app.tools.addlyrs.setSelectedSource(source);
										var newRecords = [];
										var last = 0;
										app.tools.addlyrs.selectedSource.store.each(function(record, index) {
											if (record.get("name") in names) {
												last = index;
												newRecords.push(record);
											}
										});										
										app.tools.addlyrs.addLayers(newRecords, false);	
								}
							},
							{
								icon: 'src/gxp/theme/img/silk/table_edit.png',  
								tooltip: 'Edit Data',			
								handler: function(grid, rowIndex, colIndex) {									
									var rec = grid.getStore().getAt(rowIndex);
									
									var objEditWin = new Ext.Window({
										title: 'Edit the object map information..',
										layout: 'form',
										autoHeight: true,
										plain:false,
										modal: true,
										resizable: false,
										closeable: true,
										closeAction:'hide',
										buttonAlign:'right',
										items: new Ext.form.FormPanel({
											labelWidth: 100, // label settings here cascade unless overridden
											itemId: 'objEditForm',
											frame:true,			
											bodyStyle:'padding:5px 5px 0',
											width: 350,	
											autoHeight:true,		
											defaults: {
												anchor: '100%'
											},
											items: [{
												fieldLabel: 'Name/Title',
												xtype: 'textfield',
												name: 'name',
												allowBlank: false,
												value: rec.get('nom')
											},{
												fieldLabel: 'Object Type',
												name: 'type',
												hiddenName: 'type',
												xtype: "combo",
												mode: "local",
												editable: false,
												store: new Ext.data.ArrayStore({
													id: 0,
													fields: ['myValue','displayText'],
													data: [['Buildings', 'Buildings'],['Routes', 'Routes'],['Surfaces', 'Land Use']]  // data is local
												}),
												valueField: 'myValue',
												displayField: 'displayText',
												value: rec.get('type'),
												triggerAction: 'all',
												allowBlank: false
											},{
												fieldLabel: 'Description',
												xtype: 'textarea',
												name: 'description',
												allowBlank: true,
												value: rec.get('description')
											},{
												fieldLabel: 'Remarks',
												xtype: 'textarea',
												name: 'remarks',
												allowBlank: true,
												value: rec.get('remarques')
											}],
											buttons: [{
												text: 'Update',
												icon:  'src/gxp/theme/img/silk/table_save.png',
												handler: function(){
													var form = objEditWin.getComponent('objEditForm').getForm();				
													if (form.isValid()) {
														form.submit({
															url: 'objInfo.php',
															params: {
																task: 'edit',
																ws: workspace,
																mpIndex: rec.get('indice'),
																ID: rec.get('id')
															},
															waitMsg : 'Please wait...',
															success: function(form, action) {
																Ext.Msg.alert('Success', action.result.message);
																objEditWin.close();
																Ext.getCmp('objGrid').getStore().reload({ // refresh the grid view
																	callback: function(){
																	Ext.getCmp('objGrid').getView().refresh();
																	}
																});
															},
															failure: function(form, action) {
																Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
															}
														});
													}
													else {
														Ext.Msg.alert('Validation Error', 'The form is not valid!' );
													}
												}
											},{
												text: 'Cancel',
												icon:  'src/gxp/theme/img/decline.png',
												handler: function(){
													objEditWin.close();
												}
											}]
										})											
									});
									
									objEditWin.show();
								}
							}
						]		
					}
				],
				defaults: {
					sortable: true,
					menuDisabled: false,
					width: 5
				}
			}),			
			view: new Ext.grid.GroupingView({				
				forceFit: true,
				groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
			}),						
			frame:true,
			width: 700,
			height: 450,
			title: 'Avaiable Object Maps (Layers)',
			iconCls: 'icon-grid',
			fbar: ['->', {
				text:'Add New',
				iconCls: 'add',
				handler : function(){ // show a new window to upload the shape layer to geoserver (and db)					
						var objForm = new gxp.LayerUploadPanelEaR({
							url: "/geoserver/rest",
							width: 450,
							frame: true,
							autoHeight: true,
							bodyStyle: "padding: 10px 10px 0 10px;",
							labelWidth: 75,
							defaults: {
								anchor: "95%",
								allowBlank: false,
								msgTarget: "side"
							},
							listeners: {
								uploadcomplete: function(objForm, detail) {
									var formData = objForm.getForm().getFieldValues();
									var mp_index = [];
									var layers = detail["import"].tasks;
									var item, names = {}, resource, layer;
									for (var i=0, len=layers.length; i<len; ++i) {
                                        item = layers[i];
                                        if (item.state === "ERROR") {
                                            Ext.Msg.alert(item.layer.originalName, item.errorMessage);
                                            return;
                                        }
                                        var ws;
                                        if (item.target.dataStore) {
                                            ws = item.target.dataStore.workspace.name;
                                        } else if (item.target.coverageStore) {
                                            ws = item.target.coverageStore.workspace.name;
                                        }
                                        names[ws + ":" + item.layer.name] = true;
										mp_index.push(item.layer.name);
                                    }
									
									var source = app.tools.addlyrs.target.layerSources["local"]; // need to reassign the source in case other sources such as google are being added by users
									app.tools.addlyrs.setSelectedSource(source);
									app.tools.addlyrs.selectedSource.store.load({
										params: {"_dc": Math.random()},
										callback: function(records, options, success) {
											var gridPanel, sel;
											if (app.tools.addlyrs.capGrid && app.tools.addlyrs.capGrid.isVisible()) {
													gridPanel = app.tools.addlyrs.capGrid.get(0).get(0);
													sel = gridPanel.getSelectionModel();
													sel.clearSelections();
											}
											// select newly added layers
											var newRecords = [];
											var last = 0;
											app.tools.addlyrs.selectedSource.store.each(function(record, index) {
												if (record.get("name") in names) {
													last = index;
													newRecords.push(record);
												}
											});
											if (gridPanel) {
													// this needs to be deferred because the 
													// grid view has not refreshed yet
													window.setTimeout(function() {
														sel.selectRecords(newRecords);
														gridPanel.getView().focusRow(last);
													}, 100);
											} else {
													app.tools.addlyrs.addLayers(newRecords, true);
												}                                                                                    
											},
										scope: this                                            
									});                        
									
									//saving meta data to the objects table in the selected workspace
									Ext.Ajax.request({
										url:'objInfo.php',
										params: {
											task: 'add',
											ws: workspace,
											names: Ext.encode(mp_index),											
											title: formData.title,
											desc: formData["abstract"],
											remarks: formData.remarks,
											file: formData.file,
											objType: formData.objType,
											objAltID: formData.objAltID
										},
										timeout: 900000, // <<-- Timeout value specified here in milliseconds
										success: function(response, opts) {
												objForm.getForm().reset();
												var obj = Ext.decode(response.responseText);
												Ext.Msg.alert('Information', obj.message);
												Ext.getCmp('objGrid').getStore().reload({ //refresh the grid with newly added layer
													callback: function(){
													Ext.getCmp('objGrid').getView().refresh();
													}
												});
											},
										failure: function(response, opts) {
												console.log('server-side failure with status code ' + response.status);
											}
									});							
								}
							}
						});		
						
						var objDataUpload = new Ext.Window({
							title: "Upload Object Map",
							layout: 'fit',
							autoHeight: true,
							plain:false,
							modal: true,
							maximizable: false,
							collapsible: false,
							closeable: true,
							closeAction:'hide',
							buttonAlign:'right',
							items: [objForm]
						});
						
						objDataUpload.show();
					},
				scope: this
			},{
				text:'Delete',
				iconCls: 'delete',	
				handler : function(){
					// delete the record from store, db, geoserver
					Ext.Msg.show({
						title:'Delete the object maps?',
						msg: 'Are you sure to delete the selected object maps? This action is undoable, please try to remove first any referenced risk scenarios which is being used in risk calculation!',
						buttons: Ext.Msg.YESNOCANCEL,
						fn: function(buttonId, text, opt){
							if (buttonId == 'yes'){
								var records = Ext.getCmp('objGrid').getSelectionModel().getSelections();
								var temp =[];
								for (var i=0;i<records.length;i++) {									
									temp.push(records[i].data);
								}
								Ext.Ajax.request({   
									url: 'objInfo.php',
									params: {
										task: 'delete',
										ws: workspace,
										objRecords: Ext.encode(temp)
									},
									success: function(response, opts){
										var obj = Ext.decode(response.responseText);										
										Ext.Msg.alert('Information', obj.message);	
										// reload the store and refresh
										Ext.getCmp('objGrid').getStore().reload({ 
												callback: function(){
													Ext.getCmp('objGrid').getView().refresh();
												}
											});
									},
									failure: function(response, opts){
										var obj = Ext.decode(response.responseText);										
										Ext.Msg.alert('Failed', obj.message);
									}
								});
							}
						}
					});
				}
			}]
		});
		
		return objGrid;
	},
	
	/** api: private method[initObjParaGrid]
     * initialization of the grid panel to show the default and available object parameter configurations
     */
	initObjParaGrid: function(){
		this.objParaStore = new Ext.data.GroupingStore({
				url: 'objParaInfo.php',
				sortInfo:{field: 'nom', direction: "ASC"},
				groupField:'groupe',
				reader: new Ext.data.JsonReader({
					totalProperty : 'totalCount',
					root          : 'rows',
					successProperty: 'success',
					idProperty    : 'id',
					fields: [
						{name : 'id', type : 'int'},						
						{name : 'nom', type : 'String'},
						{name : 'description', type : 'String'},
						{name : 'type', type : 'String'},
						{name : 'obj_type', type : 'String'},
						{name : 'obj_niveau', type : 'String'},
						{name : 'remarques', type : 'String'},
						{name : 'indice', type : 'String'},
						{name : 'groupe', convert : function (v, rec){
							return rec.obj_type + ', ' + rec.type;
							} 
						}
					] 
				})
		});	
		
		var objParaInfoGrid = new Ext.grid.GridPanel({
			id: 'objParaInfoGrid',
			store: this.objParaStore,
			colModel: new Ext.grid.ColumnModel({
				columns: [
					{header: "ID", dataIndex: 'id', hidden: true},					
					{header: "Name", dataIndex: 'nom'},
					{header: "Description", dataIndex: 'description'},	
					{header: "Option", dataIndex: 'type'},
					{header: "Type", dataIndex: 'obj_type'},
					{header: "Level", dataIndex: 'obj_niveau'},
					{header: "Remarks", dataIndex: 'remarques'},		
					{header: "Mapping Index", dataIndex: 'indice', hidden: true},
					{header: "Group", dataIndex: 'groupe', hidden: true},
					{
						xtype: 'actioncolumn',
						items: [
							{
								icon: 'src/gxp/theme/img/silk/table_edit.png',  								
								tooltip: 'Edit/View Data',								
								handler: function(grid, rowIndex, colIndex) {									
									var rec = grid.getStore().getAt(rowIndex);	
									
									Ext.util.Format.frMoney = function(v) 	// plug Swiss Franc currency renderer into formatter
									{
										v = (Math.round((v-0)*100))/100;
										v = (v == Math.floor(v)) ? v + ".00" : ((v*10 == Math.floor(v*10)) ? v + "0" : v);
										return ('CHF '+ v );									
									};
									
									var objParaEditGrid = new Ext.grid.EditorGridPanel({
									//	id: 'objParaEditGrid',										
										title: '',
										store: new Ext.data.GroupingStore({
											url: 'objParaInfo.php',
											baseParams: {
												task: 'loadObjParaValues',
												ws: workspace,
												mpIndex: rec.get('indice'),
												type: rec.get('type'),
												objLevel: rec.get('obj_niveau'),
												objType: rec.get('obj_type')
											},
											sortInfo:{field:'code', direction: "ASC"},
											groupField:'type',
											reader: new Ext.data.JsonReader({
												totalProperty : 'totalCount',
												root          : 'rows',
												successProperty: 'success',
												idProperty    : 'id',
												fields: [ 	
													{name : 'type', type : 'String'},
													{name : 'code', type : 'int'},
													{name : 'description', type : 'String'},
													{name : 'prix_unitaire', type : 'number'}, 
													{name : 'unite', type : 'String'},
													{name : 'humains', type : 'number'},
													{name : 'humains_exp', type : 'number'},
 													{name : 'vitesse', type : 'number'},
													{name : 'tjm', type : 'number'},
													{name : 'longueur', type : 'number'},
													{name : 'passage', type : 'number'},
													{name : 'remarques', type : 'String'}													
												]
											}),
											autoLoad: true
										}),
										colModel: new Ext.grid.ColumnModel({
											defaults: {
												width: 120,
												sortable: true,
												menuDisabled: true
											},
											columns: [
												{header: "Object Type", sortable: true, dataIndex: 'type'},
												{header: "Code", sortable: true, dataIndex: 'code'},
												{header: "Description", sortable: true, dataIndex: 'description'},
												{header: "Per unit price", sortable: true, dataIndex: 'prix_unitaire', renderer: Ext.util.Format.frMoney, editable: rec.get('type') != 'default'? true: false, editor:new Ext.form.NumberField({allowDecimals: true, decimalPrecision:4, allowBlank: false, allowNegative: false})},
												{header: "Unit", sortable: true, dataIndex: 'unite'},
												{header: "People", sortable: true, dataIndex: 'humains', editable: rec.get('type') != 'default'? true: false, editor:new Ext.form.NumberField({allowDecimals: true, decimalPrecision:4, allowBlank: false, allowNegative: false})},
												{header: "Exposure of people", sortable: true, dataIndex: 'humains_exp', hidden: rec.get('obj_type') == 'Routes'? true: false, editable: rec.get('type') != 'default'? true: false, editor:new Ext.form.NumberField({allowDecimals: true, decimalPrecision:4, minValue: 0, maxValue: 1, allowBlank: false, allowNegative: false})},
												{header: "Speed (km/h)", sortable: true, dataIndex: 'vitesse', hidden: rec.get('obj_type') == 'Routes'? false: true, editable: rec.get('type') != 'default'? true: false, editor:new Ext.form.NumberField({allowDecimals: true, decimalPrecision:4, allowBlank: false, allowNegative: false})},
												{header: "TJM", sortable: true, dataIndex: 'tjm', hidden: rec.get('obj_type') == 'Routes'? false: true, editable: rec.get('type') != 'default'? true: false, editor:new Ext.form.NumberField({allowDecimals: false, allowBlank: false, allowNegative: false})},
												{header: "Length of the vehicle (m)", sortable: true, dataIndex: 'longueur', hidden: rec.get('obj_type') == 'Routes'? false: true, editable: rec.get('type') != 'default'? true: false, editor:new Ext.form.NumberField({allowDecimals: true, decimalPrecision:4, allowBlank: false, allowNegative: false})},
												{header: "Number of passages", sortable: true, dataIndex: 'passage', hidden: rec.get('obj_type') == 'Routes'? false: true, editable: rec.get('type') != 'default'? true: false, editor:new Ext.form.NumberField({allowDecimals: false, allowBlank: false, allowNegative: false})},
												{header: "Remarks", sortable: true, dataIndex: 'remarques', editable: rec.get('type') != 'default'? true: false, editor:new Ext.form.TextField()}
											]
										}),
										view: new Ext.grid.GroupingView({
											forceFit: true,
											showGroupName: false,
											enableNoGroups: false,
											enableGroupingMenu: false,
											hideGroupedColumn: false
										}),									
										sm: new Ext.grid.RowSelectionModel({singleSelect:true}),												
										height: 300,													
										frame: true,
										stripeRows : true,
										footer: true,
										region: 'center',
										buttons: [
											{
												text: 'Save Values',
												iconCls: 'save',
												hidden: rec.get('type') == 'default'? true: false,
												handler: function(){
													// to POST only the dirty records to update the values
													var records = [];
													var objParaEditStore = objParaEditGrid.getStore();
													for (var i= 0;i < objParaEditStore.data.length;i++) {
														recs = objParaEditStore.data.items[i];
														if (recs.dirty == true){
															records.push((recs.data));
														//	console.dir(recs.data);
														}
													}
													
													if (records.length != 0) {
														Ext.Ajax.request({
															url:'objParaInfo.php',
															params: {
																task: 'save',
																ws: workspace,
																mpIndex: rec.get('indice')
															},
															jsonData: Ext.util.JSON.encode(records),
															success: function(response, opts) {
																objParaEditStore.commitChanges(); // unmark the dirty records and commit all changes to the records
																var obj = Ext.decode(response.responseText);										
																Ext.Msg.alert('Information', obj.message);						
															}
														});
													}
													else { Ext.Msg.alert('Information', 'There are no new values to be saved!');
													}
												}
											}
										]
										
									});	
									
									/* if (rec.get('obj_type') == 'Routes'){
										var colModel = objParaEditGrid.getColumnModel();
										colModel.setHidden(6, true);
									}
									else{
										var colModel = objParaEditGrid.getColumnModel();
										colModel.setHidden(6, false);
									} */
									
									var objParaEditWin = new Ext.Window({
										title: 'Edit/View the object parameter values of this configuration:',
										width: 500,
										height: 400,
										minWidth: 300,
										minHeight: 200,
										layout: 'border',
										plain:true,
										maximizable: true,
										collapsible: true,
										closeable: true,
										closeAction:'hide',
										buttonAlign:'right',
										items: [new Ext.form.FormPanel({
											labelWidth: 75, // label settings here cascade unless overridden
											itemId: 'objParaEditForm',
											region: 'north',
											frame:true,			
											autoHeight:true,
											collapsible: true,
											isCollapsed: false,
											collapseMode: 'mini',
											defaults: {
												anchor: '100%'
											},
											items: [{
												fieldLabel: 'Name',
												xtype: 'textfield',
												name: 'name',
												allowBlank: false,
												value: rec.get('nom')
											},{
												fieldLabel: 'Description',
												xtype: 'textarea',
												name: 'description',
												allowBlank: true,
												value: rec.get('description')
											},{
												fieldLabel: 'Remarks',
												xtype: 'textarea',
												name: 'remarks',
												allowBlank: true,
												value: rec.get('remarques')
											}],
											buttons: [{
												text: 'Update',
												hidden: rec.get('type') == 'default'? true: false,
												icon:  'src/gxp/theme/img/silk/table_save.png',
												handler: function(){
													var form = objParaEditWin.getComponent('objParaEditForm').getForm();				
													if (form.isValid()) {
														form.submit({
															url: 'objParaInfo.php',
															params: {
																task: 'update',
																ws: workspace,
																ID: rec.get('id')
															},
															waitMsg : 'Please wait...',
															success: function(form, action) {
																Ext.Msg.alert('Success', action.result.message);
																Ext.getCmp('objParaInfoGrid').getStore().reload({ // refresh the grid view
																	callback: function(){
																		Ext.getCmp('objParaInfoGrid').getView().refresh();
																	}
																});
															},
															failure: function(form, action) {
																Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
															}
														});
													}
													else {
														Ext.Msg.alert('Validation Error', 'The form is not valid!' );
													}
												}
											}]	
										}), objParaEditGrid]
									}).show();	
								}
							}
						]		
					}
				],
				defaults: {
					sortable: true,
					menuDisabled: false,
					width: 5
				}
			}),			
			view: new Ext.grid.GroupingView({				
				forceFit: true,
				groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
			}),						
			frame:true,
			width: 700,
			height: 450,
			title: 'Defined object parameter configurations',
			iconCls: 'icon-grid',
			fbar  : ['->', {
				text:'Add New',
				iconCls: 'add',
				handler : function(){
					
					var objParaForm = new Ext.form.FormPanel({
						width: 450,
						frame: true,
						autoHeight: true,
						bodyStyle: "padding: 10px 10px 0 10px;",
						labelWidth: 85,					
						items: [{
							xtype:'fieldset',
							title: 'Configuration information',
							collapsible: false,
							defaults: {
								anchor: "95%",					
								msgTarget: "side"
							},
							collapsed: false,
							items :[{
								fieldLabel: 'Name',
								xtype: 'textfield',
								name: 'name',
								allowBlank: false
							},{
								fieldLabel: 'Description',
								xtype: 'textarea',
								name: 'description',
								allowBlank: true
							},{
								fieldLabel: 'Object Type',
								name: 'obj_type',
								hiddenName: 'obj_type',
								xtype:'combo',
								mode: "local",
								editable: false,
								allowBlank: false,
								store: new Ext.data.ArrayStore({
									id: 0,
									fields: [
										'myId',  // numeric value is the key
										'displayText'
									],
									data: [['Buildings', 'Buildings'],['Routes', 'Routes'],['Surfaces', 'Land Use']]  // data is local
								}),
								valueField: 'myId',
								displayField: 'displayText',
								triggerAction: 'all'								
							},{
								fieldLabel: 'Object Level',
								name: 'obj_niveau',
								hiddenName: 'obj_niveau',
								xtype:'combo',
								mode: "local",
								editable: false,
								allowBlank: false,
								store: new Ext.data.ArrayStore({
									id: 0,
									fields: [
										'myId',  // numeric value is the key
										'displayText'
									],
									data: [['Category', 'Category'],['Class', 'Class'],['Code-EconoMe', 'Code-EconoMe']]  // data is local
								}),
								valueField: 'myId',
								displayField: 'displayText',
								triggerAction: 'all'								
							},{
								fieldLabel: 'Remarks',
								xtype: 'textarea',
								name: 'remarks',
								allowBlank: true
							}]
						}],
						buttons: [{
							text: 'Add',
							icon:  'src/gxp/theme/img/silk/table_add.png',
							handler: function(){
								// create a new object parameter table and populate with default values
								var form = objParaForm.getForm();		
								var fields = form.getFieldValues();
								if (form.isValid()) {			
									form.submit({
										url: 'objParaInfo.php',
										params: {
											task: 'add',
											ws: workspace
										},
										waitMsg : 'Please wait...',
										success: function(form, action) {
											Ext.Msg.alert('Information', action.result.message); 	
											form.reset(); // reset the form
											Ext.getCmp('objParaInfoGrid').getStore().reload({ // refresh the grid view
												callback: function(){
													Ext.getCmp('objParaInfoGrid').getView().refresh();
												}
											});	
										},
										// If you don't pass success:true, it will always go here
										failure: function(form, action) {
											Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
										}	
									});	
								}	
							}
						}, {
							text: 'Reset',
							icon:  'src/gxp/theme/img/decline.png', 
							handler: function(){
								// reset the form
								objParaForm.getForm().reset();
							}
						}]
					});	
					
					new Ext.Window({
						title: 'Add a new object configuration ...',
						layout: 'form',
						autoHeight: true,
						plain:false,
						modal: true,
						resizable: false,
						closeable: true,
						closeAction:'hide',
						buttonAlign:'right',
						items: [objParaForm]
					}).show();
						
				},
				scope: this
			},{
				text:'Delete',
				iconCls: 'delete',
				handler : function(){	
					// if yes, delete the selected records from store and db
					Ext.Msg.show({
						title:'Delete the selected configurations?',
						msg: 'Are you sure to delete the selected configurations? This action is undoable, please consider removing first any referenced scenarios if these configurations are being used in risk calculation!!',
						buttons: Ext.Msg.YESNOCANCEL,
						fn: function(buttonId, text, opt){
							if (buttonId == 'yes'){
								var records = Ext.getCmp('objParaInfoGrid').getSelectionModel().getSelections();
								var ids = [];
								for (var i=0;i<records.length;i++) {
									if (records[i].get('type') != 'default'){ // if default parameter table, exclude it from the list of selected records
										ids.push(records[i].data);
									}
									else {										
										Ext.Msg.alert('Information', 'The default configuration is not allowed to delete, please exclude it from the list of selected records and try again!');
										return;
									}
								}
								Ext.Ajax.request({   
									url: 'objParaInfo.php',
									params: {
										task: 'delete',
										ws: workspace,
										IDs: Ext.encode(ids)
									},
									success: function(response, opts){
										Ext.Msg.alert('Success', 'The selected object parameter configurations have been successfully deleted!');
										// reload the store and refresh
										Ext.getCmp('objParaInfoGrid').getStore().reload({ 
												callback: function(){
													Ext.getCmp('objParaInfoGrid').getView().refresh();
												}
											});
									},
									failure: function(response, opts){
										Ext.Msg.alert('Failed', response.message);
									}
								});
							}
						}
					});	
				}
			}]
		});
		return objParaInfoGrid;
	}	
});

Ext.preg(gxp.plugins.Objects.prototype.ptype, gxp.plugins.Objects);
 
 