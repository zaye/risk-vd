/**
 * @require plugins/Tool.js
 */

Ext.ns("gxp.plugins");

gxp.plugins.Project = Ext.extend(gxp.plugins.Tool, {

  ptype: "myapp_project",  
  
  /** api: method[createNewProject]
     * Shows the window to create a project.
     */
    createNewProject: function() {
		var form = new Ext.FormPanel({
        labelWidth: 75, 
        frame:true,
		baseCls: 'x-plain',
        bodyStyle:'padding:5px 5px 0',
        items: [{
            xtype:'fieldset',
            title: 'Workspace Information',
            collapsible: true,
            autoHeight:true,
			defaults: {width: 210},
            defaultType: 'textfield',
            items :[{
                    fieldLabel: 'Name',
                    emptyText: 'Name of the workspace',
					name: 'prj_name',
					anchor:'100%',
					allowBlank: false					
                },{
                    fieldLabel: 'Description',
					emptyText: 'Description of the study area',
					xtype:'textarea',
					anchor:'100%',
                    name: 'prj_desc'
                }
            ]
        },{
            xtype:'fieldset',
            checkboxToggle:true,
            title: 'Options',
            autoHeight:true,
			defaults: {width: 210},        
            collapsed: true,
            items :[{
					fieldLabel: 'Projection', //list all possible projection system
					emptyText: 'Coordinate system of the workspace',
					name: 'prj_projection',
					xtype: 'combo',
					editable: false,
					anchor:'100%',
                    allowBlank: true,
					store: new Ext.data.ArrayStore({
						id: 0,
						fields: [
							'myId',  // numeric value is the key
							'displayText'
						],
						data: [['EPSG:2056', 'EPSG:2056']]  // data is local
					}),
					mode:'local',
					valueField: 'myId',
					displayField: 'displayText',
					triggerAction: 'all'
                },{
                    fieldLabel: 'Scale', // for local/regional, etc.
					emptyText: 'Scale of the study area',
					name: 'prj_scale',
					xtype: 'combo',
					editable: false,
					anchor:'100%',
                    allowBlank: true,
					store: new Ext.data.ArrayStore({
						id: 0,
						fields: [
							'myId',  // numeric value is the key
							'displayText'
						],
						data: [['R', 'Regional scale'],['L', 'Local scale']]  // data is local
					}),
					mode:'local',
					valueField: 'myId',
					displayField: 'displayText',
					triggerAction: 'all'
                }
            ]
        }]
	});
	
	var NewProject = new Ext.Window({
				title: 'Creation of New Workspace',
				width: 380,
				autoHeight:true,			
				layout: 'form',
				maximizable: false,
				collapsible: false,
				closeable: true,
				closeAction:'hide',
				bodyStyle:'padding:5px;',
				buttonAlign:'right',
				items: form,
				buttons: [{
					text: 'Create',
					iconCls: 'page-new',
					handler: function() {
						if (form.getForm().isValid()){
						// call the php script to create a new schema in db and its cooresponding workspace in geoserver
						Ext.Ajax.request({
							url:'createws.php',
							jsonData: Ext.util.JSON.encode(form.getForm().getFieldValues()),
							success: function(response, opts) {																
								wsStore.reload(); // reload the store to allow the user to select
								NewProject.close(); //close the window
								var obj = Ext.decode(response.responseText);
								if (obj.success == true) Ext.Msg.alert('Success', obj.msg);
								else Ext.Msg.alert('Failed', obj.msg);
							},
							failure: function(response, opts) {
								Ext.Msg.alert('Failed', 'Server-side failure with status code: ' + response.status);
							}
						});	
						}	
						else {
							Ext.Msg.alert('Validation Error', 'The form is not valid!');
						}
					}			
				}]
	});
	// show the window
	NewProject.show();
	},
	
	/** api: method[manageProjects]
     * Shows the window to manage existing projects.
     */
    manageProjects: function() {
		this.projStore = new Ext.data.GroupingStore({
				url: 'wsInfo.php',
				baseParams: {
					task: 'load',
					userID: userid
				},								
				sortInfo:{field: 'ws_name', direction: "ASC"},
				groupField:'ws_scale',
				autoLoad: true,
				reader: new Ext.data.JsonReader({
					totalProperty : 'totalCount',
					root          : 'rows',
					successProperty: 'success',
					idProperty    : 'ws_id',
					fields: [
						{name : 'ws_id', type : 'int'},
						{name : 'ws_name', type : 'String'},
						{name : 'ws_projection', type : 'String'},
						{name : 'ws_scale', type : 'String'},
						{name : 'ws_description', type : 'String'}
					] 
				})
		});	
		
		this.projGrid = new Ext.grid.GridPanel({
			id: 'projGrid',
			store: this.projStore,
			colModel: new Ext.grid.ColumnModel({
				columns: [					
					{header: "ID", dataIndex: 'ws_id', hidden: true},					
					{header: "Name", dataIndex: 'ws_name'},
					{header: "Description", dataIndex: 'ws_description'},
					{header: "Scale", dataIndex: 'ws_scale'},
					{header: "Projection", dataIndex: 'ws_projection'},
					{
						xtype: 'actioncolumn',
						items: [{
							icon: 'src/gxp/theme/img/silk/table_edit.png',  								
							tooltip: 'Edit/View Data',								
							handler: function(grid, rowIndex, colIndex) {									
								var rec = grid.getStore().getAt(rowIndex);
								var projEditWin = new Ext.Window({
									title: 'Edit the workspace information..',
									layout: 'form',
									autoHeight: true,
									plain:false,
									modal: true,
									resizable: false,
									closeable: true,
									closeAction:'hide',
									buttonAlign:'right',
									items: new Ext.form.FormPanel({
										labelWidth: 100, // label settings here cascade unless overridden
										itemId: 'projEditForm',
										frame:true,			
										bodyStyle:'padding:5px 5px 0',
										width: 350,	
										autoHeight:true,		
										defaults: {
											anchor: '100%'
										},
										items: [{
											fieldLabel: 'Name',
											xtype: 'textfield',
											name: 'name',
											readOnly: true,
											value: rec.get('ws_name')
										},{
											fieldLabel: 'Description',
											xtype: 'textarea',
											name: 'description',
											allowBlank: true,
											value: rec.get('ws_description')
										},{
											fieldLabel: 'Scale',
											name: 'scale',
											hiddenName: 'ws_scale',
											xtype: 'combo',
											editable: false,
											allowBlank: true,
											store: new Ext.data.ArrayStore({
												id: 0,
												fields: [
													'myId',  // numeric value is the key
													'displayText'
												],
												data: [['R', 'Regional scale'],['L', 'Local scale']]  // data is local
											}),
											mode:'local',
											valueField: 'myId',
											displayField: 'displayText',
											triggerAction: 'all',											
											value: rec.get('ws_scale')
										},{
											fieldLabel: 'Projection', 
											name: 'projection',
											xtype: 'combo',
											editable: false,
											allowBlank: true,
											store: new Ext.data.ArrayStore({
												id: 0,
												fields: [
													'myId',  // numeric value is the key
													'displayText'
												],
												data: [['EPSG:2056', 'EPSG:2056']]  // data is local
											}),
											mode:'local',
											valueField: 'myId',
											displayField: 'displayText',
											triggerAction: 'all',
											value: rec.get('ws_projection')
										}],
										buttons: [{
												text: 'Update',
												icon:  'src/gxp/theme/img/silk/table_save.png',
												handler: function(){
													var form = projEditWin.getComponent('projEditForm').getForm();				
													if (form.isValid()) {
														form.submit({
															url: 'wsInfo.php',
															params: {
																task: 'edit',
																wsID: rec.get('ws_id')
															},
															waitMsg : 'Please wait...',
															success: function(form, action) {
																Ext.Msg.alert('Success', action.result.message);
																projEditWin.close();
																Ext.getCmp('projGrid').getStore().reload({ // refresh the grid view
																	callback: function(){
																	Ext.getCmp('projGrid').getView().refresh();
																	}
																});
															},
															failure: function(form, action) {
																Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
															}
														});
													}
													else {
														Ext.Msg.alert('Validation Error', 'The form is not valid!' );
													}
												}
											},{
												text: 'Cancel',
												icon:  'src/gxp/theme/img/decline.png',
												handler: function(){
													projEditWin.close();
												}
											}]
										})											
									});
									
								projEditWin.show();
							}	
						}]
					}
				],
				defaults: {
					sortable: true,
					menuDisabled: false,
					width: 5
				}
			}),
			view: new Ext.grid.GroupingView({				
				forceFit: true,
				groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
			}),						
			frame:true,
			width: 700,
			height: 450,
			title: 'Avaiable workspaces (projects)',
			iconCls: 'icon-grid',
			fbar: ['->',{
				text:'Add New',
				iconCls: 'add',
				handler : function(){
					this.showNewProject();
				},
				scope: this
			},{
				text:'Delete',
				iconCls: 'delete',	
				handler : function(){
					// delete the record from store, db, geoserver
					Ext.Msg.show({
						title:'Delete the workspaces?',
						msg: 'Are you sure to delete the selected workspaces? This action is undoable, it will delete all of the data stored in selected workspaces!',
						buttons: Ext.Msg.YESNOCANCEL,
						fn: function(buttonId, text, opt){
							if (buttonId == 'yes'){
								var records = Ext.getCmp('projGrid').getSelectionModel().getSelections();
								var temp =[];
								for (var i=0;i<records.length;i++) {									
									temp.push(records[i].data);
								}
								Ext.Ajax.request({   
									url: 'wsInfo.php',
									params: {
										task: 'delete',
										objRecords: Ext.encode(temp)
									},
									success: function(response, opts){
										var obj = Ext.decode(response.responseText);										
										Ext.Msg.alert('Information', obj.message);	
										// reload the store and refresh
										Ext.getCmp('projGrid').getStore().reload({ 
												callback: function(){
													Ext.getCmp('projGrid').getView().refresh();
												}
											});
									},
									failure: function(response, opts){
										var obj = Ext.decode(response.responseText);										
										Ext.Msg.alert('Failed', obj.message);
									}
								});
							}
						}
					});
				}
			}]
		});
		
		this.projWin = new Ext.Window({
			title: 'Manage existing workspaces',
			layout: 'fit',
			plain:false,
			maximizable: true,
			collapsible: true,
			closeable: true,
			closeAction:'hide',
			buttonAlign:'right',
			items: this.projGrid
		});
		
		this.projWin.show();
	},	
  
  /** api: method[showNewProject]
     * Shows the window to create a project.
     */
    showNewProject: function() {
		this.createNewProject();
	},

  /** api: method[showProjects]
     * Shows the window to manage existing projects.
     */
    showProjects: function() {
		this.manageProjects();
	}		
 });

Ext.preg(gxp.plugins.Project.prototype.ptype, gxp.plugins.Project);