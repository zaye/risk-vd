/**
 * Copyright (c) 2011-2016 
 * 
 * Published under the GPL license.
 */
 
 /**
 * @require plugins/Tool.js
 * @require OpenLayers/WPSClient.js
 */

/** api: (define)
 *  module = gxp.plugins
 *  class = ExportLayer
 */

/** api: (extends)
 *  plugins/Tool.js
 */
Ext.namespace("gxp.plugins");

/** api: constructor
 *  .. class:: ExportLayer(config)
 *
 *    Plugin for exporting selected features from a selected layer of the map.
 *    
 */
gxp.plugins.ExportLayer = Ext.extend(gxp.plugins.Tool, {
    
    /** api: ptype = myapp_ExportLayer */
    ptype: "myapp_ExportLayer",
	
	/** api: config[buttonText]
     *  ``String``
     *  Text for the Export button (i18n).
     */
    buttonText: "Export Layer",
	
	 /** api: config[exportTooltip]
     *  ``String``
     *  Text for the Export action tooltip (i18n).
     */
    exportTooltip: "Export Layer",
	
	/** api: config[downloadMenuText]
     *  ``String``
     *  Text for Download menu item (i18n).
     */
    downloadMenuText: "Download layer",

    /** api: config[downloadActionTip]
     *  ``String``
     *  Text for Download action tooltip (i18n).
     */
    downloadActionTip: "Download the selected layer",
	
	/** api: config[addMenuText]
     *  ``String``
     *  Text for Add menu item (i18n).
     */
    addMenuText: "Create layer",

    /** api: config[addActionTip]
     *  ``String``
     *  Text for Add action tooltip (i18n).
     */
    addActionTip: "Create a new layer",
	
	/** api: method[addActions]
     */
    addActions: function() {	
		this.activeIndex = 0;
		this.button = new Ext.SplitButton({
            iconCls: "gxp-icon-exportlayer",
            tooltip: this.exportTooltip,
            buttonText: this.buttonText,
            enableToggle: true,
            toggleGroup: this.toggleGroup,
            allowDepress: true,
			disabled: true,
            handler: function(button, event) {
                if(button.pressed) {
                    button.menu.items.itemAt(this.activeIndex).setChecked(true);
                }
            },
            scope: this,
            listeners: {
                toggle: function(button, pressed) {
                    // toggleGroup should handle this
                    if(!pressed) {
                        button.menu.items.each(function(i) {
                            i.setChecked(false);
                        });
                    }
                },
                render: function(button) {
                    // toggleGroup should handle this
                    Ext.ButtonToggleMgr.register(button);
                }
            },
            menu: new Ext.menu.Menu({
                items: [
                    new Ext.menu.CheckItem(
                        new Ext.Action({
							text: this.downloadMenuText,
							icon:  'src/gxp/theme/img/silk/compress.png',
							toggleGroup: this.toggleGroup,
                            group: this.toggleGroup,
                            allowDepress: false,
							handler: function() {
								// download the layer
								var featureManager = this.target.tools[this.featureManager];
								var url = featureManager.schema.url;
								var layerName = this.selectedRecord.get("name");
								url += 'service=WFS&request=GetFeature&version=1.0.0&typeName=' + layerName +
									'&format_options=filename:'+ layerName +'.zip' +
									'&outputFormat=SHAPE-ZIP';
						
								window.open(url);
							},	
							listeners: {
                                checkchange: function(item, checked) {
                                    this.activeIndex = 0;
                                    this.button.toggle(checked);
                                    if (checked) {
                                        this.button.setIcon(item.icon);
                                    }
                                },
                                scope: this
                            },
							scope: this
						})
                    ),
                    new Ext.menu.CheckItem(
                        new Ext.Action({
							text: this.addMenuText,
							icon:  'src/gxp/theme/img/silk/map_add.png',
							toggleGroup: this.toggleGroup,
                            group: this.toggleGroup,
                            allowDepress: false,
							handler: function() {
								// create a new layer with config
								this.addOutput();
							},	
							listeners: {
                                checkchange: function(item, checked) {
                                    this.activeIndex = 1;
                                    this.button.toggle(checked);
                                    if (checked) {
                                        this.button.setIcon(item.icon);
                                    }
                                },
                                scope: this
                            },
							scope: this
						})
                    )
                ]
            })
        });
		
		this.target.on("layerselectionchange", function(record) {
			this.selectedRecord = record;			
			this.button.setDisabled(
                this.target.mapPanel.layers.getCount() <= 1 || !record || !record.get('layer') || !record.get("properties")
            );            
        }, this); 
		
		return gxp.plugins.ExportLayer.superclass.addActions.apply(this, [this.button]);
		
	},
	
	 addOutput: function(config) {
        config = config || {};
		
		// Create a WPSClient instance for use with the local GeoServer
        this.wpsClient = new OpenLayers.WPSClient({
            servers: {
                local: '/geoserver/wps'
            }
        });
		
		var featureManager = this.target.tools[this.featureManager];
		var url = featureManager.schema.url;
		var layerName = this.selectedRecord.get("name");
		
		this.exportForm = new Ext.form.FormPanel({
					url:'exportSelectedLayer.php',
					width: 400,
					frame: true,
					autoHeight: true,
					bodyStyle: "padding: 10px 10px 0 10px;",
					labelWidth: 90,
					defaults: {
						anchor: "95%",
						allowBlank: false,
						msgTarget: "side"
					},
					items: [{
						xtype: "textfield",
						name: "title",
						fieldLabel: 'Name',
						emptyText: 'Name of the layer',
						allowBlank: false
					},{
						xtype: "textarea",
						name: "abstract",
						fieldLabel: 'Description',
						emptyText: 'Description of the layer',
						allowBlank: true
					},{
						xtype: "textarea",
						name: "remarks",
						fieldLabel: 'Remarks',
						emptyText: 'Remarks of the layer',
						allowBlank: true
					},{
						xtype: "fieldset",
						title: "Properties of the layer:",
						autoHeight: true,	
						labelWidth: 95,
						collapsible: true,
						items: [{
							fieldLabel: 'Type',
							name: 'layerType',
							xtype: "combo",
							mode: "local",
							editable: false,
							store: new Ext.data.ArrayStore({
								id: 0,
								fields: ['myValue','displayText'],
								data: [['Hazards','Hazards'], ['Objects','Objects']]  // data is local
							}),
							valueField: 'myValue',
							displayField: 'displayText',
							emptyText: 'Type of the layer',
							triggerAction: 'all',
							allowBlank: false,
							listeners: {
								select: function(combo, record, index) {
									if (combo.getValue() == 'Hazards') {										
										this.exportForm.items.items[3].getComponent('hazType').setVisible(true);
										this.exportForm.items.items[3].getComponent('hazRP').setVisible(true);
										this.exportForm.items.items[3].getComponent('hazType').enable();
										this.exportForm.items.items[3].getComponent('hazRP').enable();
										this.exportForm.items.items[3].getComponent('objType').setVisible(false);
										this.exportForm.items.items[3].getComponent('objType').disable();
									}	
									else {
										this.exportForm.items.items[3].getComponent('objType').enable();
										this.exportForm.items.items[3].getComponent('objType').setVisible(true);
										this.exportForm.items.items[3].getComponent('hazType').setVisible(false);
										this.exportForm.items.items[3].getComponent('hazRP').setVisible(false);
										this.exportForm.items.items[3].getComponent('hazType').disable();
										this.exportForm.items.items[3].getComponent('hazRP').disable();
									}
								},
								scope: this
							}			
						},{
							fieldLabel: 'Hazard',
							name: 'hazType',
							hiddenName: 'hazType',
							itemId: 'hazType',
							xtype: "combo",
							mode: "local",
							editable: false,
							hidden: true,
							disabled: true,
							store: new Ext.data.ArrayStore({
								id: 0,
								fields: ['myValue','displayText'],
								data: [['AVA', 'Avalanches'],['LTO', 'Laves torrentielles'],['INO','Inondation'],['INOD','Inondation dynamique'],['INOS','Inondation statique'], ['CPB', 'Chutes de pierre et de blocs'],['GPP', 'Glissements de terrain permanents'],['GPP-TR', 'Glissements de terrain permanents (TR)'],['GSS', 'Glissements de terrain spontanés']]  // data is local
							}),
							valueField: 'myValue',
							displayField: 'displayText',
							emptyText: 'Type of the hazard layer',
							triggerAction: 'all',
							allowBlank: false
						},{
							fieldLabel: 'Return Period',
							name: 'hazRP',
							itemId: 'hazRP',
							xtype: "combo",
							mode: "local",
							editable: false,
							hidden: true,
							disabled: true,
							store: new Ext.data.ArrayStore({
								id: 0,
								fields: ['myValue','displayText'],
								data: [['TFreq','TFreq'], ['T30','T30'],['T100','T100'],['T300','T300'],['TEx','TEx']]  // data is local
							}),
							valueField: 'myValue',
							displayField: 'displayText',
							emptyText: 'Return period of the hazard layer',
							triggerAction: 'all',
							allowBlank: false
						},{
							fieldLabel: 'Object',
							name: 'objType',
							itemId: 'objType',
							xtype: "combo",
							mode: "local",
							editable: false,
							hidden: true,
							disabled: true,
							store: new Ext.data.ArrayStore({
								id: 0,
								fields: ['myValue','displayText'],
								data: [['Buildings', 'Buildings'],['Routes', 'Routes'],['Surfaces', 'Land Use']]  // data is local
							}),
							valueField: 'myValue',
							displayField: 'displayText',
							emptyText: 'Type of the object layer',
							triggerAction: 'all',
							allowBlank: false
						},{
							fieldLabel: 'Alternative',	
							emptyText: 'For which alternative?',
							itemId: 'altName',
							hiddenName: 'altID',
							name: 'altName',							
							allowBlank: false,
							editable: false,
							xtype: 'combo',					
							mode:'local',
							triggerAction: 'all',
							displayField: 'nom',
							valueField: 'id',
							store: new Ext.data.JsonStore({ 
								url: 'altInfo.php',										
								root: 'rows',
								fields : ['nom', 'id'],
								autoLoad: true,
								baseParams: {
										ws: workspace,
										task: 'load'
									}
							})							
						}]
					}],
					buttons: [{
						text: 'Add',
						icon:  'src/gxp/theme/img/silk/map_add.png',
						handler: function(){
							var form = this.exportForm.getForm(); // get the form data 
							if (form.isValid()) {
							 	var filename = form.getFieldValues().title; 	
								url += 'service=WFS&amp;request=GetFeature&amp;version=1.0.0&amp;typeName=' + layerName +
								 '&amp;outputFormat=json'; 
								
								form.submit({
									url: 'exportSelectedLayer.php',
									params: {
										ws: workspace,
										lyrname: layerName
									},
									success: function(form, action) {
										var obj = action.result;
										var names = {}; 
										var temp = obj.message;
										names[temp] = true; 	
										
										if (obj.success == true){
											// add the newly exported (created)layer to the map
											var source = app.tools.addlyrs.target.layerSources["local"]; // need to reassign the source in case other sources such as google are being added by users										
											app.tools.addlyrs.setSelectedSource(source);
											
											app.tools.addlyrs.selectedSource.store.load({
												callback: function(records, options, success) {
													var newRecords = [];
													var last = 0;
													app.tools.addlyrs.selectedSource.store.each(function(record, index) {
														if (record.get("name") in names) {
															last = index;
															newRecords.push(record);
														}
													});	
													app.tools.addlyrs.addLayers(newRecords, false);		
												},
												scope: this                                            
											});	

											//	show the message box to initate the window for selecting a workspace
											Ext.MessageBox.show({
												   title: 'Help Support!',
												   msg: 'The new layer is created and added into the map.' + '\n' + obj.dbmessage,
												   buttons: Ext.MessageBox.OK,
												   animEl: 'mb9',
												   icon: Ext.MessageBox.INFO
											   });
										}
										else {
											Ext.Msg.alert('Failed', obj.message);
										}
										
										exportWin.close();
									},// If you don't pass success:true, it will always go here
									failure: function(form, action) {
										Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
										exportWin.close();
									}
								});	
							}
							else {
								Ext.Msg.alert('Information', 'The form is not valid!');
							}
						},
							scope: this
					},{
						text: 'Reset',
						icon:  'src/gxp/theme/img/silk/arrow_refresh.png', 
						handler: function(){
							this.exportForm.getForm().reset(); // reset the form data 
							this.exportForm.items.items[3].getComponent('hazType').setVisible(false);							
							this.exportForm.items.items[3].getComponent('objType').setVisible(false);
							this.exportForm.items.items[3].getComponent('hazRP').setVisible(false);
							this.exportForm.items.items[3].getComponent('hazType').disable();							
							this.exportForm.items.items[3].getComponent('objType').disable();
							this.exportForm.items.items[3].getComponent('hazRP').disable();
						},
						scope: this
					}]	
				});	
				
				var exportWin = new Ext.Window({
					title: "Create a new layer ...",
					layout: 'form',
					autoHeight: true,
					plain:false,
					modal: true,
					maximizable: false,
					collapsible: false,
					closeable: true,
					closeAction:'hide',
					buttonAlign:'right',
					items: [this.exportForm],
					tools:[
							{
								id:'help',
								qtip: 'Get Help',
								handler: function(event, toolEl, panel){
									Ext.Msg.show({
										title:'Help Information',
										msg: 'Enter the required input data and click the Add button to import (or create a new layer of) the currently selected map layer.' + "<br><br>" + 'The is useful especially for the creation of new layers after consideration of a risk reduction alternative scenario. In this case, select the corresponding alternative scenario name in the Alternative combo box.',
										buttons: Ext.Msg.OK,															   
										animEl: 'elId',
										icon: Ext.MessageBox.INFO
									});
							}
						}]
				}).show();	
	}	
});

Ext.preg(gxp.plugins.ExportLayer.prototype.ptype, gxp.plugins.ExportLayer);
	