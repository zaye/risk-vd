/**
 * Copyright (c) 2011-2016 
 * 
 * Published under the GPL license.
 */
 
/**
 * @require plugins/Tool.js
 * @require GeoExt/widgets/Action.js
 * @require OpenLayers/Control/DrawFeature.js
 * @require OpenLayers/Handler/RegularPolygon.js
 */
 
 /** api: (define)
 *  module = gxp.plugins
 *  class = RiskCalculator
 */
 
 /** api: (extends)
 *  plugins/Tool.js
 */
 
Ext.ns("gxp.plugins");

/** api: constructor
 *  .. class:: RiskCalculator(config)
 *
 *    Plugin for calculating risk based on vector-based spatial analysis.
 *    
 */

gxp.plugins.RiskCalculator = Ext.extend(gxp.plugins.Tool, {

	 /** api: ptype = myapp_RiskCalculator */
	ptype: "myapp_RiskCalculator",
	
	/** api: config[riskMenuText]
     *  ``String``
     *  Text for remove menu item (i18n).
     */
    riskMenuText: "Calculate risk",

    /** api: config[riskActionTip]
     *  ``String``
     *  Text for remove action tooltip (i18n).
     */
    riskActionTip: "Calculate risk",
    
    /** api: method[addActions]
     */
    addActions: function() {
	
		var actions = gxp.plugins.RiskCalculator.superclass.addActions.apply(this, [{
            menuText: this.riskMenuText,
            iconCls: "gxp-icon-riskcalculator",
			text: 'Risk Calculator',
            tooltip: this.riskActionTip,
            handler: function() {
			
				// Get a reference to the vector layer from app.js
				/* this.layer = app.getLayerRecordFromMap({
					name: 'sketch',
					source: 'ol'
				}).getLayer(); */
				
				this.layer = new OpenLayers.Layer.Vector("Zone_layer");				
				
				// Some action defaults for a draw polygon button
				var actionDefaults = {
					map: app.mapPanel.map,
					enableToggle: true,
					toogleGroup: "controls",
					allowDepress: true
				};
				
				// Control of the polygon drawing
				var poly = new OpenLayers.Control.DrawFeature(
					this.layer, OpenLayers.Handler.Polygon,{						
						/* handlerOptions: {
							sides: 4,
							snapAngle: 90,
							irregular: true,
							persist: true
						}, */
						eventListeners: {
							featureadded: function (event) {								
								Ext.getCmp('areaPoly').setValue(event.feature.geometry); // set value of the polygon coordinates in 900913  
								poly.deactivate();
							}
						},
						callbacks: {
							create: function(data) { // remove previously drawn feature
								if (this.layer.features.length > 0)
								{
									this.layer.removeAllFeatures();
								}
							}/* ,	
							done: function(bbox) {							
								var bounds = bbox.getBounds(); // get the bounding box (900913 coordinates)							
								boundsWGS84 = bounds.clone().transform(app.mapPanel.map.getProjectionObject(), new OpenLayers.Projection("EPSG:4326"));	// get the bounding box (4326 coordinates)	
								Ext.getCmp('areaBBox').setValue(boundsWGS84); // set value of the bounding box 
								
								var feature = new OpenLayers.Feature.Vector(bounds.toGeometry());	// add drawn feature				 
								this.layer.addFeatures(feature);
								box.deactivate();  
							} */
						} 
				});	

				// Control of the polygon editing/dragging
				var modify = new OpenLayers.Control.ModifyFeature(this.layer, {
					mode: OpenLayers.Control.ModifyFeature.RESHAPE | OpenLayers.Control.ModifyFeature.DRAG, 
					standalone: false
				});		
				
				this.layer.events.on({
					afterfeaturemodified: function (event) {
						Ext.getCmp('areaPoly').setValue(event.feature.geometry); // set value of the polygon coordinates in 900913 
						modify.deactivate();
					}
				});
				
				this.riskCalForm = new Ext.form.FormPanel({
					labelWidth: 75, // label settings here cascade unless overridden
					url:'riskScenarioInfo.php',
					frame:true,	
					bodyStyle:'padding:5px 5px 0',
					width: 450,	
					autoHeight:true,		
					items: [{
						xtype:'fieldset',
						title: 'Risk Scenario Information',
						defaultType: 'textfield',
						defaults: {
							anchor: "95%",
							allowBlank: false,
							msgTarget: "side"
						},
						collapsible: true,
						autoHeight: true,
						items :[{
								fieldLabel: 'Name',
								name: 'scenario_name',
								emptyText: 'Name of the scenario ...'
							},{
								fieldLabel: 'Description',
								xtype: 'textarea',
								name: 'scenario_description',
								allowBlank: true,
								emptyText: 'Description of the scenario ...'
							},{
								fieldLabel: 'Alternative',
								name: 'alt_scenario_name',
								hiddenName: 'alt_scenario_name',
								itemId: 'alt_scenario_name',
								editable: false,
								xtype: 'combo',					
								mode:'local',
								triggerAction: 'all',
								displayField: 'nom',
								valueField: 'id',
								store: new Ext.data.JsonStore({ 
									url: 'altInfo.php',										
									root: 'rows',
									fields : ['nom', 'id'],
									autoLoad: true,
									baseParams: {
										ws: workspace,
										task: 'load'
									}
								}),
								emptyText: 'Select the alternative scenario ...'
							},{
								fieldLabel: 'Option',
								name: 'option',
								hiddenName: 'option',
								xtype:'combo',
								emptyText: 'Select the extent of risk calculation ...',
								mode: "local",
								editable: false,
								store: new Ext.data.ArrayStore({
									id: 0,
									fields: [
										'myId',  // numeric value is the key
										'displayText'
									],
									data: [['L', 'Layer-based (as a whole)'], ['Z', 'Zone-based (as a defined polygon area)']]  // data is local
								}),
								valueField: 'myId',
								displayField: 'displayText',
								triggerAction: 'all',
								listeners: {
									select: function(combo, record, index) {
										if (combo.getValue() == 'Z') {
											app.mapPanel.map.addLayer(this.layer);
											this.riskCalForm.items.items[0].items.items[4].setVisible(true);
											Ext.getCmp('areaPoly').enable();
										}
										else {
											app.mapPanel.map.removeLayer(this.layer);
											this.riskCalForm.items.items[0].items.items[4].setVisible(false);
											Ext.getCmp('areaPoly').disable();
										}
									},
									scope: this
								}	
							},{
								fieldLabel: 'Area',
								xtype : 'compositefield',	
								hidden: true,
								items: [{
									xtype: 'textfield',
									id: 'areaPoly',
									readOnly: true,
									disabled: true,
									name: 'poly',
									flex: 1.8,
									emptyText: 'Define the zone using DRAW tool ...'
								}, 	new Ext.Button(
										new GeoExt.Action(Ext.apply({
										text: 'Draw',
										control: poly
									}, actionDefaults))),
									new Ext.Button(
										new GeoExt.Action(Ext.apply({
										text: 'Modify',
										control: modify
									}, actionDefaults)))
								]
							}]
						},{
							xtype:'tabpanel',
							plain:true,			
							activeTab: 0,
							height: 350,
							deferredRender: false,
							defaults:{bodyStyle:'padding:10px'},
							items:[{
								title: 'Hazard',
								defaultType: 'textfield',
								layout:'form',
								defaults: {
									anchor: "95%",
									allowBlank: false,
									msgTarget: "side"
								},
								items :[{
									fieldLabel: 'Type',
									name: 'haz_type',
									hiddenName: 'haz_type',
									itemId: 'haz_type',
									xtype:'combo',
									mode: "local",
									editable: false,
									emptyText: 'Select the hazard type ...',
									store: new Ext.data.ArrayStore({
											id: 0,
											fields: [
												'myId',  // numeric value is the key
												'displayText'
											],
											data: [['AVA', 'Avalanches'],['LTO', 'Laves torrentielles'],['INO','Inondation'],['INOD','Inondation dynamique'],['INOS','Inondation statique'],['CPB', 'Chutes de pierre et de blocs'],['GPP', 'Glissements de terrain permanents'],['GPP-TR', 'Glissements de terrain permanents (TR)'],['GSS', 'Glissements de terrain spontanÚs']]  // data is local
									}),
									valueField: 'myId',
									displayField: 'displayText',
									triggerAction: 'all',
									listeners: {
										select: function(combo, record, index) {
											this.riskCalForm.items.items[1].items.items[0].items.items[3].getComponent('hazNameTfreq').clearValue();
											this.riskCalForm.items.items[1].items.items[0].items.items[3].getComponent('hazNameT30').clearValue();
											this.riskCalForm.items.items[1].items.items[0].items.items[3].getComponent('hazNameT100').clearValue();
											this.riskCalForm.items.items[1].items.items[0].items.items[3].getComponent('hazNameT300').clearValue();
											this.riskCalForm.items.items[1].items.items[0].items.items[3].getComponent('hazNameTex').clearValue();
											this.riskCalForm.items.items[1].items.items[0].items.items[3].getComponent('hazNameTfreq').getStore().reload({ params: 
												{ 	ws: workspace,													
													task: 'loadSelected',
													type: combo.getValue(),
													prob: 'TFreq'
												} // reload the store to retrieve uploaded maps from the selected type and return period
											});
											this.riskCalForm.items.items[1].items.items[0].items.items[3].getComponent('hazNameT30').getStore().reload({ params: 
												{ 	ws: workspace,													
													task: 'loadSelected',
													type: combo.getValue(),
													prob: 'T30'
												} // reload the store to retrieve uploaded maps from the selected type and return period
											});
											this.riskCalForm.items.items[1].items.items[0].items.items[3].getComponent('hazNameT100').getStore().reload({ params: 
												{ 	ws: workspace,													
													task: 'loadSelected',
													type: combo.getValue(),
													prob: 'T100'
												} // reload the store to retrieve uploaded maps from the selected type and return period
											});
											this.riskCalForm.items.items[1].items.items[0].items.items[3].getComponent('hazNameT300').getStore().reload({ params: 
												{ 	ws: workspace,													
													task: 'loadSelected',
													type: combo.getValue(),
													prob: 'T300'
												} // reload the store to retrieve uploaded maps from the selected type and return period
											});
											this.riskCalForm.items.items[1].items.items[0].items.items[3].getComponent('hazNameTex').getStore().reload({ params: 
												{ 	ws: workspace,													
													task: 'loadSelected',
													type: combo.getValue(),
													prob: 'TEx'
												} // reload the store to retrieve uploaded maps from the selected type and return period
											});											
										},
										scope: this
									}
								}/* ,{
									fieldLabel: 'Layer',
									name: 'haz_name',
									itemId: 'hazName',
									hiddenName: 'haz_name',
									xtype: 'combo',			
									emptyText: 'Select the hazard layer ...',
									triggerAction: 'all',
									editable: false,
									mode:'local',
									triggerAction: 'all',
									displayField: 'nom',
									valueField: 'indice',
									store: new Ext.data.JsonStore({ //list all the available layers
										url: 'hazInfo.php',										
										root: 'rows',
										fields : ['nom', 'indice']
									})									
								} */,{
									fieldLabel: 'PrA',
									name: 'spatial_prob',
									hiddenName: 'spatial_prob',
									xtype:'combo',
									itemId: 'PrA',
									emptyText: 'Select the mode for PrA parameters ...',
									mode: "local",
									editable: false,
									store: new Ext.data.ArrayStore({
										id: 0,
										fields: [
											'myId',  // numeric value is the key
											'displayText'
										],
										data: [['D', 'Default'], ['M', 'User Defined']]  // data is local
									}),
									valueField: 'myId',
									displayField: 'displayText',
									triggerAction: 'all',
									listeners: {
										select: function(combo, record, index) {
											if (combo.getValue() == 'M') {
												this.riskCalForm.items.items[1].items.items[0].getComponent('PrA_manual').enable();
												this.riskCalForm.items.items[1].items.items[0].getComponent('PrA_manual').setVisible(true);
												this.riskCalForm.items.items[1].items.items[0].getComponent('PrA_manual').getStore().reload({ params: 
													{ 	ws: workspace,
														task: 'loadUserDefined'
													} 
												});
													/* this.riskCalForm.items.items[1].items.items[0].getComponent('valuePrA').enable();
													this.riskCalForm.items.items[1].items.items[0].getComponent('valuePrA').setVisible(true); */
											}
											else {
												this.riskCalForm.items.items[1].items.items[0].getComponent('PrA_manual').disable();
												this.riskCalForm.items.items[1].items.items[0].getComponent('PrA_manual').setVisible(false);
													/* this.riskCalForm.items.items[1].items.items[0].getComponent('valuePrA').disable();
													this.riskCalForm.items.items[1].items.items[0].getComponent('valuePrA').setVisible(false); */
												}
											},
										scope: this
									}
								},{
									fieldLabel: 'Name',
									name: 'spatial_prob_manual',
									hiddenName: 'spatial_prob_manual',
									itemId: 'PrA_manual',
									xtype: 'combo',
									disabled: true,
									hidden: true,   
									emptyText: 'Select the name of PrA configuration ...',
									triggerAction: 'all',
									editable: false,
									mode:'local',
									triggerAction: 'all',
									displayField: 'nom',
									valueField: 'indice',
									store: new Ext.data.JsonStore({ 
										url: 'PrAInfo.php',	
										autoLoad: false,
										root: 'rows',
										fields : ['nom', 'indice']
									})
								},{
									xtype:'fieldset',
									title: 'Intensity Layers Information',
									defaults: {
										anchor: "95%",
										allowBlank: false,
										msgTarget: "side"
									},
									autoHeight: true,
									items :[{
										fieldLabel: 'T<30',
										name: 'haz_name_Tfreq',
										hiddenName: 'haz_name_Tfreq',
										itemId: 'hazNameTfreq',
										xtype: 'lovcombo',	
										emptyText: 'Select the hazard layer ...',
										triggerAction: 'all',
										editable: false,
										hideOnSelect:false,
										mode:'local',
										triggerAction: 'all',
										displayField: 'nom',
										valueField: 'indice',
										store: new Ext.data.JsonStore({ //list all the available layers
											url: 'hazInfo.php',										
											root: 'rows',
											fields : ['nom', 'indice', 'ans', 'temp_de_retour']
										})										
									}, {									
										fieldLabel: 'T30',
										name: 'haz_name_T30',
										hiddenName: 'haz_name_T30',
										itemId: 'hazNameT30',
										xtype: 'combo',	
										emptyText: 'Select the hazard layer ...',
										triggerAction: 'all',
										editable: false,
										mode:'local',
										triggerAction: 'all',
										displayField: 'nom',
										valueField: 'indice',
										store: new Ext.data.JsonStore({ //list all the available layers
											url: 'hazInfo.php',										
											root: 'rows',
											fields : ['nom', 'indice', 'ans', 'temp_de_retour']
										})
									},{
										fieldLabel: 'T100',
										name: 'haz_name_T100',
										hiddenName: 'haz_name_T100',
										itemId: 'hazNameT100',
										xtype: 'combo',	
										emptyText: 'Select the hazard layer ...',
										triggerAction: 'all',
										editable: false,
										mode:'local',
										triggerAction: 'all',
										displayField: 'nom',
										valueField: 'indice',
										store: new Ext.data.JsonStore({ //list all the available layers
											url: 'hazInfo.php',										
											root: 'rows',
											fields : ['nom', 'indice', 'ans', 'temp_de_retour']
										})
									},{
										fieldLabel: 'T300',
										name: 'haz_name_T300',
										hiddenName: 'haz_name_T300',
										itemId: 'hazNameT300',
										xtype: 'combo',	
										emptyText: 'Select the hazard layer ...',
										triggerAction: 'all',
										editable: false,
										mode:'local',
										triggerAction: 'all',
										displayField: 'nom',
										valueField: 'indice',
										store: new Ext.data.JsonStore({ //list all the available layers
											url: 'hazInfo.php',										
											root: 'rows',
											fields : ['nom', 'indice', 'ans', 'temp_de_retour']
										})
									},{
										fieldLabel: 'T>300',
										name: 'haz_name_Tex',
										hiddenName: 'haz_name_Tex',
										itemId: 'hazNameTex',
										xtype: 'lovcombo',	
										emptyText: 'Select the hazard layer ...',
										triggerAction: 'all',
										editable: false,
										hideOnSelect:false,
										mode:'local',
										triggerAction: 'all',
										displayField: 'nom',
										valueField: 'indice',
										store: new Ext.data.JsonStore({ //list all the available layers
											url: 'hazInfo.php',										
											root: 'rows',
											fields : ['nom', 'indice', 'ans', 'temp_de_retour']
										})
									}]
								}/* ,{
									title: 'Spatial Probability Values',
									name: 'value_pra',
									xtype: 'editorgrid',
									itemId: 'valuePrA',
									height: 150,						
									disabled: true,
									hidden: true,                       
									store: new Ext.data.ArrayStore({ //list all the available layers 
										id: 0,
										fields: [
											{name:'RP'},
											{name:'frequence', allowBlank: false},
											{name:'valuePrA', allowBlank: false}
										],
										data: [['TFreq', 0.1, 0.8], ['T30', 0.033, 0.7], ['T100', 0.01, 0.6], ['T300', 0.0033, 0.8], ['TEx', 0.001, 1]]  // data is local									
									}),
									colModel: new Ext.grid.ColumnModel({
										defaults: {
											width: 120,
											sortable: false
										},
										columns: [ 
											{header: "Return Period", dataIndex: 'RP'},
											{header: "Frequency", dataIndex: 'frequence', editor: new Ext.form.NumberField()},
											{header: "PrA Value", dataIndex: 'valuePrA', editor: new Ext.form.NumberField()}		
										]
									}),
									viewConfig: {
										forceFit: true
									},
									frame: true
								} */
								]
							},{
								title: 'Object',
								defaultType: 'textfield',
								layout: 'form',
								defaults: {
									anchor: "95%",
									allowBlank: false,
									msgTarget: "side"
								},
								items :[{
									fieldLabel: 'Type',
									name: 'object_type',
									hiddenName: 'object_type',
									itemId: 'object_type',
									xtype:'combo',
									mode: "local",
									editable: false,
									emptyText: 'Select the object type ...',
									store: new Ext.data.ArrayStore({
											id: 0,
											fields: [
												'myId',  // numeric value is the key
												'displayText'
											],
											data: [['Buildings', 'Buildings'],['Routes', 'Routes'],['Surfaces', 'Land Use']]  // data is local
									}),
									valueField: 'myId',
									displayField: 'displayText',
									triggerAction: 'all',
									listeners: {
										select: function(combo, record, index) {
											this.riskCalForm.items.items[1].items.items[1].getComponent('objName').clearValue();
											this.riskCalForm.items.items[1].items.items[1].getComponent('objName').getStore().reload({ params: 
												{ 	ws: workspace,													
													task: 'loadSelected',
													type: combo.getValue()
												} // reload the store to retrieve uploaded maps from the selected type
											});											
										},
										scope: this
									}
								},{
									fieldLabel: 'Layer',
									name: 'object_name',
									itemId: 'objName',
									hiddenName: 'object_name',
									xtype: 'combo',			
									emptyText: 'Select the object layer ...',
									triggerAction: 'all',
									editable: false,
									mode:'local',
									triggerAction: 'all',
									displayField: 'nom',
									valueField: 'indice',
									store: new Ext.data.JsonStore({ //list all the available layers
										url: 'objInfo.php',										
										root: 'rows',
										fields : ['nom', 'indice']
									})									
								},{
										fieldLabel: 'Option',
										xtype: 'combo',
										mode: 'local',
										triggerAction: 'all',
										editable: false,
										emptyText: 'Select the level of object parameters ...',
										name: 'obj_level',
										hiddenName: 'obj_level',	
										itemId: 'objectLevel',
										store: new Ext.data.ArrayStore({
											id: 0,
											fields: [
												'myId',  // numeric value is the key
												'displayText'
											],
											data: [['Category', 'Category'],['Class', 'Class'],['Code-EconoMe', 'Code-EconoMe'],['Manual', 'Manual']]  // data is local
										}),
										valueField: 'myId',
										displayField: 'displayText',
										listeners: {
											select: function(combo, record, index) {
												if ( combo.getValue() == 'Category' || combo.getValue() == 'Class' || combo.getValue() == 'Code-EconoMe') { 
													this.riskCalForm.items.items[1].items.items[1].getComponent('objectMode').enable();
												}
												else { 
													this.riskCalForm.items.items[1].items.items[1].getComponent('objectMode').disable();													
												}
												this.riskCalForm.items.items[1].items.items[1].getComponent('objectMode').clearValue();
												this.riskCalForm.items.items[1].items.items[1].getComponent('object_manual').disable();
												this.riskCalForm.items.items[1].items.items[1].getComponent('object_manual').setVisible(false);
											},
											scope: this
										}	
								},{
										fieldLabel: 'Mode',
										name: 'object_mode',
										hiddenName: 'object_mode',
										xtype:'combo',
										itemId: 'objectMode',
										emptyText: 'Select the mode for option (Category/Class/Code-EconoMe) ...',
										mode: "local",
										disabled: true,
										editable: false,
										store: new Ext.data.ArrayStore({
												id: 0,
												fields: [
													'myId',  // numeric value is the key
													'displayText'
												],
												data: [['D', 'Default'], ['M', 'User Defined']]  // data is local
										}),
										valueField: 'myId',
										displayField: 'displayText',
										triggerAction: 'all',
										listeners: {
											select: function(combo, record, index) {
												if (combo.getValue() == 'M') {
													this.riskCalForm.items.items[1].items.items[1].getComponent('object_manual').clearValue();
													this.riskCalForm.items.items[1].items.items[1].getComponent('object_manual').enable();
													this.riskCalForm.items.items[1].items.items[1].getComponent('object_manual').setVisible(true);
													this.riskCalForm.items.items[1].items.items[1].getComponent('object_manual').getStore().reload({ params: 
														{ 	ws: workspace,
															task: 'loadUserDefined',
															objLevel: this.riskCalForm.items.items[1].items.items[1].getComponent('objectLevel').getValue()
														} 
													});
													/* this.riskCalForm.items.items[1].items.items[1].getComponent('valueObj').enable();
													this.riskCalForm.items.items[1].items.items[1].getComponent('valueObj').setVisible(true); */
												}
												else {
													this.riskCalForm.items.items[1].items.items[1].getComponent('object_manual').disable();
													this.riskCalForm.items.items[1].items.items[1].getComponent('object_manual').setVisible(false);
													/* this.riskCalForm.items.items[1].items.items[1].getComponent('valueObj').disable();
													this.riskCalForm.items.items[1].items.items[1].getComponent('valueObj').setVisible(false); */
												}
											},
											scope: this
										}
								},{
									fieldLabel: 'Name',
									name: 'object_manual',
									hiddenName: 'object_manual',
									itemId: 'object_manual',
									xtype: 'combo',
									disabled: true,
									hidden: true,   
									emptyText: 'Select the name of object configuration ...',
									triggerAction: 'all',
									editable: false,
									mode:'local',
									triggerAction: 'all',
									displayField: 'nom',
									valueField: 'indice',
									store: new Ext.data.JsonStore({ 
										url: 'objParaInfo.php',	
										autoLoad: false,
										root: 'rows',
										fields : ['nom','indice']
									})
								}]
									/* ,{
									title: 'Parameters (value/people) of the object',
									name: 'value_object',
									xtype: 'editorgrid',
									itemId: 'valueObj',
									height: 180,						
									disabled: true,
									hidden: true,                       
									store: new Ext.data.ArrayStore({ //list the default values
										id: 0,
										fields: [
											{name:'category'},
											{name:'value', allowBlank: false},
											{name:'unit'},
											{name:'person', allowBlank: false},
											{name:'exp', allowBlank: false}
										],
										data: [['1010',0,'unit',0,0],
											['1021',650000,'Housing unit',2.24,0.7],
											['1025',550000,'Housing unit',2.24,0.7],
											['1030',0,'Housing unit (principal)',0,0],
											['1040',0,'Housing unit (partial)',0,0],
											['1060',0,'m2',0,0],
											['1080',0,'unit',0,0]]											
									}),
									colModel: new Ext.grid.ColumnModel({
										defaults: {
											width: 70,
											sortable: false
										},
										columns: [ 
											{header: "Code GKAT", dataIndex: 'category'},
											{header: "CHF (per unit)", dataIndex: 'value', editor: new Ext.form.NumberField()},
											{header: "Unit", dataIndex: 'unit'},
											{header: "People", dataIndex: 'person', editor: new Ext.form.NumberField()},
											{header: "Exposition", dataIndex: 'exp', editor: new Ext.form.NumberField()}
										]
									}),
									viewConfig: {
										forceFit: true
									},
									frame: true
								} */
								
							},{
								title: 'Vulnerability',
								defaultType: 'textfield',
								layout: 'form',
								defaults: {
									anchor: "95%",
									allowBlank: false,
									msgTarget: "side"
								},
								items :[{
									fieldLabel: 'Option',
									xtype: 'combo',
									mode: 'local',
									triggerAction: 'all',
									editable: false,
									emptyText: 'Select the option for vulnerability parameters ...',
									name: 'vul_level',
									hiddenName: 'vul_level',	
									itemId: 'vulLevel',
									store: new Ext.data.ArrayStore({
											id: 0,
											fields: [
												'myId',  // numeric value is the key
												'displayText'
											],
											data: [['Category', 'Category'],['Class', 'Class'],['Code-EconoMe', 'Code-EconoMe'],['Manual', 'Manual']]  // data is local
										}),
										valueField: 'myId',
										displayField: 'displayText',
										listeners: {
											select: function(combo, record, index) {
												if ( combo.getValue() == 'Category' || combo.getValue() == 'Class' || combo.getValue() == 'Code-EconoMe') { 
													this.riskCalForm.items.items[1].items.items[2].getComponent('vulMode').enable();
												}
												else { 
													this.riskCalForm.items.items[1].items.items[2].getComponent('vulMode').disable();													
												}
												this.riskCalForm.items.items[1].items.items[2].getComponent('vulMode').clearValue();												
												this.riskCalForm.items.items[1].items.items[2].getComponent('vul_manual').disable();
												this.riskCalForm.items.items[1].items.items[2].getComponent('vul_manual').setVisible(false);												
											},
											scope: this
										}	
								},{
									fieldLabel: 'Mode',
									name: 'vul_mode',
									hiddenName: 'vul_mode',
									xtype:'combo',
									itemId: 'vulMode',
									emptyText: 'Select the mode for option (Category/Class/Code-EconoMe)...',
									mode: "local",									
									editable: false,
									store: new Ext.data.ArrayStore({
											id: 0,
											fields: [
												'myId',  // numeric value is the key
												'displayText'
											],
											data: [['D', 'Default'], ['M', 'User Defined']]  // data is local
									}),
									valueField: 'myId',
									displayField: 'displayText',
									triggerAction: 'all',
									listeners: {
										select: function(combo, record, index) {
											if (combo.getValue() == 'M') {
												this.riskCalForm.items.items[1].items.items[2].getComponent('vul_manual').clearValue();
												this.riskCalForm.items.items[1].items.items[2].getComponent('vul_manual').enable();
												this.riskCalForm.items.items[1].items.items[2].getComponent('vul_manual').setVisible(true);
												this.riskCalForm.items.items[1].items.items[2].getComponent('vul_manual').getStore().reload({ params: 
													{ 	ws: workspace,
														task: 'loadUserDefined',
														hazType: this.riskCalForm.items.items[1].items.items[0].getComponent('haz_type').getValue(),
														objType: this.riskCalForm.items.items[1].items.items[1].getComponent('object_type').getValue(),
														vulLevel: this.riskCalForm.items.items[1].items.items[2].getComponent('vulLevel').getValue()
													} 
												});
										/* 		this.riskCalForm.items.items[1].items.items[2].getComponent('valueVul').enable();
												this.riskCalForm.items.items[1].items.items[2].getComponent('valueVul').setVisible(true); */
											}
											else {
												this.riskCalForm.items.items[1].items.items[2].getComponent('vul_manual').clearValue();
												this.riskCalForm.items.items[1].items.items[2].getComponent('vul_manual').disable();
												this.riskCalForm.items.items[1].items.items[2].getComponent('vul_manual').setVisible(false);
												/* this.riskCalForm.items.items[1].items.items[2].getComponent('valueVul').disable();
												this.riskCalForm.items.items[1].items.items[2].getComponent('valueVul').setVisible(false); */
											}
										},
										scope: this
									}
								},{
									fieldLabel: 'Name',
									name: 'vul_manual',
									hiddenName: 'vul_manual',
									itemId: 'vul_manual',
									xtype: 'combo',
									disabled: true,
									hidden: true,   
									emptyText: 'Select the name of vulnerability configuration ...',
									triggerAction: 'all',
									editable: false,
									mode:'local',
									triggerAction: 'all',
									displayField: 'nom',
									valueField: 'indice',
									store: new Ext.data.JsonStore({ 
										url: 'vulInfo.php',	
										autoLoad: false,
										root: 'rows',
										fields : ['nom', 'indice']
									})
								}/* ,{
									title: 'Vulnerability-Intensity parameters (object/people)',
									name: 'value_vul',
									xtype: 'editorgrid',
									itemId: 'valueVul',
									height: 180,						
									disabled: true,
									hidden: true,                       
									store: new Ext.data.ArrayStore({ //list the default values
										id: 0,
										fields: [
											{name:'category'},
											{name:'LO', allowBlank: false},
											{name:'MO', allowBlank: false},
											{name:'HO', allowBlank: false},
											{name:'LP', allowBlank: false},
											{name:'MP', allowBlank: false},
											{name:'HP', allowBlank: false}
										],
										data: [['1010',1,1,1,1,1,1],
											['1021',0.03,0.5,1,0.0015,0.15,0.4],
											['1025',0.03,0.4,0.8,0.0015,0.06,0.16],
											['1030',1,1,1,1,1,1],
											['1040',1,1,1,1,1,1],
											['1060',1,1,1,1,1,1],
											['1080',1,1,1,1,1,1]]												
									}),
									colModel: new Ext.grid.ColumnModel({
										defaults: {
											width: 70,
											sortable: false
										},
										columns: [ 
											{header: "Code GKAT", dataIndex: 'category'},
											{header: "Low (Object)", dataIndex: 'LO', editor: new Ext.form.NumberField()},
											{header: "Medium (Object)", dataIndex: 'MO', editor: new Ext.form.NumberField()},
											{header: "High (Object)", dataIndex: 'HO', editor: new Ext.form.NumberField()},
											{header: "Low (People)", dataIndex: 'LP', editor: new Ext.form.NumberField()},
											{header: "Medium (People)", dataIndex: 'MP', editor: new Ext.form.NumberField()},
											{header: "High (People)", dataIndex: 'HP', editor: new Ext.form.NumberField()}
										]
									}),
									viewConfig: {
										forceFit: true
									},
									frame: true
								} */
								]
							}]	
						}],
						buttons: [{
							text: 'Run',
							icon:  'src/gxp/theme/img/silk/control_play_blue.png', 
							handler: function(){
								var form = this.riskCalForm.getForm(); // get the form data	
								
								// get array of any checked field values (indice and year) for the frequent events
								var multiComboTfreq = this.riskCalForm.items.items[1].items.items[0].items.items[3].getComponent('hazNameTfreq');
								var str = multiComboTfreq.getCheckedValue();
								var res = str.split(","); var hazRPTfreq = [];
								
								for (i = 0; i < res.length; i++) { 
									var tempRec = multiComboTfreq.findRecord(multiComboTfreq.valueField || multiComboTfreq.displayField, res[i]);
									hazRPTfreq.push([tempRec.get('indice'),tempRec.get('ans'),tempRec.get('temp_de_retour')]);
								}
								
								// get array of any checked field values (indice and year) for the extreme events
								var multiComboTex = this.riskCalForm.items.items[1].items.items[0].items.items[3].getComponent('hazNameTex');								
								var str = multiComboTex.getCheckedValue();
								var res = str.split(","); var hazRPTex = [];
								
								for (i = 0; i < res.length; i++) { 
									var tempRec = multiComboTex.findRecord(multiComboTex.valueField || multiComboTex.displayField, res[i]);
									hazRPTex.push([tempRec.get('indice'),tempRec.get('ans'),tempRec.get('temp_de_retour')]);
								}						
								
								// submit the form if valid								
								if (form.isValid()) {
									form.submit({
										url: 'riskScenarioInfo.php',
										params: {
											task: 'add',
											ws: workspace,
											hazFreqArr: Ext.encode(hazRPTfreq),
											hazExArr: Ext.encode(hazRPTex)											
										},
										timeout: 86400, // <<-- Timeout value specified here in seconds
										waitMsg : 'Please wait...',
										success: function(form, action) {
											Ext.Msg.alert('Success', action.result.message);	
											form.reset();
											// load (refresh) the risk scenario grid panel
											Ext.getCmp('riskGrid').getStore().reload({
												callback: function(){
												Ext.getCmp('riskGrid').getView().refresh();
												}
											});	
											
											// add the calculated layer to the map	
											var lyr_name = action.result.mpIndex;
											var names = {};
											names[workspace + ":" + lyr_name] = true; 
											
											var source = app.tools.addlyrs.target.layerSources["local"]; // need to reassign the source in case other sources such as google are being added by users
											app.tools.addlyrs.setSelectedSource(source);																		
											app.tools.addlyrs.selectedSource.store.load({
												callback: function(records, options, success) {
													var gridPanel, sel;
														if (app.tools.addlyrs.capGrid && app.tools.addlyrs.capGrid.isVisible()) {
															gridPanel = app.tools.addlyrs.capGrid.get(0).get(0);
															sel = gridPanel.getSelectionModel();
															sel.clearSelections();
														}
														// select newly added layers
														var newRecords = [];
														var last = 0;
														app.tools.addlyrs.selectedSource.store.each(function(record, index) {
															if (record.get("name") in names) {
																last = index;
																newRecords.push(record);
															}
														});
														if (gridPanel) {
															// this needs to be deferred because the 
															// grid view has not refreshed yet
															window.setTimeout(function() {
																sel.selectRecords(newRecords);
																ridPanel.getView().focusRow(last);
															}, 100);
														} else {
															app.tools.addlyrs.addLayers(newRecords, true);
														}                                                                                    
												},
												scope: this                                            
											});
										},
										// If you don't pass success:true, it will always go here
										failure: function(form, action) {
											Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
										}
									});
								}	
							},
							scope: this
						},{
							text: 'Reset',
							icon:  'src/gxp/theme/img/silk/arrow_refresh.png', 
							handler: function(){
								this.riskCalForm.getForm().reset(); // reset the form data 								
								this.riskCalForm.items.items[0].items.items[4].setVisible(false); // make invisible of the zone text box and buttons
								Ext.getCmp('areaPoly').disable(); // make disabled of the zone text box
								this.riskCalForm.items.items[1].items.items[0].getComponent('PrA_manual').disable();
								this.riskCalForm.items.items[1].items.items[0].getComponent('PrA_manual').setVisible(false);
								this.riskCalForm.items.items[1].items.items[1].getComponent('objectMode').disable();
								this.riskCalForm.items.items[1].items.items[1].getComponent('object_manual').disable();
								this.riskCalForm.items.items[1].items.items[1].getComponent('object_manual').setVisible(false);
								this.riskCalForm.items.items[1].items.items[2].getComponent('vul_manual').disable();
								this.riskCalForm.items.items[1].items.items[2].getComponent('vul_manual').setVisible(false); 
								app.mapPanel.map.removeLayer(this.layer); // remove the layer (if needed)
							},
							scope: this
						}]	
					});	
					
				var riskCalWin = new Ext.Window({
					title: 'Calculate a new risk scenario..',
					layout: 'fit',	
					autoScroll: true,
					plain:false,
					modal: false,
					maximizable: true,
					collapsible: true,
					closeable: true,
					closeAction:'hide',
					buttonAlign:'right',
					items: [this.riskCalForm]
				});
				
			//	console.log(this.riskCalForm.items.items[0].items);
				this.riskCalForm.items.items[0].items.items[2].getStore().reload({ params: 
							{ ws: workspace,
							 task: 'load'} // reload the alternative store with the selected workspace 
						});
						
				riskCalWin.show();
			},
            scope: this
        }]);
		
		return actions;
	}
 
});

Ext.preg(gxp.plugins.RiskCalculator.prototype.ptype, gxp.plugins.RiskCalculator);