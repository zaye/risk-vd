Ext.grid.DynamicColumnModel = function(store){
    var cols = [];
    var recordType = store.recordType;
    var fields = recordType.prototype.fields;
    
    for (var i = 0; i < fields.keys.length; i++)
    {
        var fieldName = fields.keys[i];
        var field = recordType.getField(fieldName);
        cols[i] = {header: field.name, dataIndex: field.name, width:300};
    }
    Ext.grid.DynamicColumnModel.superclass.constructor.call(this, cols);
};
Ext.extend(Ext.grid.DynamicColumnModel, Ext.grid.ColumnModel, {});