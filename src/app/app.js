/**
 * Add all your dependencies here.
 *
 * @require widgets/Viewer.js
 * @require widgets/WMSLayerPanel.js
 * @require widgets/GoogleEarthPanel.js
 * @require plugins/WMSRasterStylesDialog.js
 * @require plugins/LayerTree.js
 * @require plugins/LayerProperties.js
 * @require plugins/OLSource.js
 * @require plugins/OSMSource.js
 * @require plugins/WMSCSource.js
 * @require plugins/ZoomToExtent.js
 * @require plugins/ZoomToLayerExtent.js
 * @require plugins/NavigationHistory.js
 * @require plugins/Zoom.js
 * @require plugins/Measure.js
 * @require plugins/AddLayers.js
 * @require plugins/RemoveLayer.js
 * @require plugins/WMSGetFeatureInfo.js
 * @require plugins/Legend.js
 * @require plugins/Styler.js
 * @require plugins/GoogleSource.js
 * @require plugins/GoogleGeocoder.js
 * @require plugins/GoogleEarth.js
 * @require plugins/OLSource.js
 * @require plugins/GeoNodeCatalogueSource.js
 * @require plugins/MapBoxSource.js
 * @require plugins/FeatureManager.js
 * @require plugins/FeatureEditor.js
 * @require plugins/SnappingAgent.js
 * @require plugins/FeatureGrid.js
 * @require plugins/FeatureEditorForm.js
 * @require plugins/SelectedFeatureActions.js
 * @require plugins/QueryForm.js
 * @require plugins/Print.js
 * @require PrintPreview.js
 * @require plugins/Project.js
 * @require plugins/Hazards.js
 * @require plugins/Objects.js
 * @require plugins/Vulnerability.js
 * @require plugins/RiskAnalysis.js
 * @require plugins/RiskCalculator.js
 * @require plugins/AlternativeManagement.js
 * @require plugins/CostManagement.js
 * @require plugins/CostBenefitManagement.js
 * @require plugins/ExportSelectedFeatures.js
 * @require plugins/ExportLayer.js 
 * @require RowEditor.js
 * @require RowExpander.js
 * @require ColorManager.js
 * @require FileUploadField.js
 * @require PanelCollapsedTitle.js
 * @require SuperBoxSelect.js
 * @require LovCombo.js 
 * @require Spinner.js
 * @require SpinnerField.js
 * @require GroupSummary.js
 * @require DynamicColumnModel.js  
 * @require TreeGridSorter.js
 * @require TreeGridColumnResizer.js
 * @require TreeGridNodeUI.js
 * @require TreeGridLoader.js
 * @require TreeGridColumns.js
 * @require TreeGrid.js

 */

var ProjectID; 
var wsStore, workspace;
var app = new gxp.Viewer({
	// proxy: "/geoserver/rest/proxy?url=", 
	// defaultSourceType: "gxp_wmscsource",
	// configuration of the viewer portal for this application
    portalConfig: {
        layout: "border",
		region: "center",
		defaults: {
			collapsible: true,
			split: true
		},
        // by configuring items here, we don't need to configure portalItems
        // and save a wrapping container
        items: [			
		{
			id: "centerpanel",
            region: "center",	
			collapsible: false,			
			layout: "border",
			defaults: {
				split: true
			},
			margins: '53 0 0 0',
            items: [{			
				layout: "accordion",
				region: 'west',
				title: "Layer and Legend View",
				xtype: "panel",	
				plugins: [Ext.ux.PanelCollapsedTitle],
				collapseMode: 'mini',
				width: 200,
				minSize: 100,
				maxSize: 300,
				collapsible: true,
				collapsed: false,
				items: [{
					id: "layer_panel",
					title: "Layers"
				}, {
					id: "legend_panel",
					title: "Legend",
					autoScroll: true,
					containerScroll: true
				}]	
			},{			
				region: "center",			
				xtype: "panel",		
				layout: "fit",
				id: "panel",				
				items: ["mymap"]
			}]
        },{
            id: "southpanel",
			region: "south",
			title: "Data View",
			xtype: "panel",
			plugins: [Ext.ux.PanelCollapsedTitle],
			layout: "fit",
			collapseMode: 'mini',
			collapsed: false,
			height: 150,	
			minSize: 70,
			maxSize: 250
        },{
			id: "westpanel",	
			header: true,
			layout: "fit",
			margins: '53 0 0 0',
			collapsible: false,
			xtype: "panel",
			region: "west",
			width: 250,
			minSize: 100,
			maxSize: 350,
			items: [{
				id: "toolsPanel",
				title: 'Tools',
				xtype: 'treepanel',
				useArrows:true,
				autoScroll:true,
				animate:true,
				containerScroll: true,
				rootVisible: false,
				frame: true,
				loader: new Ext.tree.TreeLoader(),
				root: new Ext.tree.AsyncTreeNode({
					expanded: true,
					children: [{
						text: 'Data Management',
						icon: 'src/gxp/theme/img/silk/folder_wrench.png',
						qtip: 'Includes tools to manage hazard, object layers and vulnerability information.',
						singleClickExpand: true,
						expanded: true,
						children: [						
							{
								text: 'Hazards',
								icon: 'src/gxp/theme/img/silk/weather_rain.png',
								qtip: 'Includes tools to manage hazard layers and its related parameters.',
								singleClickExpand: true,
								children: [{
									text: 'Hazard Layers',
									id: 'hazInfo',
									icon: 'src/gxp/theme/img/silk/map.png',
									qtip: 'A tool to upload and visualize hazard maps.',
									leaf: true
								},{
									text: 'Parameter Configuration',
									id: 'PrAInfo',
									icon: 'src/gxp/theme/img/silk/wrench.png',
									qtip: 'A tool to create, edit and view spatial probability values.',
									leaf: true
								}]
							}, {
								text: 'Objects',
								icon: 'src/gxp/theme/img/silk/building.png',
								qtip: 'Includes tools to manage object layers and its related parameters.',
								singleClickExpand: true,
								children: [{
									text: 'Object Layers',
									id: 'objInfo',
									icon: 'src/gxp/theme/img/silk/map.png',
									qtip: 'A tool to upload and visualize object maps.',
									leaf: true
								},{
									text: 'Parameter Configuration',
									id: 'objParaInfo',
									icon: 'src/gxp/theme/img/silk/wrench.png',
									qtip: 'A tool to create, edit and view related object parameter values.',
									leaf: true
								}]
							}, {
								text: 'Vulnerability',
								id: 'vulInfo',
								icon: 'src/gxp/theme/img/silk/table.png',
								qtip: 'A tool to create, edit and view vulnerability information.',
								leaf: true
							}
						]
					},{
						text: 'Risk Management',
						icon: 'src/gxp/theme/img/silk/folder_wrench.png',
						qtip: 'Includes tools to manage risk, risk reduction scenarios and cost information.',
						singleClickExpand: true,
						expanded: true,
						children: [
						{
							text: 'Risk Analysis',
							icon: 'src/gxp/theme/img/silk/plugin.png',
							qtip: 'Includes quantitative risk analysis tools.',
							singleClickExpand: true,
							children: [{
								text: 'Collective Risk Scenarios',
								id: 'colRiskInfo',
								qtip: 'A tool to visualize collective risk scenarios.',
								leaf: true
							}]
						},{
							text: 'Risk Reduction',
							icon: 'src/gxp/theme/img/silk/plugin.png',
							qtip: 'Includes interactive tools to formulate and compare different risk reduction alternatives.',
							singleClickExpand: true,
							children: [{
								text: 'Alternative Scenarios',
								icon: 'src/gxp/theme/img/silk/vector.png',
								qtip: 'A tool to formulate alternatives (set of risk reduction measures).',
								id: 'altInfo',
								leaf: true
							},{
								text: 'Cost Sheets',
								icon: 'src/gxp/theme/img/silk/money.png',
								qtip: 'A tool to calculate the cost of alternatives.',
								id: 'costInfo',
								leaf: true
							},{
								text: 'Cost Benefit Reports',
								icon: 'src/gxp/theme/img/silk/coins.png',
								qtip: 'A tool to generate the cost-benefit reports.',
								id: 'costBenInfo',
								leaf: true
							}]
						}]
					}]	
				}),				
				listeners: {
					click: function(n) {					
						if (n.attributes.id == 'hazInfo' || n.attributes.id == 'PrAInfo') { app.tools.hazards.showCmp(n);}
						else if (n.attributes.id == 'objInfo' || n.attributes.id == 'objParaInfo') { app.tools.objects.show(n);}
						else if (n.attributes.id == 'vulInfo') { app.tools.vulnerability.show();}
						else if (n.attributes.id == 'altInfo') { app.tools.alternativemanagement.showAltMgt();}
						else if (n.attributes.id == 'costInfo') { app.tools.cost.showCostMgt();}
						else if (n.attributes.id == 'costBenInfo') { app.tools.costbenefit.showCostBenMgt();}
						else if (n.attributes.id == 'colRiskInfo') { app.tools.riskanalysis.show(n);}
						else { }
					}
				}
			}],
			tools: [{ 
				id: 'manage',
				qtip: 'Manage existing workspaces', 
				hidden: role != 'admin' ? true : false,
				handler: function(e, toolEl, panel, tc){
					app.tools.project.showProjects();
				}
			},{ 
				id: 'save',
				qtip: 'create a new workspace', 
				hidden: role != 'admin' ? true : false,
				handler: function(e, toolEl, panel, tc){
					app.tools.project.showNewProject();
				}
			},{
				id: 'help',
				qtip: 'Help Information', 
				handler: function(e, toolEl, panel, tc){
					window.open('/Manual.pdf','_blank');				
				}
			}],
			listeners: { 
				// make the panel header switchable to another workspace
				afterrender: function(panel){
					var cfg = {
						shadow: false,
						completeOnEnter: true,
						cancelOnEsc: true,
						updateEl: true,
						ignoreNoChange: true
					};			
			
                var titleEditor = new Ext.Editor(Ext.apply({
                    cls: 'x-small-editor',
                    alignment: 'bl-bl?',
                    offsets: [0, 3],
                    listeners: {
                        complete: function(ed, value, oldValue){
							workspace = value;			
							app.mapPanel.layers.each(function(rec) {  // remove existing overlays from the map viewer upon changing workspace
								if (rec.get("group") != "background" && rec.get("name") != "sketch") {
									app.mapPanel.layers.remove(rec);
								}	
							});
                        }
                    },
                    field: {
                        width: 110,
                        triggerAction: 'all',
                        xtype: 'combo',
                        editable: false,
                        forceSelection: true,
						mode:'local',
						triggerAction: 'all',
						displayField: 'ws_name',
						valueField: 'ws_name',
                        store: wsStore
                    }
                }, cfg));
				
				panel.header.child('.x-panel-header-text').on('dblclick', function(e, t){
                    titleEditor.startEdit(t);
                });
			}}	// end of workspace switcher
		}],
        bbar: {id: "mybbar"}
    },
    
    // configuration of all plugin tools for this application
    tools: [{
        ptype: "gxp_layertree",
        outputConfig: {
            id: "tree",
            border: false,
			containerScroll: true,
			autoScroll: true,
            tbar: [] // we will add buttons to "tree.bbar" later
        },
        outputTarget: "layer_panel"
    }, {
        ptype: "gxp_addlayers",
		id: 'addlyrs',
        actionTarget: "tree.tbar"/* ,
		upload: true,
		uploadSource: "local",
		uploadText: "Upload layers" */
    }, {
        ptype: "gxp_removelayer",		
        actionTarget: ["tree.tbar", "tree.contextMenu"]
    }, {
        ptype: "gxp_layerproperties",		
        actionTarget: ["tree.tbar", "tree.contextMenu"]
    }, {
		ptype: "gxp_zoomtolayerextent",
		actionTarget: ["tree.tbar", "tree.contextMenu"]
	}, {
        ptype: "gxp_zoomtoextent",
        actionTarget: "map.tbar"
    }, {
        ptype: "gxp_zoom",
        actionTarget: "map.tbar"
    }, {
        ptype: "gxp_navigationhistory",
        actionTarget: "map.tbar"
    }, {
        ptype: "gxp_measure",
        actionTarget: "map.tbar"
    },
	/* {
        ptype: "gxp_print",
        customParams: {outputFilename: 'RiskCal-print'},
		printService: "/geoserver/pdf/",
		actionTarget: "map.tbar",
		includeLegend: true
    }, */
	{
		ptype: "gxp_wmsgetfeatureinfo",
		format: "grid",
		outputConfig: {
			width: 300
		},
		actionTarget: "map.tbar"
	}, {
		ptype: "gxp_legend",
		outputTarget: "legend_panel",
		outputConfig: {
			autoHeight: true,
			autoWidth: true			
		}	
	}, {
		ptype: "gxp_styler",	
		actionTarget: ["tree.tbar", "tree.contextMenu"],
		sameOriginStyling: false,
		rasterStyling: true,
		requireDescribeLayer: false
	}, {
		ptype: "gxp_featuremanager",
		id: "manager",
		paging: true,		
		autoSetLayer: true,
		autoLoadFeatures: true
	}, {
		ptype: "gxp_featureeditor",
		featureManager: "manager",
		autoLoadFeature: true,
		snappingAgent: "snapping-agent",
		supportNoGeometry: false, 
		splitButton: false,
		showSelectedOnly: false
	}, {
		ptype: "gxp_featuregrid",
		featureManager: "manager",
		alwaysDisplayOnMap: true,
		selectOnMap: true,
		showTotalResults: true,
		outputConfig: {
			id: 'featuregrid',
			loadMask: true
		},
		outputTarget: "southpanel",
		controlOptions: {  
			multiple: false, 
			hover: false,
            toggleKey: "ctrlKey", // ctrl key removes from selection
            multipleKey: "shiftKey" // shift key adds to selection
		}
	},{
		ptype: "gxp_snappingagent",
		id: "snapping-agent",
		targets: [{
				source: "local",
				name: "usa:states"
			}]
	}, {
        ptype: "gxp_queryform",
        featureManager: "manager",
		outputConfig: {
            title: "Query",
            width: 320
        },
        actionTarget: ["tree.contextMenu"]
    }, {
		ptype: "gxp_selectedfeatureactions",
		featureManager: "manager",
		actionTarget: "featuregrid.bbar",
		actions: [{
			menuText: "Search for title",
			urlTemplate: "http://google.com/search?q={title}"
		 // iconCls: ""
		}]
	}, {
		ptype: "gxp_googlegeocoder",
		outputTarget: "map.tbar",
		outputConfig: {
			emptyText: "Search for a location ..."
		}
	},
	/*{
		ptype: "myapp_ExportSelectedFeatures",
		featureManager: "manager",
		actionTarget: ["tree.tbar", "tree.contextMenu"]
	} */ 
	{
		ptype: "myapp_ExportLayer",
		featureManager: "manager",
		actionTarget: ["tree.tbar"]
	}, {
		ptype: "myapp_RiskCalculator",
		actionTarget: "map.tbar"
	},{
		ptype: "myapp_project",
		id: 'project'
	},{
		ptype: "myapp_hazards",
		id: 'hazards'
	},{
		ptype: "myapp_objects",
		id: 'objects'
	},{
		ptype: "myapp_vulnerability",
		id: 'vulnerability'
	},{
		ptype: "myapp_riskanalysis",
		id: 'riskanalysis'
	}, {
		ptype: "myapp_alternativemanagement",
		id: 'alternativemanagement'
	}, {
		ptype: "myapp_cost",
		id: 'cost'
	},{
		ptype: "myapp_costbenefit",
		id: 'costbenefit'
	}],
    // layer sources
    sources: {
        local: {
            ptype: "gxp_wmscsource",
            url: "/geoserver/wms",
            version: "1.1.1",
			hidden: true 
        }, 		
		geonode: {
			ptype: "gxp_geonodecataloguesource"
		},
        osm: {
            ptype: "gxp_osmsource"
        },
		google: {
			ptype: "gxp_googlesource"
		},
		ol: {
			ptype: "gxp_olsource"
		}, 
		mapbox: {
			ptype: "gxp_mapboxsource"
		}
    },
    
    // map and layers
    map: {
        id: "mymap", // id needed to reference map in portalConfig above
		title: "2D Map View",
		projection: "EPSG:900913",		
		center: [796657.18624, 5836769.57608],
	/* 	projection: "EPSG:2056",
		center:[2720000, 1095000], */
        zoom: 10,
		controls: [
			new OpenLayers.Control.Navigation({
                zoomWheelOptions: {interval: 250},
				dragPanOptions: {enableKinetic: true}
            }),
            new OpenLayers.Control.PanPanel(),
            new OpenLayers.Control.ZoomPanel(),
            new OpenLayers.Control.Attribution()
		],
        layers: [		
		{
            source: "google",
            name: "SATELLITE",
            group: "background"
        },{
            source: "google",
            name: "ROADMAP",
            group: "background"
        },{
            source: "osm",
            name: "mapnik",
            group: "background"
        }],
        items: [{
            xtype: "gx_zoomslider",
            vertical: true,
            height: 100
        }]
    }
});
	
//	show the message box to initate the window for selecting a workspace
Ext.MessageBox.show({
    title: 'Help Support!',
    msg: 'Please locate a workspace to start using the system.',
    buttons: Ext.MessageBox.OK,
    animEl: 'mb9',
    fn: selectws(),
    icon: Ext.MessageBox.INFO
});	
								
// select workspace function								
function selectws(){
	wsStore = new Ext.data.JsonStore({ // assign a store to the global var 'wsStore'
		url: 'loadws.php',									
		root: 'rows',
		fields : ['ws_name']
	});
	wsStore.load(); //load the store
		
	new Ext.Window({
		title: 'Selection of a workspace',
		id: 'myws',
		width: 240,
		autoHeight:true,			
		layout: 'hbox',
		modal: true,
		plain:false,
		closable: false,
		resizable : false,
		buttonAlign:'right',
		items: [{
			editable: false,
			allowBlank: false,
			width: 180,
			name: 'ws_name',
			id: 'ws_name',
			xtype: 'combo',					
			margins: {top:5, right:5, bottom:5, left:5},
			msgTarget: 'Please select a workspace',
			mode:'local',
			triggerAction: 'all',
			displayField: 'ws_name',
			valueField: 'ws_name',
			store: wsStore,
			listeners:{
				scope: this,
				'select': function(combo, record, index){
					workspace = record.get('ws_name'); 	// assign the selected workspace to a global variable 
					Ext.getCmp('westpanel').setTitle(workspace); // render the workspace name to the left panel title 
					new Ext.ToolTip({ 
						target: Ext.getCmp('westpanel').header,
						html: 'Double-click on the workspace name (on left side) to switch.'
					});
				}	
			}
		},{
			iconCls: 'page-new',
			xtype: 'button',
			disabled: role != 'admin' ? true : false,
			margins: {top:5, right:5, bottom:5, left:5},
			handler: function() {
				app.tools.project.showNewProject();
			}
		}],	
		buttons: [{
			text: 'OK',
			handler: function() {
				if (Ext.getCmp('ws_name').isValid()) {
					Ext.getCmp('myws').close();	
				}								
			}			
		}]
	});	

	Ext.getCmp('myws').show();	
	
	Ext.override(Ext.ux.form.LovCombo, {  // needed for LovCombo box for selection of multivalues with focus
		beforeBlur: Ext.emptyFn
	});

	Ext.override(Ext.grid.PivotGridView, {
    /**
     * @private
     * Renders rows between start and end indexes
     * @param {Number} startRow Index of the first row to render
     * @param {Number} endRow Index of the last row to render
     */
    renderRows : function(startRow, endRow) {
        var grid          = this.grid,
            rows          = grid.extractData(),
            rowCount      = rows.length,
            templates     = this.templates,
            renderer      = grid.renderer,
            hasRenderer   = typeof renderer == 'function',
            getCellCls    = this.getCellCls,
            hasGetCellCls = typeof getCellCls == 'function',
            cellTemplate  = templates.cell,
            rowTemplate   = templates.row,
            rowBuffer     = [],
            meta          = {},
            tstyle        = 'width:' + this.getGridInnerWidth() + 'px;',
            colBuffer, column, i;
        
        startRow = startRow || 0;
        endRow   = Ext.isDefined(endRow) ? endRow : rowCount - 1;
        
        for (i = 0; i < rowCount; i++) {
            row = rows[i];
            colCount  = row.length;
            colBuffer = [];
            
            rowIndex = startRow + i;

            //build up each column's HTML
            for (j = 0; j < colCount; j++) {
                cell = row[j];

                meta.css   = j === 0 ? 'x-grid3-cell-first ' : (j == (colCount - 1) ? 'x-grid3-cell-last ' : '');
                meta.attr  = meta.cellAttr = '';
                meta.value = cell;

                if (Ext.isEmpty(meta.value)) {
                    meta.value = ' ';
                }
                
                if (hasRenderer) {
                    meta.value = renderer(meta.value, meta);
                }
                
                if (hasGetCellCls) {
                    meta.css += getCellCls(meta.value) + ' ';
                }

                colBuffer[colBuffer.length] = cellTemplate.apply(meta);
            }
            
            rowBuffer[rowBuffer.length] = rowTemplate.apply({
                tstyle: tstyle,
                cols  : colCount,
                cells : colBuffer.join(""),
                alt   : ''
            });
        }
        
        return rowBuffer.join("");
    }
	});
}
