<?php
	$workspace = $_POST['ws'];
	$task = $_POST['task'];	
		
	$dbconn=pg_connect("host=localhost port=5432 dbname=dbname user=username password=mypassword");	// replace with your db connection information	
	if (!$dbconn){
		echo "An error occured.\n";
		exit;
		}
		
	if ($task == 'load') {
		$query = "SELECT * FROM ".$workspace.".vulnerability;"; 		
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}

	if ($task == 'loadUserDefined') {
		$hazType = $_POST['hazType'];
		$objType = $_POST['objType'];
		$vulLevel = $_POST['vulLevel'];
		
		$query = "SELECT nom, indice FROM ".$workspace.".vulnerability WHERE type != 'default' AND processus_type = '$hazType' AND obj_type = '$objType' AND obj_niveau = '$vulLevel';"; 
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	if ($task == 'loadVulValues') {
		// retrieve POST data
		$mpIndex = $_POST['mpIndex'];
		$type = $_POST['type'];
		$hazType = $_POST['hazType'];
		$objType = $_POST['objType'];
		$objLevel = $_POST['objLevel'];
		
		if ($type == 'default') {
			$query = "SELECT * FROM ".$workspace.".vulnerability_default WHERE processus = '$hazType' AND type = '$objType' AND obj_niveau = '$objLevel';";
		}
		else { 
			$query = "SELECT * FROM ".$workspace.".".$mpIndex.";"; 		
		}
		
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	if ($task == 'add') {
		// retrieve POST data 
		$name = $_POST['name'];
		$description = $_POST['description'];
		$hazType = $_POST['haz_type'];
		$objType = $_POST['obj_type'];
		$objLevel = $_POST['obj_niveau'];
		$remarks = $_POST['remarks'];
		
		// STEP 1: Create a new table with default data values, extracted from vulnerability_default by filtering hazard type, obj type and level
		// STEP 2: Register a new record in 'vulnerability' table 
		$query = "DO
				$$
				BEGIN
				EXECUTE format('CREATE TABLE $workspace.%I AS SELECT * FROM $workspace.vulnerability_default WHERE obj_niveau = ''$objLevel'' AND processus = ''$hazType'' AND type = ''$objType''', 'vul_para_' || (SELECT last_value + 1 FROM $workspace.vulnerability_id_seq));
				EXECUTE format('INSERT INTO $workspace.vulnerability VALUES (DEFAULT, ''$name'', ''$description'', ''user defined'', ''$hazType'', ''$objType'', ''$objLevel'', ''$remarks'', ''%I'')', 'vul_para_' || (SELECT last_value + 1 FROM $workspace.vulnerability_id_seq));
				END;
				$$ LANGUAGE plpgsql;";
		
		/* $mapping_index = $name; // change later to a unique index name 
		$query = "CREATE TABLE ".$workspace.".".$mapping_index." AS (SELECT * FROM ".$workspace.".vulnerability_default WHERE processus = '$hazType' AND type = '$objType');";
		$query .= "INSERT INTO ".$workspace.".vulnerability VALUES (DEFAULT, '$name', '$description', 'user defined', '$hazType', '$objType','$remarks', '$mapping_index');"; */
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
			Echo '{success:true,message:"The new vulnerability table has been created. You can now edit the default values!"}';
		}
	}

	// to save the dirty matrix values to the corresponding table 
	if ($_GET['task'] == 'save'){
		$workspace = $_GET['ws'];
		$mpIndex = $_GET['mpIndex'];
		$postdata = file_get_contents("php://input"); 
		$phpArray = json_decode($postdata, true);
		$length = count($phpArray);	
		
		for ($i = 0; $i < $length; $i++) { // loop for each of the dirty records
			$temp = $phpArray[$i];
 			$id = $temp['id'];
			$code = $temp['code'];		
			$vulObject = $temp['valeur_objet'];	
			$vulPerson = $temp['valeur_letalite'];
			$remark = $temp['remarques'];			
			
			$query .= "UPDATE ".$workspace.".".$mpIndex." SET valeur_objet = $vulObject, valeur_letalite = $vulPerson, remarques = '$remark' WHERE id = $id;";						
		}
		
		If (!$rs = pg_query($dbconn,$query)) {
				Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
			}
		else {			
			Echo '{success:true,message:"The vulnerability values have been updated!"}';
		}	
	}
	
	// to update the information of the configuration
	if ($task == 'update') {
		$ID = $_POST['ID'];	
		$nom = $_POST['name'];
		$desc = $_POST['description'];
		$remarques = $_POST['remarks'];
		
		$query= "UPDATE ".$workspace.".vulnerability SET nom = '$nom', description = '$desc', remarques = '$remarques' WHERE id = $ID;";
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
			Echo '{success:true,message:"The data has been updated!"}';
		}
	}
	
	// to delete the configuration
	if ($task == 'delete') {
		$temp = $_POST['IDs'];		
		$records = json_decode($temp, true);
		$length = count($records);
		
		for ($i = 0; $i < $length; $i++) {	// for each of the selected configurations
			$ID = $records[$i]['id'];			
			$mpIndex = $records[$i]['indice'];
			
			### Step1: remove the records from the vulnerability table 
			$query .= "DELETE FROM ".$workspace.".vulnerability WHERE id = $ID;";
			
			### Step2: drop the respective table using indice column from the db 
			$query .= "DROP TABLE ".$workspace.".$mpIndex;";
		}
		
		if (!$rs = pg_query($dbconn,$query)){	
			Echo '{success:false, message:'.json_encode(pg_last_error($dbconn)).'}';								
		}
		else {
			Echo '{success:true,message:"The selected vulnerability parameter configurations have been deleted!"}';
		}			
	}
	
	pg_close($dbconn);
?>