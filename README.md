#**An open-source webGIS platform for risk management of natural hazards in Canton Vaud**

This research project is funded by Canton Vaud (Switzerland), and the aim of this prototype application is to assist users in performing a rapid evaluation of risk before and after the consideration of protection measures, and to test the efficiency of measures using a simple cost-benefit analysis tool. This application is developed based on EconoMe and Valdorisk.

*The Boundless SDK Suite is used to build a fully functional prototype using open-source components and standards. The **PostgreSQL** database with **PostGIS** extension is integrated for data storage of spatial and non-spatial data. **GeoServer** and **GeoWebCache** are used for application servers to access and render the spatial data through web map services. **GeoExt**, **ExtJS** and **Open-Layers** (JavaScript libraries) are applied for the user interface framework of the interactive web map application across web browsers. To build and deploy this JavaScript-based application, a client-side software development kit (SDK) environment backed up by Boundless is used, which is based on **GXP** template (a JavaScript SDK) for developing high-level GeoExt-based application with OpenLayers 2. The various components (modules) of the application are developed as plugins (dependencies).*

---

## Instructions for installation

This section describes how to set up the application and its background architecture on the Ubuntu server machine. 

1.	Install Java (GeoServer requires a Java 8 environment (JRE) to be installed).
2.	Install Apache Tomcat application server.
3. 	Install GeoServer Web Archive.
	- Install GeoWebCache if it’s already not integrated in GeoServer installation.
	- Install *WPS* and *Importer* extensions matched to the installed GeoServer version. 
	- Create a default workspace  (e.g., *opengeo*) with a default PostGIS datastore, linked to a default schema in the PostGIS database (see the next step).
	- Create a default intensity map style (named *CIN*) in GeoServer globally (and not under specific workspaces). Refer to [SLD](https://drive.switch.ch/index.php/s/byFaK3hO6h0JR3Z) version of the ArcGIS Layer style provided by the Canton to create the style in GeoServer. 
4. 	Install PostgreSQL database.
	- Install PostGIS extension.
	- Create a new user account (e.g., *admin*) to be used by the application in PHP scripts for the connection to the database. 
	- Create a new database  (e.g., *geoserver*) with the created user account to be used by the application in PHP scripts for the connection to the database.
		- Enable the PostGIS extension on the database.
		- Create a new schema  (e.g., *opengeo*) in the created database to be used by the application to access the default datastore in GeoServer. 
			- Link this schema to the GeoServer as a default PostGIS datastore by creating a default workspace (e.g. *opengeo*) in GeoServer.
			- Under the *public* schema of the created database, create *users* and *workspace* tables to store data related to user accounts and associated workspaces (projects) in the application. Refer to [SQL](https://drive.switch.ch/index.php/s/OUw3dDzxpoCDtBc) script to create tables and insert a new record into the users table to start using the application. 
		- Under the *public* schema of the created database, create a function (i.e.,  *risque_f*). Refer to [SQL](https://drive.switch.ch/index.php/s/HdP7LgyuCbkGt0G) script to create this function in the database.
5.	Install GDAL/OGR package to be used by the application for importing shape and DBF files to the PostGIS database.
6. 	Install Boundless suite-SDK :
	- Install Java Runtime Environment (JRE).
	- Install Apache Ant.
	- [Download](https://drive.switch.ch/index.php/s/PtoqYd0L9k8wBWJ) Boundless SDK. This version is intended to use with Ubuntu Server (18.04.1 LTS).
	- Extract the archive to a suitable location on the file system.
	- Add the SDK bin directory to the PATH.
	- To verify the SDK is installed properly, execute the command suite-sdk from a command prompt. Regarding the suite-sdk commands used to create and package the application, refer to the [documentation](http://suite.opengeo.org/docs/4.8/webapps/sdk.html#webapps-sdk) of Boundless.
7.	Install Apache HTTP server with PHP.
	- Install *php-pgsql*, *php-curl* and *php-zip* extensions. 
	- Set PHP settings: *upload_max_filesize*, *memory_limit* and *post_max_size* if needed. 
	- Configure a *ProxyPass* and *ProxyPassReverse* for GeoServer.  
	- Enable cross-Origin Resource Sharing (CORS)  in Tomcat for GeoServer. 
	- Set a proxy base URL in GeoServer  (Settings > Global) to the proxied external URL.
8. Extract the packaged WAR file of the application to the web directory of the Apache (usually at: /var/www/html).
	- Make sure that the web server has the permission to write to the **upload** folder of the application.
	- Make sure that the application connects to the correct database in PHP scripts of the application. 
	- Make sure that the application connects to the GeoServer using the correct credential information. If not, user name and password are to be corrected in PHP scripts of the application.

---