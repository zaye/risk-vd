<?php
	ob_start();
	session_start();
	
	// Connect to the central database to check the login information of users
	$dbconn=pg_connect("host=localhost port=5432 dbname=dbname user=username password=mypassword");	// replace with your db connection information
	
	if (!$dbconn) {
		echo '{"An error occurred.\n"}';
		exit;
	}

	// username and password sent from form 
	$myusername=$_POST['myusername']; 
	$temp=$_POST['mypassword'];  
	$mypassword=md5($temp);

	// query the database
	$query="SELECT * FROM public.users WHERE user_name='$myusername' and password='$mypassword'";
	$result=pg_query($dbconn,$query);

	// pg_num_row is counting table row
	$count=pg_num_rows($result);

	//find the object of the selected record
	$data = pg_fetch_object($result);
	
	// If result matched $myusername and $mypassword, table row must be 1 row
	if($count==1){

		// Register $myusername and redirect to the file "vaudapp.php", otherwise stay at the main login page.
		$_SESSION['usrname']= $myusername;
		$_SESSION['dbname']= $myusername;
		$_SESSION['displayname']= $data->display_name;
		$_SESSION['email']= $data->email_address;
		$_SESSION['role']= $data->role;
		$_SESSION['userid']= $data->user_id;		

		// cURL session to set the cookies of geoserver
		$geoserverURL = "http://localhost:8080/geoserver/j_spring_security_check";
		$ch = curl_init($geoserverURL);			
		$str = "username=username&password=mypassword";	// replace with your GeoServer username and password	
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_POST, True);			
		curl_setopt($ch, CURLOPT_POSTFIELDS, $str);
		
		// grab URL and pass it to the browser
		$result = curl_exec($ch);	
		
		// extract cookie from the geoserver response 
		preg_match('/^Set-Cookie:\s*([^;]*)/mi', $result, $m);
		parse_str($m[1], $cookies);
		$cookieValue = $cookies['JSESSIONID'];

		$cookieName = "JSESSIONID";
		$cookieDomain = "http://localhost:8080";
		$cookiePath = "/geoserver";
		$cookieExpiration = 0;

		setcookie($cookieName,$cookieValue,$cookieExpiration,$cookiePath);

		// close cURL resource, and free up system resources
		curl_close($ch);
		
		// direct to the page
		header("location:vaudapp.php");
		exit();		
	}
	else {
		// show the error information 
		$_SESSION['error']= 'There is no account with this username and/or password! The username is sensititve, please make sure you enter it correctly and try again!';	
		header("location:login.php");
		exit();
	}
	ob_end_flush();
	pg_close($dbconn);
?>