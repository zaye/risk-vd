<?php
	$workspace = $_POST['ws'];
	$task = $_POST['task'];	
		
	$dbconn=pg_connect("host=localhost port=5432 dbname=dbname user=username password=mypassword");	// replace with your db connection information
	if (!$dbconn){
		echo "An error occured.\n";
		exit;
		}
		
	if ($task == 'load') {
		$query = "SELECT id, nom, description, type, obj_type, obj_niveau, remarques, indice FROM ".$workspace.".objects_parameters;"; 		
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}

	if ($task == 'loadUserDefined') {
		$objLevel = $_POST['objLevel'];
		
		$query = "SELECT nom, indice FROM ".$workspace.".objects_parameters WHERE type != 'default' AND obj_niveau = '$objLevel';"; 		
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	if ($task == 'loadObjParaValues') {
		// retrieve POST data
		$mpIndex = $_POST['mpIndex'];
		$type = $_POST['type'];
		$objLevel = $_POST['objLevel'];
		$objType = $_POST['objType'];
		
		if ($type == 'default') {
			$query = "SELECT * FROM ".$workspace.".".$mpIndex." WHERE niveau = '$objLevel' AND type = '$objType';";
		}
		else { 
			$query = "SELECT * FROM ".$workspace.".".$mpIndex.";"; 		
		}
		
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	if ($task == 'add') {
		// retrieve POST data 
		$name = $_POST['name'];
		$description = $_POST['description'];
		$objLevel = $_POST['obj_niveau'];
		$objType = $_POST['obj_type'];
		$remarks = $_POST['remarks'];
		

		// STEP 1: Create a new table with default data values, extracted from objects_default by filtering obj types and levels
		// STEP 2: Register a new record in 'objects_parameters' table 
		
		if ($objType == 'Routes') {
			$query = "DO
				$$
				BEGIN
				EXECUTE format('CREATE TABLE $workspace.%I AS SELECT * FROM $workspace.objects_default_routes WHERE niveau = ''$objLevel'' AND type = ''$objType''', 'object_para_' || (SELECT last_value + 1 FROM $workspace.objects_parameters_id_seq));
				EXECUTE format('INSERT INTO $workspace.objects_parameters VALUES (DEFAULT, ''$name'', ''$description'', ''user defined'', ''$objType'', ''$objLevel'', ''$remarks'', ''%I'')', 'object_para_' || (SELECT last_value + 1 FROM $workspace.objects_parameters_id_seq));
				END;
				$$ LANGUAGE plpgsql;";
		}
		else {
			$query = "DO
				$$
				BEGIN
				EXECUTE format('CREATE TABLE $workspace.%I AS SELECT * FROM $workspace.objects_default WHERE niveau = ''$objLevel'' AND type = ''$objType''', 'object_para_' || (SELECT last_value + 1 FROM $workspace.objects_parameters_id_seq));
				EXECUTE format('INSERT INTO $workspace.objects_parameters VALUES (DEFAULT, ''$name'', ''$description'', ''user defined'', ''$objType'', ''$objLevel'', ''$remarks'', ''%I'')', 'object_para_' || (SELECT last_value + 1 FROM $workspace.objects_parameters_id_seq));
				END;
				$$ LANGUAGE plpgsql;";
		}
		
				
		/* $mapping_index = $name; // change later to a unique index name 
		$query = "CREATE TABLE ".$workspace.".".$mapping_index." AS TABLE ".$workspace.".objects_default;";
		$query .= "INSERT INTO ".$workspace.".objects_parameters VALUES (DEFAULT, '$name', '$description', 'user defined', '$remarks', '$mapping_index');"; */
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
			Echo '{success:true,message:"The new configuration table has been created. You can now edit the default values!"}';
		}
	}

	// to save the dirty matrix values to the corresponding table 
	if ($_GET['task'] == 'save'){
		$workspace = $_GET['ws'];
		$mpIndex = $_GET['mpIndex'];
		$postdata = file_get_contents("php://input"); 
		$phpArray = json_decode($postdata, true);
		$length = count($phpArray);	
		
		for ($i = 0; $i < $length; $i++) { // loop for each of the dirty records
			$temp = $phpArray[$i]; 
			$type = $temp['type'];	
			$code = $temp['code'];		
			$unitPrice = $temp['prix_unitaire'];	
			$people = $temp['humains'];
			$peopleExp = $temp['humains_exp'];
			$vitesse =  $temp['vitesse'];
			$tjm =  $temp['tjm'];
			$longueur =  $temp['longueur'];
			$passage =  $temp['passage'];
			$remark = $temp['remarques'];
			
			if ($type == 'Routes') {
				$query .= "UPDATE ".$workspace.".".$mpIndex." SET prix_unitaire = $unitPrice, humains = $people, vitesse = $vitesse, tjm = $tjm, longueur = $longueur, passage = $passage, remarques = '$remark' WHERE code = $code;";						
			}
			else {
				$query .= "UPDATE ".$workspace.".".$mpIndex." SET prix_unitaire = $unitPrice, humains = $people, humains_exp = $peopleExp, remarques = '$remark' WHERE code = $code;";						
			}			
		}
		
		If (!$rs = pg_query($dbconn,$query)) {
				Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
			}
		else {			
			Echo '{success:true,message:"The object parameter values have been updated!"}';
		}	
	}
	
	// to update the information of the configuration
	if ($task == 'update') {
		$ID = $_POST['ID'];	
		$nom = $_POST['name'];
		$desc = $_POST['description'];
		$remarques = $_POST['remarks'];
		
		$query= "UPDATE ".$workspace.".objects_parameters SET nom = '$nom', description = '$desc', remarques = '$remarques' WHERE id = $ID;";
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
			Echo '{success:true,message:"The data has been updated!"}';
		}
	}
	
	// to delete the configuration
	if ($task == 'delete') {
		$temp = $_POST['IDs'];		
		$records = json_decode($temp, true);
		$length = count($records);
		
		for ($i = 0; $i < $length; $i++) {	// for each of the selected configurations
			$ID = $records[$i]['id'];			
			$mpIndex = $records[$i]['indice'];
			
			### Step1: remove the records from the objects_parameters table 
			$query = "DELETE FROM ".$workspace.".objects_parameters WHERE id = $ID;";
			
			### Step2: drop the respective table using indice column from the db 
			$query .= "DROP TABLE ".$workspace.".$mpIndex;";
				
			if (!$rs = pg_query($dbconn,$query)){			
				$message .= 'Failed to delete the configuration: '.$records[$i]['nom'].' due to the error: '.json_encode(pg_last_error($dbconn));						
			}
			else {
				$message .= 'The selected configuration: '.$records[$i]['nom'].' has been deleted!';
			}
		}
	}
	
	pg_close($dbconn);
?>