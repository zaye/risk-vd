<?php
	
	$workspace = $_POST['ws'];	
	$copy_lyr_name = $_POST['lyrname'];
	$datastore = $workspace;
	
	$dbconn=pg_connect("host=localhost port=5432 dbname=dbname user=username password=mypassword");	// replace with your db connection information
	if (!$dbconn){
		echo "An error occured.\n";
		exit;
	}
	
	$layer_name = $layer_title = $_POST['title'];
	$layer_desc = $_POST['abstract'];
	$layer_remarks = $_POST['remarks'];
	$layer_type = $_POST['layerType'];
	$haz_type = $_POST['hazType'];
	$haz_rp = $_POST['hazRP'];
	$obj_type = $_POST['objType'];
	$alt_id = $_POST['altID'];
	
	$layer_name = preg_replace('/\s+/','_',$layer_name); // replace whitespaces and spaces with underscore 
	$layer_name = strtolower($layer_name); // change to lowercase // change later to a unique index name
	
	$str = $workspace.':'.$layer_name;
	
	### Step 1: publish the selected layer with a default styled called "CIN" for hazard layers
	if ($layer_type == 'Hazards') $xml = '<?xml version="1.0" encoding="UTF-8"?><wps:Execute version="1.0.0" service="WPS" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.opengis.net/wps/1.0.0" xmlns:wfs="http://www.opengis.net/wfs" xmlns:wps="http://www.opengis.net/wps/1.0.0" xmlns:ows="http://www.opengis.net/ows/1.1" xmlns:gml="http://www.opengis.net/gml" xmlns:ogc="http://www.opengis.net/ogc" xmlns:wcs="http://www.opengis.net/wcs/1.1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/wps/1.0.0 http://schemas.opengis.net/wps/1.0.0/wpsAll.xsd"><ows:Identifier>gs:Import</ows:Identifier><wps:DataInputs><wps:Input><ows:Identifier>features</ows:Identifier><wps:Reference mimeType="text/xml" xlink:href="http://geoserver/wfs" method="POST"><wps:Body><wfs:GetFeature service="WFS" version="1.0.0" outputFormat="GML2" xmlns:'.$workspace.'="http://'.$workspace.'"><wfs:Query typeName="'.$copy_lyr_name.'"/></wfs:GetFeature></wps:Body></wps:Reference></wps:Input><wps:Input><ows:Identifier>workspace</ows:Identifier><wps:Data><wps:LiteralData>'.$workspace.'</wps:LiteralData></wps:Data></wps:Input><wps:Input><ows:Identifier>store</ows:Identifier><wps:Data><wps:LiteralData>'.$datastore.'</wps:LiteralData></wps:Data></wps:Input><wps:Input><ows:Identifier>name</ows:Identifier><wps:Data><wps:LiteralData>'.$layer_name.'</wps:LiteralData></wps:Data></wps:Input><wps:Input><ows:Identifier>styleName</ows:Identifier><wps:Data><wps:LiteralData>CIN</wps:LiteralData></wps:Data></wps:Input></wps:DataInputs><wps:ResponseForm><wps:RawDataOutput><ows:Identifier>layerName</ows:Identifier></wps:RawDataOutput></wps:ResponseForm></wps:Execute>';
	else $xml = '<?xml version="1.0" encoding="UTF-8"?><wps:Execute version="1.0.0" service="WPS" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.opengis.net/wps/1.0.0" xmlns:wfs="http://www.opengis.net/wfs" xmlns:wps="http://www.opengis.net/wps/1.0.0" xmlns:ows="http://www.opengis.net/ows/1.1" xmlns:gml="http://www.opengis.net/gml" xmlns:ogc="http://www.opengis.net/ogc" xmlns:wcs="http://www.opengis.net/wcs/1.1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xsi:schemaLocation="http://www.opengis.net/wps/1.0.0 http://schemas.opengis.net/wps/1.0.0/wpsAll.xsd"><ows:Identifier>gs:Import</ows:Identifier><wps:DataInputs><wps:Input><ows:Identifier>features</ows:Identifier><wps:Reference mimeType="text/xml" xlink:href="http://geoserver/wfs" method="POST"><wps:Body><wfs:GetFeature service="WFS" version="1.0.0" outputFormat="GML2" xmlns:'.$workspace.'="http://'.$workspace.'"><wfs:Query typeName="'.$copy_lyr_name.'"/></wfs:GetFeature></wps:Body></wps:Reference></wps:Input><wps:Input><ows:Identifier>workspace</ows:Identifier><wps:Data><wps:LiteralData>'.$workspace.'</wps:LiteralData></wps:Data></wps:Input><wps:Input><ows:Identifier>store</ows:Identifier><wps:Data><wps:LiteralData>'.$datastore.'</wps:LiteralData></wps:Data></wps:Input><wps:Input><ows:Identifier>name</ows:Identifier><wps:Data><wps:LiteralData>'.$layer_name.'</wps:LiteralData></wps:Data></wps:Input></wps:DataInputs><wps:ResponseForm><wps:RawDataOutput><ows:Identifier>layerName</ows:Identifier></wps:RawDataOutput></wps:ResponseForm></wps:Execute>';
		// Initiate cURL session
		$link = "http://localhost:8080/geoserver/wps"; // WPS request URL
		$ch = curl_init($link);

		//Required request settings
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, True);
		curl_setopt($ch, CURLOPT_POST, true); // -X POST		
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: xml")); // -H
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
		 
		$passwordStr = "username:mypassword"; // replace with your GeoServer username and password
		curl_setopt($ch, CURLOPT_USERPWD, $passwordStr);

		//PUT return code: status OK
		$successCode = 200;

		$data = curl_exec($ch); // Execute the curl request
		$info = curl_getinfo($ch);
		
		if ($info['http_code'] != $successCode) {
				$msgStr = "# Unsuccessful cURL request to ";
				$msgStr .= $link." [". $info['http_code']. "]\n";
				Echo '{message:'.json_encode($msgStr).'}';
			}
		else {			
			### Step 2: if returned data from WPS process is the layer name, save the records in the database (either in the hazards or objects table)
			if ($data == $str) { // if success
				if ($layer_type == 'Hazards') {
					if ($haz_rp == 'TFreq') {
						if ($haz_type != 'GPP') {
							$query = "INSERT INTO $workspace.hazards(id, nom, description, type, remarques, nom_origine, indice, temp_de_retour, ans)
								VALUES(DEFAULT, '$layer_title', '$layer_desc', '$haz_type', '$layer_remarks', '$copy_lyr_name', '$layer_name', '$haz_rp',10);";
						}
						else {
							$query = "INSERT INTO $workspace.hazards(id, nom, description, type, remarques, nom_origine, indice, temp_de_retour, ans)
								VALUES(DEFAULT, '$layer_title', '$layer_desc', '$haz_type', '$layer_remarks', '$copy_lyr_name', '$layer_name', '$haz_rp',1);";
						}
					}
					elseif ($haz_rp == 'TEx' ) {
						$query = "INSERT INTO $workspace.hazards(id, nom, description, type, remarques, nom_origine, indice, temp_de_retour, ans)
							VALUES(DEFAULT, '$layer_title', '$layer_desc', '$haz_type', '$layer_remarks', '$copy_lyr_name', '$layer_name', '$haz_rp',1000);";
					}
					else { 
						$query = "INSERT INTO $workspace.hazards(id, nom, description, type, remarques, nom_origine, indice, temp_de_retour, ans)
							VALUES(DEFAULT, '$layer_title', '$layer_desc', '$haz_type', '$layer_remarks', '$copy_lyr_name', '$layer_name', '$haz_rp', substring('$haz_rp' from 2)::int);";
					}
					
					$query.= "INSERT INTO ".$workspace.".hazards_alternatives VALUES ((SELECT id FROM ".$workspace.".hazards WHERE indice = '$layer_name'), (SELECT id FROM ".$workspace.".alternatives WHERE id = '$alt_id'));"; 
				}
				elseif ($layer_type == 'Objects') {
					$query = "INSERT INTO ".$workspace.".objects VALUES (DEFAULT, '$layer_title', '$layer_desc', '$obj_type', '$layer_remarks', '$copy_lyr_name', '$layer_name');";
					$query.= "INSERT INTO ".$workspace.".objects_alternatives VALUES ((SELECT id FROM ".$workspace.".objects WHERE indice = '$layer_name'), (SELECT id FROM ".$workspace.".alternatives WHERE id = '$alt_id'));"; 
				}
				else {}
				
				If (!$rs = pg_query($dbconn,$query)) {
					Echo '{success:true,message:'.json_encode($data).',dbmessage:'.json_encode(pg_last_error($dbconn)).'}';
				}
				else {
					Echo '{success:true,message:'.json_encode($data).',dbmessage: "The layer information is also saved in the respective tables of the database."}';
				}
			} 
			else { // if not success, still return the wps error message
				Echo '{success:false,message:'.json_encode($data).'}';
			}
		}	

		curl_close($ch);	
		pg_close($dbconn);
?>	