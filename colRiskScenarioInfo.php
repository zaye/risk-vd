<?php

	$workspace = $_POST['ws'];
	$task = $_POST['task'];	
	$datastore = $workspace;
	
	//	connect to the database
	$dbconn=pg_connect("host=localhost port=5432 dbname=dbname user=username password=mypassword");	// replace with your db connection information
	if (!$dbconn){
		echo "An error occured.\n";
		exit;
	}
	
	if ($task == 'load') {
		/* $query = "SELECT risk_scenarios.*, alternatives.nom AS alt_nom, spatial_probability.nom AS probabilite_nom, objects.nom AS objet_nom, objects_parameters.nom AS objet_para_nom, vulnerability.nom AS vul_nom
			FROM ".$workspace.".risk_scenarios, ".$workspace.".alternatives, ".$workspace.".spatial_probability, ".$workspace.".objects, ".$workspace.".objects_parameters, ".$workspace.".vulnerability
			WHERE risk_scenarios.alt_id = alternatives.id 
			AND risk_scenarios.pra_id = spatial_probability.id 
			AND risk_scenarios.objet_id = objects.id 
			AND risk_scenarios.objet_para_id = objects_parameters.id
			AND risk_scenarios.vul_id = vulnerability.id;"; */
		
		$query = "SELECT risk_scenarios.*, alternatives.nom AS alt_nom, spatial_probability.nom AS probabilite_nom, objects.nom AS objet_nom, objects_parameters.nom AS objet_para_nom, vulnerability.nom AS vul_nom
		FROM ".$workspace.".risk_scenarios
		LEFT OUTER JOIN ".$workspace.".alternatives ON (risk_scenarios.alt_id = alternatives.id)
		LEFT OUTER JOIN ".$workspace.".spatial_probability ON (risk_scenarios.pra_id = spatial_probability.id)
		LEFT OUTER JOIN ".$workspace.".objects ON (risk_scenarios.objet_id = objects.id)
		LEFT OUTER JOIN ".$workspace.".objects_parameters ON (risk_scenarios.objet_para_id = objects_parameters.id)
		LEFT OUTER JOIN ".$workspace.".vulnerability ON (risk_scenarios.vul_id = vulnerability.id)
		ORDER BY risk_scenarios.id ASC;";
			
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}	
	}
	
	if ($task == 'loadChartStoreGrid') {
		$mpIndex = $_POST['mpIndex'];
		$id = $_POST['id'];
		$objetType = $_POST['objetType'];
		
		if ($objetType == 'Routes'){
			$query = "SELECT t1.id, t1.id_sce, t1.temp_de_retour, t1.indice_risque_sce, COALESCE(sum(risque_obj),0) as risque_obj, COALESCE(sum(risque_humain),0) as risque_humain, COALESCE(sum(risque_obj)+sum(risque_humain),0) as risque_total, COALESCE((sum(risque_humain)/5000000),0) as risque_humain_morts, COALESCE(sum(risque_individuel),0) as risque_individuel
			FROM (SELECT id, id_sce, temp_de_retour, indice_risque_sce
			FROM ".$workspace.".risk_scenarios_hazards
			WHERE id_sce = $id) as t1 LEFT JOIN ".$workspace.".$mpIndex
			ON cin_sce_probabilite = temp_de_retour
			WHERE (objectid, longueur, ST_AsBinary(geom)) 
			NOT IN (SELECT DISTINCT objectid, longueur, ST_AsBinary(geom) 
			FROM ".$workspace.".$mpIndex 
			WHERE cin_lvin_1 = '-4' OR cin_lvin_1 = '-9' OR cin_lvin_1 = '999')
			GROUP BY t1.id, t1.id_sce, t1.temp_de_retour, t1.indice_risque_sce;"; 
		}
		elseif ($objetType == 'Surfaces'){
			$query = "SELECT t1.id, t1.id_sce, t1.temp_de_retour, t1.indice_risque_sce, COALESCE(sum(risque_obj),0) as risque_obj, COALESCE(sum(risque_humain),0) as risque_humain, COALESCE(sum(risque_obj)+sum(risque_humain),0) as risque_total, COALESCE((sum(risque_humain)/5000000),0) as risque_humain_morts, COALESCE(sum(risque_individuel),0) as risque_individuel
			FROM (SELECT id, id_sce, temp_de_retour, indice_risque_sce
			FROM ".$workspace.".risk_scenarios_hazards
			WHERE id_sce = $id) as t1 LEFT JOIN ".$workspace.".$mpIndex
			ON cin_sce_probabilite = temp_de_retour
			WHERE (objectid, area, ST_AsBinary(geom)) 
			NOT IN (SELECT DISTINCT objectid, area, ST_AsBinary(geom) 
			FROM ".$workspace.".$mpIndex 
			WHERE cin_lvin_1 = '-4' OR cin_lvin_1 = '-9' OR cin_lvin_1 = '999')
			GROUP BY t1.id, t1.id_sce, t1.temp_de_retour, t1.indice_risque_sce;";
		}	
		else {
			$query = "SELECT t1.id, t1.id_sce, t1.temp_de_retour, t1.indice_risque_sce, COALESCE(sum(risque_obj),0) as risque_obj, COALESCE(sum(risque_humain),0) as risque_humain, COALESCE(sum(risque_obj)+sum(risque_humain),0) as risque_total, COALESCE((sum(risque_humain)/5000000),0) as risque_humain_morts, COALESCE(sum(risque_individuel),0) as risque_individuel
			FROM (SELECT id, id_sce, temp_de_retour, indice_risque_sce
			FROM ".$workspace.".risk_scenarios_hazards
			WHERE id_sce = $id) as t1 LEFT JOIN ".$workspace.".$mpIndex
			ON cin_sce_probabilite = temp_de_retour
			WHERE egid 
			NOT IN (SELECT DISTINCT egid
			FROM ".$workspace.".$mpIndex 
			WHERE cin_lvin_1 = '-4' OR cin_lvin_1 = '-9' OR cin_lvin_1 = '999')
			GROUP BY t1.id, t1.id_sce, t1.temp_de_retour, t1.indice_risque_sce;";
		}
		
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	
	}
	
	if ($task == 'delete') {
		$temp = $_POST['scenarioRecords'];		
		$scenarioRecords = json_decode($temp, true);
		$length = count($scenarioRecords);
		
		for ($i = 0; $i < $length; $i++) {	// for each of the selected scenarios
			$ID = $scenarioRecords[$i]['id'];
			$mpIndex = $scenarioRecords[$i]['indice'];
			$objectType = $scenarioRecords[$i]['objet_type'];
			
			### Step1: query the relationship records from the risk_scenarios_hazards table first to obtain the names of the geoserver layers to be deleted
			$query = "SELECT indice_risque_sce FROM ".$workspace.".risk_scenarios_hazards WHERE id_sce = $ID AND indice_processus != 'None';";
			If (!$rs = pg_query($dbconn,$query)) {
				Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
			}
			else {
					// if success, store the layer names
					while($obj = pg_fetch_object($rs)){
						$arr[] = $obj->indice_risque_sce;
					}
					
				### Step2: remove the records from the risk_scenarios and its referenced records in the risk_scenarios_hazards table
				$query = "DELETE FROM ".$workspace.".risk_scenarios WHERE id = $ID;";
				
				### Step3: drop the respective layer table using indice column from the db (including the view)
				$query .= "DROP TABLE ".$workspace.".$mpIndex;";
			//	$query .= "DROP VIEW ".$workspace.".$mpIndex"."_view CASCADE;";
			
				if (!$rs = pg_query($dbconn,$query)){			
					$message .= 'Failed to delete the record: '.$scenarioRecords[$i]['nom'].' due to the error: '.json_encode(pg_last_error($dbconn));						
				}
				else {
					// if success
					### Step4: remove the published maps from the geoserver (loop through the $arr + $mpIndex + [$mpIndex_sum for the route object type])
					
					array_push($arr, $mpIndex);
					if ($objectType == 'Routes'){
						array_push($arr, $mpIndex.'_sum');
					}					
					
					foreach ($arr as $value) {						
					
						// Initiate cURL session
						$service = "http://localhost:8080/geoserver/"; // replace with your URL
						$request = "rest/workspaces/".$workspace."/datastores/".$datastore."/featuretypes/".$value.".xml?recurse=true"; // to delete a feature type

						$url = $service . $request;
						$ch = curl_init($url);

						// Optional settings for debugging
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //option to return string
						curl_setopt($ch, CURLOPT_VERBOSE, true);
						curl_setopt($ch, CURLOPT_STDERR, $logfh); // logs curl messages

						//Required DELETE request settings
						curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
						$passwordStr = "username:mypassword"; // replace with your GeoServer username and password
						curl_setopt($ch, CURLOPT_USERPWD, $passwordStr);

						//POST return code
						$successCode = 200;

						// Execute the curl request
						$buffer = curl_exec($ch); 

						// Check for errors and process results
						$info = curl_getinfo($ch);			
						if ($info['http_code'] != $successCode) {
							$message .= "Unuccessful cURL request to ".$url."\n";						
						}
						else {
							$message .= 'The selected map layer: '.$value.' has been deleted!\n';
						}
						curl_close($ch); // free resources if curl handle will not be reused
					}	
				}			
			}			
		}
		Echo '{success: true, message:'.json_encode($message).'}';
	}

	pg_close($dbconn);
?>	