<?php

	$workspace = $_POST['ws'];
	$task = $_POST['task'];	
	$datastore = $workspace;
	
	//	connect to the database
	$dbconn=pg_connect("host=localhost port=5432 dbname=dbname user=username password=mypassword");	// replace with your db connection information
	if (!$dbconn){
		echo "An error occured.\n";
		exit;
	}
	
	if ($task == 'load') {
		$query = "SELECT temp.*, hazards.nom as processus_nom FROM
		(SELECT risk_scenarios.id, risk_scenarios.nom, risk_scenarios.description, alternatives.nom AS alt_nom, 
		risk_scenarios_hazards.temp_de_retour as temp_de_retour, risk_scenarios_hazards.indice_processus, risk_scenarios_hazards.indice_risque_sce,
		risk_scenarios.processus_type, spatial_probability.nom AS probabilite_nom, 
		objects.nom AS objet_nom, risk_scenarios.objet_type, objects_parameters.nom AS objet_para_nom, vulnerability.nom AS vul_nom, risk_scenarios.indice
		FROM ".$workspace.".risk_scenarios 
		INNER JOIN ".$workspace.".alternatives ON ".$workspace.".risk_scenarios.alt_id = ".$workspace.".alternatives.id 
		INNER JOIN ".$workspace.".risk_scenarios_hazards ON ".$workspace.".risk_scenarios.id = ".$workspace.".risk_scenarios_hazards.id_sce
		INNER JOIN ".$workspace.".spatial_probability ON ".$workspace.".risk_scenarios.pra_id = ".$workspace.".spatial_probability.id
		INNER JOIN ".$workspace.".objects ON ".$workspace.".risk_scenarios.objet_id = ".$workspace.".objects.id
		INNER JOIN ".$workspace.".objects_parameters ON ".$workspace.".risk_scenarios.objet_para_id = ".$workspace.".objects_parameters.id
		INNER JOIN ".$workspace.".vulnerability ON ".$workspace.".risk_scenarios.vul_id = ".$workspace.".vulnerability.id) AS temp, ".$workspace.".hazards
		WHERE temp.indice_processus = ".$workspace.".hazards.indice;";
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	if ($task == 'add') {
		
		// retrieve POST data submitted by form
		$scenario_name = $_POST['scenario_name'];
		$scenario_description = $_POST['scenario_description'];
		$alt_scenario_id = $_POST['alt_scenario_name'];	
		$option = $_POST['option'];	
		$area = $_POST['poly'];	

		// a new risk scenario table name to store the affected object records
		$mapping_index = preg_replace('/\s+/','_',$scenario_name); // replace whitespaces and spaces with underscore 
		$mapping_index = strtolower($mapping_index); // change to lowercase // change later to a unique index name 	
		
		// retrieve POST data submitted by form (hazard tab)
		$haz_type = $_POST['haz_type']; 
		$haz_name = $mapping_index.'_view'; // view for a combination of different hazard return periods (same as a unique indice)
		$haz_tab_union = $mapping_index.'_view'; // view for the union of different hazard return periods for the routes (same as a unique indice)
		$freqArr =  json_decode($_POST['hazFreqArr'], true);
	//	$haz_name_Tfreq = $_POST['haz_name_Tfreq'];	
		$haz_name_T30 = $_POST['haz_name_T30'];	
		$haz_name_T100 = $_POST['haz_name_T100'];	
		$haz_name_T300 = $_POST['haz_name_T300'];
		$exArr = json_decode($_POST['hazExArr'], true);
	//	$haz_name_Tex = $_POST['haz_name_Tex'];	
		$spatial_prob = $_POST['spatial_prob']; 
		$spatial_prob_manual = $_POST['spatial_prob_manual']; 
		
		// retrieve POST data submitted by form (object tab)
		$object_type = $_POST['object_type'];	
		$object_name = $_POST['object_name']; 
		$object_level = $_POST['obj_level']; 
		$object_mode = $_POST['object_mode'];	
		$object_manual = $_POST['object_manual'];	
		
		// retrieve POST data submitted by form (vulnerability tab)
		$vul_level = $_POST['vul_level'];
		$vul_mode = $_POST['vul_mode'];	
		$vul_manual = $_POST['vul_manual'];	
		
		// change later to a reasonable index name			
		$mapping_index_t30 = $mapping_index.'_t30';
		$mapping_index_t100 = $mapping_index.'_t100';
		$mapping_index_t300 = $mapping_index.'_t300';
		
		$count = 0;
		
		// create an array to store the pair of return periods	to publish 	
		$rpArr = array();		
		for ($i = 0; $i < count($freqArr); $i++) { // loop for each of the freq records
			if ($freqArr[$i][0] != 'None') {
				array_push($rpArr,array($freqArr[$i][0], $mapping_index.'_t'.$freqArr[$i][1], $freqArr[$i][2], $freqArr[$i][1]));
				if ($haz_type == 'GPP-TR'){
					$tmpQueryStr .= "SELECT gid, globalid, idobjet, alea, cin_lvin_1, cin_scepro, geom FROM ".$workspace.".".$freqArr[$i][0]." UNION ";
				}
				else {
					$tmpQueryStr .= "SELECT * FROM ".$workspace.".".$freqArr[$i][0]." UNION ";
				}
			}
		}
		
		array_push($rpArr, 
			  array($haz_name_T30, $mapping_index_t30, 'T30', '30'),
			  array($haz_name_T100, $mapping_index_t100, 'T100', '100'),
			  array($haz_name_T300, $mapping_index_t300, 'T300', '300'));			
		
		for ($i = 0; $i < count($exArr); $i++) { // loop for each of the extreme records
			if ($exArr[$i][0] != 'None') {
				array_push($rpArr, array($exArr[$i][0], $mapping_index.'_t'.$exArr[$i][1], $exArr[$i][2], $exArr[$i][1]));
				if ($haz_type == 'GPP-TR'){
					$tmpQueryStr2 .= "SELECT gid, globalid, idobjet, alea, cin_lvin_1, cin_scepro, geom FROM ".$workspace.".".$exArr[$i][0]." UNION ";
				}
				else {
					$tmpQueryStr2 .= "SELECT * FROM ".$workspace.".".$exArr[$i][0]." UNION ";
				}
			}
		}		
		
		### query to create a combined intensity view for all return periods of the intensity layers *** assuming as at least one RP is NOT None ***
		$query = "CREATE TEMP TABLE ".$haz_name." AS (";
		$query .= "SELECT row_number() over() as rownum, * FROM (";
		$query .= $tmpQueryStr;
		if ($haz_name_T30 != 'None') {
			if ($haz_type == 'GPP-TR'){
				$query .= "SELECT gid, globalid, idobjet, alea, cin_lvin_1, cin_scepro, geom FROM ".$workspace.".".$haz_name_T30." UNION ";
			}
			else {
				$query .= "SELECT * FROM ".$workspace.".".$haz_name_T30." UNION ";
			}
		}
		if ($haz_name_T100 != 'None') {
			if ($haz_type == 'GPP-TR'){
				$query .= "SELECT gid, globalid, idobjet, alea, cin_lvin_1, cin_scepro, geom FROM ".$workspace.".".$haz_name_T100." UNION ";
			}
			else {
				$query .= "SELECT * FROM ".$workspace.".".$haz_name_T100." UNION ";
			}
		}
		if ($haz_name_T300 != 'None') {
			if ($haz_type == 'GPP-TR'){
				$query .= "SELECT gid, globalid, idobjet, alea, cin_lvin_1, cin_scepro, geom FROM ".$workspace.".".$haz_name_T300." UNION ";
			}
			else {
				$query .= "SELECT * FROM ".$workspace.".".$haz_name_T300." UNION ";
			}
		}
		$query .= $tmpQueryStr2;
		
		// remove the last phrase UNION and add ORDER BY at the end
		$query = rtrim($query, 'UNION ');
		if ($haz_type == 'GPP-TR'){
			$query.= " ORDER BY gid, cin_lvin_1) as t);";	
		}
		else {
			$query.= " ORDER BY cin_fksce ASC) as t);";	
		}
		
		### query to check if there are overlapping polygons 
		if ($object_type == 'Routes' || $object_type == 'Surfaces'){
			$tquery = $query;
			$query = '';
		//	str_replace("CREATE OR REPLACE VIEW","CREATE TEMP TABLE",$tquery);
			$tquery .= "SELECT *
				FROM ".$haz_name." a
				INNER JOIN ".$haz_name." b ON 
				   (a.geom && b.geom AND ST_Relate(a.geom, b.geom, '2********'))
				WHERE a.gid != b.gid
				LIMIT 1;";
			
			If (!$tres = pg_query($dbconn,$tquery)) {
				Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
			}
			else {			
				$count = pg_num_rows($tres);
			}
			
		//	echo 'count:'.$count;
		}
		
		### query to create a basic table for affected objects (*attention to case sensentive column names of objects layer)		
		if ($option == 'L') { // if option is Layer-based, take object layer as a whole for calculation
			if ($object_level == 'Class' || $object_level == 'Category' || $object_level == 'Code-EconoMe') {
				if ($vul_level == 'Class' || $vul_level == 'Category' || $vul_level == 'Code-EconoMe'){
					if ($haz_type == 'GPP-TR'){
						if ($object_type == 'Routes' || $object_type == 'Surfaces'){
							// for routes and surfaces with the option 'Code-EconoMe'
							if ($count == 1) { // do the UNION if overlapping polygons
								$haz_tab_union = $mapping_index.'_union_view';
								$query .= 'CREATE TEMP TABLE '.$haz_tab_union.' AS
								SELECT a.rownum, a.gid, a.globalid, a.idobjet, a.alea, a.cin_lvin_1, a.cin_scepro, new_polys.the_geom as geom 
								FROM (SELECT geom AS the_geom, ST_PointOnSurface(geom) AS pip 
								FROM ST_Dump((
									SELECT ST_Polygonize(the_geom) AS the_geom FROM (
										SELECT ST_Node(ST_Collect(the_geom)) AS the_geom FROM (
											SELECT ST_ExteriorRing(ST_GeometryN(geom, 1)) AS the_geom
								FROM '.$haz_name.') AS lines
									) AS noded_lines
								)) 
								) AS new_polys
								LEFT JOIN '.$haz_name.' a ON St_Within(new_polys.pip,a.geom);';
							}							
							
							if ($object_type == 'Routes'){
								// PAY ATTENTION to the ST_Length function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								SELECT t2.fid, t2."OBJECTID" as objectid, t2."CODE_ECONO" as code_econo, t2."OBJEKTART" as categorie, t2."KUNSTBAUTE" as domain, ST_Length(t2.the_geom) AS longueur_tot, 0.0 As longueur, rownum, gid, globalid, idobjet, alea, cin_lvin_1, cin_scepro as cin_sce_probabilite
								, CASE 
								   WHEN ST_CoveredBy(t2.the_geom, t1.geom) 
								   THEN t2.the_geom
								   ELSE 
									ST_Multi(
									  ST_Intersection(t2.the_geom, t1.geom)
									  ) 
								   END AS geom
								FROM '.$haz_tab_union.' t1, '.$workspace.'."'.$object_name.'" t2
								WHERE ST_Intersects(t1.geom, t2.the_geom)
								AND t2."CODE_ECONO" <> 0;';
								
								$query .= 'UPDATE '.$workspace.'.'.$mapping_index.' SET longueur = ST_length(geom);';
							}
							else {
								// PAY ATTENTION to the ST_Area function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								SELECT *, ST_Area(geom) AS area
								FROM 
								(SELECT n.fid, n."OBJECTID" as objectid, n."CODE_ECONO" as code_econo, n."TYPE_PRINC" as categorie, rownum, gid, globalid, idobjet, alea, cin_lvin_1, cin_scepro as cin_sce_probabilite
								 , CASE 
								   WHEN ST_CoveredBy(p.geom, n.the_geom) 
								   THEN ST_Multi(p.geom)
								   ELSE 
									ST_Multi(
									  ST_Intersection(p.geom,n.the_geom)
									  ) 
								   END AS geom 
								FROM '.$haz_tab_union.' AS p 
								INNER JOIN '.$workspace.'."'.$object_name.'" AS n 
								ON (ST_Intersects(p.geom, n.the_geom) 
								AND NOT ST_Touches(p.geom, n.the_geom)
								AND n."CODE_ECONO" <> 0)) t
								WHERE ST_Area(geom) > 0.1;';
							}
						}
						else{
							// for buildings with the options 'Class' and 'Category'
							$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS (
							SELECT the_geom as geom, "EGID" as egid, "CATEGORIE" as categorie, "CLASSE" as classe, "NB_LOG" as nbre_log, "VOLUME" as volume, "SURF_TOT" as surface, rownum, gid, globalid, idobjet, alea, cin_lvin_1, cin_scepro as cin_sce_probabilite								
							FROM '.$haz_name.', '.$workspace.'."'.$object_name.'"			
							WHERE ST_Intersects("'.$object_name.'".the_geom, '.$haz_name.".geom)							
							ORDER BY egid, cin_lvin_1
							);";
						}							
					}					
					elseif ($haz_type == 'LTO'){
						if ($object_type == 'Routes' || $object_type == 'Surfaces'){
							// for routes and surfaces with the option 'Code-EconoMe'
							if ($count == 1) { // do the UNION if overlapping polygons
								$haz_tab_union = $mapping_index.'_union_view';
								$query .= 'CREATE TEMP TABLE '.$haz_tab_union.' AS
								SELECT a.gid, a.cin_fksce, a.cin_lvinte, a.cin_lvin_1, a.cin_lvin_2, a.cin_lvin_3, a.cin_scepro, new_polys.the_geom as geom 
								FROM (SELECT geom AS the_geom, ST_PointOnSurface(geom) AS pip 
								FROM ST_Dump((
									SELECT ST_Polygonize(the_geom) AS the_geom FROM (
										SELECT ST_Node(ST_Collect(the_geom)) AS the_geom FROM (
											SELECT ST_ExteriorRing(ST_GeometryN(geom,1)) AS the_geom
								FROM '.$haz_name.') AS lines
									) AS noded_lines
								)) 
								) AS new_polys
								LEFT JOIN '.$haz_name.' a ON St_Within(new_polys.pip,a.geom);';
							}							
							
							if ($object_type == 'Routes'){
								// PAY ATTENTION to the ST_Length function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								SELECT t2.fid, t2."OBJECTID" as objectid, t2."CODE_ECONO" as code_econo, t2."OBJEKTART" as categorie, t2."KUNSTBAUTE" as domain, ST_Length(t2.the_geom) AS longueur_tot, 0.0 AS longueur, gid, cin_fksce, cin_lvinte, cin_lvin_1 as int_1, cin_lvin_2, cin_lvin_3 as int_2, cin_lvin_1, cin_scepro as cin_sce_probabilite
								, CASE 
								   WHEN ST_CoveredBy(t2.the_geom, t1.geom) 
								   THEN t2.the_geom
								   ELSE 
									ST_Multi(
									  ST_Intersection(t2.the_geom, t1.geom)
									  ) 
								   END AS geom
								FROM '.$haz_tab_union.' t1, '.$workspace.'."'.$object_name.'" t2
								WHERE ST_Intersects(t1.geom, t2.the_geom)
								AND t2."CODE_ECONO" <> 0;';
								
								$query .= 'UPDATE '.$workspace.'.'.$mapping_index.' SET longueur = ST_length(geom);';
							}
							else {
								// PAY ATTENTION to the ST_Area function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								SELECT *, ST_Area(geom) AS area
								FROM 
								(SELECT n.fid, n."OBJECTID" as objectid, n."CODE_ECONO" as code_econo, n."TYPE_PRINC" as categorie, gid, cin_fksce, cin_lvinte, cin_lvin_1 as int_1, cin_lvin_2, cin_lvin_3 as int_2, cin_lvin_1, cin_scepro as cin_sce_probabilite
								 , CASE 
								   WHEN ST_CoveredBy(p.geom, n.the_geom) 
								   THEN ST_Multi(p.geom)
								   ELSE 
									ST_Multi(
									  ST_Intersection(p.geom,n.the_geom)
									  ) 
								   END AS geom 
								FROM '.$haz_tab_union.' AS p 
								INNER JOIN '.$workspace.'."'.$object_name.'" AS n 
								ON (ST_Intersects(p.geom, n.the_geom) 
								AND NOT ST_Touches(p.geom, n.the_geom)
								AND n."CODE_ECONO" <> 0)) t
								WHERE ST_Area(geom) > 0.1;';
							}
						}
						else {							
							// for buildings with the options 'Class' and 'Category'
							$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS (
							SELECT the_geom as geom, "EGID" as egid, "CATEGORIE" as categorie, "CLASSE" as classe, "NB_LOG" as nbre_log, "VOLUME" as volume, "SURF_TOT" as surface, objectid, cin_fksce, cin_lvinte, cin_lvin_1 as int_1, cin_lvin_2, cin_lvin_3 as int_2, cin_lvin_1, cin_scepro as cin_sce_probabilite								
							FROM '.$haz_name.', '.$workspace.'."'.$object_name.'"			
							WHERE ST_Intersects("'.$object_name.'".the_geom, '.$haz_name.".geom)							
							ORDER BY egid
							);";
						}						
					}
					else {
						### for other types of hazards						
						if ($object_type == 'Routes' || $object_type == 'Surfaces'){
							// for routes and surfaces with the option 'Code-EconoMe'
							if ($count == 1) { // do the UNION if overlapping polygons
								$haz_tab_union = $mapping_index.'_union_view';
								$query .= 'CREATE TEMP TABLE '.$haz_tab_union.' AS
								SELECT a.gid, a.cin_fksce, a.cin_lvinte, a.cin_lvin_1, a.cin_scepro, new_polys.the_geom as geom 
								FROM (SELECT geom AS the_geom, ST_PointOnSurface(geom) AS pip 
								FROM ST_Dump((
									SELECT ST_Polygonize(the_geom) AS the_geom FROM (
										SELECT ST_Node(ST_Collect(the_geom)) AS the_geom FROM (
											SELECT ST_ExteriorRing(ST_GeometryN(geom,1)) AS the_geom
								FROM '.$haz_name.') AS lines
									) AS noded_lines
								)) 
								) AS new_polys
								LEFT JOIN '.$haz_name.' a ON St_Within(new_polys.pip,a.geom);';
							}							
							
							if ($object_type == 'Routes'){
								// PAY ATTENTION to the ST_Length function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								SELECT t2.fid, t2."OBJECTID" as objectid, t2."CODE_ECONO" as code_econo, t2."OBJEKTART" as categorie, t2."KUNSTBAUTE" as domain, ST_Length(t2.the_geom) AS longueur_tot, 0.0 AS longueur, gid, cin_fksce, cin_lvinte, cin_lvin_1, cin_scepro as cin_sce_probabilite
								, CASE 
								   WHEN ST_CoveredBy(t2.the_geom, t1.geom) 
								   THEN t2.the_geom
								   ELSE 
									ST_Multi(
									  ST_Intersection(t2.the_geom, t1.geom)
									  ) 
								   END AS geom
								FROM '.$haz_tab_union.' t1, '.$workspace.'."'.$object_name.'" t2
								WHERE ST_Intersects(t1.geom, t2.the_geom)
								AND t2."CODE_ECONO" <> 0;';
								
								$query .= 'UPDATE '.$workspace.'.'.$mapping_index.' SET longueur = ST_length(geom);';
							}
							else {
								// PAY ATTENTION to the ST_Area function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								SELECT *, ST_Area(geom) AS area
								FROM 
								(SELECT n.fid, n."OBJECTID" as objectid, n."CODE_ECONO" as code_econo, n."TYPE_PRINC" as categorie, gid, cin_fksce, cin_lvinte, cin_lvin_1, cin_scepro as cin_sce_probabilite
								 , CASE 
								   WHEN ST_CoveredBy(p.geom, n.the_geom) 
								   THEN ST_Multi(p.geom)
								   ELSE 
									ST_Multi(
									  ST_Intersection(p.geom,n.the_geom)
									  ) 
								   END AS geom 
								FROM '.$haz_tab_union.' AS p 
								INNER JOIN '.$workspace.'."'.$object_name.'" AS n 
								ON (ST_Intersects(p.geom, n.the_geom) 
								AND NOT ST_Touches(p.geom, n.the_geom)
								AND n."CODE_ECONO" <> 0)) t
								WHERE ST_Area(geom) > 0.1;';
							}
						}
						else {
							// for buildings with the options 'Class' and 'Category'
							$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS (
							SELECT the_geom as geom, "EGID" as egid, "CATEGORIE" as categorie, "CLASSE" as classe, "NB_LOG" as nbre_log, "VOLUME" as volume, "SURF_TOT" as surface, objectid, cin_fksce, cin_lvinte, cin_lvin_1, cin_scepro as cin_sce_probabilite								
							FROM '.$haz_name.', '.$workspace.'."'.$object_name.'"			
							WHERE ST_Intersects("'.$object_name.'".the_geom, '.$haz_name.".geom)							
							ORDER BY egid
							);";
						}							
					}					
				}
				else {
					// for the vulnerability option 'Manual'
					if ($haz_type == 'GPP-TR'){
						if ($object_type == 'Routes' || $object_type == 'Surfaces'){							
							if ($count == 1) { // do the UNION if overlapping polygons
								$haz_tab_union = $mapping_index.'_union_view';
								$query .= 'CREATE TEMP TABLE '.$haz_tab_union.' AS
								SELECT a.rownum, a.gid, a.globalid, a.idobjet, a.alea, a.cin_lvin_1, a.cin_scepro, new_polys.the_geom as geom 
								FROM (SELECT geom AS the_geom, ST_PointOnSurface(geom) AS pip 
								FROM ST_Dump((
									SELECT ST_Polygonize(the_geom) AS the_geom FROM (
										SELECT ST_Node(ST_Collect(the_geom)) AS the_geom FROM (
											SELECT ST_ExteriorRing(ST_GeometryN(geom,1)) AS the_geom
								FROM '.$haz_name.') AS lines
									) AS noded_lines
								)) 
								) AS new_polys
								LEFT JOIN '.$haz_name.' a ON St_Within(new_polys.pip,a.geom);';
							}							
							
							if ($object_type == 'Routes'){
								// PAY ATTENTION to the ST_Length function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								SELECT t2.fid, t2."OBJECTID" as objectid, t2."CODE_ECONO" as code_econo, t2."OBJEKTART" as categorie, t2."KUNSTBAUTE" as domain, ST_Length(t2.the_geom) AS longueur_tot, 0.0 As longueur, "VUL_OBJ_F" as vul_obj_f, "VUL_OBJ_M" as vul_obj_m, "VUL_OBJ_H" as vul_obj_h, "LET_PERS_F" as let_pers_f, "LET_PERS_M" as let_pers_m, "LET_PERS_H" as let_pers_h, rownum, gid, globalid, idobjet, alea, cin_lvin_1, cin_scepro as cin_sce_probabilite
								, CASE 
								   WHEN ST_CoveredBy(t2.the_geom, t1.geom) 
								   THEN t2.the_geom
								   ELSE 
									ST_Multi(
									  ST_Intersection(t2.the_geom, t1.geom)
									  ) 
								   END AS geom
								FROM '.$haz_tab_union.' t1, '.$workspace.'."'.$object_name.'" t2
								WHERE ST_Intersects(t1.geom, t2.the_geom)
								AND t2."CODE_ECONO" <> 0;';
								
								$query .= 'UPDATE '.$workspace.'.'.$mapping_index.' SET longueur = ST_length(geom);';
							}
							else {
								// PAY ATTENTION to the ST_Area function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								SELECT *, ST_Area(geom) AS area
								FROM 
								(SELECT n.fid, n."OBJECTID" as objectid, n."CODE_ECONO" as code_econo, n."TYPE_PRINC" as categorie, "VUL_OBJ_F" as vul_obj_f, "VUL_OBJ_M" as vul_obj_m, "VUL_OBJ_H" as vul_obj_h, "LET_PERS_F" as let_pers_f, "LET_PERS_M" as let_pers_m, "LET_PERS_H" as let_pers_h, rownum, gid, globalid, idobjet, alea, cin_lvin_1, cin_scepro as cin_sce_probabilite
								   WHEN ST_CoveredBy(p.geom, n.the_geom) 
								   THEN ST_Multi(p.geom)
								   ELSE 
									ST_Multi(
									  ST_Intersection(p.geom,n.the_geom)
									  ) 
								   END AS geom 
								FROM '.$haz_tab_union.' AS p 
								INNER JOIN '.$workspace.'."'.$object_name.'" AS n 
								ON (ST_Intersects(p.geom, n.the_geom) 
								AND NOT ST_Touches(p.geom, n.the_geom)
								AND n."CODE_ECONO" <> 0)) t
								WHERE ST_Area(geom) > 0.1;';
							}
						}
						else {
							// for buildings 
							$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS (
							SELECT the_geom as geom, "EGID" as egid, "CATEGORIE" as categorie, "CLASSE" as classe, "NB_LOG" as nbre_log, "VOLUME" as volume, "SURF_TOT" as surface, "VUL_OBJ_F" as vul_obj_f, "VUL_OBJ_M" as vul_obj_m, "VUL_OBJ_H" as vul_obj_h, "LET_PERS_F" as let_pers_f, "LET_PERS_M" as let_pers_m, "LET_PERS_H" as let_pers_h, rownum, gid, globalid, idobjet, alea, cin_lvin_1, cin_scepro as cin_sce_probabilite								
							FROM '.$haz_name.', '.$workspace.'."'.$object_name.'"			
							WHERE ST_Intersects("'.$object_name.'".the_geom, '.$haz_name.".geom)							
							ORDER BY egid, cin_lvin_1
							);";
						}						
					}					
					elseif ($haz_type == 'LTO'){
						if ($object_type == 'Routes' || $object_type == 'Surfaces'){							
							if ($count == 1) { // do the UNION if overlapping polygons
								$haz_tab_union = $mapping_index.'_union_view';
								$query .= 'CREATE TEMP TABLE '.$haz_tab_union.' AS
								SELECT a.gid, a.cin_fksce, a.cin_lvinte, a.cin_lvin_1, a.cin_lvin_2, a.cin_lvin_3, a.cin_scepro, new_polys.the_geom as geom 
								FROM (SELECT geom AS the_geom, ST_PointOnSurface(geom) AS pip 
								FROM ST_Dump((
									SELECT ST_Polygonize(the_geom) AS the_geom FROM (
										SELECT ST_Node(ST_Collect(the_geom)) AS the_geom FROM (
											SELECT ST_ExteriorRing(ST_GeometryN(geom,1)) AS the_geom
								FROM '.$haz_name.') AS lines
									) AS noded_lines
								)) 
								) AS new_polys
								LEFT JOIN '.$haz_name.' a ON St_Within(new_polys.pip,a.geom);';
							}							
							
							if ($object_type == 'Routes'){
								// PAY ATTENTION to the ST_Length function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								SELECT t2.fid, t2."OBJECTID" as objectid, t2."CODE_ECONO" as code_econo, t2."OBJEKTART" as categorie, t2."KUNSTBAUTE" as domain, ST_Length(t2.the_geom) AS longueur_tot, 0.0 As longueur, "VUL_OBJ_F" as vul_obj_f, "VUL_OBJ_M" as vul_obj_m, "VUL_OBJ_H" as vul_obj_h, "LET_PERS_F" as let_pers_f, "LET_PERS_M" as let_pers_m, "LET_PERS_H" as let_pers_h, gid, cin_fksce, cin_lvinte, cin_lvin_1 as int_1, cin_lvin_2, cin_lvin_3 as int_2, cin_lvin_1, cin_scepro as cin_sce_probabilite
								, CASE 
								   WHEN ST_CoveredBy(t2.the_geom, t1.geom) 
								   THEN t2.the_geom
								   ELSE 
									ST_Multi(
									  ST_Intersection(t2.the_geom, t1.geom)
									  ) 
								   END AS geom
								FROM '.$haz_tab_union.' t1, '.$workspace.'."'.$object_name.'" t2
								WHERE ST_Intersects(t1.geom, t2.the_geom)
								AND t2."CODE_ECONO" <> 0;';
								
								$query .= 'UPDATE '.$workspace.'.'.$mapping_index.' SET longueur = ST_length(geom);';
							}
							else {
								// PAY ATTENTION to the ST_Area function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								SELECT *, ST_Area(geom) AS area
								FROM 
								(SELECT n.fid, n."OBJECTID" as objectid, n."CODE_ECONO" as code_econo, n."TYPE_PRINC" as categorie, "VUL_OBJ_F" as vul_obj_f, "VUL_OBJ_M" as vul_obj_m, "VUL_OBJ_H" as vul_obj_h, "LET_PERS_F" as let_pers_f, "LET_PERS_M" as let_pers_m, "LET_PERS_H" as let_pers_h, gid, cin_fksce, cin_lvinte, cin_lvin_1 as int_1, cin_lvin_2, cin_lvin_3 as int_2, cin_lvin_1, cin_scepro as cin_sce_probabilite
								 , CASE 
								   WHEN ST_CoveredBy(p.geom, n.the_geom) 
								   THEN ST_Multi(p.geom)
								   ELSE 
									ST_Multi(
									  ST_Intersection(p.geom,n.the_geom)
									  ) 
								   END AS geom 
								FROM '.$haz_tab_union.' AS p 
								INNER JOIN '.$workspace.'."'.$object_name.'" AS n 
								ON (ST_Intersects(p.geom, n.the_geom) 
								AND NOT ST_Touches(p.geom, n.the_geom)
								AND n."CODE_ECONO" <> 0)) t
								WHERE ST_Area(geom) > 0.1;';
							}
						}
						else {							
							// for buildings 
							$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS (
							SELECT the_geom as geom, "EGID" as egid, "CATEGORIE" as categorie, "CLASSE" as classe, "NB_LOG" as nbre_log, "VOLUME" as volume, "SURF_TOT" as surface, "VUL_OBJ_F" as vul_obj_f, "VUL_OBJ_M" as vul_obj_m, "VUL_OBJ_H" as vul_obj_h, "LET_PERS_F" as let_pers_f, "LET_PERS_M" as let_pers_m, "LET_PERS_H" as let_pers_h, objectid, cin_fksce, cin_lvinte, cin_lvin_1 as int_1, cin_lvin_2, cin_lvin_3 as int_2, cin_lvin_1, cin_scepro as cin_sce_probabilite								
							FROM '.$haz_name.', '.$workspace.'."'.$object_name.'"			
							WHERE ST_Intersects("'.$object_name.'".the_geom, '.$haz_name.".geom)							
							ORDER BY egid
							);";							
						}						
					}
					else {
						### for other types of hazards
						if ($object_type == 'Routes' || $object_type == 'Surfaces'){							
							if ($count == 1) { // do the UNION if overlapping polygons
								$haz_tab_union = $mapping_index.'_union_view';
								$query .= 'CREATE TEMP TABLE '.$haz_tab_union.' AS
								SELECT a.gid, a.cin_fksce, a.cin_lvinte, a.cin_lvin_1, a. cin_scepro, new_polys.the_geom as geom 
								FROM (SELECT geom AS the_geom, ST_PointOnSurface(geom) AS pip 
								FROM ST_Dump((
									SELECT ST_Polygonize(the_geom) AS the_geom FROM (
										SELECT ST_Node(ST_Collect(the_geom)) AS the_geom FROM (
											SELECT ST_ExteriorRing(ST_GeometryN(geom,1)) AS the_geom
								FROM '.$haz_name.') AS lines
									) AS noded_lines
								)) 
								) AS new_polys
								LEFT JOIN '.$haz_name.' a ON St_Within(new_polys.pip,a.geom);';
							}							
							
							if ($object_type == 'Routes'){
								// PAY ATTENTION to the ST_Length function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								SELECT t2.fid, t2."OBJECTID" as objectid, t2."CODE_ECONO" as code_econo, t2."OBJEKTART" as categorie, t2."KUNSTBAUTE" as domain, ST_Length(t2.the_geom) AS longueur_tot, 0.0 As longueur, "VUL_OBJ_F" as vul_obj_f, "VUL_OBJ_M" as vul_obj_m, "VUL_OBJ_H" as vul_obj_h, "LET_PERS_F" as let_pers_f, "LET_PERS_M" as let_pers_m, "LET_PERS_H" as let_pers_h, gid, cin_fksce, cin_lvinte, cin_lvin_1, cin_scepro as cin_sce_probabilite
								, CASE 
								   WHEN ST_CoveredBy(t2.the_geom, t1.geom) 
								   THEN t2.the_geom
								   ELSE 
									ST_Multi(
									  ST_Intersection(t2.the_geom, t1.geom)
									  ) 
								   END AS geom
								FROM '.$haz_tab_union.' t1, '.$workspace.'."'.$object_name.'" t2
								WHERE ST_Intersects(t1.geom, t2.the_geom)
								AND t2."CODE_ECONO" <> 0;';
								
								$query .= 'UPDATE '.$workspace.'.'.$mapping_index.' SET longueur = ST_length(geom);';
							}
							else {
								// PAY ATTENTION to the ST_Area function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								SELECT *, ST_Area(geom) AS area
								FROM 
								(SELECT n.fid, n."OBJECTID" as objectid, n."CODE_ECONO" as code_econo, n."TYPE_PRINC" as categorie, "VUL_OBJ_F" as vul_obj_f, "VUL_OBJ_M" as vul_obj_m, "VUL_OBJ_H" as vul_obj_h, "LET_PERS_F" as let_pers_f, "LET_PERS_M" as let_pers_m, "LET_PERS_H" as let_pers_h, gid, cin_fksce, cin_lvinte, cin_lvin_1, cin_scepro as cin_sce_probabilite
								 , CASE 
								   WHEN ST_CoveredBy(p.geom, n.the_geom) 
								   THEN ST_Multi(p.geom)
								   ELSE 
									ST_Multi(
									  ST_Intersection(p.geom,n.the_geom)
									  ) 
								   END AS geom 
								FROM '.$haz_tab_union.' AS p 
								INNER JOIN '.$workspace.'."'.$object_name.'" AS n 
								ON (ST_Intersects(p.geom, n.the_geom) 
								AND NOT ST_Touches(p.geom, n.the_geom)
								AND n."CODE_ECONO" <> 0)) t
								WHERE ST_Area(geom) > 0.1;';
							}
						}
						else {
							// for buildings 
							$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS (
							SELECT the_geom as geom, "EGID" as egid, "CATEGORIE" as categorie, "CLASSE" as classe, "NB_LOG" as nbre_log, "VOLUME" as volume, "SURF_TOT" as surface, "VUL_OBJ_F" as vul_obj_f, "VUL_OBJ_M" as vul_obj_m, "VUL_OBJ_H" as vul_obj_h, "LET_PERS_F" as let_pers_f, "LET_PERS_M" as let_pers_m, "LET_PERS_H" as let_pers_h, objectid, cin_fksce, cin_lvinte, cin_lvin_1, cin_scepro as cin_sce_probabilite								
							FROM '.$haz_name.', '.$workspace.'."'.$object_name.'"			
							WHERE ST_Intersects("'.$object_name.'".the_geom, '.$haz_name.".geom)							
							ORDER BY egid
							);";
						}							
					}					
				}
			}
			else {
				// for the object option 'Manual'
				if ($vul_level == 'Class' || $vul_level == 'Category' || $vul_level == 'Code-EconoMe'){
					if ($haz_type == 'GPP-TR'){
						if ($object_type == 'Routes' || $object_type == 'Surfaces'){
							// for routes and surfaces with the vulnerability option 'Code-EconoMe'
							if ($count == 1) { // do the UNION if overlapping polygons
								$haz_tab_union = $mapping_index.'_union_view';
								$query .= 'CREATE TEMP TABLE '.$haz_tab_union.' AS
								SELECT a.rownum, a.gid, a.globalid, a.idobjet, a.alea, a.cin_lvin_1, a.cin_scepro, new_polys.the_geom as geom 
								FROM (SELECT geom AS the_geom, ST_PointOnSurface(geom) AS pip 
								FROM ST_Dump((
									SELECT ST_Polygonize(the_geom) AS the_geom FROM (
										SELECT ST_Node(ST_Collect(the_geom)) AS the_geom FROM (
											SELECT ST_ExteriorRing(ST_GeometryN(geom,1)) AS the_geom
								FROM '.$haz_name.') AS lines
									) AS noded_lines
								)) 
								) AS new_polys
								LEFT JOIN '.$haz_name.' a ON St_Within(new_polys.pip,a.geom);';
							}							
							
							if ($object_type == 'Routes'){
								// PAY ATTENTION to the ST_Length function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								SELECT t2.fid, t2."OBJECTID" as objectid, t2."CODE_ECONO" as code_econo, t2."OBJEKTART" as categorie, t2."KUNSTBAUTE" as domain, ST_Length(t2.the_geom) AS longueur_tot, 0.0 As longueur, "PRIX_M" as prix_unite, "TJM" as tjm, "VITESSE" as vitesse, "L_VEHICULE" as l_m, "NB_PERS" as humains, "NB_PASSAGE" as passage, rownum, gid, globalid, idobjet, alea, cin_lvin_1, cin_scepro as cin_sce_probabilite
								, CASE 
								   WHEN ST_CoveredBy(t2.the_geom, t1.geom) 
								   THEN t2.the_geom
								   ELSE 
									ST_Multi(
									  ST_Intersection(t2.the_geom, t1.geom)
									  ) 
								   END AS geom
								FROM '.$haz_tab_union.' t1, '.$workspace.'."'.$object_name.'" t2
								WHERE ST_Intersects(t1.geom, t2.the_geom)
								AND t2."CODE_ECONO" <> 0;';
								
								$query .= 'UPDATE '.$workspace.'.'.$mapping_index.' SET longueur = ST_length(geom);';
							}
							else {
								// PAY ATTENTION to the ST_Area function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								SELECT *, ST_Area(geom) AS area
								FROM 
								(SELECT n.fid, n."OBJECTID" as objectid, n."CODE_ECONO" as code_econo, n."TYPE_PRINC" as categorie, "PRIX" as prix_unite, "NB_PERS" as humains, "EXP_PERS" as exp_pers, rownum, gid, globalid, idobjet, alea, cin_lvin_1, cin_scepro as cin_sce_probabilite
								 , CASE 
								   WHEN ST_CoveredBy(p.geom, n.the_geom) 
								   THEN ST_Multi(p.geom)
								   ELSE 
									ST_Multi(
									  ST_Intersection(p.geom,n.the_geom)
									  ) 
								   END AS geom 
								FROM '.$haz_tab_union.' AS p 
								INNER JOIN '.$workspace.'."'.$object_name.'" AS n 
								ON (ST_Intersects(p.geom, n.the_geom) 
								AND NOT ST_Touches(p.geom, n.the_geom)
								AND n."CODE_ECONO" <> 0)) t
								WHERE ST_Area(geom) > 0.1;';
							}
						}
						else {
							// for buildings with the vulnerability options 'Class' and 'Category'
							$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS (
							SELECT the_geom as geom, "EGID" as egid, "CATEGORIE" as categorie, "CLASSE" as classe, "PRIX_TOT" as prix_tot, "NB_PERS" as nb_pers, "EXP_PERS" as exp_pers, rownum, gid, globalid, idobjet, alea, cin_lvin_1, cin_scepro as cin_sce_probabilite								
							FROM '.$haz_name.', '.$workspace.'."'.$object_name.'"			
							WHERE ST_Intersects("'.$object_name.'".the_geom, '.$haz_name.".geom)							
							ORDER BY egid, cin_lvin_1
							);";
						}						
					}					
					elseif ($haz_type == 'LTO'){
						if ($object_type == 'Routes' || $object_type == 'Surfaces'){
							// for routes and surfaces with the vulnerability option 'Code-EconoMe'
							if ($count == 1) { // do the UNION if overlapping polygons
								$haz_tab_union = $mapping_index.'_union_view';
								$query .= 'CREATE TEMP TABLE '.$haz_tab_union.' AS
								SELECT a.gid, a.cin_fksce, a.cin_lvinte, a.cin_lvin_1, a.cin_lvin_2, a.cin_lvin_3, a.cin_scepro, new_polys.the_geom as geom 
								FROM (SELECT geom AS the_geom, ST_PointOnSurface(geom) AS pip 
								FROM ST_Dump((
									SELECT ST_Polygonize(the_geom) AS the_geom FROM (
										SELECT ST_Node(ST_Collect(the_geom)) AS the_geom FROM (
											SELECT ST_ExteriorRing(ST_GeometryN(geom,1)) AS the_geom
								FROM '.$haz_name.') AS lines
									) AS noded_lines
								)) 
								) AS new_polys
								LEFT JOIN '.$haz_name.' a ON St_Within(new_polys.pip,a.geom);';
							}							
							
							if ($object_type == 'Routes'){
								// PAY ATTENTION to the ST_Length function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								SELECT t2.fid, t2."OBJECTID" as objectid, t2."CODE_ECONO" as code_econo, t2."OBJEKTART" as categorie, t2."KUNSTBAUTE" as domain, ST_Length(t2.the_geom) AS longueur_tot, 0.0 As longueur, "PRIX_M" as prix_unite, "TJM" as tjm, "VITESSE" as vitesse, "L_VEHICULE" as l_m, "NB_PERS" as humains, "NB_PASSAGE" as passage, gid, cin_fksce, cin_lvinte, cin_lvin_1 as int_1, cin_lvin_2, cin_lvin_3 as int_2, cin_lvin_1, cin_scepro as cin_sce_probabilite
								, CASE 
								   WHEN ST_CoveredBy(t2.the_geom, t1.geom) 
								   THEN t2.the_geom
								   ELSE 
									ST_Multi(
									  ST_Intersection(t2.the_geom, t1.geom)
									  ) 
								   END AS geom
								FROM '.$haz_tab_union.' t1, '.$workspace.'."'.$object_name.'" t2
								WHERE ST_Intersects(t1.geom, t2.the_geom)
								AND t2."CODE_ECONO" <> 0;';
								
								$query .= 'UPDATE '.$workspace.'.'.$mapping_index.' SET longueur = ST_length(geom);';
							}
							else {
								// PAY ATTENTION to the ST_Area function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								SELECT *, ST_Area(geom) AS area
								FROM 
								(SELECT n.fid, n."OBJECTID" as objectid, n."CODE_ECONO" as code_econo, n."TYPE_PRINC" as categorie, "PRIX" as prix_unite, "NB_PERS" as humains, "EXP_PERS" as exp_pers, gid, cin_fksce, cin_lvinte, cin_lvin_1 as int_1, cin_lvin_2, cin_lvin_3 as int_2, cin_lvin_1, cin_scepro as cin_sce_probabilite
								 , CASE 
								   WHEN ST_CoveredBy(p.geom, n.the_geom) 
								   THEN ST_Multi(p.geom)
								   ELSE 
									ST_Multi(
									  ST_Intersection(p.geom,n.the_geom)
									  ) 
								   END AS geom 
								FROM '.$haz_tab_union.' AS p 
								INNER JOIN '.$workspace.'."'.$object_name.'" AS n 
								ON (ST_Intersects(p.geom, n.the_geom) 
								AND NOT ST_Touches(p.geom, n.the_geom)
								AND n."CODE_ECONO" <> 0)) t
								WHERE ST_Area(geom) > 0.1;';
							}
						}
						else {							
							// for buildings with the vulnerability options 'Class' and 'Category'
							$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS (
							SELECT the_geom as geom, "EGID" as egid, "CATEGORIE" as categorie, "CLASSE" as classe, "PRIX_TOT" as prix_tot, "NB_PERS" as nb_pers, "EXP_PERS" as exp_pers, objectid, cin_fksce, cin_lvinte, cin_lvin_1 as int_1, cin_lvin_2, cin_lvin_3 as int_2, cin_lvin_1, cin_scepro as cin_sce_probabilite								
							FROM '.$haz_name.', '.$workspace.'."'.$object_name.'"			
							WHERE ST_Intersects("'.$object_name.'".the_geom, '.$haz_name.".geom)							
							ORDER BY egid
							);";						
						}						
					}
					else {
						// for other types of hazards
						if ($object_type == 'Routes' || $object_type == 'Surfaces'){
							// for routes and surfaces with the vulnerability option 'Code-EconoMe'
							if ($count == 1) { // do the UNION if overlapping polygons
								$haz_tab_union = $mapping_index.'_union_view';
								$query .= 'CREATE TEMP TABLE '.$haz_tab_union.' AS
								SELECT a.gid, a.cin_fksce, a.cin_lvinte, a.cin_lvin_1, a. cin_scepro, new_polys.the_geom as geom 
								FROM (SELECT geom AS the_geom, ST_PointOnSurface(geom) AS pip 
								FROM ST_Dump((
									SELECT ST_Polygonize(the_geom) AS the_geom FROM (
										SELECT ST_Node(ST_Collect(the_geom)) AS the_geom FROM (
											SELECT ST_ExteriorRing(ST_GeometryN(geom,1)) AS the_geom
								FROM '.$haz_name.') AS lines
									) AS noded_lines
								)) 
								) AS new_polys
								LEFT JOIN '.$haz_name.' a ON St_Within(new_polys.pip,a.geom);';
							}							
							
							if ($object_type == 'Routes'){
								// PAY ATTENTION to the ST_Length function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								SELECT t2.fid, t2."OBJECTID" as objectid, t2."CODE_ECONO" as code_econo, t2."OBJEKTART" as categorie, t2."KUNSTBAUTE" as domain, ST_Length(t2.the_geom) AS longueur_tot, 0.0 As longueur, "PRIX_M" as prix_unite, "TJM" as tjm, "VITESSE" as vitesse, "L_VEHICULE" as l_m, "NB_PERS" as humains, "NB_PASSAGE" as passage, gid, cin_fksce, cin_lvinte, cin_lvin_1, cin_scepro as cin_sce_probabilite
								, CASE 
								   WHEN ST_CoveredBy(t2.the_geom, t1.geom) 
								   THEN t2.the_geom
								   ELSE 
									ST_Multi(
									  ST_Intersection(t2.the_geom, t1.geom)
									  ) 
								   END AS geom
								FROM '.$haz_tab_union.' t1, '.$workspace.'."'.$object_name.'" t2
								WHERE ST_Intersects(t1.geom, t2.the_geom)
								AND t2."CODE_ECONO" <> 0;';
								
								$query .= 'UPDATE '.$workspace.'.'.$mapping_index.' SET longueur = ST_length(geom);';
							}
							else {
								// PAY ATTENTION to the ST_Area function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								SELECT *, ST_Area(geom) AS area
								FROM 
								(SELECT n.fid, n."OBJECTID" as objectid, n."CODE_ECONO" as code_econo, n."TYPE_PRINC" as categorie, "PRIX" as prix_unite, "NB_PERS" as humains, "EXP_PERS" as exp_pers, gid, cin_fksce, cin_lvinte, cin_lvin_1, cin_scepro as cin_sce_probabilite
								 , CASE 
								   WHEN ST_CoveredBy(p.geom, n.the_geom) 
								   THEN ST_Multi(p.geom)
								   ELSE 
									ST_Multi(
									  ST_Intersection(p.geom,n.the_geom)
									  ) 
								   END AS geom 
								FROM '.$haz_tab_union.' AS p 
								INNER JOIN '.$workspace.'."'.$object_name.'" AS n 
								ON (ST_Intersects(p.geom, n.the_geom) 
								AND NOT ST_Touches(p.geom, n.the_geom)
								AND n."CODE_ECONO" <> 0)) t
								WHERE ST_Area(geom) > 0.1;';
							}
						}
						else {
							// for buildings with the vulnerability options 'Class' and 'Category'
							$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS (
							SELECT the_geom as geom, "EGID" as egid, "CATEGORIE" as categorie, "CLASSE" as classe, "PRIX_TOT" as prix_tot, "NB_PERS" as nb_pers, "EXP_PERS" as exp_pers, objectid, cin_fksce, cin_lvinte, cin_lvin_1, cin_scepro as cin_sce_probabilite								
							FROM '.$haz_name.', '.$workspace.'."'.$object_name.'"			
							WHERE ST_Intersects("'.$object_name.'".the_geom, '.$haz_name.".geom)							
							ORDER BY egid
							);";
						}						
					}					
				}
				else{
					// for the vulnerability option 'Manual'
					if ($haz_type == 'GPP-TR'){
						if ($object_type == 'Routes' || $object_type == 'Surfaces'){
							if ($count == 1) { // do the UNION if overlapping polygons
								$haz_tab_union = $mapping_index.'_union_view';
								$query .= 'CREATE TEMP TABLE '.$haz_tab_union.' AS
								SELECT a.rownum, a.gid, a.globalid, a.idobjet, a.alea, a.cin_lvin_1, a.cin_scepro, new_polys.the_geom as geom 
								FROM (SELECT geom AS the_geom, ST_PointOnSurface(geom) AS pip 
								FROM ST_Dump((
									SELECT ST_Polygonize(the_geom) AS the_geom FROM (
										SELECT ST_Node(ST_Collect(the_geom)) AS the_geom FROM (
											SELECT ST_ExteriorRing(ST_GeometryN(geom,1)) AS the_geom
								FROM '.$haz_name.') AS lines
									) AS noded_lines
								)) 
								) AS new_polys
								LEFT JOIN '.$haz_name.' a ON St_Within(new_polys.pip,a.geom);';
							}

							if ($object_type == 'Routes'){
								// PAY ATTENTION to the ST_Length function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								SELECT t2.fid, t2."OBJECTID" as objectid, t2."CODE_ECONO" as code_econo, t2."OBJEKTART" as categorie, t2."KUNSTBAUTE" as domain, ST_Length(t2.the_geom) AS longueur_tot, 0.0 As longueur, "PRIX_M" as prix_unite, "TJM" as tjm, "VITESSE" as vitesse, "L_VEHICULE" as l_m, "NB_PERS" as humains, "NB_PASSAGE" as passage, "VUL_OBJ_F" as vul_obj_f, "VUL_OBJ_M" as vul_obj_m, "VUL_OBJ_H" as vul_obj_h, "LET_PERS_F" as let_pers_f, "LET_PERS_M" as let_pers_m, "LET_PERS_H" as let_pers_h, rownum, gid, globalid, idobjet, alea, cin_lvin_1, cin_scepro as cin_sce_probabilite
								, CASE 
								   WHEN ST_CoveredBy(t2.the_geom, t1.geom) 
								   THEN t2.the_geom
								   ELSE 
									ST_Multi(
									  ST_Intersection(t2.the_geom, t1.geom)
									  ) 
								   END AS geom
								FROM '.$haz_tab_union.' t1, '.$workspace.'."'.$object_name.'" t2
								WHERE ST_Intersects(t1.geom, t2.the_geom)
								AND t2."CODE_ECONO" <> 0;';
								
								$query .= 'UPDATE '.$workspace.'.'.$mapping_index.' SET longueur = ST_length(geom);';
							}
							else {
								// PAY ATTENTION to the ST_Area function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								SELECT *, ST_Area(geom) AS area
								FROM 
								(SELECT n.fid, n."OBJECTID" as objectid, n."CODE_ECONO" as code_econo, n."TYPE_PRINC" as categorie, "PRIX" as prix_unite, "NB_PERS" as humains, "EXP_PERS" as exp_pers, "VUL_OBJ_F" as vul_obj_f, "VUL_OBJ_M" as vul_obj_m, "VUL_OBJ_H" as vul_obj_h, "LET_PERS_F" as let_pers_f, "LET_PERS_M" as let_pers_m, "LET_PERS_H" as let_pers_h, rownum, gid, globalid, idobjet, alea, cin_lvin_1, cin_scepro as cin_sce_probabilite
								 , CASE 
								   WHEN ST_CoveredBy(p.geom, n.the_geom) 
								   THEN ST_Multi(p.geom)
								   ELSE 
									ST_Multi(
									  ST_Intersection(p.geom,n.the_geom)
									  ) 
								   END AS geom 
								FROM '.$haz_tab_union.' AS p 
								INNER JOIN '.$workspace.'."'.$object_name.'" AS n 
								ON (ST_Intersects(p.geom, n.the_geom) 
								AND NOT ST_Touches(p.geom, n.the_geom)
								AND n."CODE_ECONO" <> 0)) t
								WHERE ST_Area(geom) > 0.1;';
							}							
						}
						else {
							// for buildings 
							$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS (
							SELECT the_geom as geom, "EGID" as egid, "CATEGORIE" as categorie, "CLASSE" as classe, "PRIX_TOT" as prix_tot, "NB_PERS" as nb_pers, "EXP_PERS" as exp_pers, "VUL_OBJ_F" as vul_obj_f, "VUL_OBJ_M" as vul_obj_m, "VUL_OBJ_H" as vul_obj_h, "LET_PERS_F" as let_pers_f, "LET_PERS_M" as let_pers_m, "LET_PERS_H" as let_pers_h, rownum, gid, globalid, idobjet, alea, cin_lvin_1, cin_scepro as cin_sce_probabilite
							FROM '.$haz_name.', '.$workspace.'."'.$object_name.'"			
							WHERE ST_Intersects("'.$object_name.'".the_geom, '.$haz_name.".geom)							
							ORDER BY egid, cin_lvin_1
							);";
						}						
					}
					elseif ($haz_type == 'LTO'){
						if ($object_type == 'Routes' || $object_type == 'Surfaces'){							
							if ($count == 1) { // do the UNION if overlapping polygons
								$haz_tab_union = $mapping_index.'_union_view';
								$query .= 'CREATE TEMP TABLE '.$haz_tab_union.' AS
								SELECT a.gid, a.cin_fksce, a.cin_lvinte, a.cin_lvin_1, a.cin_lvin_2, a.cin_lvin_3, a.cin_scepro, new_polys.the_geom as geom 
								FROM (SELECT geom AS the_geom, ST_PointOnSurface(geom) AS pip 
								FROM ST_Dump((
									SELECT ST_Polygonize(the_geom) AS the_geom FROM (
										SELECT ST_Node(ST_Collect(the_geom)) AS the_geom FROM (
											SELECT ST_ExteriorRing(ST_GeometryN(geom,1)) AS the_geom
								FROM '.$haz_name.') AS lines
									) AS noded_lines
								)) 
								) AS new_polys
								LEFT JOIN '.$haz_name.' a ON St_Within(new_polys.pip,a.geom);';
							}							
							
							if ($object_type == 'Routes'){
								// PAY ATTENTION to the ST_Length function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								SELECT t2.fid, t2."OBJECTID" as objectid, t2."CODE_ECONO" as code_econo, t2."OBJEKTART" as categorie, t2."KUNSTBAUTE" as domain, ST_Length(t2.the_geom) AS longueur_tot, 0.0 As longueur, "PRIX_M" as prix_unite, "TJM" as tjm, "VITESSE" as vitesse, "L_VEHICULE" as l_m, "NB_PERS" as humains, "NB_PASSAGE" as passage, "VUL_OBJ_F" as vul_obj_f, "VUL_OBJ_M" as vul_obj_m, "VUL_OBJ_H" as vul_obj_h, "LET_PERS_F" as let_pers_f, "LET_PERS_M" as let_pers_m, "LET_PERS_H" as let_pers_h, gid, cin_fksce, cin_lvinte, cin_lvin_1 as int_1, cin_lvin_2, cin_lvin_3 as int_2, cin_lvin_1, cin_scepro as cin_sce_probabilite
								, CASE 
								   WHEN ST_CoveredBy(t2.the_geom, t1.geom) 
								   THEN t2.the_geom
								   ELSE 
									ST_Multi(
									  ST_Intersection(t2.the_geom, t1.geom)
									  ) 
								   END AS geom
								FROM '.$haz_tab_union.' t1, '.$workspace.'."'.$object_name.'" t2
								WHERE ST_Intersects(t1.geom, t2.the_geom)
								AND t2."CODE_ECONO" <> 0;';	

								$query .= 'UPDATE '.$workspace.'.'.$mapping_index.' SET longueur = ST_length(geom);';
							}
							else {
								// PAY ATTENTION to the ST_Area function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								SELECT *, ST_Area(geom) AS area
								FROM 
								(SELECT n.fid, n."OBJECTID" as objectid, n."CODE_ECONO" as code_econo, n."TYPE_PRINC" as categorie, "PRIX" as prix_unite, "NB_PERS" as humains, "EXP_PERS" as exp_pers, "VUL_OBJ_F" as vul_obj_f, "VUL_OBJ_M" as vul_obj_m, "VUL_OBJ_H" as vul_obj_h, "LET_PERS_F" as let_pers_f, "LET_PERS_M" as let_pers_m, "LET_PERS_H" as let_pers_h, gid, cin_fksce, cin_lvinte, cin_lvin_1 as int_1, cin_lvin_2, cin_lvin_3 as int_2, cin_lvin_1, cin_scepro as cin_sce_probabilite								
								 , CASE 
								   WHEN ST_CoveredBy(p.geom, n.the_geom) 
								   THEN ST_Multi(p.geom)
								   ELSE 
									ST_Multi(
									  ST_Intersection(p.geom,n.the_geom)
									  ) 
								   END AS geom 
								FROM '.$haz_tab_union.' AS p 
								INNER JOIN '.$workspace.'."'.$object_name.'" AS n 
								ON (ST_Intersects(p.geom, n.the_geom) 
								AND NOT ST_Touches(p.geom, n.the_geom)
								AND n."CODE_ECONO" <> 0)) t
								WHERE ST_Area(geom) > 0.1;';
							}
						}
						else {							
							// for buildings 
							$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS (
							SELECT the_geom as geom, "EGID" as egid, "CATEGORIE" as categorie, "CLASSE" as classe, "PRIX_TOT" as prix_tot, "NB_PERS" as nb_pers, "EXP_PERS" as exp_pers, "VUL_OBJ_F" as vul_obj_f, "VUL_OBJ_M" as vul_obj_m, "VUL_OBJ_H" as vul_obj_h, "LET_PERS_F" as let_pers_f, "LET_PERS_M" as let_pers_m, "LET_PERS_H" as let_pers_h, objectid, cin_fksce, cin_lvinte, cin_lvin_1 as int_1, cin_lvin_2, cin_lvin_3 as int_2, cin_lvin_1, cin_scepro as cin_sce_probabilite								
							FROM '.$haz_name.', '.$workspace.'."'.$object_name.'"			
							WHERE ST_Intersects("'.$object_name.'".the_geom, '.$haz_name.".geom)							
							ORDER BY egid
							);";					
						}						
					}
					else {
						// for other types of hazards
						if ($object_type == 'Routes' || $object_type == 'Surfaces'){							
							if ($count == 1) { // do the UNION if overlapping polygons
								$haz_tab_union = $mapping_index.'_union_view';
								$query .= 'CREATE TEMP TABLE '.$haz_tab_union.' AS
								SELECT a.gid, a.cin_fksce, a.cin_lvinte, a.cin_lvin_1, a. cin_scepro, new_polys.the_geom as geom 
								FROM (SELECT geom AS the_geom, ST_PointOnSurface(geom) AS pip 
								FROM ST_Dump((
									SELECT ST_Polygonize(the_geom) AS the_geom FROM (
										SELECT ST_Node(ST_Collect(the_geom)) AS the_geom FROM (
											SELECT ST_ExteriorRing(ST_GeometryN(geom,1)) AS the_geom
								FROM '.$haz_name.') AS lines
									) AS noded_lines
								)) 
								) AS new_polys
								LEFT JOIN '.$haz_name.' a ON St_Within(new_polys.pip,a.geom);';
							}							
							
							if ($object_type == 'Routes'){
								// PAY ATTENTION to the ST_Length function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								SELECT t2.fid, t2."OBJECTID" as objectid, t2."CODE_ECONO" as code_econo, t2."OBJEKTART" as categorie, t2."KUNSTBAUTE" as domain, ST_Length(t2.the_geom) AS longueur_tot, 0.0 As longueur, "PRIX_M" as prix_unite, "TJM" as tjm, "VITESSE" as vitesse, "L_VEHICULE" as l_m, "NB_PERS" as humains, "NB_PASSAGE" as passage, "VUL_OBJ_F" as vul_obj_f, "VUL_OBJ_M" as vul_obj_m, "VUL_OBJ_H" as vul_obj_h, "LET_PERS_F" as let_pers_f, "LET_PERS_M" as let_pers_m, "LET_PERS_H" as let_pers_h, gid, cin_fksce, cin_lvinte, cin_lvin_1, cin_scepro as cin_sce_probabilite
								, CASE 
								   WHEN ST_CoveredBy(t2.the_geom, t1.geom) 
								   THEN t2.the_geom
								   ELSE 
									ST_Multi(
									  ST_Intersection(t2.the_geom, t1.geom)
									  ) 
								   END AS geom
								FROM '.$haz_tab_union.' t1, '.$workspace.'."'.$object_name.'" t2
								WHERE ST_Intersects(t1.geom, t2.the_geom)
								AND t2."CODE_ECONO" <> 0;';
								
								$query .= 'UPDATE '.$workspace.'.'.$mapping_index.' SET longueur = ST_length(geom);';
							}
							else {
								// PAY ATTENTION to the ST_Area function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								SELECT *, ST_Area(geom) AS area
								FROM 
								(SELECT n.fid, n."OBJECTID" as objectid, n."CODE_ECONO" as code_econo, n."TYPE_PRINC" as categorie, "PRIX" as prix_unite, "NB_PERS" as humains, "EXP_PERS" as exp_pers, "VUL_OBJ_F" as vul_obj_f, "VUL_OBJ_M" as vul_obj_m, "VUL_OBJ_H" as vul_obj_h, "LET_PERS_F" as let_pers_f, "LET_PERS_M" as let_pers_m, "LET_PERS_H" as let_pers_h, gid, cin_fksce, cin_lvinte, cin_lvin_1, cin_scepro as cin_sce_probabilite
								 , CASE 
								   WHEN ST_CoveredBy(p.geom, n.the_geom) 
								   THEN ST_Multi(p.geom)
								   ELSE 
									ST_Multi(
									  ST_Intersection(p.geom,n.the_geom)
									  ) 
								   END AS geom 
								FROM '.$haz_tab_union.' AS p 
								INNER JOIN '.$workspace.'."'.$object_name.'" AS n 
								ON (ST_Intersects(p.geom, n.the_geom) 
								AND NOT ST_Touches(p.geom, n.the_geom)
								AND n."CODE_ECONO" <> 0)) t
								WHERE ST_Area(geom) > 0.1;';
							}
						}
						else {
							// for buildings
							$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS (
							SELECT the_geom as geom, "EGID" as egid, "CATEGORIE" as categorie, "CLASSE" as classe, "PRIX_TOT" as prix_tot, "NB_PERS" as nb_pers, "EXP_PERS" as exp_pers, "VUL_OBJ_F" as vul_obj_f, "VUL_OBJ_M" as vul_obj_m, "VUL_OBJ_H" as vul_obj_h, "LET_PERS_F" as let_pers_f, "LET_PERS_M" as let_pers_m, "LET_PERS_H" as let_pers_h, objectid, cin_fksce, cin_lvinte, cin_lvin_1, cin_scepro as cin_sce_probabilite								
							FROM '.$haz_name.', '.$workspace.'."'.$object_name.'"			
							WHERE ST_Intersects("'.$object_name.'".the_geom, '.$haz_name.".geom)							
							ORDER BY egid
							);";
						}						
					}					
				}				
			}				
		}
		else { // if option is Zone-based, take object layer within the defined zone for calculation			
			$polyGeomStr = "ST_Transform(ST_GeomFromText('$area',900913),2056)";
			
			if ($object_level == 'Class' || $object_level == 'Category' || $object_level == 'Code-EconoMe') {
				if ($vul_level == 'Class' || $vul_level == 'Category' || $vul_level == 'Code-EconoMe'){
					if ($haz_type == 'GPP-TR'){
						if ($object_type == 'Routes' || $object_type == 'Surfaces'){
							// for routes and surfaces with the vulnerability option 'Code-EconoMe'
							if ($count == 1) { // do the UNION if overlapping polygons
								$haz_tab_union = $mapping_index.'_union_view';
								$query .= 'CREATE TEMP TABLE '.$haz_tab_union.' AS 
								WITH clip AS (SELECT rownum, gid, globalid, idobjet, alea, cin_lvin_1, cin_scepro, (ST_Intersection(geom,'.$polyGeomStr.')) As geom 
								FROM '.$haz_name.'
								WHERE ST_Intersects(geom,'.$polyGeomStr.'))								
								SELECT a.rownum, a.gid, a.globalid, a.idobjet, a.alea, a.cin_lvin_1, a.cin_scepro, new_polys.the_geom as geom 
								FROM (SELECT geom AS the_geom, ST_PointOnSurface(geom) AS pip 
								FROM ST_Dump((
									SELECT ST_Polygonize(the_geom) AS the_geom FROM (
										SELECT ST_Node(ST_Collect(the_geom)) AS the_geom FROM (
											SELECT ST_ExteriorRing(ST_GeometryN(geom,1)) AS the_geom
								FROM clip) AS lines
									) AS noded_lines
								)) 
								) AS new_polys
								LEFT JOIN '.$haz_name.' a ON St_Within(new_polys.pip,a.geom);';
							}

							if ($object_type == 'Routes'){
								// PAY ATTENTION to the ST_Length function -- no need to transform if the geometry is already in the right coordinate system													
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								WITH obj AS (SELECT fid, "OBJECTID", "CODE_ECONO", "OBJEKTART", "KUNSTBAUTE", (ST_Intersection(the_geom,'.$polyGeomStr.')) As the_geom
								FROM '.$workspace.'."'.$object_name.'"
								WHERE ST_Intersects(the_geom,'.$polyGeomStr.')
								AND "CODE_ECONO" <> 0)
								SELECT t2.fid, t2."OBJECTID" as objectid, t2."CODE_ECONO" as code_econo, t2."OBJEKTART" as categorie, t2."KUNSTBAUTE" as domain, ST_Length(t2.the_geom) AS longueur_tot, 0.0 As longueur, rownum, gid, globalid, idobjet, alea, cin_lvin_1, cin_scepro as cin_sce_probabilite
								, CASE 
								   WHEN ST_CoveredBy(t2.the_geom, t1.geom) 
								   THEN t2.the_geom
								   ELSE 
									ST_Multi(
									  ST_Intersection(t2.the_geom, t1.geom)
									  ) 
								   END AS geom
								FROM '.$haz_tab_union.' t1, obj t2
								WHERE ST_Intersects(t1.geom, t2.the_geom);';
								
								$query .= 'UPDATE '.$workspace.'.'.$mapping_index.' SET longueur = ST_length(geom);';
							}
							else {
								// PAY ATTENTION to the ST_Area function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								WITH obj AS (SELECT fid, "OBJECTID", "CODE_ECONO", "TYPE_PRINC", (ST_Intersection(the_geom,'.$polyGeomStr.')) As the_geom
								FROM '.$workspace.'."'.$object_name.'"
								WHERE ST_Intersects(the_geom,'.$polyGeomStr.'))
								SELECT *, ST_Area(geom) AS area
								FROM 
								(SELECT n.fid, n."OBJECTID" as objectid, n."CODE_ECONO" as code_econo, n."TYPE_PRINC" as categorie, rownum, gid, globalid, idobjet, alea, cin_lvin_1, cin_scepro as cin_sce_probabilite
								 , CASE 
								   WHEN ST_CoveredBy(p.geom, n.the_geom) 
								   THEN ST_Multi(p.geom)
								   ELSE 
									ST_Multi(
									  ST_Intersection(p.geom,n.the_geom)
									  ) 
								   END AS geom 
								FROM '.$haz_tab_union.' AS p 
								INNER JOIN obj AS n 
								ON (ST_Intersects(p.geom, n.the_geom) 
								AND NOT ST_Touches(p.geom, n.the_geom)
								AND n."CODE_ECONO" <> 0)) t
								WHERE ST_Area(geom) > 0.1;';
							}							
						}
						else {
							// for buildings with the vulnerability options 'Class' and 'Category'
							$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS (
							WITH obj AS (SELECT *
							FROM '.$workspace.'."'.$object_name.'"
							WHERE ST_Intersects(the_geom,'.$polyGeomStr.'))
							SELECT obj.the_geom as geom, "EGID" as egid, "CATEGORIE" as categorie, "CLASSE" as classe, "NB_LOG" as nbre_log, "VOLUME" as volume, "SURF_TOT" as surface, rownum, gid, globalid, idobjet, alea, cin_lvin_1, cin_scepro as cin_sce_probabilite
							FROM '.$haz_name.', obj			
							WHERE ST_Intersects(obj.the_geom, '.$haz_name.".geom)							
							ORDER BY egid, cin_lvin_1
							);";
						}						
					}
					elseif ($haz_type == 'LTO'){
						if ($object_type == 'Routes' || $object_type == 'Surfaces'){
							// for routes and surfaces with the vulnerability option 'Code-EconoMe'
							if ($count == 1) { // do the UNION if overlapping polygons
								$haz_tab_union = $mapping_index.'_union_view';
								$query .= 'CREATE TEMP TABLE '.$haz_tab_union.' AS 
								WITH clip AS (SELECT gid, cin_fksce, cin_lvinte, cin_lvin_1, cin_lvin_2, cin_lvin_3, cin_scepro, (ST_Intersection(geom,'.$polyGeomStr.')) As geom 
								FROM '.$haz_name.'
								WHERE ST_Intersects(geom,'.$polyGeomStr.'))								
								SELECT a.gid, a.cin_fksce, a.cin_lvinte, a.cin_lvin_1, a.cin_lvin_2, a.cin_lvin_3, a.cin_scepro, new_polys.the_geom as geom 
								FROM (SELECT geom AS the_geom, ST_PointOnSurface(geom) AS pip 
								FROM ST_Dump((
									SELECT ST_Polygonize(the_geom) AS the_geom FROM (
										SELECT ST_Node(ST_Collect(the_geom)) AS the_geom FROM (
											SELECT ST_ExteriorRing(ST_GeometryN(geom,1)) AS the_geom
								FROM clip) AS lines
									) AS noded_lines
								)) 
								) AS new_polys
								LEFT JOIN '.$haz_name.' a ON St_Within(new_polys.pip,a.geom);';
							}							
							
							if ($object_type == 'Routes'){
								// PAY ATTENTION to the ST_Length function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								WITH obj AS (SELECT fid, "OBJECTID", "CODE_ECONO", "OBJEKTART", "KUNSTBAUTE", (ST_Intersection(the_geom,'.$polyGeomStr.')) As the_geom
								FROM '.$workspace.'."'.$object_name.'"
								WHERE ST_Intersects(the_geom,'.$polyGeomStr.')
								AND "CODE_ECONO" <> 0)
								SELECT t2.fid, t2."OBJECTID" as objectid, t2."CODE_ECONO" as code_econo, t2."OBJEKTART" as categorie, t2."KUNSTBAUTE" as domain, ST_Length(t2.the_geom) AS longueur_tot, 0.0 As longueur, gid, cin_fksce, cin_lvinte, cin_lvin_1 as int_1, cin_lvin_2, cin_lvin_3 as int_2, cin_lvin_1, cin_scepro as cin_sce_probabilite
								, CASE 
								   WHEN ST_CoveredBy(t2.the_geom, t1.geom) 
								   THEN t2.the_geom
								   ELSE 
									ST_Multi(
									  ST_Intersection(t2.the_geom, t1.geom)
									  ) 
								   END AS geom
								FROM '.$haz_tab_union.' t1, obj t2
								WHERE ST_Intersects(t1.geom, t2.the_geom);';

								$query .= 'UPDATE '.$workspace.'.'.$mapping_index.' SET longueur = ST_length(geom);';
							}
							else {
								// PAY ATTENTION to the ST_Area function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								WITH obj AS (SELECT fid, "OBJECTID", "CODE_ECONO", "TYPE_PRINC", (ST_Intersection(the_geom,'.$polyGeomStr.')) As the_geom
								FROM '.$workspace.'."'.$object_name.'"
								WHERE ST_Intersects(the_geom,'.$polyGeomStr.'))
								SELECT *, ST_Area(geom) AS area
								FROM 
								(SELECT n.fid, n."OBJECTID" as objectid, n."CODE_ECONO" as code_econo, n."TYPE_PRINC" as categorie, gid, cin_fksce, cin_lvinte, cin_lvin_1 as int_1, cin_lvin_2, cin_lvin_3 as int_2, cin_lvin_1, cin_scepro as cin_sce_probabilite
								 , CASE 
								   WHEN ST_CoveredBy(p.geom, n.the_geom) 
								   THEN ST_Multi(p.geom)
								   ELSE 
									ST_Multi(
									  ST_Intersection(p.geom,n.the_geom)
									  ) 
								   END AS geom 
								FROM '.$haz_tab_union.' AS p 
								INNER JOIN obj AS n 
								ON (ST_Intersects(p.geom, n.the_geom) 
								AND NOT ST_Touches(p.geom, n.the_geom)
								AND n."CODE_ECONO" <> 0)) t
								WHERE ST_Area(geom) > 0.1;';
							}
						}
						else {							
							// for buildings with the vulnerability options 'Class' and 'Category'
							$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS (
							WITH obj AS (SELECT *
							FROM '.$workspace.'."'.$object_name.'"
							WHERE ST_Intersects(the_geom,'.$polyGeomStr.'))
							SELECT obj.the_geom as geom, "EGID" as egid, "CATEGORIE" as categorie, "CLASSE" as classe, "NB_LOG" as nbre_log, "VOLUME" as volume, "SURF_TOT" as surface, objectid, cin_fksce, cin_lvinte, cin_lvin_1 as int_1, cin_lvin_2, cin_lvin_3 as int_2, cin_lvin_1, cin_scepro as cin_sce_probabilite								
							FROM '.$haz_name.', obj			
							WHERE ST_Intersects(obj.the_geom, '.$haz_name.".geom)							
							ORDER BY egid
							);";					
						}						
					}
					else {
						// for other types of hazards
						if ($object_type == 'Routes' || $object_type == 'Surfaces'){
							// for routes and surfaces with the vulnerability option 'Code-EconoMe'
							if ($count == 1) { // do the UNION if overlapping polygons
								$haz_tab_union = $mapping_index.'_union_view';	
								$query .= 'CREATE TEMP TABLE '.$haz_tab_union.' AS 
								WITH clip AS (SELECT gid, cin_fksce, cin_lvinte, cin_lvin_1, cin_scepro, (ST_Intersection(geom,'.$polyGeomStr.')) As geom 
								FROM '.$haz_name.'
								WHERE ST_Intersects(geom,'.$polyGeomStr.'))								
								SELECT a.gid, a.cin_fksce, a.cin_lvinte, a.cin_lvin_1, a. cin_scepro, new_polys.the_geom as geom 
								FROM (SELECT geom AS the_geom, ST_PointOnSurface(geom) AS pip 
								FROM ST_Dump((
									SELECT ST_Polygonize(the_geom) AS the_geom FROM (
										SELECT ST_Node(ST_Collect(the_geom)) AS the_geom FROM (
											SELECT ST_ExteriorRing(ST_GeometryN(geom,1)) AS the_geom
								FROM clip) AS lines
									) AS noded_lines
								)) 
								) AS new_polys
								LEFT JOIN '.$haz_name.' a ON St_Within(new_polys.pip,a.geom);';
							}		

							if ($object_type == 'Routes'){
								// PAY ATTENTION to the ST_Length function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								WITH obj AS (SELECT fid, "OBJECTID", "CODE_ECONO", "OBJEKTART", "KUNSTBAUTE", (ST_Intersection(the_geom,'.$polyGeomStr.')) As the_geom
								FROM '.$workspace.'."'.$object_name.'"
								WHERE ST_Intersects(the_geom,'.$polyGeomStr.')
								AND "CODE_ECONO" <> 0)
								SELECT t2.fid, t2."OBJECTID" as objectid, t2."CODE_ECONO" as code_econo, t2."OBJEKTART" as categorie, t2."KUNSTBAUTE" as domain, ST_Length(t2.the_geom) AS longueur_tot, 0.0 As longueur, gid, cin_fksce, cin_lvinte, cin_lvin_1, cin_scepro as cin_sce_probabilite
								, CASE 
								   WHEN ST_CoveredBy(t2.the_geom, t1.geom) 
								   THEN t2.the_geom
								   ELSE 
									ST_Multi(
									  ST_Intersection(t2.the_geom, t1.geom)
									  ) 
								   END AS geom
								FROM '.$haz_tab_union.' t1, obj t2
								WHERE ST_Intersects(t1.geom, t2.the_geom);';
								
								$query .= 'UPDATE '.$workspace.'.'.$mapping_index.' SET longueur = ST_length(geom);';
							}
							else {
								// PAY ATTENTION to the ST_Area function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								WITH obj AS (SELECT fid, "OBJECTID", "CODE_ECONO", "TYPE_PRINC", (ST_Intersection(the_geom,'.$polyGeomStr.')) As the_geom
								FROM '.$workspace.'."'.$object_name.'"
								WHERE ST_Intersects(the_geom,'.$polyGeomStr.'))
								SELECT *, ST_Area(geom) AS area
								FROM 
								(SELECT n.fid, n."OBJECTID" as objectid, n."CODE_ECONO" as code_econo, n."TYPE_PRINC" as categorie, gid, cin_fksce, cin_lvinte, cin_lvin_1, cin_scepro as cin_sce_probabilite
								 , CASE 
								   WHEN ST_CoveredBy(p.geom, n.the_geom) 
								   THEN ST_Multi(p.geom)
								   ELSE 
									ST_Multi(
									  ST_Intersection(p.geom,n.the_geom)
									  ) 
								   END AS geom 
								FROM '.$haz_tab_union.' AS p 
								INNER JOIN obj AS n 
								ON (ST_Intersects(p.geom, n.the_geom) 
								AND NOT ST_Touches(p.geom, n.the_geom)
								AND n."CODE_ECONO" <> 0)) t
								WHERE ST_Area(geom) > 0.1;';
							}							
						}
						else {
							// for buildings with the vulnerability options 'Class' and 'Category'
							$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS (
							WITH obj AS (SELECT *
							FROM '.$workspace.'."'.$object_name.'"
							WHERE ST_Intersects(the_geom,'.$polyGeomStr.'))
							SELECT obj.the_geom as geom, "EGID" as egid, "CATEGORIE" as categorie, "CLASSE" as classe, "NB_LOG" as nbre_log, "VOLUME" as volume, "SURF_TOT" as surface, objectid, cin_fksce, cin_lvinte, cin_lvin_1, cin_scepro as cin_sce_probabilite								
							FROM '.$haz_name.', obj			
							WHERE ST_Intersects(obj.the_geom, '.$haz_name.".geom)							
							ORDER BY egid
							);";
						}						
					}					
				}
				else {
					// for the vulnerability option 'Manual'
					if ($haz_type == 'GPP-TR'){
						if ($object_type == 'Routes' || $object_type == 'Surfaces'){							
							if ($count == 1) { // do the UNION if overlapping polygons
								$haz_tab_union = $mapping_index.'_union_view';
								$query .= 'CREATE TEMP TABLE '.$haz_tab_union.' AS 
								WITH clip AS (SELECT rownum, gid, globalid, idobjet, alea, cin_lvin_1, cin_scepro, (ST_Intersection(geom,'.$polyGeomStr.')) As geom 
								FROM '.$haz_name.'
								WHERE ST_Intersects(geom,'.$polyGeomStr.'))								
								SELECT a.rownum, a.gid, a.globalid, a.idobjet, a.alea, a.cin_lvin_1, a.cin_scepro, new_polys.the_geom as geom 
								FROM (SELECT geom AS the_geom, ST_PointOnSurface(geom) AS pip 
								FROM ST_Dump((
									SELECT ST_Polygonize(the_geom) AS the_geom FROM (
										SELECT ST_Node(ST_Collect(the_geom)) AS the_geom FROM (
											SELECT ST_ExteriorRing(ST_GeometryN(geom,1)) AS the_geom
								FROM clip) AS lines
									) AS noded_lines
								)) 
								) AS new_polys
								LEFT JOIN '.$haz_name.' a ON St_Within(new_polys.pip,a.geom);';
							}							
							
							if ($object_type == 'Routes'){
								// PAY ATTENTION to the ST_Length function -- no need to transform if the geometry is already in the right coordinate system													
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								WITH obj AS (SELECT fid, "OBJECTID", "CODE_ECONO", "OBJEKTART", "KUNSTBAUTE", "VUL_OBJ_F", "VUL_OBJ_M", "VUL_OBJ_H", "LET_PERS_F", "LET_PERS_M", "LET_PERS_H", (ST_Intersection(the_geom,'.$polyGeomStr.')) As the_geom
								FROM '.$workspace.'."'.$object_name.'"
								WHERE ST_Intersects(the_geom,'.$polyGeomStr.')
								AND "CODE_ECONO" <> 0)
								SELECT t2.fid, t2."OBJECTID" as objectid, t2."CODE_ECONO" as code_econo, t2."OBJEKTART" as categorie, t2."KUNSTBAUTE" as domain, ST_Length(t2.the_geom) AS longueur_tot, 0.0 As longueur, "VUL_OBJ_F" as vul_obj_f, "VUL_OBJ_M" as vul_obj_m, "VUL_OBJ_H" as vul_obj_h, "LET_PERS_F" as let_pers_f, "LET_PERS_M" as let_pers_m, "LET_PERS_H" as let_pers_h, rownum, gid, globalid, idobjet, alea, cin_lvin_1, cin_scepro as cin_sce_probabilite
								, CASE 
								   WHEN ST_CoveredBy(t2.the_geom, t1.geom) 
								   THEN t2.the_geom
								   ELSE 
									ST_Multi(
									  ST_Intersection(t2.the_geom, t1.geom)
									  ) 
								   END AS geom
								FROM '.$haz_tab_union.' t1, obj t2
								WHERE ST_Intersects(t1.geom, t2.the_geom);';
								
								$query .= 'UPDATE '.$workspace.'.'.$mapping_index.' SET longueur = ST_length(geom);';
							}
							else {
								// PAY ATTENTION to the ST_Area function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								WITH obj AS (SELECT fid, "OBJECTID", "CODE_ECONO", "TYPE_PRINC", "VUL_OBJ_F", "VUL_OBJ_M", "VUL_OBJ_H", "LET_PERS_F", "LET_PERS_M", "LET_PERS_H", (ST_Intersection(the_geom,'.$polyGeomStr.')) As the_geom
								FROM '.$workspace.'."'.$object_name.'"
								WHERE ST_Intersects(the_geom,'.$polyGeomStr.'))
								SELECT *, ST_Area(geom) AS area
								FROM 
								(SELECT n.fid, n."OBJECTID" as objectid, n."CODE_ECONO" as code_econo, n."TYPE_PRINC" as categorie, "VUL_OBJ_F" as vul_obj_f, "VUL_OBJ_M" as vul_obj_m, "VUL_OBJ_H" as vul_obj_h, "LET_PERS_F" as let_pers_f, "LET_PERS_M" as let_pers_m, "LET_PERS_H" as let_pers_h, rownum, gid, globalid, idobjet, alea, cin_lvin_1, cin_scepro as cin_sce_probabilite
								 , CASE 
								   WHEN ST_CoveredBy(p.geom, n.the_geom) 
								   THEN ST_Multi(p.geom)
								   ELSE 
									ST_Multi(
									  ST_Intersection(p.geom,n.the_geom)
									  ) 
								   END AS geom 
								FROM '.$haz_tab_union.' AS p 
								INNER JOIN obj AS n 
								ON (ST_Intersects(p.geom, n.the_geom) 
								AND NOT ST_Touches(p.geom, n.the_geom)
								AND n."CODE_ECONO" <> 0)) t
								WHERE ST_Area(geom) > 0.1;';
							}
						}
						else {
							// for buildings
							$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS (
							WITH obj AS (SELECT *
							FROM '.$workspace.'."'.$object_name.'"
							WHERE ST_Intersects(the_geom,'.$polyGeomStr.'))
							SELECT obj.the_geom as geom, "EGID" as egid, "CATEGORIE" as categorie, "CLASSE" as classe, "NB_LOG" as nbre_log, "VOLUME" as volume, "SURF_TOT" as surface, "VUL_OBJ_F" as vul_obj_f, "VUL_OBJ_M" as vul_obj_m, "VUL_OBJ_H" as vul_obj_h, "LET_PERS_F" as let_pers_f, "LET_PERS_M" as let_pers_m, "LET_PERS_H" as let_pers_h, rownum, gid, globalid, idobjet, alea, cin_lvin_1, cin_scepro as cin_sce_probabilite
							FROM '.$haz_name.', obj			
							WHERE ST_Intersects(obj.the_geom, '.$haz_name.".geom)							
							ORDER BY egid, cin_lvin_1
							);";
						}						
					}
					elseif ($haz_type == 'LTO'){
						if ($object_type == 'Routes' || $object_type == 'Surfaces'){
							// for routes 
							if ($count == 1) { // do the UNION if overlapping polygons
								$haz_tab_union = $mapping_index.'_union_view';
								$query .= 'CREATE TEMP TABLE '.$haz_tab_union.' AS 
								WITH clip AS (SELECT gid, cin_fksce, cin_lvinte, cin_lvin_1, cin_lvin_2, cin_lvin_3, cin_scepro, (ST_Intersection(geom,'.$polyGeomStr.')) As geom 
								FROM '.$haz_name.'
								WHERE ST_Intersects(geom,'.$polyGeomStr.'))								
								SELECT a.gid, a.cin_fksce, a.cin_lvinte, a.cin_lvin_1, a.cin_lvin_2, a.cin_lvin_3, a.cin_scepro, new_polys.the_geom as geom 
								FROM (SELECT geom AS the_geom, ST_PointOnSurface(geom) AS pip 
								FROM ST_Dump((
									SELECT ST_Polygonize(the_geom) AS the_geom FROM (
										SELECT ST_Node(ST_Collect(the_geom)) AS the_geom FROM (
											SELECT ST_ExteriorRing(ST_GeometryN(geom,1)) AS the_geom
								FROM clip) AS lines
									) AS noded_lines
								)) 
								) AS new_polys
								LEFT JOIN '.$haz_name.' a ON St_Within(new_polys.pip,a.geom);';
							}							
							
							if ($object_type == 'Routes'){
								// PAY ATTENTION to the ST_Length function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								WITH obj AS (SELECT fid, "OBJECTID", "CODE_ECONO", "OBJEKTART", "KUNSTBAUTE", "VUL_OBJ_F", "VUL_OBJ_M", "VUL_OBJ_H", "LET_PERS_F", "LET_PERS_M", "LET_PERS_H",(ST_Intersection(the_geom,'.$polyGeomStr.')) As the_geom
								FROM '.$workspace.'."'.$object_name.'"
								WHERE ST_Intersects(the_geom,'.$polyGeomStr.')
								AND "CODE_ECONO" <> 0)
								SELECT t2.fid, t2."OBJECTID" as objectid, t2."CODE_ECONO" as code_econo, t2."OBJEKTART" as categorie, t2."KUNSTBAUTE" as domain, ST_Length(t2.the_geom) AS longueur_tot, 0.0 As longueur, "VUL_OBJ_F" as vul_obj_f, "VUL_OBJ_M" as vul_obj_m, "VUL_OBJ_H" as vul_obj_h, "LET_PERS_F" as let_pers_f, "LET_PERS_M" as let_pers_m, "LET_PERS_H" as let_pers_h, gid, cin_fksce, cin_lvinte, cin_lvin_1 as int_1, cin_lvin_2, cin_lvin_3 as int_2, cin_lvin_1, cin_scepro as cin_sce_probabilite
								, CASE 
								   WHEN ST_CoveredBy(t2.the_geom, t1.geom) 
								   THEN t2.the_geom
								   ELSE 
									ST_Multi(
									  ST_Intersection(t2.the_geom, t1.geom)
									  ) 
								   END AS geom
								FROM '.$haz_tab_union.' t1, obj t2
								WHERE ST_Intersects(t1.geom, t2.the_geom);';
								
								$query .= 'UPDATE '.$workspace.'.'.$mapping_index.' SET longueur = ST_length(geom);';
							}
							else {
								// PAY ATTENTION to the ST_Area function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								WITH obj AS (SELECT fid, "OBJECTID", "CODE_ECONO", "TYPE_PRINC", "VUL_OBJ_F", "VUL_OBJ_M", "VUL_OBJ_H", "LET_PERS_F", "LET_PERS_M", "LET_PERS_H", (ST_Intersection(the_geom,'.$polyGeomStr.')) As the_geom
								FROM '.$workspace.'."'.$object_name.'"
								WHERE ST_Intersects(the_geom,'.$polyGeomStr.'))
								SELECT *, ST_Area(geom) AS area
								FROM 
								(SELECT n.fid, n."OBJECTID" as objectid, n."CODE_ECONO" as code_econo, n."TYPE_PRINC" as categorie, "VUL_OBJ_F" as vul_obj_f, "VUL_OBJ_M" as vul_obj_m, "VUL_OBJ_H" as vul_obj_h, "LET_PERS_F" as let_pers_f, "LET_PERS_M" as let_pers_m, "LET_PERS_H" as let_pers_h, gid, cin_fksce, cin_lvinte, cin_lvin_1 as int_1, cin_lvin_2, cin_lvin_3 as int_2, cin_lvin_1, cin_scepro as cin_sce_probabilite
								 , CASE 
								   WHEN ST_CoveredBy(p.geom, n.the_geom) 
								   THEN ST_Multi(p.geom)
								   ELSE 
									ST_Multi(
									  ST_Intersection(p.geom,n.the_geom)
									  ) 
								   END AS geom 
								FROM '.$haz_tab_union.' AS p 
								INNER JOIN obj AS n 
								ON (ST_Intersects(p.geom, n.the_geom) 
								AND NOT ST_Touches(p.geom, n.the_geom)
								AND n."CODE_ECONO" <> 0)) t
								WHERE ST_Area(geom) > 0.1;';
							}
						}
						else {							
							// for buildings 
							$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS (
							WITH obj AS (SELECT *
							FROM '.$workspace.'."'.$object_name.'"
							WHERE ST_Intersects(the_geom,'.$polyGeomStr.'))
							SELECT obj.the_geom as geom, "EGID" as egid, "CATEGORIE" as categorie, "CLASSE" as classe, "NB_LOG" as nbre_log, "VOLUME" as volume, "SURF_TOT" as surface, "VUL_OBJ_F" as vul_obj_f, "VUL_OBJ_M" as vul_obj_m, "VUL_OBJ_H" as vul_obj_h, "LET_PERS_F" as let_pers_f, "LET_PERS_M" as let_pers_m, "LET_PERS_H" as let_pers_h, objectid, cin_fksce, cin_lvinte, cin_lvin_1 as int_1, cin_lvin_2, cin_lvin_3 as int_2, cin_lvin_1, cin_scepro as cin_sce_probabilite								
							FROM '.$haz_name.', obj			
							WHERE ST_Intersects(obj.the_geom, '.$haz_name.".geom)							
							ORDER BY egid
							);";					
						}						
					}
					else{
						// for other types of hazards
						if ($object_type == 'Routes' || $object_type == 'Surfaces'){
							// for routes
							if ($count == 1) { // do the UNION if overlapping polygons
								$haz_tab_union = $mapping_index.'_union_view';
								$query .= 'CREATE TEMP TABLE '.$haz_tab_union.' AS 
								WITH clip AS (SELECT gid, cin_fksce, cin_lvinte, cin_lvin_1, cin_scepro, (ST_Intersection(geom,'.$polyGeomStr.')) As geom 
								FROM '.$haz_name.'
								WHERE ST_Intersects(geom,'.$polyGeomStr.'))								
								SELECT a.gid, a.cin_fksce, a.cin_lvinte, a.cin_lvin_1, a. cin_scepro, new_polys.the_geom as geom 
								FROM (SELECT geom AS the_geom, ST_PointOnSurface(geom) AS pip 
								FROM ST_Dump((
									SELECT ST_Polygonize(the_geom) AS the_geom FROM (
										SELECT ST_Node(ST_Collect(the_geom)) AS the_geom FROM (
											SELECT ST_ExteriorRing(ST_GeometryN(geom,1)) AS the_geom
								FROM clip) AS lines
									) AS noded_lines
								)) 
								) AS new_polys
								LEFT JOIN '.$haz_name.' a ON St_Within(new_polys.pip,a.geom);';
							}							
							
							if ($object_type == 'Routes'){
								// PAY ATTENTION to the ST_Length function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								WITH obj AS (SELECT fid, "OBJECTID", "CODE_ECONO", "OBJEKTART", "KUNSTBAUTE", "VUL_OBJ_F", "VUL_OBJ_M", "VUL_OBJ_H", "LET_PERS_F", "LET_PERS_M", "LET_PERS_H", (ST_Intersection(the_geom,'.$polyGeomStr.')) As the_geom
								FROM '.$workspace.'."'.$object_name.'"
								WHERE ST_Intersects(the_geom,'.$polyGeomStr.')
								AND "CODE_ECONO" <> 0)
								SELECT t2.fid, t2."OBJECTID" as objectid, t2."CODE_ECONO" as code_econo, t2."OBJEKTART" as categorie, t2."KUNSTBAUTE" as domain, ST_Length(t2.the_geom) AS longueur_tot, 0.0 As longueur, "VUL_OBJ_F" as vul_obj_f, "VUL_OBJ_M" as vul_obj_m, "VUL_OBJ_H" as vul_obj_h, "LET_PERS_F" as let_pers_f, "LET_PERS_M" as let_pers_m, "LET_PERS_H" as let_pers_h, gid, cin_fksce, cin_lvinte, cin_lvin_1, cin_scepro as cin_sce_probabilite
								, CASE 
								   WHEN ST_CoveredBy(t2.the_geom, t1.geom) 
								   THEN t2.the_geom
								   ELSE 
									ST_Multi(
									  ST_Intersection(t2.the_geom, t1.geom)
									  ) 
								   END AS geom
								FROM '.$haz_tab_union.' t1, obj t2
								WHERE ST_Intersects(t1.geom, t2.the_geom);';
								
								$query .= 'UPDATE '.$workspace.'.'.$mapping_index.' SET longueur = ST_length(geom);';
							}
							else {
								// PAY ATTENTION to the ST_Area function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								WITH obj AS (SELECT fid, "OBJECTID", "CODE_ECONO", "TYPE_PRINC", "VUL_OBJ_F", "VUL_OBJ_M", "VUL_OBJ_H", "LET_PERS_F", "LET_PERS_M", "LET_PERS_H", (ST_Intersection(the_geom,'.$polyGeomStr.')) As the_geom
								FROM '.$workspace.'."'.$object_name.'"
								WHERE ST_Intersects(the_geom,'.$polyGeomStr.'))
								SELECT *, ST_Area(geom) AS area
								FROM 
								(SELECT n.fid, n."OBJECTID" as objectid, n."CODE_ECONO" as code_econo, n."TYPE_PRINC" as categorie, "VUL_OBJ_F" as vul_obj_f, "VUL_OBJ_M" as vul_obj_m, "VUL_OBJ_H" as vul_obj_h, "LET_PERS_F" as let_pers_f, "LET_PERS_M" as let_pers_m, "LET_PERS_H" as let_pers_h, gid, cin_fksce, cin_lvinte, cin_lvin_1, cin_scepro as cin_sce_probabilite
								 , CASE 
								   WHEN ST_CoveredBy(p.geom, n.the_geom) 
								   THEN ST_Multi(p.geom)
								   ELSE 
									ST_Multi(
									  ST_Intersection(p.geom,n.the_geom)
									  ) 
								   END AS geom 
								FROM '.$haz_tab_union.' AS p 
								INNER JOIN obj AS n 
								ON (ST_Intersects(p.geom, n.the_geom) 
								AND NOT ST_Touches(p.geom, n.the_geom)
								AND n."CODE_ECONO" <> 0)) t
								WHERE ST_Area(geom) > 0.1;';
							}
						}
						else {
							// for buildings
							$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS (
							WITH obj AS (SELECT *
							FROM '.$workspace.'."'.$object_name.'"
							WHERE ST_Intersects(the_geom,'.$polyGeomStr.'))
							SELECT obj.the_geom as geom, "EGID" as egid, "CATEGORIE" as categorie, "CLASSE" as classe, "NB_LOG" as nbre_log, "VOLUME" as volume, "SURF_TOT" as surface, "VUL_OBJ_F" as vul_obj_f, "VUL_OBJ_M" as vul_obj_m, "VUL_OBJ_H" as vul_obj_h, "LET_PERS_F" as let_pers_f, "LET_PERS_M" as let_pers_m, "LET_PERS_H" as let_pers_h, objectid, cin_fksce, cin_lvinte, cin_lvin_1, cin_scepro as cin_sce_probabilite								
							FROM '.$haz_name.', obj			
							WHERE ST_Intersects(obj.the_geom, '.$haz_name.".geom)							
							ORDER BY egid
							);";
						}						
					}										
				}
			}
			else {
				// for the object option 'Manual'
				if ($vul_level == 'Class' || $vul_level == 'Category' || $vul_level == 'Code-EconoMe'){
					if ($haz_type == 'GPP-TR'){
						if ($object_type == 'Routes' || $object_type == 'Surfaces'){
							// for routes with the vulnerability option 'Code-EconoMe'
							if ($count == 1) { // do the UNION if overlapping polygons
								$haz_tab_union = $mapping_index.'_union_view';
								$query .= 'CREATE TEMP TABLE '.$haz_tab_union.' AS 
									WITH clip AS (SELECT rownum, gid, globalid, idobjet, alea, cin_lvin_1, cin_scepro, (ST_Intersection(geom,'.$polyGeomStr.')) As geom 
									FROM '.$haz_name.'
									WHERE ST_Intersects(geom,'.$polyGeomStr.'))								
									SELECT a.rownum, a.gid, a.globalid, a.idobjet, a.alea, a.cin_lvin_1, a.cin_scepro, new_polys.the_geom as geom 
									FROM (SELECT geom AS the_geom, ST_PointOnSurface(geom) AS pip 
									FROM ST_Dump((
										SELECT ST_Polygonize(the_geom) AS the_geom FROM (
											SELECT ST_Node(ST_Collect(the_geom)) AS the_geom FROM (
												SELECT ST_ExteriorRing(ST_GeometryN(geom,1)) AS the_geom
									FROM clip) AS lines
										) AS noded_lines
									)) 
									) AS new_polys
									LEFT JOIN '.$haz_name.' a ON St_Within(new_polys.pip,a.geom);';
							}							
							
							if ($object_type == 'Routes'){
								// PAY ATTENTION to the ST_Length function -- no need to transform if the geometry is already in the right coordinate system													
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								WITH obj AS (SELECT fid, "OBJECTID", "CODE_ECONO", "OBJEKTART", "KUNSTBAUTE", "PRIX_M", "TJM", "VITESSE", "L_VEHICULE", "NB_PERS", "NB_PASSAGE", (ST_Intersection(the_geom,'.$polyGeomStr.')) As the_geom
								FROM '.$workspace.'."'.$object_name.'"
								WHERE ST_Intersects(the_geom,'.$polyGeomStr.')
								AND "CODE_ECONO" <> 0)
								SELECT t2.fid, t2."OBJECTID" as objectid, t2."CODE_ECONO" as code_econo, t2."OBJEKTART" as categorie, t2."KUNSTBAUTE" as domain, ST_Length(t2.the_geom) AS longueur_tot, 0.0 As longueur, "PRIX_M" as prix_unite, "TJM" as tjm, "VITESSE" as vitesse, "L_VEHICULE" as l_m, "NB_PERS" as humains, "NB_PASSAGE" as passage, rownum, gid, globalid, idobjet, alea, cin_lvin_1, cin_scepro as cin_sce_probabilite
								, CASE 
								   WHEN ST_CoveredBy(t2.the_geom, t1.geom) 
								   THEN t2.the_geom
								   ELSE 
									ST_Multi(
									  ST_Intersection(t2.the_geom, t1.geom)
									  ) 
								   END AS geom
								FROM '.$haz_tab_union.' t1, obj t2
								WHERE ST_Intersects(t1.geom, t2.the_geom);';
								
								$query .= 'UPDATE '.$workspace.'.'.$mapping_index.' SET longueur = ST_length(geom);';
							}
							else {
								// PAY ATTENTION to the ST_Area function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								WITH obj AS (SELECT fid, "OBJECTID", "CODE_ECONO", "TYPE_PRINC", "PRIX", "NB_PERS", "EXP_PERS", (ST_Intersection(the_geom,'.$polyGeomStr.')) As the_geom
								FROM '.$workspace.'."'.$object_name.'"
								WHERE ST_Intersects(the_geom,'.$polyGeomStr.'))
								SELECT *, ST_Area(geom) AS area
								FROM 
								(SELECT n.fid, n."OBJECTID" as objectid, n."CODE_ECONO" as code_econo, n."TYPE_PRINC" as categorie, "PRIX" as prix_unite, "NB_PERS" as humains, "EXP_PERS" as exp_pers, rownum, gid, globalid, idobjet, alea, cin_lvin_1, cin_scepro as cin_sce_probabilite
								 , CASE 
								   WHEN ST_CoveredBy(p.geom, n.the_geom) 
								   THEN ST_Multi(p.geom)
								   ELSE 
									ST_Multi(
									  ST_Intersection(p.geom,n.the_geom)
									  ) 
								   END AS geom 
								FROM '.$haz_tab_union.' AS p 
								INNER JOIN obj AS n 
								ON (ST_Intersects(p.geom, n.the_geom) 
								AND NOT ST_Touches(p.geom, n.the_geom)
								AND n."CODE_ECONO" <> 0)) t
								WHERE ST_Area(geom) > 0.1;';
							}
						}
						else {
							// for buildings with the vulnerability options 'Class' and 'Category'
							$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS (
							WITH obj AS (SELECT *
							FROM '.$workspace.'."'.$object_name.'"
							WHERE ST_Intersects(the_geom,'.$polyGeomStr.'))
							SELECT obj.the_geom as geom, "EGID" as egid, "CATEGORIE" as categorie, "CLASSE" as classe, "PRIX_TOT" as prix_tot, "NB_PERS" as nb_pers, "EXP_PERS" as exp_pers, rownum, gid, globalid, idobjet, alea, cin_lvin_1, cin_scepro as cin_sce_probabilite								
							FROM '.$haz_name.', obj			
							WHERE ST_Intersects(obj.the_geom, '.$haz_name.".geom)							
							ORDER BY egid, cin_lvin_1
							);";
						}						
					}					
					elseif ($haz_type == 'LTO'){
						if ($object_type == 'Routes' || $object_type == 'Surfaces'){
							// for routes with the vulnerability option 'Code-EconoMe'
							if ($count == 1) { // do the UNION if overlapping polygons
								$haz_tab_union = $mapping_index.'_union_view';
								$query .= 'CREATE TEMP TABLE '.$haz_tab_union.' AS 
								WITH clip AS (SELECT gid, cin_fksce, cin_lvinte, cin_lvin_1, cin_lvin_2, cin_lvin_3, cin_scepro, (ST_Intersection(geom,'.$polyGeomStr.')) As geom 
								FROM '.$haz_name.'
								WHERE ST_Intersects(geom,'.$polyGeomStr.'))								
								SELECT a.gid, a.cin_fksce, a.cin_lvinte, a.cin_lvin_1, a.cin_lvin_2, a.cin_lvin_3, a.cin_scepro, new_polys.the_geom as geom 
								FROM (SELECT geom AS the_geom, ST_PointOnSurface(geom) AS pip 
								FROM ST_Dump((
									SELECT ST_Polygonize(the_geom) AS the_geom FROM (
										SELECT ST_Node(ST_Collect(the_geom)) AS the_geom FROM (
											SELECT ST_ExteriorRing(ST_GeometryN(geom,1)) AS the_geom
								FROM clip) AS lines
									) AS noded_lines
								)) 
								) AS new_polys
								LEFT JOIN '.$haz_name.' a ON St_Within(new_polys.pip,a.geom);';
							}							
							
							if ($object_type == 'Routes'){
								// PAY ATTENTION to the ST_Length function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								WITH obj AS (SELECT fid, "OBJECTID", "CODE_ECONO", "OBJEKTART", "KUNSTBAUTE", "PRIX_M", "TJM", "VITESSE", "L_VEHICULE", "NB_PERS", "NB_PASSAGE",(ST_Intersection(the_geom,'.$polyGeomStr.')) As the_geom
								FROM '.$workspace.'."'.$object_name.'"
								WHERE ST_Intersects(the_geom,'.$polyGeomStr.')
								AND "CODE_ECONO" <> 0)
								SELECT t2.fid, t2."OBJECTID" as objectid, t2."CODE_ECONO" as code_econo, t2."OBJEKTART" as categorie, t2."KUNSTBAUTE" as domain, ST_Length(t2.the_geom) AS longueur_tot, 0.0 As longueur, "PRIX_M" as prix_unite, "TJM" as tjm, "VITESSE" as vitesse, "L_VEHICULE" as l_m, "NB_PERS" as humains, "NB_PASSAGE" as passage, gid, cin_fksce, cin_lvinte, cin_lvin_1 as int_1, cin_lvin_2, cin_lvin_3 as int_2, cin_lvin_1, cin_scepro as cin_sce_probabilite
								, CASE 
								   WHEN ST_CoveredBy(t2.the_geom, t1.geom) 
								   THEN t2.the_geom
								   ELSE 
									ST_Multi(
									  ST_Intersection(t2.the_geom, t1.geom)
									  ) 
								   END AS geom
								FROM '.$haz_tab_union.' t1, obj t2
								WHERE ST_Intersects(t1.geom, t2.the_geom);';
								
								$query .= 'UPDATE '.$workspace.'.'.$mapping_index.' SET longueur = ST_length(geom);';
							}
							else {
								// PAY ATTENTION to the ST_Area function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								WITH obj AS (SELECT fid, "OBJECTID", "CODE_ECONO", "TYPE_PRINC", "PRIX", "NB_PERS", "EXP_PERS", (ST_Intersection(the_geom,'.$polyGeomStr.')) As the_geom
								FROM '.$workspace.'."'.$object_name.'"
								WHERE ST_Intersects(the_geom,'.$polyGeomStr.'))
								SELECT *, ST_Area(geom) AS area
								FROM 
								(SELECT n.fid, n."OBJECTID" as objectid, n."CODE_ECONO" as code_econo, n."TYPE_PRINC" as categorie, "PRIX" as prix_unite, "NB_PERS" as humains, "EXP_PERS" as exp_pers, gid, cin_fksce, cin_lvinte, cin_lvin_1 as int_1, cin_lvin_2, cin_lvin_3 as int_2, cin_lvin_1, cin_scepro as cin_sce_probabilite
								 , CASE 
								   WHEN ST_CoveredBy(p.geom, n.the_geom) 
								   THEN ST_Multi(p.geom)
								   ELSE 
									ST_Multi(
									  ST_Intersection(p.geom,n.the_geom)
									  ) 
								   END AS geom 
								FROM '.$haz_tab_union.' AS p 
								INNER JOIN obj AS n 
								ON (ST_Intersects(p.geom, n.the_geom) 
								AND NOT ST_Touches(p.geom, n.the_geom)
								AND n."CODE_ECONO" <> 0)) t
								WHERE ST_Area(geom) > 0.1;';
							}
						}
						else {							
							// for buildings with the vulnerability options 'Class' and 'Category'
							$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS (
							WITH obj AS (SELECT *
							FROM '.$workspace.'."'.$object_name.'"
							WHERE ST_Intersects(the_geom,'.$polyGeomStr.'))
							SELECT obj.the_geom as geom, "EGID" as egid, "CATEGORIE" as categorie, "CLASSE" as classe, "PRIX_TOT" as prix_tot, "NB_PERS" as nb_pers, "EXP_PERS" as exp_pers, objectid, cin_fksce, cin_lvinte, cin_lvin_1 as int_1, cin_lvin_2, cin_lvin_3 as int_2, cin_lvin_1, cin_scepro as cin_sce_probabilite								
							FROM '.$haz_name.', obj			
							WHERE ST_Intersects(obj.the_geom, '.$haz_name.".geom)							
							ORDER BY egid
							);";					
						}						
					}
					else {
						// for other types of hazards
						if ($object_type == 'Routes' || $object_type == 'Surfaces'){
							// for routes with the vulnerability option 'Code-EconoMe'
							if ($count == 1) { // do the UNION if overlapping polygons
								$haz_tab_union = $mapping_index.'_union_view';
								$query .= 'CREATE TEMP TABLE '.$haz_tab_union.' AS 
								WITH clip AS (SELECT gid, cin_fksce, cin_lvinte, cin_lvin_1, cin_scepro, (ST_Intersection(geom,'.$polyGeomStr.')) As geom 
								FROM '.$haz_name.'
								WHERE ST_Intersects(geom,'.$polyGeomStr.'))								
								SELECT a.gid, a.cin_fksce, a.cin_lvinte, a.cin_lvin_1, a. cin_scepro, new_polys.the_geom as geom 
								FROM (SELECT geom AS the_geom, ST_PointOnSurface(geom) AS pip 
								FROM ST_Dump((
									SELECT ST_Polygonize(the_geom) AS the_geom FROM (
										SELECT ST_Node(ST_Collect(the_geom)) AS the_geom FROM (
											SELECT ST_ExteriorRing(ST_GeometryN(geom,1)) AS the_geom
								FROM clip) AS lines
									) AS noded_lines
								)) 
								) AS new_polys
								LEFT JOIN '.$haz_name.' a ON St_Within(new_polys.pip,a.geom);';
							}							
							
							if ($object_type == 'Routes'){
								// PAY ATTENTION to the ST_Length function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								WITH obj AS (SELECT fid, "OBJECTID", "CODE_ECONO", "OBJEKTART", "KUNSTBAUTE", "PRIX_M", "TJM", "VITESSE", "L_VEHICULE", "NB_PERS", "NB_PASSAGE", (ST_Intersection(the_geom,'.$polyGeomStr.')) As the_geom
								FROM '.$workspace.'."'.$object_name.'"
								WHERE ST_Intersects(the_geom,'.$polyGeomStr.')
								AND "CODE_ECONO" <> 0)
								SELECT t2.fid, t2."OBJECTID" as objectid, t2."CODE_ECONO" as code_econo, t2."OBJEKTART" as categorie, t2."KUNSTBAUTE" as domain, ST_Length(t2.the_geom) AS longueur_tot, 0.0 As longueur, "PRIX_M" as prix_unite, "TJM" as tjm, "VITESSE" as vitesse, "L_VEHICULE" as l_m, "NB_PERS" as humains, "NB_PASSAGE" as passage, gid, cin_fksce, cin_lvinte, cin_lvin_1, cin_scepro as cin_sce_probabilite
								, CASE 
								   WHEN ST_CoveredBy(t2.the_geom, t1.geom) 
								   THEN t2.the_geom
								   ELSE 
									ST_Multi(
									  ST_Intersection(t2.the_geom, t1.geom)
									  ) 
								   END AS geom
								FROM '.$haz_tab_union.' t1, obj t2
								WHERE ST_Intersects(t1.geom, t2.the_geom);';
								
								$query .= 'UPDATE '.$workspace.'.'.$mapping_index.' SET longueur = ST_length(geom);';
							}
							else {
								// PAY ATTENTION to the ST_Area function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								WITH obj AS (SELECT fid, "OBJECTID", "CODE_ECONO", "TYPE_PRINC", "PRIX", "NB_PERS", "EXP_PERS", (ST_Intersection(the_geom,'.$polyGeomStr.')) As the_geom
								FROM '.$workspace.'."'.$object_name.'"
								WHERE ST_Intersects(the_geom,'.$polyGeomStr.'))
								SELECT *, ST_Area(geom) AS area
								FROM 
								(SELECT n.fid, n."OBJECTID" as objectid, n."CODE_ECONO" as code_econo, n."TYPE_PRINC" as categorie, "PRIX" as prix_unite, "NB_PERS" as humains, "EXP_PERS" as exp_pers, gid, cin_fksce, cin_lvinte, cin_lvin_1, cin_scepro as cin_sce_probabilite
								 , CASE 
								   WHEN ST_CoveredBy(p.geom, n.the_geom) 
								   THEN ST_Multi(p.geom)
								   ELSE 
									ST_Multi(
									  ST_Intersection(p.geom,n.the_geom)
									  ) 
								   END AS geom 
								FROM '.$haz_tab_union.' AS p 
								INNER JOIN obj AS n 
								ON (ST_Intersects(p.geom, n.the_geom) 
								AND NOT ST_Touches(p.geom, n.the_geom)
								AND n."CODE_ECONO" <> 0)) t
								WHERE ST_Area(geom) > 0.1;';
							}
						}
						else {
							// for buildings with the vulnerability options 'Class' and 'Category'
							$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS (
							WITH obj AS (SELECT *
							FROM '.$workspace.'."'.$object_name.'"
							WHERE ST_Intersects(the_geom,'.$polyGeomStr.'))
							SELECT obj.the_geom as geom, "EGID" as egid, "CATEGORIE" as categorie, "CLASSE" as classe, "PRIX_TOT" as prix_tot, "NB_PERS" as nb_pers, "EXP_PERS" as exp_pers, objectid, cin_fksce, cin_lvinte, cin_lvin_1, cin_scepro as cin_sce_probabilite								
							FROM '.$haz_name.', obj			
							WHERE ST_Intersects(obj.the_geom, '.$haz_name.".geom)							
							ORDER BY egid
							);";
						}						
					}					
				}
				else{
					// for the vulnerability option 'Manual'
					if ($haz_type == 'GPP-TR'){
						if ($object_type == 'Routes' || $object_type == 'Surfaces'){
							// for routes 
							if ($count == 1) { // do the UNION if overlapping polygons
								$haz_tab_union = $mapping_index.'_union_view';
								$query .= 'CREATE TEMP TABLE '.$haz_tab_union.' AS 
									WITH clip AS (SELECT rownum, gid, globalid, idobjet, alea, cin_lvin_1, cin_scepro, (ST_Intersection(geom,'.$polyGeomStr.')) As geom 
									FROM '.$haz_name.'
									WHERE ST_Intersects(geom,'.$polyGeomStr.'))								
									SELECT a.rownum, a.gid, a.globalid, a.idobjet, a.alea, a.cin_lvin_1, a.cin_scepro, new_polys.the_geom as geom 
									FROM (SELECT geom AS the_geom, ST_PointOnSurface(geom) AS pip 
									FROM ST_Dump((
										SELECT ST_Polygonize(the_geom) AS the_geom FROM (
											SELECT ST_Node(ST_Collect(the_geom)) AS the_geom FROM (
												SELECT ST_ExteriorRing(ST_GeometryN(geom,1)) AS the_geom
									FROM clip) AS lines
										) AS noded_lines
									)) 
									) AS new_polys
									LEFT JOIN '.$haz_name.' a ON St_Within(new_polys.pip,a.geom);';
							}							
							
							if ($object_type == 'Routes'){
								// PAY ATTENTION to the ST_Length function -- no need to transform if the geometry is already in the right coordinate system													
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								WITH obj AS (SELECT fid, "OBJECTID", "CODE_ECONO", "OBJEKTART", "KUNSTBAUTE", "PRIX_M", "TJM", "VITESSE", "L_VEHICULE", "NB_PERS", "NB_PASSAGE", "VUL_OBJ_F", "VUL_OBJ_M", "VUL_OBJ_H", "LET_PERS_F", "LET_PERS_M", "LET_PERS_H", (ST_Intersection(the_geom,'.$polyGeomStr.')) As the_geom
								FROM '.$workspace.'."'.$object_name.'"
								WHERE ST_Intersects(the_geom,'.$polyGeomStr.')
								AND "CODE_ECONO" <> 0)
								SELECT t2.fid, t2."OBJECTID" as objectid, t2."CODE_ECONO" as code_econo, t2."OBJEKTART" as categorie, t2."KUNSTBAUTE" as domain, ST_Length(t2.the_geom) AS longueur_tot, 0.0 As longueur, "PRIX_M" as prix_unite, "TJM" as tjm, "VITESSE" as vitesse, "L_VEHICULE" as l_m, "NB_PERS" as humains, "NB_PASSAGE" as passage, "VUL_OBJ_F" as vul_obj_f, "VUL_OBJ_M" as vul_obj_m, "VUL_OBJ_H" as vul_obj_h, "LET_PERS_F" as let_pers_f, "LET_PERS_M" as let_pers_m, "LET_PERS_H" as let_pers_h, rownum, gid, globalid, idobjet, alea, cin_lvin_1, cin_scepro as cin_sce_probabilite
								, CASE 
								   WHEN ST_CoveredBy(t2.the_geom, t1.geom) 
								   THEN t2.the_geom
								   ELSE 
									ST_Multi(
									  ST_Intersection(t2.the_geom, t1.geom)
									  ) 
								   END AS geom
								FROM '.$haz_tab_union.' t1, obj t2
								WHERE ST_Intersects(t1.geom, t2.the_geom);';
								
								$query .= 'UPDATE '.$workspace.'.'.$mapping_index.' SET longueur = ST_length(geom);';
							}
							else {
								// PAY ATTENTION to the ST_Area function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								WITH obj AS (SELECT fid, "OBJECTID", "CODE_ECONO", "TYPE_PRINC", "PRIX", "NB_PERS", "EXP_PERS", "VUL_OBJ_F", "VUL_OBJ_M", "VUL_OBJ_H", "LET_PERS_F", "LET_PERS_M", "LET_PERS_H", (ST_Intersection(the_geom,'.$polyGeomStr.')) As the_geom
								FROM '.$workspace.'."'.$object_name.'"
								WHERE ST_Intersects(the_geom,'.$polyGeomStr.'))
								SELECT *, ST_Area(geom) AS area
								FROM 
								(SELECT n.fid, n."OBJECTID" as objectid, n."CODE_ECONO" as code_econo, n."TYPE_PRINC" as categorie, "PRIX" as prix_unite, "NB_PERS" as humains, "EXP_PERS" as exp_pers, "VUL_OBJ_F" as vul_obj_f, "VUL_OBJ_M" as vul_obj_m, "VUL_OBJ_H" as vul_obj_h, "LET_PERS_F" as let_pers_f, "LET_PERS_M" as let_pers_m, "LET_PERS_H" as let_pers_h, rownum, gid, globalid, idobjet, alea, cin_lvin_1, cin_scepro as cin_sce_probabilite 
								 , CASE 
								   WHEN ST_CoveredBy(p.geom, n.the_geom) 
								   THEN ST_Multi(p.geom)
								   ELSE 
									ST_Multi(
									  ST_Intersection(p.geom,n.the_geom)
									  ) 
								   END AS geom 
								FROM '.$haz_tab_union.' AS p 
								INNER JOIN obj AS n 
								ON (ST_Intersects(p.geom, n.the_geom) 
								AND NOT ST_Touches(p.geom, n.the_geom)
								AND n."CODE_ECONO" <> 0)) t
								WHERE ST_Area(geom) > 0.1;';
							}
						}
						else {
							// for buildings
							$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS (
							WITH obj AS (SELECT *
							FROM '.$workspace.'."'.$object_name.'"
							WHERE ST_Intersects(the_geom,'.$polyGeomStr.'))
							SELECT obj.the_geom as geom, "EGID" as egid, "CATEGORIE" as categorie, "CLASSE" as classe, "PRIX_TOT" as prix_tot, "NB_PERS" as nb_pers, "EXP_PERS" as exp_pers, "VUL_OBJ_F" as vul_obj_f, "VUL_OBJ_M" as vul_obj_m, "VUL_OBJ_H" as vul_obj_h, "LET_PERS_F" as let_pers_f, "LET_PERS_M" as let_pers_m, "LET_PERS_H" as let_pers_h, rownum, gid, globalid, idobjet, alea, cin_lvin_1, cin_scepro as cin_sce_probabilite
							FROM '.$haz_name.', obj			
							WHERE ST_Intersects(obj.the_geom, '.$haz_name.".geom)							
							ORDER BY egid, cin_lvin_1
							);";	
						}										
					}					
					elseif ($haz_type == 'LTO'){
						if ($object_type == 'Routes' || $object_type == 'Surfaces'){
							// for routes 
							if ($count == 1) { // do the UNION if overlapping polygons
								$haz_tab_union = $mapping_index.'_union_view';
								$query .= 'CREATE TEMP TABLE '.$haz_tab_union.' AS 
								WITH clip AS (SELECT gid, cin_fksce, cin_lvinte, cin_lvin_1, cin_lvin_2, cin_lvin_3, cin_scepro, (ST_Intersection(geom,'.$polyGeomStr.')) As geom 
								FROM '.$haz_name.'
								WHERE ST_Intersects(geom,'.$polyGeomStr.'))								
								SELECT a.gid, a.cin_fksce, a.cin_lvinte, a.cin_lvin_1, a.cin_lvin_2, a.cin_lvin_3, a.cin_scepro, new_polys.the_geom as geom 
								FROM (SELECT geom AS the_geom, ST_PointOnSurface(geom) AS pip 
								FROM ST_Dump((
									SELECT ST_Polygonize(the_geom) AS the_geom FROM (
										SELECT ST_Node(ST_Collect(the_geom)) AS the_geom FROM (
											SELECT ST_ExteriorRing(ST_GeometryN(geom,1)) AS the_geom
								FROM clip) AS lines
									) AS noded_lines
								)) 
								) AS new_polys
								LEFT JOIN '.$haz_name.' a ON St_Within(new_polys.pip,a.geom);';
							}							
							
							if ($object_type == 'Routes'){
								// PAY ATTENTION to the ST_Length function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								WITH obj AS (SELECT fid, "OBJECTID", "CODE_ECONO", "OBJEKTART", "KUNSTBAUTE", "PRIX_M", "TJM", "VITESSE", "L_VEHICULE", "NB_PERS", "NB_PASSAGE", "VUL_OBJ_F", "VUL_OBJ_M", "VUL_OBJ_H", "LET_PERS_F", "LET_PERS_M", "LET_PERS_H", (ST_Intersection(the_geom,'.$polyGeomStr.')) As the_geom
								FROM '.$workspace.'."'.$object_name.'"
								WHERE ST_Intersects(the_geom,'.$polyGeomStr.')
								AND "CODE_ECONO" <> 0)
								SELECT t2.fid, t2."OBJECTID" as objectid, t2."CODE_ECONO" as code_econo, t2."OBJEKTART" as categorie, t2."KUNSTBAUTE" as domain, ST_Length(t2.the_geom) AS longueur_tot, 0.0 As longueur, "PRIX_M" as prix_unite, "TJM" as tjm, "VITESSE" as vitesse, "L_VEHICULE" as l_m, "NB_PERS" as humains, "NB_PASSAGE" as passage, "VUL_OBJ_F" as vul_obj_f, "VUL_OBJ_M" as vul_obj_m, "VUL_OBJ_H" as vul_obj_h, "LET_PERS_F" as let_pers_f, "LET_PERS_M" as let_pers_m, "LET_PERS_H" as let_pers_h, gid, cin_fksce, cin_lvinte, cin_lvin_1 as int_1, cin_lvin_2, cin_lvin_3 as int_2, cin_lvin_1, cin_scepro as cin_sce_probabilite
								, CASE 
								   WHEN ST_CoveredBy(t2.the_geom, t1.geom) 
								   THEN t2.the_geom
								   ELSE 
									ST_Multi(
									  ST_Intersection(t2.the_geom, t1.geom)
									  ) 
								   END AS geom
								FROM '.$haz_tab_union.' t1, obj t2
								WHERE ST_Intersects(t1.geom, t2.the_geom);';
								
								$query .= 'UPDATE '.$workspace.'.'.$mapping_index.' SET longueur = ST_length(geom);';
							}
							else {
								// PAY ATTENTION to the ST_Area function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								WITH obj AS (SELECT fid, "OBJECTID", "CODE_ECONO", "TYPE_PRINC", "PRIX", "NB_PERS", "EXP_PERS", "VUL_OBJ_F", "VUL_OBJ_M", "VUL_OBJ_H", "LET_PERS_F", "LET_PERS_M", "LET_PERS_H", (ST_Intersection(the_geom,'.$polyGeomStr.')) As the_geom
								FROM '.$workspace.'."'.$object_name.'"
								WHERE ST_Intersects(the_geom,'.$polyGeomStr.'))
								SELECT *, ST_Area(geom) AS area
								FROM 
								(SELECT n.fid, n."OBJECTID" as objectid, n."CODE_ECONO" as code_econo, n."TYPE_PRINC" as categorie, "PRIX" as prix_unite, "NB_PERS" as humains, "EXP_PERS" as exp_pers, "VUL_OBJ_F" as vul_obj_f, "VUL_OBJ_M" as vul_obj_m, "VUL_OBJ_H" as vul_obj_h, "LET_PERS_F" as let_pers_f, "LET_PERS_M" as let_pers_m, "LET_PERS_H" as let_pers_h, gid, cin_fksce, cin_lvinte, cin_lvin_1 as int_1, cin_lvin_2, cin_lvin_3 as int_2, cin_lvin_1, cin_scepro as cin_sce_probabilite
								 , CASE 
								   WHEN ST_CoveredBy(p.geom, n.the_geom) 
								   THEN ST_Multi(p.geom)
								   ELSE 
									ST_Multi(
									  ST_Intersection(p.geom,n.the_geom)
									  ) 
								   END AS geom 
								FROM '.$haz_tab_union.' AS p 
								INNER JOIN obj AS n 
								ON (ST_Intersects(p.geom, n.the_geom) 
								AND NOT ST_Touches(p.geom, n.the_geom)
								AND n."CODE_ECONO" <> 0)) t
								WHERE ST_Area(geom) > 0.1;';
							}
						}
						else {							
							// for buildings 
							$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS (
							WITH obj AS (SELECT *
							FROM '.$workspace.'."'.$object_name.'"
							WHERE ST_Intersects(the_geom,'.$polyGeomStr.'))
							SELECT obj.the_geom as geom, "EGID" as egid, "CATEGORIE" as categorie, "CLASSE" as classe, "PRIX_TOT" as prix_tot, "NB_PERS" as nb_pers, "EXP_PERS" as exp_pers, "VUL_OBJ_F" as vul_obj_f, "VUL_OBJ_M" as vul_obj_m, "VUL_OBJ_H" as vul_obj_h, "LET_PERS_F" as let_pers_f, "LET_PERS_M" as let_pers_m, "LET_PERS_H" as let_pers_h, objectid, cin_fksce, cin_lvinte, cin_lvin_1 as int_1, cin_lvin_2, cin_lvin_3 as int_2, cin_lvin_1, cin_scepro as cin_sce_probabilite								
							FROM '.$haz_name.', obj			
							WHERE ST_Intersects(obj.the_geom, '.$haz_name.".geom)							
							ORDER BY egid
							);";				
						}						
					}
					else {
						// for other types of hazards
						if ($object_type == 'Routes' || $object_type == 'Surfaces'){
							// for routes 
							if ($count == 1) { // do the UNION if overlapping polygons
								$haz_tab_union = $mapping_index.'_union_view';
								$query .= 'CREATE TEMP TABLE '.$haz_tab_union.' AS 
								WITH clip AS (SELECT gid, cin_fksce, cin_lvinte, cin_lvin_1, cin_scepro, (ST_Intersection(geom,'.$polyGeomStr.')) As geom 
								FROM '.$haz_name.'
								WHERE ST_Intersects(geom,'.$polyGeomStr.'))								
								SELECT a.gid, a.cin_fksce, a.cin_lvinte, a.cin_lvin_1, a. cin_scepro, new_polys.the_geom as geom 
								FROM (SELECT geom AS the_geom, ST_PointOnSurface(geom) AS pip 
								FROM ST_Dump((
									SELECT ST_Polygonize(the_geom) AS the_geom FROM (
										SELECT ST_Node(ST_Collect(the_geom)) AS the_geom FROM (
											SELECT ST_ExteriorRing(ST_GeometryN(geom,1)) AS the_geom
								FROM clip) AS lines
									) AS noded_lines
								)) 
								) AS new_polys
								LEFT JOIN '.$haz_name.' a ON St_Within(new_polys.pip,a.geom);';
							}							
							
							if ($object_type == 'Routes'){
								// PAY ATTENTION to the ST_Length function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								WITH obj AS (SELECT fid, "OBJECTID", "CODE_ECONO", "OBJEKTART", "KUNSTBAUTE", "PRIX_M", "TJM", "VITESSE", "L_VEHICULE", "NB_PERS", "NB_PASSAGE", "VUL_OBJ_F", "VUL_OBJ_M", "VUL_OBJ_H", "LET_PERS_F", "LET_PERS_M", "LET_PERS_H", (ST_Intersection(the_geom,'.$polyGeomStr.')) As the_geom
								FROM '.$workspace.'."'.$object_name.'"
								WHERE ST_Intersects(the_geom,'.$polyGeomStr.')
								AND "CODE_ECONO" <> 0)
								SELECT t2.fid, t2."OBJECTID" as objectid, t2."CODE_ECONO" as code_econo, t2."OBJEKTART" as categorie, t2."KUNSTBAUTE" as domain, ST_Length(t2.the_geom) AS longueur_tot, 0.0 As longueur, "PRIX_M" as prix_unite, "TJM" as tjm, "VITESSE" as vitesse, "L_VEHICULE" as l_m, "NB_PERS" as humains, "NB_PASSAGE" as passage, "VUL_OBJ_F" as vul_obj_f, "VUL_OBJ_M" as vul_obj_m, "VUL_OBJ_H" as vul_obj_h, "LET_PERS_F" as let_pers_f, "LET_PERS_M" as let_pers_m, "LET_PERS_H" as let_pers_h, gid, cin_fksce, cin_lvinte, cin_lvin_1, cin_scepro as cin_sce_probabilite
								, CASE 
								   WHEN ST_CoveredBy(t2.the_geom, t1.geom) 
								   THEN t2.the_geom
								   ELSE 
									ST_Multi(
									  ST_Intersection(t2.the_geom, t1.geom)
									  ) 
								   END AS geom
								FROM '.$haz_tab_union.' t1, obj t2
								WHERE ST_Intersects(t1.geom, t2.the_geom);';
								
								$query .= 'UPDATE '.$workspace.'.'.$mapping_index.' SET longueur = ST_length(geom);';
							}
							else {
								// PAY ATTENTION to the ST_Area function -- no need to transform if the geometry is already in the right coordinate system
								$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS
								WITH obj AS (SELECT fid, "OBJECTID", "CODE_ECONO", "TYPE_PRINC", "PRIX", "NB_PERS", "EXP_PERS", "VUL_OBJ_F", "VUL_OBJ_M", "VUL_OBJ_H", "LET_PERS_F", "LET_PERS_M", "LET_PERS_H", (ST_Intersection(the_geom,'.$polyGeomStr.')) As the_geom
								FROM '.$workspace.'."'.$object_name.'"
								WHERE ST_Intersects(the_geom,'.$polyGeomStr.'))
								SELECT *, ST_Area(geom) AS area
								FROM 
								(SELECT n.fid, n."OBJECTID" as objectid, n."CODE_ECONO" as code_econo, n."TYPE_PRINC" as categorie, "PRIX" as prix_unite, "NB_PERS" as humains, "EXP_PERS" as exp_pers, "VUL_OBJ_F" as vul_obj_f, "VUL_OBJ_M" as vul_obj_m, "VUL_OBJ_H" as vul_obj_h, "LET_PERS_F" as let_pers_f, "LET_PERS_M" as let_pers_m, "LET_PERS_H" as let_pers_h, gid, cin_fksce, cin_lvinte, cin_lvin_1, cin_scepro as cin_sce_probabilite
								 , CASE 
								   WHEN ST_CoveredBy(p.geom, n.the_geom) 
								   THEN ST_Multi(p.geom)
								   ELSE 
									ST_Multi(
									  ST_Intersection(p.geom,n.the_geom)
									  ) 
								   END AS geom 
								FROM '.$haz_tab_union.' AS p 
								INNER JOIN obj AS n 
								ON (ST_Intersects(p.geom, n.the_geom) 
								AND NOT ST_Touches(p.geom, n.the_geom)
								AND n."CODE_ECONO" <> 0)) t
								WHERE ST_Area(geom) > 0.1;';
							}
						}
						else {
							// for buildings
							$query .= 'CREATE TABLE '.$workspace.'.'.$mapping_index.' AS (
							WITH obj AS (SELECT *
							FROM '.$workspace.'."'.$object_name.'"
							WHERE ST_Intersects(the_geom,'.$polyGeomStr.'))
							SELECT obj.the_geom as geom, "EGID" as egid, "CATEGORIE" as categorie, "CLASSE" as classe, "PRIX_TOT" as prix_tot, "NB_PERS" as nb_pers, "EXP_PERS" as exp_pers, "VUL_OBJ_F" as vul_obj_f, "VUL_OBJ_M" as vul_obj_m, "VUL_OBJ_H" as vul_obj_h, "LET_PERS_F" as let_pers_f, "LET_PERS_M" as let_pers_m, "LET_PERS_H" as let_pers_h, objectid, cin_fksce, cin_lvinte, cin_lvin_1, cin_scepro as cin_sce_probabilite								
							FROM '.$haz_name.', obj			
							WHERE ST_Intersects(obj.the_geom, '.$haz_name.".geom)							
							ORDER BY egid
							);";	
						}										
					}
				}				
			}						
		}
					
		/* WITH obj AS (SELECT *
		FROM ormont_dessus."Buildings_Ormont_dessus_proj"
		WHERE the_geom && ST_MakeEnvelope(7.131876684390441, 46.35411567202716, 7.135159708224689, 46.35743302690013, 4326)) 
		SELECT obj.the_geom as geom, "EGID" as egid, "SURFACE" as surface, "NBRE_LOG" as nbre_log, "CATEGORIE" as categorie, objectid, cin_fksce, cin_lvinte, cin_lvin_1, cin_scepro as cin_sce_probabilite								
		FROM ormont_dessus.inod_bat_ormont_dessus_default_view, obj	
		WHERE ST_Intersects(obj.the_geom, inod_bat_ormont_dessus_default_view.geom)
		AND cin_lvin_1 <> '0'
		ORDER BY egid	 */
		
		### query to add new columns 
		if ($object_type == 'Buildings'){
			if ($object_level == 'Class' || $object_level == 'Category') {
				// for the options 'Class' and 'Category'
				$query .= "ALTER TABLE ".$workspace.".".$mapping_index." 
					ADD COLUMN frequence double precision, 
					ADD COLUMN pra_valeur double precision, 
					ADD COLUMN prix_unite double precision,				
					ADD COLUMN montant_valeur double precision, 
					ADD COLUMN vul_obj_valeur double precision, 
					ADD COLUMN humains double precision,	
					ADD COLUMN humains_tot double precision,
					ADD COLUMN humain_exp double precision,
					ADD COLUMN vul_humain_valeur double precision,
					ADD COLUMN degats_obj double precision,
					ADD COLUMN degats_humain double precision,
					ADD COLUMN risque_obj double precision,
					ADD COLUMN risque_humain double precision,	
					ADD COLUMN risque_tot double precision,
					ADD COLUMN risque_individuel double precision
				;";
			}
			else {
				// for the option 'Manual'
				$query .= "ALTER TABLE ".$workspace.".".$mapping_index." 
					ADD COLUMN frequence double precision, 
					ADD COLUMN pra_valeur double precision, 
					ADD COLUMN montant_valeur double precision, 
					ADD COLUMN vul_obj_valeur double precision, 
					ADD COLUMN humains_tot double precision,
					ADD COLUMN humain_exp double precision,
					ADD COLUMN vul_humain_valeur double precision,
					ADD COLUMN degats_obj double precision,
					ADD COLUMN degats_humain double precision,
					ADD COLUMN risque_obj double precision,
					ADD COLUMN risque_humain double precision,
					ADD COLUMN risque_tot double precision,
					ADD COLUMN risque_individuel double precision
				;";
			}
		}
		elseif ($object_type == 'Routes'){
			if ($object_level == 'Code-EconoMe'){
				// for the option 'Code-EconoMe'
				$query .= "ALTER TABLE ".$workspace.".".$mapping_index."
					ADD COLUMN TJM double precision, 
					ADD COLUMN l_m double precision, 
					ADD COLUMN vitesse double precision, 
					ADD COLUMN passage double precision, 
					ADD COLUMN frequence double precision, 
					ADD COLUMN pra_valeur double precision, 
					ADD COLUMN prix_unite double precision,				
					ADD COLUMN montant_valeur double precision, 
					ADD COLUMN vul_obj_valeur double precision, 
					ADD COLUMN humains double precision,	
					ADD COLUMN humains_tot double precision,
					ADD COLUMN l_def_sce double precision,
					ADD COLUMN d_m double precision,
					ADD COLUMN humain_exp double precision,
					ADD COLUMN humain_exp_in double precision,
					ADD COLUMN vul_humain_valeur double precision,
					ADD COLUMN vul_humain_definie double precision,
					ADD COLUMN degats_obj double precision,
					ADD COLUMN degats_humain double precision,
					ADD COLUMN risque_obj double precision,
					ADD COLUMN risque_humain double precision,
					ADD COLUMN risque_tot double precision,
					ADD COLUMN risque_individuel double precision
				;";
			}
			else{
				// for the option 'Manual'
				$query .= "ALTER TABLE ".$workspace.".".$mapping_index."														
					ADD COLUMN frequence double precision, 
					ADD COLUMN pra_valeur double precision, 							
					ADD COLUMN montant_valeur double precision, 
					ADD COLUMN vul_obj_valeur double precision, 					
					ADD COLUMN humains_tot double precision,
					ADD COLUMN l_def_sce double precision,
					ADD COLUMN d_m double precision,
					ADD COLUMN humain_exp double precision,
					ADD COLUMN humain_exp_in double precision,
					ADD COLUMN vul_humain_valeur double precision,
					ADD COLUMN vul_humain_definie double precision,
					ADD COLUMN degats_obj double precision,
					ADD COLUMN degats_humain double precision,
					ADD COLUMN risque_obj double precision,
					ADD COLUMN risque_humain double precision,
					ADD COLUMN risque_tot double precision,
					ADD COLUMN risque_individuel double precision
				;";
			}
		}
		else{ // for area objects
			if ($object_level == 'Code-EconoMe'){
				// for the option 'Code-EconoMe'
				$query .= "ALTER TABLE ".$workspace.".".$mapping_index."					
					ADD COLUMN frequence double precision, 
					ADD COLUMN pra_valeur double precision, 
					ADD COLUMN prix_unite double precision,				
					ADD COLUMN montant_valeur double precision, 
					ADD COLUMN vul_obj_valeur double precision, 
					ADD COLUMN humains double precision,	
					ADD COLUMN humains_tot double precision,					
					ADD COLUMN humain_exp double precision,					
					ADD COLUMN vul_humain_valeur double precision,
					ADD COLUMN degats_obj double precision,
					ADD COLUMN degats_humain double precision,
					ADD COLUMN risque_obj double precision,
					ADD COLUMN risque_humain double precision,
					ADD COLUMN risque_tot double precision,
					ADD COLUMN risque_individuel double precision
				;";
			}
			else {
				// for the option 'Manual' 
				$query .= "ALTER TABLE ".$workspace.".".$mapping_index." 
					ADD COLUMN frequence double precision, 
					ADD COLUMN pra_valeur double precision, 
					ADD COLUMN montant_valeur double precision, 
					ADD COLUMN vul_obj_valeur double precision, 
					ADD COLUMN humains_tot double precision,
					ADD COLUMN humain_exp double precision,
					ADD COLUMN vul_humain_valeur double precision,
					ADD COLUMN degats_obj double precision,
					ADD COLUMN degats_humain double precision,
					ADD COLUMN risque_obj double precision,
					ADD COLUMN risque_humain double precision,
					ADD COLUMN risque_tot double precision,
					ADD COLUMN risque_individuel double precision
				;";
			}
		}
		
		### query to update the intensity classes (i.e., 4, 5 and 9) to take the maximum value in the case of LTO 
		if ($haz_type != 'GPP-TR'){
			$query .= "UPDATE ".$workspace.".".$mapping_index." 
			SET cin_lvin_1 = concat('-',cin_lvin_1) 
			WHERE cin_lvin_1::integer = 4
			OR cin_lvin_1::integer = 9;";
		
			$query .= "UPDATE ".$workspace.".".$mapping_index." 
			SET cin_lvin_1 = '1' 
			WHERE cin_lvin_1::integer = 5;";
		}		
		
		if ($haz_type == 'LTO'){
			// update the first and second intensity classes (i.e., 4,5 and 9)
			$query .= "UPDATE ".$workspace.".".$mapping_index." 
			SET int_1 = concat('-',int_1) 
			WHERE int_1::integer = 4
			OR int_1::integer = 9;";
			
			$query .= "UPDATE ".$workspace.".".$mapping_index." 
			SET int_2 = concat('-',int_2) 
			WHERE int_2::integer = 4
			OR int_2::integer = 9;";
			
			$query .= "UPDATE ".$workspace.".".$mapping_index." 
			SET int_1 = '1' 
			WHERE int_1::integer = 5;";
			
			$query .= "UPDATE ".$workspace.".".$mapping_index." 
			SET int_2 = '1' 
			WHERE int_2::integer = 5;";
			
			// update the real (to be used) intensity class by assigning the maximum value of two intensity classes
			$query .= "UPDATE ".$workspace.".".$mapping_index." 
			SET cin_lvin_1 = int_2 
			WHERE cin_lvin_1::integer < int_2::integer;";
			
			// if the first intensity class is NULL, take the second intensity class (assuming that it is not also NULL)
			$query .= "UPDATE ".$workspace.".".$mapping_index." 
			SET cin_lvin_1 = int_2 
			WHERE cin_lvin_1 IS NULL;";
		}
			
		### query to populate the frequency value
		for ($i = 0; $i < count($rpArr); $i++) {
			$prob = $rpArr[$i][2];
			$year = $rpArr[$i][3];
			$query .= "UPDATE ".$workspace.".".$mapping_index." 
			SET frequence = ($year)^(-1)
			WHERE '$prob' = cin_sce_probabilite;";
		}
		
		### query to populate the spatial probability value
		if ($spatial_prob == 'D'){ 
			// default mode 		
			$query .= "UPDATE ".$workspace.".".$mapping_index." 
			SET pra_valeur = valeur
			FROM ".$workspace.".spatial_prob_default
			WHERE processus = '".$haz_type."'
			AND probabilite = cin_sce_probabilite;";
			
			$query .= "UPDATE ".$workspace.".".$mapping_index." 
			SET pra_valeur = valeur
			FROM ".$workspace.".spatial_prob_default
			WHERE processus = '".$haz_type."'
			AND probabilite = 'TFreq'
			AND $mapping_index.frequence > 30^(-1);";
			
			$query .= "UPDATE ".$workspace.".".$mapping_index." 
			SET pra_valeur = valeur
			FROM ".$workspace.".spatial_prob_default
			WHERE processus = '".$haz_type."'
			AND probabilite = 'TEx'
			AND $mapping_index.frequence < 300^(-1);";
		}	
		else {
			// user defined mode
			$query .= "UPDATE ".$workspace.".".$mapping_index." 
			SET pra_valeur = valeur
			FROM ".$workspace.".".$spatial_prob_manual."
			WHERE processus = '".$haz_type."'
			AND probabilite = cin_sce_probabilite;";
			
			$query .= "UPDATE ".$workspace.".".$mapping_index." 
			SET pra_valeur = valeur
			FROM ".$workspace.".".$spatial_prob_manual."
			WHERE processus = '".$haz_type."'
			AND probabilite = 'TFreq'
			AND $mapping_index.frequence > 30^(-1);";
			
			$query .= "UPDATE ".$workspace.".".$mapping_index." 
			SET pra_valeur = valeur
			FROM ".$workspace.".".$spatial_prob_manual."
			WHERE processus = '".$haz_type."'
			AND probabilite = 'TEx'
			AND $mapping_index.frequence < 300^(-1);";
		}
			
		### query to update the object values according to the input parameter (Category, Class, Code-EconoMe or Manual)
		if ($object_type == 'Routes'){
			if ($object_level == 'Code-EconoMe'){
				if ($object_mode == 'D'){
					// default mode
					$query .= "UPDATE ".$workspace.".".$mapping_index."
						SET prix_unite = prix_unitaire, 
						humains = objects_default_routes.humains,	
						tjm = objects_default_routes.tjm,
						vitesse = objects_default_routes.vitesse,
						l_m = objects_default_routes.longueur,
						passage = objects_default_routes.passage
						FROM ".$workspace.".objects_default_routes
						WHERE code = code_econo::integer;";
				}
				else {
					// user defined mode
					$query .= "UPDATE ".$workspace.".".$mapping_index."
						SET prix_unite = prix_unitaire, 
						humains = ".$object_manual.".humains,	
						tjm = ".$object_manual.".tjm,
						vitesse = ".$object_manual.".vitesse,
						l_m = ".$object_manual.".longueur,
						passage = ".$object_manual.".passage
						FROM ".$workspace.".".$object_manual."
						WHERE code = code_econo::integer;";
				}
			}
		}
		elseif ($object_type == 'Buildings') {			
			if ($object_level == 'Class'){ 
				if ($object_mode == 'D'){ 
					// default mode 			
					$query .= "UPDATE ".$workspace.".".$mapping_index."
						SET prix_unite = prix_unitaire, 
						humains = objects_default.humains, 
						humain_exp = objects_default.humains_exp
						FROM ".$workspace.".objects_default
						WHERE code = classe::integer;";
				}
				else {
					// user defined mode
					$query .= "UPDATE ".$workspace.".".$mapping_index."
						SET prix_unite = prix_unitaire, 
						humains = ".$object_manual.".humains, 
						humain_exp = ".$object_manual.".humains_exp
						FROM ".$workspace.".".$object_manual."
						WHERE code = classe::integer;";
				}
			}
			elseif ($object_level == 'Category'){ 
				if ($object_mode == 'D'){ 
					// default mode 			
					$query .= "UPDATE ".$workspace.".".$mapping_index."
						SET prix_unite = prix_unitaire, 
						humains = objects_default.humains, 
						humain_exp = objects_default.humains_exp
						FROM ".$workspace.".objects_default
						WHERE code = categorie::integer;";
				}
				else {
					// user defined mode
					$query .= "UPDATE ".$workspace.".".$mapping_index."
						SET prix_unite = prix_unitaire, 
						humains = ".$object_manual.".humains, 
						humain_exp = ".$object_manual.".humains_exp
						FROM ".$workspace.".".$object_manual."
						WHERE code = categorie::integer;";
				}
			}
			elseif ($object_level == 'Manual'){
				// copy corresponding data columns from user-uploaded object table
				$query .= "UPDATE ".$workspace.".".$mapping_index."
					SET humain_exp = exp_pers;";
			}
		}
		else { // for area objects
			if ($object_level == 'Code-EconoMe'){
				if ($object_mode == 'D'){
					// default mode
					$query .= "UPDATE ".$workspace.".".$mapping_index."
						SET prix_unite = prix_unitaire, 
						humains = objects_default.humains,	
						humain_exp = objects_default.humains_exp
						FROM ".$workspace.".objects_default
						WHERE code = code_econo::integer;";
				}
				else {
					// user defined mode
					$query .= "UPDATE ".$workspace.".".$mapping_index."
						SET prix_unite = prix_unitaire, 
						humains = ".$object_manual.".humains,	
						humain_exp = ".$object_manual.".humains_exp
						FROM ".$workspace.".".$object_manual."
						WHERE code = code_econo::integer;";
				}
			}			
			elseif ($object_level == 'Manual'){
				// copy corresponding data columns from user-uploaded object table
				$query .= "UPDATE ".$workspace.".".$mapping_index."
					SET humain_exp = exp_pers;";
			}
		}				
		
		### query to populate the total amount column according to the input parameter (Category, Class, Code-EconoMe or Manual)	### need to validate ###
		if ($object_type == 'Buildings'){
			if ($object_level == 'Class'){ 
				$query .= "UPDATE ".$workspace.".".$mapping_index." SET montant_valeur = prix_unite * volume WHERE classe = '1211' OR classe = '1212' OR classe = '1220' OR classe = '1230' OR classe = '1241' OR classe = '1251' OR classe = '1252' OR classe = '1261' OR classe = '1262' OR classe = '1263' OR classe = '1264' OR classe = '1265' OR classe = '1271' OR classe = '1272' OR classe = '1273' OR classe = '1274';";
				$query .= "UPDATE ".$workspace.".".$mapping_index." SET montant_valeur = prix_unite * 1 WHERE classe = '1110';";
				$query .= "UPDATE ".$workspace.".".$mapping_index." SET montant_valeur = prix_unite * 2 WHERE classe = '1121';";
				$query .= "UPDATE ".$workspace.".".$mapping_index." SET montant_valeur = prix_unite * nbre_log WHERE classe = '1122' OR classe = '1130';";
				$query .= "UPDATE ".$workspace.".".$mapping_index." SET montant_valeur = prix_unite WHERE classe = '1242';";
			}
			elseif ($object_level == 'Category'){ 
				$query .= "UPDATE ".$workspace.".".$mapping_index." SET montant_valeur = prix_unite * volume WHERE categorie = '1010' OR categorie = '1040' OR categorie = '1060' OR categorie = '1080';";
				$query .= "UPDATE ".$workspace.".".$mapping_index." SET montant_valeur = prix_unite * 1 WHERE categorie = '1021';";
				$query .= "UPDATE ".$workspace.".".$mapping_index." SET montant_valeur = prix_unite * nbre_log WHERE categorie = '1025' OR categorie = '1030';";
			}
			elseif ($object_level == 'Manual'){
				$query .= "UPDATE ".$workspace.".".$mapping_index." SET montant_valeur = prix_tot;";
			}
		}
		elseif ($object_type == 'Routes'){
			if ($object_level == 'Code-EconoMe' || $object_level == 'Manual'){
				$query .= "UPDATE ".$workspace.".".$mapping_index." SET montant_valeur = prix_unite * longueur;";
			}
		}
		else { // for area objects
			if ($object_level == 'Code-EconoMe' || $object_level == 'Manual'){
				// note that the price/unit of the area objects are in 'a', which needs to be transformed to 'm2'
				$query .= "UPDATE ".$workspace.".".$mapping_index." SET montant_valeur = (prix_unite * 0.01) * area;"; 
			}
		}
		
		### query to populate the total no. of humans column according to the input parameter (Category, Class, Code-EconoMe or Manual)	### need to validate ###
		if ($object_type == 'Buildings'){
			if ($object_level == 'Class'){ 
				$query .= "UPDATE ".$workspace.".".$mapping_index." SET humains_tot = humains * surface WHERE classe = '1211' OR classe = '1212' OR classe = '1220' OR classe = '1230' OR classe = '1241' OR classe = '1251' OR classe = '1252' OR classe = '1261' OR classe = '1262' OR classe = '1263' OR classe = '1264' OR classe = '1265' OR classe = '1271' OR classe = '1272' OR classe = '1273' OR classe = '1274';";
				$query .= "UPDATE ".$workspace.".".$mapping_index." SET humains_tot = humains * 1 WHERE classe = '1110';";
				$query .= "UPDATE ".$workspace.".".$mapping_index." SET humains_tot = humains * 2 WHERE classe = '1121';";
				$query .= "UPDATE ".$workspace.".".$mapping_index." SET humains_tot = humains * nbre_log WHERE classe = '1122' OR classe = '1130';";
				$query .= "UPDATE ".$workspace.".".$mapping_index." SET humains_tot = humains * surface WHERE classe = '1242';";
			}
			elseif ($object_level == 'Category'){ 
				$query .= "UPDATE ".$workspace.".".$mapping_index." SET humains_tot = humains * surface WHERE categorie = '1010' OR categorie = '1040' OR categorie = '1060' OR categorie = '1080';";
				$query .= "UPDATE ".$workspace.".".$mapping_index." SET humains_tot = humains * 1 WHERE categorie = '1021';";
				$query .= "UPDATE ".$workspace.".".$mapping_index." SET humains_tot = humains * nbre_log WHERE categorie = '1025' OR categorie = '1030';";
			}
			else{
				$query .= "UPDATE ".$workspace.".".$mapping_index." SET humains_tot = nb_pers;";
			}
		}
		elseif ($object_type == 'Routes'){
			if ($object_level == 'Code-EconoMe' || $object_level == 'Manual'){
				// the l_def_sce value should NOT sum all the lengths of duplicated geometries within the same return periods (i.e., only one length should be taken into account in case of duplicated geometries)
				// update the l_def_sce value after considering duplicated geometries
				$query .= "UPDATE ".$workspace.".".$mapping_index." 
					SET l_def_sce = t.sum
					FROM (SELECT fid, cin_sce_probabilite, sum(longueur) FROM
					(SELECT ROW_NUMBER() OVER(PARTITION BY fid, cin_sce_probabilite, ST_AsBinary(geom) ORDER BY cin_lvin_1::integer DESC) AS row, COUNT(*) OVER(PARTITION BY fid, cin_sce_probabilite, ST_AsBinary(geom)), t.*
					FROM ".$workspace.".".$mapping_index." t) dups
					WHERE dups.row = 1
					GROUP BY fid, cin_sce_probabilite) t
					WHERE ".$workspace.".".$mapping_index.".fid = t.fid
					AND ".$workspace.".".$mapping_index.".cin_sce_probabilite = t.cin_sce_probabilite;";
				
			/* 	$query .= "UPDATE ".$workspace.".".$mapping_index." 
					SET l_def_sce = (SELECT SUM(longueur)
					FROM ".$workspace.".".$mapping_index." t
					WHERE ".$workspace.".".$mapping_index.".fid = t.fid
					AND ".$workspace.".".$mapping_index.".cin_sce_probabilite = t.cin_sce_probabilite);"; */
				$query .= "UPDATE ".$workspace.".".$mapping_index." SET d_m = l_def_sce * pra_valeur;";
				$query .= "UPDATE ".$workspace.".".$mapping_index." SET humains_tot = ((d_m + 4)* humains)/l_m;";
				$query .= "UPDATE ".$workspace.".".$mapping_index." SET humains_tot = humains WHERE humains_tot > humains;";
			}
		}
		else { // for area objects
			if ($object_level == 'Code-EconoMe' || $object_level == 'Manual'){ 
				// note that the no_pers/unit of the area objects are in 'm2'
				$query .= "UPDATE ".$workspace.".".$mapping_index." SET humains_tot = humains * area;";
			}
		}
		
		### query to populate the vulnerability values of the object and people inside the object, according to the input parameter (Category, Class, Code-EconoMe or Manual)
		if ($vul_level == 'Class'){
			if ($vul_mode == 'D'){
				// default mode
				$query .= "UPDATE ".$workspace.".".$mapping_index." 
					SET vul_obj_valeur = valeur_objet
					FROM ".$workspace.".vulnerability_default
					WHERE processus = '".$haz_type."'
					AND type = '".$object_type."'
					AND intensite_valeur = cin_lvin_1::integer
					AND classe::integer = code
					AND obj_niveau = 'Class';";
				
				$query .= "UPDATE ".$workspace.".".$mapping_index." 
					SET vul_humain_valeur = valeur_letalite
					FROM ".$workspace.".vulnerability_default
					WHERE processus = '".$haz_type."'
					AND type = '".$object_type."'
					AND intensite_valeur = cin_lvin_1::integer
					AND classe::integer = code
					AND obj_niveau = 'Class';";
			}
			else {
				// user defined mode
				$query .= "UPDATE ".$workspace.".".$mapping_index." 
					SET vul_obj_valeur = valeur_objet
					FROM ".$workspace.".".$vul_manual."
					WHERE processus = '".$haz_type."'
					AND type = '".$object_type."'
					AND intensite_valeur = cin_lvin_1::integer
					AND classe::integer = code
					AND obj_niveau = 'Class';";
				
				$query .= "UPDATE ".$workspace.".".$mapping_index." 
					SET vul_humain_valeur = valeur_letalite
					FROM ".$workspace.".".$vul_manual."
					WHERE processus = '".$haz_type."'
					AND type = '".$object_type."'
					AND intensite_valeur = cin_lvin_1::integer
					AND classe::integer = code
					AND obj_niveau = 'Class';";
			}
		}
		elseif ($vul_level == 'Category'){
			if ($vul_mode == 'D'){
				// default mode
				$query .= "UPDATE ".$workspace.".".$mapping_index." 
					SET vul_obj_valeur = valeur_objet
					FROM ".$workspace.".vulnerability_default
					WHERE processus = '".$haz_type."'
					AND type = '".$object_type."'
					AND intensite_valeur = cin_lvin_1::integer
					AND categorie::integer = code
					AND obj_niveau = 'Category';";
				
				$query .= "UPDATE ".$workspace.".".$mapping_index." 
					SET vul_humain_valeur = valeur_letalite
					FROM ".$workspace.".vulnerability_default
					WHERE processus = '".$haz_type."'
					AND type = '".$object_type."'
					AND intensite_valeur = cin_lvin_1::integer
					AND categorie::integer = code
					AND obj_niveau = 'Category';";
			}
			else {
				// user defined mode
				$query .= "UPDATE ".$workspace.".".$mapping_index." 
					SET vul_obj_valeur = valeur_objet
					FROM ".$workspace.".".$vul_manual."
					WHERE processus = '".$haz_type."'
					AND type = '".$object_type."'
					AND intensite_valeur = cin_lvin_1::integer
					AND categorie::integer = code
					AND obj_niveau = 'Category';";
				
				$query .= "UPDATE ".$workspace.".".$mapping_index." 
					SET vul_humain_valeur = valeur_letalite
					FROM ".$workspace.".".$vul_manual."
					WHERE processus = '".$haz_type."'
					AND type = '".$object_type."'
					AND intensite_valeur = cin_lvin_1::integer
					AND categorie::integer = code
					AND obj_niveau = 'Category';";
			}
		}
		elseif ($vul_level == 'Code-EconoMe'){
			if ($vul_mode == 'D'){
				// default mode
				$query .= "UPDATE ".$workspace.".".$mapping_index." 
					SET vul_obj_valeur = valeur_objet
					FROM ".$workspace.".vulnerability_default
					WHERE processus = '".$haz_type."'
					AND type = '".$object_type."'
					AND intensite_valeur = cin_lvin_1::integer
					AND code_econo::integer = code
					AND obj_niveau = 'Code-EconoMe';";
				
				$query .= "UPDATE ".$workspace.".".$mapping_index." 
					SET vul_humain_valeur = valeur_letalite
					FROM ".$workspace.".vulnerability_default
					WHERE processus = '".$haz_type."'
					AND type = '".$object_type."'
					AND intensite_valeur = cin_lvin_1::integer
					AND code_econo::integer = code
					AND obj_niveau = 'Code-EconoMe';";
			}
			else {
				// user defined mode
				$query .= "UPDATE ".$workspace.".".$mapping_index." 
					SET vul_obj_valeur = valeur_objet
					FROM ".$workspace.".".$vul_manual."
					WHERE processus = '".$haz_type."'
					AND type = '".$object_type."'
					AND intensite_valeur = cin_lvin_1::integer
					AND code_econo::integer = code
					AND obj_niveau = 'Code-EconoMe';";
				
				$query .= "UPDATE ".$workspace.".".$mapping_index." 
					SET vul_humain_valeur = valeur_letalite
					FROM ".$workspace.".".$vul_manual."
					WHERE processus = '".$haz_type."'
					AND type = '".$object_type."'
					AND intensite_valeur = cin_lvin_1::integer
					AND code_econo::integer = code
					AND obj_niveau = 'Code-EconoMe';";
			}
		}
		else{
			// if option 'Manual', update the vulnerability values accordingly to the exposed intensity level
			$query .= "UPDATE ".$workspace.".".$mapping_index." 
					SET vul_obj_valeur = vul_obj_f, 
					vul_humain_valeur = let_pers_f
					WHERE 1 = cin_lvin_1::integer;";
					
			$query .= "UPDATE ".$workspace.".".$mapping_index." 
					SET vul_obj_valeur = vul_obj_m, 
					vul_humain_valeur = let_pers_m
					WHERE 2 = cin_lvin_1::integer;";
			
			$query .= "UPDATE ".$workspace.".".$mapping_index." 
					SET vul_obj_valeur = vul_obj_h, 
					vul_humain_valeur = let_pers_h
					WHERE 3 = cin_lvin_1::integer;";
		}
		
		### query to populate the damages of the objects and people	
		if ($object_type == 'Buildings' || $object_type == 'Surfaces'){
			$query .= "UPDATE ".$workspace.".".$mapping_index." SET degats_obj = pra_valeur * vul_obj_valeur * montant_valeur,
			degats_humain = pra_valeur * vul_humain_valeur * humains_tot * humain_exp * 5000000;";
		}
		elseif ($object_type == 'Routes'){			
			$query .= "UPDATE ".$workspace.".".$mapping_index." SET vul_humain_definie = (vul_humain_valeur * longueur)/NULLIF(l_def_sce,0);";
			$query .= "UPDATE ".$workspace.".".$mapping_index." SET humain_exp = (tjm/24000)*((l_m + d_m)/vitesse),
			humain_exp_in = (passage/24000)*((4 + d_m)/vitesse);";
			$query .= "UPDATE ".$workspace.".".$mapping_index." SET degats_obj = montant_valeur * pra_valeur * vul_obj_valeur,
			degats_humain = humain_exp * vul_humain_definie * humains_tot * 5000000;"; 
		}			
		
		### query to populate the risk total of the objects and people
		if ($haz_type == 'GPP-TR'){ // validate unique combination in WHERE clause to update risk
			if ($object_type == 'Routes'){
				/* // to consider only the maximum intensity class when superposed in the risk calculation
				$query .= "UPDATE ".$workspace.".".$mapping_index." SET 
				risque_obj = ((temp.frequence - coalesce(temp.next_frequence, 0)) * degats_obj),
				risque_humain = ((temp.frequence - coalesce(temp.next_frequence, 0)) * degats_humain),				
				risque_individuel = ((temp.frequence - coalesce(temp.next_frequence, 0)) * humain_exp_in * vul_humain_definie * 1)
				FROM  (
				SELECT fid, rownum, gid, cin_sce_probabilite, longueur, cin_lvin_1, frequence, lead(frequence) OVER w AS next_frequence
				FROM ".$workspace.".".$mapping_index."				
				WINDOW w AS (
					PARTITION BY fid, ST_AsBinary(geom)
					ORDER BY fid ASC, frequence DESC, cin_lvin_1 ASC)
				) temp
				WHERE ".$workspace.".".$mapping_index.".fid = temp.fid
				AND ".$workspace.".".$mapping_index.".rownum = temp.rownum
				AND ".$workspace.".".$mapping_index.".longueur = temp.longueur 
				AND ".$workspace.".".$mapping_index.".cin_sce_probabilite = temp.cin_sce_probabilite;";	// longueur is important to update the next frequency properly */
				
				// to consider all intensity classes when superposed in the risk calculation
				$query .= "UPDATE ".$workspace.".".$mapping_index." SET 
				risque_obj = ((temp.frequence - coalesce(temp.next_frequence, 0)) * degats_obj),
				risque_humain = ((temp.frequence - coalesce(temp.next_frequence, 0)) * degats_humain),				
				risque_individuel = ((temp.frequence - coalesce(temp.next_frequence, 0)) * humain_exp_in * vul_humain_definie * 1)
				FROM  (
				SELECT fid, cin_sce_probabilite, longueur, frequence, lead(frequence) OVER w AS next_frequence
				FROM ".$workspace.".".$mapping_index."
				GROUP BY fid, cin_sce_probabilite, frequence, longueur, geom
				WINDOW w AS (
					PARTITION BY fid, ST_AsBinary(geom)
					ORDER BY fid ASC, frequence DESC)
				) temp
				WHERE ".$workspace.".".$mapping_index.".fid = temp.fid
				AND ".$workspace.".".$mapping_index.".longueur = temp.longueur 
				AND ".$workspace.".".$mapping_index.".cin_sce_probabilite = temp.cin_sce_probabilite;";		
			}
			elseif ($object_type == 'Surfaces'){
				// to consider all intensity classes when superposed in the risk calculation
				$query .= "UPDATE ".$workspace.".".$mapping_index." SET 
				risque_obj = ((temp.frequence - coalesce(temp.next_frequence, 0)) * degats_obj),
				risque_humain = ((temp.frequence - coalesce(temp.next_frequence, 0)) * degats_humain),				
				risque_individuel = ((temp.frequence - coalesce(temp.next_frequence, 0)) * pra_valeur * vul_humain_valeur * humain_exp)
				FROM  (
				SELECT fid, cin_sce_probabilite, area, frequence, lead(frequence) OVER w AS next_frequence
				FROM ".$workspace.".".$mapping_index."
				GROUP BY fid, cin_sce_probabilite, frequence, area, geom
				WINDOW w AS (
					PARTITION BY fid, ST_AsBinary(geom)
					ORDER BY fid ASC, frequence DESC)
				) temp
				WHERE ".$workspace.".".$mapping_index.".fid = temp.fid
				AND ".$workspace.".".$mapping_index.".area = temp.area 
				AND ".$workspace.".".$mapping_index.".cin_sce_probabilite = temp.cin_sce_probabilite;";	
			}
			else {
				/* // to consider only the maximum intensity class when superposed in the risk calculation
				$query .= "UPDATE ".$workspace.".".$mapping_index." SET 
					risque_obj = ((temp.frequence - coalesce(temp.next_frequence, 0)) * degats_obj),
					risque_humain = ((temp.frequence - coalesce(temp.next_frequence, 0)) * degats_humain),
					risque_individuel = ((temp.frequence - coalesce(temp.next_frequence, 0)) * pra_valeur * vul_humain_valeur * humain_exp)
					FROM  (
					select egid, rownum, gid, cin_sce_probabilite, frequence, lead(frequence) OVER w as next_frequence
					from ".$workspace.".".$mapping_index."
					WINDOW w AS (
							PARTITION BY egid
							ORDER BY egid asc, frequence desc, cin_lvin_1 asc
						)
					) temp
					WHERE ".$mapping_index.".egid = temp.egid AND ".$mapping_index.".rownum = temp.rownum;"; */
				
				// to consider all intensity classes when superposed in the risk calculation	
				$query .= "UPDATE ".$workspace.".".$mapping_index." SET 
					risque_obj = ((temp.frequence - coalesce(temp.next_frequence, 0)) * degats_obj),
					risque_humain = ((temp.frequence - coalesce(temp.next_frequence, 0)) * degats_humain),
					risque_individuel = ((temp.frequence - coalesce(temp.next_frequence, 0)) * pra_valeur * vul_humain_valeur * humain_exp)
					FROM  (
					select egid, cin_sce_probabilite, frequence, lead(frequence) OVER w as next_frequence
					from ".$workspace.".".$mapping_index."
					GROUP BY egid, cin_sce_probabilite, frequence
					WINDOW w AS (
							PARTITION BY egid
							ORDER BY egid asc, frequence desc
						)
					) temp
					WHERE ".$mapping_index.".egid = temp.egid AND ".$mapping_index.".cin_sce_probabilite = temp.cin_sce_probabilite;";
			}			
		}
		else{
			if ($object_type == 'Routes'){
				/* SELECT fid, cin_sce_probabilite, cin_lvin_1, frequence, lead(frequence) OVER w AS next_frequence, *
				FROM test_cpb_routes.risques_cpb_routes
				WINDOW w AS (
				PARTITION BY fid, ST_AsBinary(geom)
				ORDER BY fid ASC, frequence DESC, cin_lvin_1::integer ASC) */
				
				/* // to consider only the maximum intensity class when superposed in the risk calculation
				$query .= "UPDATE ".$workspace.".".$mapping_index." SET 
				risque_obj = ((temp.frequence - coalesce(temp.next_frequence, 0)) * degats_obj),
				risque_humain = ((temp.frequence - coalesce(temp.next_frequence, 0)) * degats_humain),				
				risque_individuel = ((temp.frequence - coalesce(temp.next_frequence, 0)) * humain_exp_in * vul_humain_definie * 1)
				FROM  (
				SELECT fid, gid, cin_sce_probabilite, longueur, cin_lvin_1, frequence, lead(frequence) OVER w AS next_frequence
				FROM ".$workspace.".".$mapping_index."				
				WINDOW w AS (
					PARTITION BY fid, ST_AsBinary(geom)
					ORDER BY fid ASC, frequence DESC, cin_lvin_1::integer ASC)
				) temp
				WHERE ".$workspace.".".$mapping_index.".fid = temp.fid
				AND ".$workspace.".".$mapping_index.".gid = temp.gid
				AND ".$workspace.".".$mapping_index.".longueur = temp.longueur 
				AND ".$workspace.".".$mapping_index.".cin_sce_probabilite = temp.cin_sce_probabilite;";	// longueur is important to update the next frequency properly */
				
				// to consider all intensity classes when superposed in the risk calculation
				$query .= "UPDATE ".$workspace.".".$mapping_index." SET 
				risque_obj = ((temp.frequence - coalesce(temp.next_frequence, 0)) * degats_obj),
				risque_humain = ((temp.frequence - coalesce(temp.next_frequence, 0)) * degats_humain),				
				risque_individuel = ((temp.frequence - coalesce(temp.next_frequence, 0)) * humain_exp_in * vul_humain_definie * 1)
				FROM  (
				SELECT fid, cin_sce_probabilite, longueur, frequence, lead(frequence) OVER w AS next_frequence
				FROM ".$workspace.".".$mapping_index."
				GROUP BY fid, cin_sce_probabilite, frequence, longueur, geom
				WINDOW w AS (
					PARTITION BY fid, ST_AsBinary(geom)
					ORDER BY fid ASC, frequence DESC)
				) temp
				WHERE ".$workspace.".".$mapping_index.".fid = temp.fid
				AND ".$workspace.".".$mapping_index.".longueur = temp.longueur 
				AND ".$workspace.".".$mapping_index.".cin_sce_probabilite = temp.cin_sce_probabilite;";		
			}
			elseif ($object_type == 'Surfaces'){
				// to consider all intensity classes when superposed in the risk calculation
				$query .= "UPDATE ".$workspace.".".$mapping_index." SET 
				risque_obj = ((temp.frequence - coalesce(temp.next_frequence, 0)) * degats_obj),
				risque_humain = ((temp.frequence - coalesce(temp.next_frequence, 0)) * degats_humain),				
				risque_individuel = ((temp.frequence - coalesce(temp.next_frequence, 0)) * pra_valeur * vul_humain_valeur * humain_exp)
				FROM  (
				SELECT fid, cin_sce_probabilite, area, frequence, lead(frequence) OVER w AS next_frequence
				FROM ".$workspace.".".$mapping_index."
				GROUP BY fid, cin_sce_probabilite, frequence, area, geom
				WINDOW w AS (
					PARTITION BY fid, ST_AsBinary(geom)
					ORDER BY fid ASC, frequence DESC)
				) temp
				WHERE ".$workspace.".".$mapping_index.".fid = temp.fid
				AND ".$workspace.".".$mapping_index.".area = temp.area 
				AND ".$workspace.".".$mapping_index.".cin_sce_probabilite = temp.cin_sce_probabilite;";	
			}
			else {
				/* // to consider only the maximum intensity class when superposed in the risk calculation
				$query .= "UPDATE ".$workspace.".".$mapping_index." SET 
					risque_obj = ((temp.frequence - coalesce(temp.next_frequence, 0)) * degats_obj),
					risque_humain = ((temp.frequence - coalesce(temp.next_frequence, 0)) * degats_humain),
					risque_individuel = ((temp.frequence - coalesce(temp.next_frequence, 0)) * pra_valeur * vul_humain_valeur * humain_exp)
					FROM  (
					select egid, objectid, cin_sce_probabilite, cin_lvin_1, frequence, lead(frequence) OVER w as next_frequence
					from ".$workspace.".".$mapping_index."
					WINDOW w AS (
							PARTITION BY egid
							ORDER BY egid asc, frequence desc, cin_lvin_1::integer asc
						)
					) temp
					WHERE ".$mapping_index.".egid = temp.egid AND ".$mapping_index.".objectid = temp.objectid;"; */
					
				// to consider all intensity classes when superposed in the risk calculation	
				$query .= "UPDATE ".$workspace.".".$mapping_index." SET 
					risque_obj = ((temp.frequence - coalesce(temp.next_frequence, 0)) * degats_obj),
					risque_humain = ((temp.frequence - coalesce(temp.next_frequence, 0)) * degats_humain),
					risque_individuel = ((temp.frequence - coalesce(temp.next_frequence, 0)) * pra_valeur * vul_humain_valeur * humain_exp)
					FROM  (
					select egid, cin_sce_probabilite, frequence, lead(frequence) OVER w as next_frequence
					from ".$workspace.".".$mapping_index."
					GROUP BY egid, cin_sce_probabilite, frequence
					WINDOW w AS (
							PARTITION BY egid
							ORDER BY egid asc, frequence desc
						)
					) temp
					WHERE ".$mapping_index.".egid = temp.egid AND ".$mapping_index.".cin_sce_probabilite = temp.cin_sce_probabilite;";
			}
		}
		
		$query .= "UPDATE ".$workspace.".".$mapping_index." SET risque_tot = COALESCE(risque_obj,0) + COALESCE(risque_humain,0);";

		### update the risk table for the records of intensity classes 4 and 9 (and 999 for GPP-TR)
		### update a set of risk rows for which at least one row per set meets the condition (intensity classes 4 and 9 (and 999 for GPP-TR)) to have same output results in R_TRs and R_Tot
		if ($object_type == 'Routes') {
			$query .= "UPDATE ".$workspace.".".$mapping_index." SET risque_obj = 999, risque_humain = 999, risque_individuel = 999, risque_tot = 999
			WHERE (objectid, longueur, ST_AsBinary(geom)) 
			IN (SELECT DISTINCT objectid, longueur, ST_AsBinary(geom) 
			FROM ".$workspace.".".$mapping_index." 
			WHERE cin_lvin_1 = '-4' OR cin_lvin_1 = '-9' OR cin_lvin_1 = '999');";
		}		
		elseif ($object_type == 'Surfaces'){
			$query .= "UPDATE ".$workspace.".".$mapping_index." SET risque_obj = 999, risque_humain = 999, risque_individuel = 999, risque_tot = 999
			WHERE (objectid, area, ST_AsBinary(geom)) 
			IN (SELECT DISTINCT objectid, area, ST_AsBinary(geom) 
			FROM ".$workspace.".".$mapping_index." 
			WHERE cin_lvin_1 = '-4' OR cin_lvin_1 = '-9' OR cin_lvin_1 = '999');";
		}
		else {
			$query .= "UPDATE ".$workspace.".".$mapping_index." SET risque_obj = 999, risque_humain = 999, risque_individuel = 999, risque_tot = 999
			WHERE egid IN (SELECT DISTINCT egid FROM ".$workspace.".".$mapping_index." WHERE cin_lvin_1 = '-4' OR cin_lvin_1 = '-9' OR cin_lvin_1 = '999');";
		}
		
		### begin of adding a record to the risk scenario table
		$query .= "INSERT INTO ".$workspace.".risk_scenarios (id, nom, description, alt_id, processus_type, pra_id, objet_id, objet_type, objet_niveau, objet_para_id, vul_niveau, vul_id, indice, objet_total, humain_total, risque_total, risque_individuel, option, poly_texte)
			VALUES (DEFAULT, '$scenario_name', '$scenario_description', $alt_scenario_id, '$haz_type',";
		
		if ($spatial_prob == 'D'){ $query .= "(SELECT id FROM ".$workspace.".spatial_probability WHERE type = 'default'),"; }
		else { $query .= "(SELECT id FROM ".$workspace.".spatial_probability WHERE indice = '$spatial_prob_manual'),";}
		
		$query .= "(SELECT id FROM ".$workspace.".objects WHERE indice = '$object_name'), '$object_type', '$object_level', "; 
		
		if ($object_mode == 'D'){ $query .= "(SELECT id FROM ".$workspace.".objects_parameters WHERE type = 'default' AND obj_type = '$object_type' AND obj_niveau = '$object_level'),";}
		elseif ($object_mode == 'M') { $query .= "(SELECT id FROM ".$workspace.".objects_parameters WHERE indice = '$object_manual'),";}
		else { $query .= "NULL, ";}
		
		$query .= "'$vul_level', ";
		
		if ($vul_mode == 'D'){ $query .= "(SELECT id FROM ".$workspace.".vulnerability WHERE type = 'default' AND processus_type = '$haz_type' AND obj_type = '$object_type' AND obj_niveau = '$vul_level'),'$mapping_index', ";}
		elseif ($vul_mode == 'M') { $query .= "(SELECT id FROM ".$workspace.".vulnerability WHERE indice = '$vul_manual'),'$mapping_index', "; }
		else { $query .= "NULL, '$mapping_index', ";}
		
	//	$query .= "DEFAULT, DEFAULT, DEFAULT, DEFAULT);";
	
		if ($object_type == 'Routes') {
			$query .= "(SELECT COALESCE(sum(risque_obj),0) FROM ".$workspace.".".$mapping_index." 
					WHERE (objectid, longueur, ST_AsBinary(geom)) 
					NOT IN (SELECT DISTINCT objectid, longueur, ST_AsBinary(geom) 
					FROM ".$workspace.".".$mapping_index." 
					WHERE cin_lvin_1 = '-4' OR cin_lvin_1 = '-9' OR cin_lvin_1 = '999')), 
					(SELECT COALESCE(sum(risque_humain),0) FROM ".$workspace.".".$mapping_index." 
					WHERE (objectid, longueur, ST_AsBinary(geom)) 
					NOT IN (SELECT DISTINCT objectid, longueur, ST_AsBinary(geom) 
					FROM ".$workspace.".".$mapping_index." 
					WHERE cin_lvin_1 = '-4' OR cin_lvin_1 = '-9' OR cin_lvin_1 = '999')), 
					(SELECT COALESCE(sum(risque_tot),0) FROM ".$workspace.".".$mapping_index." 
					WHERE (objectid, longueur, ST_AsBinary(geom)) 
					NOT IN (SELECT DISTINCT objectid, longueur, ST_AsBinary(geom) 
					FROM ".$workspace.".".$mapping_index." 
					WHERE cin_lvin_1 = '-4' OR cin_lvin_1 = '-9' OR cin_lvin_1 = '999')), 
					(SELECT COALESCE(sum(risque_individuel),0) FROM ".$workspace.".".$mapping_index." 
					WHERE (objectid, longueur, ST_AsBinary(geom)) 
					NOT IN (SELECT DISTINCT objectid, longueur, ST_AsBinary(geom) 
					FROM ".$workspace.".".$mapping_index." 
					WHERE cin_lvin_1 = '-4' OR cin_lvin_1 = '-9' OR cin_lvin_1 = '999')), '$option','$area');";
		}
		elseif ($object_type == 'Surfaces') {
			$query .= "(SELECT COALESCE(sum(risque_obj),0) FROM ".$workspace.".".$mapping_index." 
					WHERE (objectid, area, ST_AsBinary(geom)) 
					NOT IN (SELECT DISTINCT objectid, area, ST_AsBinary(geom) 
					FROM ".$workspace.".".$mapping_index." 
					WHERE cin_lvin_1 = '-4' OR cin_lvin_1 = '-9' OR cin_lvin_1 = '999')), 
					(SELECT COALESCE(sum(risque_humain),0) FROM ".$workspace.".".$mapping_index." 
					WHERE (objectid, area, ST_AsBinary(geom)) 
					NOT IN (SELECT DISTINCT objectid, area, ST_AsBinary(geom) 
					FROM ".$workspace.".".$mapping_index." 
					WHERE cin_lvin_1 = '-4' OR cin_lvin_1 = '-9' OR cin_lvin_1 = '999')), 
					(SELECT COALESCE(sum(risque_tot),0) FROM ".$workspace.".".$mapping_index." 
					WHERE (objectid, area, ST_AsBinary(geom)) 
					NOT IN (SELECT DISTINCT objectid, area, ST_AsBinary(geom) 
					FROM ".$workspace.".".$mapping_index." 
					WHERE cin_lvin_1 = '-4' OR cin_lvin_1 = '-9' OR cin_lvin_1 = '999')), 
					(SELECT COALESCE(sum(risque_individuel),0) FROM ".$workspace.".".$mapping_index." 
					WHERE (objectid, area, ST_AsBinary(geom)) 
					NOT IN (SELECT DISTINCT objectid, area, ST_AsBinary(geom) 
					FROM ".$workspace.".".$mapping_index." 
					WHERE cin_lvin_1 = '-4' OR cin_lvin_1 = '-9' OR cin_lvin_1 = '999')), '$option','$area');";
		}
		else {
			$query .= "(SELECT COALESCE(sum(risque_obj),0) FROM ".$workspace.".".$mapping_index." 
					WHERE egid
					NOT IN (SELECT DISTINCT egid
					FROM ".$workspace.".".$mapping_index." 
					WHERE cin_lvin_1 = '-4' OR cin_lvin_1 = '-9' OR cin_lvin_1 = '999')), 
					(SELECT COALESCE(sum(risque_humain),0) FROM ".$workspace.".".$mapping_index." 
					WHERE egid
					NOT IN (SELECT DISTINCT egid 
					FROM ".$workspace.".".$mapping_index." 
					WHERE cin_lvin_1 = '-4' OR cin_lvin_1 = '-9' OR cin_lvin_1 = '999')), 
					(SELECT COALESCE(sum(risque_tot),0) FROM ".$workspace.".".$mapping_index." 
					WHERE egid
					NOT IN (SELECT DISTINCT egid 
					FROM ".$workspace.".".$mapping_index." 
					WHERE cin_lvin_1 = '-4' OR cin_lvin_1 = '-9' OR cin_lvin_1 = '999')), 
					(SELECT COALESCE(sum(risque_individuel),0) FROM ".$workspace.".".$mapping_index." 
					WHERE egid 
					NOT IN (SELECT DISTINCT egid
					FROM ".$workspace.".".$mapping_index." 
					WHERE cin_lvin_1 = '-4' OR cin_lvin_1 = '-9' OR cin_lvin_1 = '999')), '$option','$area');";
		}
		
		### end of adding a record to the risk scenario table ###
		
		### update the risk table for the records of intensity classes 4 and 9 (and 999 for GPP-TR)
	//	$query .= "UPDATE ".$workspace.".".$mapping_index." SET risque_obj = 999, risque_humain = 999, risque_individuel = 999, risque_tot = 999 WHERE cin_lvin_1 = '-4' OR cin_lvin_1 = '-9' OR cin_lvin_1 = '999';";
		
		### adding records to the relationship table
		for ($i = 0; $i < count($rpArr); $i++) {
			$a = $rpArr[$i][2];
			$b = $rpArr[$i][0];
			$c = $rpArr[$i][1];
			$query .= "INSERT INTO ".$workspace.".risk_scenarios_hazards VALUES (DEFAULT,(SELECT id FROM ".$workspace.".risk_scenarios WHERE indice = '$mapping_index'),'$a','$b','$c');";
		}		
		
		/* $query .= "UPDATE ".$workspace.".risk_scenarios 
				SET objet_total = (SELECT sum(risque_obj) FROM ".$workspace.".".$mapping_index."), 
				humain_total = (SELECT sum(risque_humain) FROM ".$workspace.".".$mapping_index."),
				risque_total = (SELECT sum(risque_obj)+ sum(risque_humain) FROM ".$workspace.".".$mapping_index."),
				risque_individuel = (SELECT sum(risque_individuel) FROM ".$workspace.".".$mapping_index.")
				WHERE indice = '$mapping_index';";	 */	
		
	//	echo $query; 
		
		If (!$rs = pg_query($dbconn,$query)) {
				Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
				exit;
			}
		else {
			// if success, arrives here			
			# publish the extracted (simplied) view on the calculated risk table to the geoserver
			
			// Initiate cURL session
			$service = "http://localhost:8080/geoserver/"; // replace with your URL
			$request = "rest/workspaces/".$workspace."/datastores/".$datastore."/featuretypes"; // to add a new featuretype

			$url = $service . $request;
			$ch = curl_init($url);
			
			// Optional settings for debugging
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //option to return string
			curl_setopt($ch, CURLOPT_VERBOSE, true);
			curl_setopt($ch, CURLOPT_STDERR, $logfh); // logs curl messages

			//Required POST request settings
			curl_setopt($ch, CURLOPT_POST, True);
			$passwordStr = "username:mypassword"; // replace with your GeoServer username and password
			curl_setopt($ch, CURLOPT_USERPWD, $passwordStr);
			
			//POST data 
			curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: application/xml"));
			
			//POST return code
			$successCode = 201;	
			
			for ($i = 0; $i < count($rpArr); $i++) {
				$temp = $rpArr[$i][2];
				if ($rpArr[$i][0] != 'None') {
					if ($object_type == 'Buildings'){
					//	$xmlStr = "<featureType><name>".$rpArr[$i][1]."</name><title>".$rpArr[$i][1]."</title><metadata><entry key='JDBC_VIRTUAL_TABLE'><virtualTable><name>".$rpArr[$i][1]."</name><sql>WITH t1 AS (SELECT egid, count(*) as nb_int, sum(risque_tot) as sum_risque_tot FROM ".$workspace.".".$mapping_index." WHERE cin_sce_probabilite = '$temp' GROUP BY egid ORDER BY egid asc) (SELECT t2.*, t1.nb_int FROM t1, ".$workspace.".".$mapping_index." t2 WHERE t1.egid = t2.egid AND t2.cin_sce_probabilite = '$temp' AND t2.risque_tot != 0 AND t1.sum_risque_tot != 0 AND t1.nb_int > 1) UNION (SELECT t2.*, t1.nb_int FROM t1, ".$workspace.".".$mapping_index." t2 WHERE t1.egid = t2.egid AND t2.cin_sce_probabilite = '$temp' AND t1.nb_int = 1) UNION (SELECT distinct on (t2.egid) t2.*, t1.nb_int FROM t1 JOIN ".$workspace.".".$mapping_index." t2 ON (t1.egid = t2.egid) WHERE t2.cin_sce_probabilite = '$temp' AND t1.sum_risque_tot = 0 AND t1.nb_int > 1) </sql><geometry><name>geom</name><type>Geometry</type><srid>4326</srid></geometry></virtualTable></entry></metadata></featureType>";
						$xmlStr = "<featureType><name>".$rpArr[$i][1]."</name><title>".$rpArr[$i][1]."</title><metadata><entry key='JDBC_VIRTUAL_TABLE'><virtualTable><name>".$rpArr[$i][1]."</name><sql>SELECT *, COUNT(*) OVER(PARTITION BY egid) AS nb_couches  FROM ".$workspace.".".$mapping_index." WHERE cin_sce_probabilite = '$temp' </sql><geometry><name>geom</name><type>Geometry</type><srid>2056</srid></geometry></virtualTable></entry></metadata></featureType>";
					}
					elseif ($object_type == 'Routes'){
						/* (SELECT distinct on (t2.geom) t2.*, count(*) over(partition by t2.geom) as nb_int 
						FROM test_cpb_routes.risques_cpb_routes t2 
						WHERE t2.cin_sce_probabilite = 'T30' 
						ORDER BY t2.geom, t2.longueur ASC, cin_lvin_1 DESC); */
						/* (SELECT * FROM
						(SELECT t.*, ROW_NUMBER() OVER(PARTITION BY fid, cin_sce_probabilite, ST_AsBinary(geom) ORDER BY cin_lvin_1::integer DESC) AS row, COUNT(*) OVER(PARTITION BY fid, cin_sce_probabilite, ST_AsBinary(geom)) AS nb_int
						FROM test_cpb_routes.risques_cpb_routes t) dups
						WHERE dups.row = 1
						AND cin_sce_probabilite = 'T30'); */
					//	$xmlStr = "<featureType><name>".$rpArr[$i][1]."</name><title>".$rpArr[$i][1]."</title><metadata><entry key='JDBC_VIRTUAL_TABLE'><virtualTable><name>".$rpArr[$i][1]."</name><sql>SELECT * FROM (SELECT t.*, ROW_NUMBER() OVER(PARTITION BY fid, cin_sce_probabilite, ST_AsBinary(geom) ORDER BY cin_lvin_1::integer DESC) AS row, COUNT(*) OVER(PARTITION BY fid, cin_sce_probabilite, ST_AsBinary(geom)) AS nb_int FROM ".$workspace.".".$mapping_index." t) dups WHERE dups.row = 1 AND cin_sce_probabilite = '$temp' </sql><geometry><name>geom</name><type>MultiLineString</type><srid>4326</srid></geometry></virtualTable></entry></metadata></featureType>";
						$xmlStr = "<featureType><name>".$rpArr[$i][1]."</name><title>".$rpArr[$i][1]."</title><metadata><entry key='JDBC_VIRTUAL_TABLE'><virtualTable><name>".$rpArr[$i][1]."</name><sql>SELECT *, COUNT(*) OVER(PARTITION BY fid, ST_AsBinary(geom)) AS nb_couches FROM ".$workspace.".".$mapping_index." WHERE cin_sce_probabilite = '$temp' </sql><geometry><name>geom</name><type>MultiLineString</type><srid>2056</srid></geometry></virtualTable></entry></metadata></featureType>";
					}
					else {
						$xmlStr = "<featureType><name>".$rpArr[$i][1]."</name><title>".$rpArr[$i][1]."</title><metadata><entry key='JDBC_VIRTUAL_TABLE'><virtualTable><name>".$rpArr[$i][1]."</name><sql>SELECT *, COUNT(*) OVER(PARTITION BY fid, ST_AsBinary(geom)) AS nb_couches FROM ".$workspace.".".$mapping_index." WHERE cin_sce_probabilite = '$temp' </sql><geometry><name>geom</name><type>MultiPolygon</type><srid>2056</srid></geometry></virtualTable></entry></metadata></featureType>";
					}
					
					curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlStr);
					$buffer = curl_exec($ch);					
					$info = curl_getinfo($ch);
					if ($info['http_code'] != $successCode) {
						$msgStr .= "# Unsuccessful cURL request to ";
						$msgStr .= $url." [". $info['http_code']. "]\n"; 
					} 
				}
			}
		
			if ($object_type == 'Buildings'){
				$xmlStr = "<featureType><name>".$mapping_index."</name><title>".$scenario_name."</title><metadata><entry key='JDBC_VIRTUAL_TABLE'><virtualTable><name>".$mapping_index."</name><sql>(SELECT egid, sum(risque_obj) as total_obj, sum(risque_humain) as total_humain, sum(risque_tot) as total_risque, sum(risque_individuel) as total_individuel, geom, COUNT(*) AS nb_couches FROM ".$workspace.".".$mapping_index." WHERE egid NOT IN (SELECT DISTINCT egid FROM ".$workspace.".".$mapping_index." WHERE cin_lvin_1 = '-4' OR cin_lvin_1 = '-9' OR cin_lvin_1 = '999') GROUP BY egid, geom) UNION (SELECT egid, 999 as total_obj, 999 as total_humain, 999 as total_risque, 999 as total_individuel, geom, COUNT(*) AS nb_couches FROM ".$workspace.".".$mapping_index." WHERE egid IN (SELECT DISTINCT egid FROM ".$workspace.".".$mapping_index." WHERE cin_lvin_1 = '-4' OR cin_lvin_1 = '-9' OR cin_lvin_1 = '999') GROUP BY egid, geom) </sql><geometry><name>geom</name><type>Geometry</type><srid>2056</srid></geometry></virtualTable></entry></metadata></featureType>";				
			}
			elseif ($object_type == 'Routes'){
				$xmlStr = "<featureType><name>".$mapping_index."</name><title>".$scenario_name."</title><metadata><entry key='JDBC_VIRTUAL_TABLE'><virtualTable><name>".$mapping_index."</name><sql>(SELECT objectid, longueur, sum(risque_obj) as total_obj, sum(risque_humain) as total_humain, sum(risque_tot) as total_risque, sum(risque_individuel) as total_individuel, geom, COUNT(*) AS nb_couches FROM ".$workspace.".".$mapping_index." WHERE (objectid, longueur, ST_AsBinary(geom)) NOT IN (SELECT DISTINCT objectid, longueur, ST_AsBinary(geom) FROM ".$workspace.".".$mapping_index." WHERE cin_lvin_1 = '-4' OR cin_lvin_1 = '-9' OR cin_lvin_1 = '999') GROUP BY objectid, longueur, geom HAVING count(cin_sce_probabilite) != 0) UNION (SELECT objectid, longueur, 999 as total_obj, 999 as total_humain, 999 as total_risque, 999 as total_individuel, geom, COUNT(*) AS nb_couches  FROM ".$workspace.".".$mapping_index." WHERE (objectid, longueur, ST_AsBinary(geom)) IN (SELECT DISTINCT objectid, longueur, ST_AsBinary(geom) FROM ".$workspace.".".$mapping_index." WHERE cin_lvin_1 = '-4' OR cin_lvin_1 = '-9' OR cin_lvin_1 = '999') GROUP BY objectid, longueur, geom) </sql><geometry><name>geom</name><type>MultiLineString</type><srid>2056</srid></geometry></virtualTable></entry></metadata></featureType>";
				$xmlStr_v2 = "<featureType><name>".$mapping_index."_sum"."</name><title>".$scenario_name."_sum"."</title><metadata><entry key='JDBC_VIRTUAL_TABLE'><virtualTable><name>".$mapping_index."_sum"."</name><sql>WITH t1 AS (SELECT objectid, sum(risque_obj) as total_obj, sum(risque_humain) as total_humain, sum(risque_tot) as total_risque, sum(risque_individuel) as total_individuel FROM ".$workspace.".".$mapping_index." WHERE objectid NOT IN (SELECT DISTINCT objectid FROM ".$workspace.".".$mapping_index." WHERE cin_lvin_1 = '-4' OR cin_lvin_1 = '-9' OR cin_lvin_1 = '999') GROUP BY objectid), t2 AS (SELECT objectid, 999 as total_obj, 999 as total_humain, 999 as total_risque, 999 as total_individuel FROM ".$workspace.".".$mapping_index." WHERE objectid IN (SELECT DISTINCT objectid FROM ".$workspace.".".$mapping_index." WHERE cin_lvin_1 = '-4' OR cin_lvin_1 = '-9' OR cin_lvin_1 = '999') GROUP BY objectid), t3 AS (SELECT * FROM t1 UNION SELECT * FROM t2) SELECT t.*, t3.* FROM t3 RIGHT JOIN ".$workspace.".".'"'.$object_name.'"'." t ON t3.objectid = t.".'"OBJECTID"'.' WHERE t."CODE_ECONO" != 0'."</sql><geometry><name>the_geom</name><type>MultiLineString</type><srid>2056</srid></geometry></virtualTable></entry></metadata></featureType>";
				
				/* SELECT test_cpb.routes_chardonne_proj.*, coalesce(risque_tot_sum.TOTAL_OBJ,0) AS TOTAL_OBJ, coalesce(risque_tot_sum.TOTAL_HUMA,0) AS TOTAL_HUMA, coalesce(risque_tot_sum.TOTAL_RISQ,0) AS TOTAL_RISQ, coalesce(risque_tot_sum.TOTAL_INDI,0) AS TOTAL_INDI
				FROM (SELECT objectid, SUM(total_obj) AS TOTAL_OBJ, SUM(total_humain) AS TOTAL_HUMA, SUM(total_risque) AS TOTAL_RISQ, SUM(total_individuel) AS TOTAL_INDI
				FROM ((SELECT objectid, longueur, sum(risque_obj) as total_obj, sum(risque_humain) as total_humain, sum(risque_tot) as total_risque, sum(risque_individuel) as total_individuel, geom, COUNT(*) AS nb_couches FROM test_cpb.risques_cpb_routes WHERE (objectid, longueur, ST_AsBinary(geom)) NOT IN (SELECT DISTINCT objectid, longueur, ST_AsBinary(geom) FROM test_cpb.risques_cpb_routes WHERE cin_lvin_1 = '-4' OR cin_lvin_1 = '-9') GROUP BY objectid, longueur, geom HAVING count(cin_sce_probabilite) != 0) 
				UNION 
				(SELECT objectid, longueur, 999 as total_obj, 999 as total_humain, 999 as total_risque, 999 as total_individuel, geom, COUNT(*) AS nb_couches  FROM test_cpb.risques_cpb_routes WHERE (objectid, longueur, ST_AsBinary(geom)) IN (SELECT DISTINCT objectid, longueur, ST_AsBinary(geom) FROM test_cpb.risques_cpb_routes WHERE cin_lvin_1 = '-4' OR cin_lvin_1 = '-9') GROUP BY objectid, longueur, geom)) AS risque_tot
				GROUP BY objectid) AS risque_tot_sum RIGHT JOIN test_cpb.routes_chardonne_proj
				ON risque_tot_sum.objectid = test_cpb.routes_chardonne_proj."OBJECTID" */
			//	$xmlStr = "<featureType><name>".$mapping_index."</name><title>".$scenario_name."</title><metadata><entry key='JDBC_VIRTUAL_TABLE'><virtualTable><name>".$mapping_index."</name><sql>(WITH t AS (SELECT objectid, categorie, longueur, geom FROM ".$workspace.".".$mapping_index." EXCEPT (SELECT DISTINCT objectid, categorie, longueur, geom FROM ".$workspace.".".$mapping_index." WHERE cin_lvin_1 = '-4' OR cin_lvin_1 = '-9')) SELECT t.objectid, t.categorie, t.longueur, sum(risque_obj) as total_obj, sum(risque_humain) as total_humain, sum(risque_tot) as total_risque, sum(risque_individuel) as total_individuel, t.geom FROM t, ".$workspace.".".$mapping_index." t2 WHERE t.objectid = t2.objectid AND t.longueur = t2.longueur AND t.geom = t2.geom GROUP BY t.objectid, t.categorie, t.longueur, t.geom HAVING count(cin_sce_probabilite) != 0) UNION (SELECT DISTINCT objectid, categorie, longueur, 999 as total_obj, 999 as total_humain, 999 as total_risque, 999 as total_individuel, geom FROM ".$workspace.".".$mapping_index." WHERE cin_lvin_1 = '-4' OR cin_lvin_1 = '-9') </sql><geometry><name>geom</name><type>MultiLineString</type><srid>4326</srid></geometry></virtualTable></entry></metadata></featureType>";
			}
			else {
				$xmlStr = "<featureType><name>".$mapping_index."</name><title>".$scenario_name."</title><metadata><entry key='JDBC_VIRTUAL_TABLE'><virtualTable><name>".$mapping_index."</name><sql>(SELECT objectid, area, sum(risque_obj) as total_obj, sum(risque_humain) as total_humain, sum(risque_tot) as total_risque, sum(risque_individuel) as total_individuel, geom, COUNT(*) AS nb_couches FROM ".$workspace.".".$mapping_index." WHERE (objectid, area, ST_AsBinary(geom)) NOT IN (SELECT DISTINCT objectid, area, ST_AsBinary(geom) FROM ".$workspace.".".$mapping_index." WHERE cin_lvin_1 = '-4' OR cin_lvin_1 = '-9' OR cin_lvin_1 = '999') GROUP BY objectid, area, geom HAVING count(cin_sce_probabilite) != 0) UNION (SELECT objectid, area, 999 as total_obj, 999 as total_humain, 999 as total_risque, 999 as total_individuel, geom, COUNT(*) AS nb_couches  FROM ".$workspace.".".$mapping_index." WHERE (objectid, area, ST_AsBinary(geom)) IN (SELECT DISTINCT objectid, area, ST_AsBinary(geom) FROM ".$workspace.".".$mapping_index." WHERE cin_lvin_1 = '-4' OR cin_lvin_1 = '-9' OR cin_lvin_1 = '999') GROUP BY objectid, area, geom) </sql><geometry><name>geom</name><type>MultiPolygon</type><srid>2056</srid></geometry></virtualTable></entry></metadata></featureType>";
			}
			
			curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlStr);

			// Execute the curl request
			$buffer = curl_exec($ch); 				
			
			// Check for errors and process results
			$info = curl_getinfo($ch);
			if ($info['http_code'] != $successCode) {
				$msgStr .= "# Unsuccessful cURL request to ";
				$msgStr .= $url." [". $info['http_code']. "]\n";
				Echo '{success:false,message:'.json_encode($msgStr).'}';
			} else {
				// Execute the curl request for the second version of the risk total for the routes
				if ($object_type == 'Routes'){
					curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlStr_v2);
					$buffer = curl_exec($ch); 	
					$info = curl_getinfo($ch);
					if ($info['http_code'] != $successCode) {
						$msgStr .= "# Unsuccessful cURL request to ";
						$msgStr .= $url." [". $info['http_code']. "]\n";
						Echo '{success:false,message:'.json_encode($msgStr).'}';
					} else {
						$msgStr .= "Calculation completed and added the resulted risk total layer to the map!";
						# send the message back to the user
						Echo '{success: true, mpIndex:'.json_encode($mapping_index).',message:'.json_encode($msgStr).'}';
					}
				}
				else {
					$msgStr .= "Calculation completed and added the resulted risk total layer to the map!";
					# send the message back to the user
					Echo '{success: true, mpIndex:'.json_encode($mapping_index).',message:'.json_encode($msgStr).'}';
				}				
			}
			curl_close($ch); // free resources if curl handle will not be reused
		}
 
	}
	
	pg_close($dbconn);	
?>	