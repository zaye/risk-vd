<?php
	$workspace = $_POST['ws'];
	$task = $_POST['task'];	
	$datastore = $workspace; // postGIS datastore of the GeoServer (same name as workspace)
		
	$dbconn=pg_connect("host=localhost port=5432 dbname=dbname user=username password=mypassword");	// replace with your db connection information
	if (!$dbconn){
		echo "An error occured.\n";
		exit;
	}

	if ($task == 'load') {
		$query = "SELECT * FROM ".$workspace.".alternatives";
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	if ($task == 'loadGrid') {
		$query = "SELECT alternatives.*, string_agg(measures.ouvrage,',') AS mesures_nom, string_agg(cast(measures.id as text),',') AS mesures_id
			FROM ".$workspace.".alternatives, ".$workspace.".alternatives_measures, ".$workspace.".measures
			WHERE alternatives_measures.mes_id = measures.id
			AND alternatives.id = alternatives_measures.alt_id
			GROUP BY alternatives.id;";

		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	if ($task == 'add') {
		// retrieve POST data 
		$alternative_name = $_POST['name'];
		$alternative_description = $_POST['description'];
		$option = $_POST['option'];	
		$remarks = $_POST['remarks'];
		$arr =  explode(",",$_POST['measures']);
		
		if ($option == 'sketch') {
		### if sketch option
			$mapping_index = preg_replace('/\s+/','_',$alternative_name); // replace whitespaces and spaces with underscore 
			$mapping_index = strtolower($mapping_index); // change to lowercase // change later to a unique index name 
			$tab_name = $mapping_index;	
			
			$minx = $_POST['LatLon_minx'];
			$miny = $_POST['LatLon_miny'];
			$maxx = $_POST['LatLon_maxx'];
			$maxy = $_POST['LatLon_maxy'];
			
			// create a new empty table
			$query = "CREATE TABLE ".$workspace.".".$tab_name." (
					  fid serial NOT NULL,
					  geom geometry(MultiPolygon,4326),			  
					  mesure_nom text,
					  mesure_description text,
					  remarques text,
					  CONSTRAINT ".$tab_name."_pkey PRIMARY KEY (fid),
					  CONSTRAINT enforce_dims_the_geom CHECK (st_ndims(geom) = 2), 
					  CONSTRAINT enforce_geotype_the_geom CHECK (geometrytype(geom) = 'MULTIPOLYGON'::text OR geom IS NULL),
					  CONSTRAINT enforce_srid_the_geom CHECK (st_srid(geom) = 4326)
					)
					WITH (
					  OIDS=FALSE
					);";
		}	
		elseif ($option == 'upload') {
				// upload
				$temp = $_POST['names'];
				$array = json_decode($temp, true);
				$length = count($array);
				$temp = explode(":",$array[0]); 
				$mapping_index = end($temp);	
		}
		else {
			$mapping_index = preg_replace('/\s+/','_',$alternative_name); // replace whitespaces and spaces with underscore 
			$mapping_index = strtolower($mapping_index); // change to lowercase // change later to a unique index name
		}
		
		### add a record to the alternative table with meta info
		### save relationship records to the alternatives_measures table
			
		$query .= "INSERT INTO ".$workspace.".alternatives VALUES (DEFAULT, '$alternative_name', '$alternative_description', '$option', '$mapping_index','$remarks');"; 
		foreach ($arr as $measure){
			$query .= "INSERT INTO ".$workspace.".alternatives_measures VALUES ((SELECT id FROM ".$workspace.".alternatives WHERE indice = '$mapping_index'),(SELECT id FROM ".$workspace.".measures WHERE id = $measure));";
		}
				
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
			### if sketch option, publish the empty table to the geoserver
			if ($option == 'sketch') {
			// Initiate cURL session
				$service = "http://localhost:8080/geoserver/"; // replace with your URL
				$request = "rest/workspaces/".$workspace."/datastores/".$datastore."/featuretypes"; // to add a new featuretype

				$url = $service . $request;
				$ch = curl_init($url);

				// Optional settings for debugging
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //option to return string
				curl_setopt($ch, CURLOPT_VERBOSE, true);
				curl_setopt($ch, CURLOPT_STDERR, $logfh); // logs curl messages

				//Required POST request settings
				curl_setopt($ch, CURLOPT_POST, True);
				$passwordStr = "username:mypassword"; // replace with your GeoServer username and password
				curl_setopt($ch, CURLOPT_USERPWD, $passwordStr);

				//POST data
				curl_setopt($ch, CURLOPT_HTTPHEADER,
					array("Content-type: application/xml"));
				$xmlStr = "<featureType><name>".$tab_name."</name><latLonBoundingBox><minx>".$minx."</minx><maxx>".$maxx."</maxx><miny>".$miny."</miny><maxy>".$maxy."</maxy><crs>EPSG:4326</crs></latLonBoundingBox></featureType>";
				curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlStr);

				//POST return code
				$successCode = 201;

				// Execute the curl request
				$buffer = curl_exec($ch); 

				// Check for errors and process results
				$info = curl_getinfo($ch);
				if ($info['http_code'] != $successCode) {
					$msgStr = "# Unsuccessful cURL request to ";
					$msgStr .= $url." [". $info['http_code']. "]\n";
					Echo '{success:false,message:'.json_encode($msgStr).'}';
				} else {					
					$msgStr = "# Successful cURL request to ".$url."\n";
					Echo '{success: true, mpIndex:'.json_encode($mapping_index).',message:"The sketch layer has been created and added to the map. You can now start drawing the measures using CREATE and EDIT feature tools of the map center panel!"}';
				}

				// free resources if curl handle will not be reused
				curl_close($ch); 
			}
			else Echo '{success:true,message:"The new alternative scenario has been created!"}';			
		}
	}
	
	if ($task == 'edit') {
		// retrieve POST data 
		$mpIndex = $_POST['mpIndex'];
		$alternative_id = $_POST['ID'];
		$alternative_name = $_POST['name'];
		$alternative_description = $_POST['description'];
		$remarks = $_POST['remarks'];
		$option = $_POST['option'];
		$arr =  explode(",",$_POST['measureIDs']);
		
		$query = "UPDATE ".$workspace.".alternatives SET nom = '$alternative_name', description = '$alternative_description', remarques = '$remarks' WHERE id = $alternative_id;";
		$query .= "DELETE FROM ".$workspace.".alternatives_measures WHERE alt_id = $alternative_id;";
		
		foreach ($arr as $measureID){
			$query .= "INSERT INTO ".$workspace.".alternatives_measures VALUES ($alternative_id, $measureID);";
		}		
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
			if ($option == 'sketch' || $option == 'upload') {
				// If success, update the 'title' of layer in geoserver
				// Initiate cURL session
				$service = "http://localhost:8080/geoserver/"; // replace with your URL
				$request = "rest/workspaces/".$workspace."/datastores/".$datastore."/featuretypes/".$mpIndex.".xml"; // to update a featuretype

				$url = $service . $request;
				$ch = curl_init($url);

				// Optional settings for debugging
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //option to return string
				curl_setopt($ch, CURLOPT_VERBOSE, true);
				curl_setopt($ch, CURLOPT_STDERR, $logfh); // logs curl messages

				//Required PUT request settings
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
				$passwordStr = "username:mypassword"; // replace with your GeoServer username and password
				curl_setopt($ch, CURLOPT_USERPWD, $passwordStr);
				
				//POST data
				curl_setopt($ch, CURLOPT_HTTPHEADER,
					array("Content-type: application/xml"));
				$xmlStr = "<featureType><title>".$alternative_name."</title><keywords><string>features</string><string>".$alternative_name."</string></keywords><enabled>true</enabled></featureType>";
				curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlStr);

				//POST return code
				$successCode = 200;

				// Execute the curl request
				$buffer = curl_exec($ch); 
				
				$info = curl_getinfo($ch);
				curl_close($ch); // free resources if curl handle will not be reused
				
				if ($info['http_code'] != $successCode) {
					Echo '{success:true,message:"The data has been updated, but error in changing the layer title in geoserver!"}';
				}
				else {
					Echo '{success:true,message:"The data has been successfully updated!"}';
				}
			}
			else {
				Echo '{success:true,message:"The data has been successfully updated!"}';
			}
		}
	}

	if ($task == 'delete') {
		$temp = $_POST['altRecords'];		
		$altRecords = json_decode($temp, true);
		$length = count($altRecords);
		
		for ($i = 0; $i < $length; $i++) {	// for each of the selected alternatives	
			$altID = $altRecords[$i]['id'];
			$altMP = $altRecords[$i]['indice'];
			// if option 'descriptive', only remove the records from the alternative table
			if ($altRecords[$i]['categorie'] == 'descriptive'){
				$query = "DELETE FROM ".$workspace.".alternatives WHERE id = $altID;";
				if (!$rs = pg_query($dbconn,$query)){
					$message .= 'Failed to delete the record: '.$altRecords[$i]['nom'].' due to the error: '.json_encode(pg_last_error($dbconn));	
				}	
				else {
					$message .= 'The selected alternative: '.$altRecords[$i]['nom'].' has been deleted!\n';
				}
			}
			else {
			// if option 'Sketch'
			// Step 1: remove the records from the alternative table
				$query = "DELETE FROM ".$workspace.".alternatives WHERE id = $altID;"; 
				
			// Step 2: drop the respective tables using mapping_index from db
				$query .= "DROP TABLE ".$workspace.".$altMP;";
				
				if (!$rs = pg_query($dbconn,$query)){			
						$message .= 'Failed to delete the record: '.$altRecords[$i]['nom'].' due to the error: '.json_encode(pg_last_error($dbconn));						
					}	
				else {
					// if success
					// Step 3: remove the publish map from the geoserver
					// Initiate cURL session
					$service = "http://localhost:8080/geoserver/"; // replace with your URL
					$request = "rest/workspaces/".$workspace."/datastores/".$datastore."/featuretypes/".$altMP.".xml?recurse=true"; // to delete a featuretype

					$url = $service . $request;
					$ch = curl_init($url);

					// Optional settings for debugging
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //option to return string
					curl_setopt($ch, CURLOPT_VERBOSE, true);
					curl_setopt($ch, CURLOPT_STDERR, $logfh); // logs curl messages

					//Required DELETE request settings
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
					$passwordStr = "username:mypassword"; // replace with your GeoServer username and password
					curl_setopt($ch, CURLOPT_USERPWD, $passwordStr);

					//POST return code
					$successCode = 200;

					// Execute the curl request
					$buffer = curl_exec($ch); 

					// Check for errors and process results
					$info = curl_getinfo($ch);			
					if ($info['http_code'] != $successCode) {
						$message .= "# Unuccessful cURL request to ".$url."\n";					
					}
					else {
						$message .= 'The selected alternative: '.$altRecords[$i]['nom'].' has been deleted!\n';
					}
					curl_close($ch); // free resources if curl handle will not be reused
				}
			}
		}		
		Echo '{success: "true", message:'.json_encode($message).'}';			
	}
	
	pg_close($dbconn);
?>