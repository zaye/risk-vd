<?php
	$task = $_POST['task'];	
	$id = $_POST['userID'];
	
	$dbconn=pg_connect("host=localhost port=5432 dbname=dbname user=username password=mypassword");	// replace with your db connection information
	
	if (!$dbconn){
		echo "An error occured.\n";
		exit;
	}
	
	if ($task == 'load') {
		$query = "SELECT workspace.* FROM public.workspace WHERE users_id = $id;"; 		
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	if ($task == 'edit') {			
		$id = $_POST['wsID'];	
		$desc = $_POST['description'];
		$scale = $_POST['ws_scale'];
		$proj = $_POST['projection'];
		
		$query= "UPDATE public.workspace SET ws_description = '$desc', ws_scale = '$scale', ws_projection = '$proj' WHERE ws_id = $id;";
		$arr=array();
				
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
			Echo '{success:true,message:"The data has been successfully updated!"}';		
		}
	}	
	
	if ($task == 'delete') {
		$temp = $_POST['objRecords'];		
		$objRecords = json_decode($temp, true);
		$length = count($objRecords);
		
		for ($i = 0; $i < $length; $i++) {	// for each of the selected workspace
			$ID = $objRecords[$i]['ws_id'];
			$wsName = $objRecords[$i]['ws_name'];
			
			### Step 1: delete the workspace record from the public.workspace table
			### Step 2: delete the schema from the database
			$query = "DELETE FROM public.workspace WHERE ws_id = $ID AND ws_name = '$wsName';";
			$query .= "DROP SCHEMA $wsName CASCADE;";
			If (!$rs = pg_query($dbconn,$query)) {
				Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
			}
			else {
				// if success
				### Step 3: delete the workspace from the geoserver
				
				// Initiate cURL session
				$service = "http://localhost:8080/geoserver/"; // replace with your URL
				$request = "rest/workspaces/".$wsName.".xml?recurse=true"; // to delete a workspace

				$url = $service . $request;
				$ch = curl_init($url);

				// Optional settings for debugging
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //option to return string
				curl_setopt($ch, CURLOPT_VERBOSE, true);
				curl_setopt($ch, CURLOPT_STDERR, $logfh); // logs curl messages

				//Required DELETE request settings
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
				$passwordStr = "username:mypassword"; // replace with your GeoServer username and password
				curl_setopt($ch, CURLOPT_USERPWD, $passwordStr);

				//POST return code
				$successCode = 200;

				// Execute the curl request
				$buffer = curl_exec($ch); 

				// Check for errors and process results
				$info = curl_getinfo($ch);			
				if ($info['http_code'] != $successCode) {
					$message .= "Unuccessful cURL request to ".$url."\n";						
				}
				else {
					$message .= 'The selected workspace: '.$wsName.' has been deleted!\n';
				}
				curl_close($ch); // free resources if curl handle will not be reused
			}
		}
		Echo '{success: true, message:'.json_encode($message).'}';
	}
	
	pg_close($dbconn);
?>