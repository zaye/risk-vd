<?php
	$workspace = $_POST['ws'];
	$task = $_POST['task'];	
		
	$dbconn=pg_connect("host=localhost port=5432 dbname=dbname user=username password=mypassword");	// replace with your db connection information
	if (!$dbconn){
		echo "An error occured.\n";
		exit;
		}
		
	if ($task == 'load') {
		$query = "SELECT id, nom, description, type, remarques,indice FROM ".$workspace.".spatial_probability;"; 		
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}

	if ($task == 'loadUserDefined') {
		$query = "SELECT nom, indice FROM ".$workspace.".spatial_probability WHERE type != 'default';"; 		
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	if ($task == 'loadPrAValues') {
		// retrieve POST data
		$mpIndex = $_POST['mpIndex'];
		
		$query = "SELECT * FROM ".$workspace.".".$mpIndex.";"; 		
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	if ($task == 'add') {
		// retrieve POST data 
		$name = $_POST['name'];
		$description = $_POST['description'];
		$remarks = $_POST['remarks'];		
		
		// STEP 1: Create a new table with default data values
		// STEP 2: Register a new record in 'spatial_probability' table 
		
		$query = "DO
				$$
				BEGIN
				EXECUTE format('CREATE TABLE $workspace.%I AS SELECT * FROM $workspace.spatial_prob_default', 'spatial_prob_' || (SELECT last_value + 1 FROM $workspace.spatial_probability_id_seq));
				EXECUTE format('INSERT INTO $workspace.spatial_probability VALUES (DEFAULT, ''$name'', ''$description'', ''user defined'', ''$remarks'', ''%I'')', 'spatial_prob_' || (SELECT last_value + 1 FROM $workspace.spatial_probability_id_seq));
				END;
				$$ LANGUAGE plpgsql;";
		
		/* 
		$mapping_index = $name; // change later to a unique index name 
		$query = "CREATE TABLE ".$workspace.".".$mapping_index." AS TABLE ".$workspace.".spatial_prob_default;";
		$query .= "INSERT INTO ".$workspace.".spatial_probability VALUES (DEFAULT, '$name', '$description', 'user defined', '$remarks', '$mapping_index');"; */
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
			Echo '{success:true,message:"The new configuration table has been created. You can now edit the default values!"}';
		}
	}

	// to save the dirty matrix values to the corresponding table 
	if ($_GET['task'] == 'save'){
		$workspace = $_GET['ws'];
		$mpIndex = $_GET['mpIndex'];
		$postdata = file_get_contents("php://input"); 
		$phpArray = json_decode($postdata, true);
		$length = count($phpArray);	
		
		for ($i = 0; $i < $length; $i++) { // loop for each of the dirty records
			$temp = $phpArray[$i]; 			
			$id = $temp['id'];		
			$probabilite = $temp['probabilite'];	
			$praValue = $temp['valeur'];
			$remark = $temp['remarques'];			
			
			$query .= "UPDATE ".$workspace.".".$mpIndex." SET probabilite = '$probabilite', valeur = $praValue, remarques = '$remark' WHERE id = $id;";						
		}
		
		If (!$rs = pg_query($dbconn,$query)) {
				Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
			}
		else {			
			Echo '{success:true,message:"The spatial probability values have been updated!"}';
		}	
	}
	
	// to edit the information of the configuration
	if ($task == 'edit') {
		$ID = $_POST['ID'];	
		$nom = $_POST['name'];
		$desc = $_POST['description'];
		$remarques = $_POST['remarks'];
		
		$query= "UPDATE ".$workspace.".spatial_probability SET nom = '$nom', description = '$desc', remarques = '$remarques' WHERE id = $ID;";
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
			Echo '{success:true,message:"The data has been updated!"}';
		}
	}
	
	if ($task == 'delete') {
		$temp = $_POST['IDs'];		
		$records = json_decode($temp, true);
		$length = count($records);
		
		for ($i = 0; $i < $length; $i++) {	// for each of the selected configurations
			$ID = $records[$i]['id'];			
			$mpIndex = $records[$i]['indice'];
			
			### Step1: remove the records from the spatial_probability table 
			$query = "DELETE FROM ".$workspace.".spatial_probability WHERE id = $ID;";
			
			### Step2: drop the respective table using indice column from the db 
			$query .= "DROP TABLE ".$workspace.".$mpIndex;";
				
			if (!$rs = pg_query($dbconn,$query)){			
				$message .= 'Failed to delete the configuration: '.$records[$i]['nom'].' due to the error: '.json_encode(pg_last_error($dbconn));						
			}
			else {
				$message .= 'The selected configuration: '.$records[$i]['nom'].' has been deleted!';
			}
		}
	}
	
	pg_close($dbconn);
?>