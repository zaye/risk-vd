<?php
	$workspace = $_POST['ws'];
	$task = $_POST['task'];	
		
	$dbconn=pg_connect("host=localhost port=5432 dbname=dbname user=username password=mypassword");	// replace with your db connection information
	if (!$dbconn){
		echo "An error occured.\n";
		exit;
		}
	
	if ($task == 'load') {
		$query = "SELECT * FROM ".$workspace.".cost_benefit;";
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	if ($task == 'loadGrid') {
		$query = "SELECT DISTINCT risk_scenarios.*, COALESCE(cost_values.cout_id,0) as cout_id, cost.nom as cout_nom, alternatives.nom AS alt_nom, COALESCE(sum(cout_annuel) OVER (PARTITION BY risk_scenarios.id, cout_id), 0) AS cout_total
			FROM ".$workspace.".risk_scenarios
			LEFT OUTER JOIN ".$workspace.".alternatives ON (risk_scenarios.alt_id = alternatives.id)
			LEFT OUTER JOIN ".$workspace.".cost_values ON (alternatives.id = cost_values.alt_id)
			LEFT OUTER JOIN ".$workspace.".cost ON (cost.id = cost_values.cout_id);";	

		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}	
	
	if ($task == 'add') {
		
		$name_report = $_POST['name_report'];
		$description_report = $_POST['description_report'];			
		$remarks_report = $_POST['remarks_report'];
		
		$postdata = $_POST['records'];		
		$phpArray = json_decode($postdata, true);
		$length = count($phpArray);
		
		### STEP 1: add a record to the cost_benefit table
		$query = "INSERT INTO ".$workspace.".cost_benefit VALUES (DEFAULT, '$name_report', '$description_report', '$remarks_report');"; 
		
		###	STEP 2: insert the new records to cost_benefit_values table
		for ($i = 0; $i < $length; $i++) { // loop for each record
			$temp = $phpArray[$i]; 	
			$costID = $temp['cout_id'];	
			$riskSceID = $temp['id'];
			
			$query .= "INSERT INTO ".$workspace.".cost_benefit_values VALUES (
				(SELECT last_value FROM ".$workspace.".cost_benefit_id_seq), $costID, $riskSceID);"; 				
		}
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {				
			Echo '{success:true,message:"The new cost-benefit report has been generated!"}';
		}
	}
	
	if ($task == 'update') { // to update data from 'cost_benefit' table
		$ID = $_POST['ID'];	
		$name = $_POST['name_report'];
		$desc = $_POST['description_report'];
		$remarks = $_POST['remarks_report'];
		
		$query= "UPDATE ".$workspace.".cost_benefit SET nom = '$name', description = '$desc', remarques = '$remarks' WHERE id = $ID;";
				
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
			Echo '{success:true,message:"The data has been updated!"}';
		}
	}
	
	if ($task == 'loadChartStoreGrid') {
		$id = $_POST['id'];
		
		$query = "WITH t2 AS 
			(WITH t1 AS (SELECT cost_benefit_values.cout_id, cost.nom as cout_nom, alternatives.nom as alt_nom, sum(risque_total) as risque_total 
			FROM ".$workspace.".cost_benefit_values, ".$workspace.".cost, ".$workspace.".alternatives, ".$workspace.".risk_scenarios
			WHERE cb_id = $id
			AND cost_benefit_values.cout_id = cost.id
			AND cost_benefit_values.risque_sce_id = risk_scenarios.id
			AND cost.alt_id = alternatives.id
			GROUP BY cost_benefit_values.cout_id, cout_nom, alt_nom)
			SELECT t1.*, (SELECT t1.risque_total as tp FROM t1 WHERE cout_id = 0), COALESCE(sum(cout_annuel),0) as cout_annuel, 0 as benefice, 0 as rapport FROM t1 LEFT JOIN ".$workspace.".cost_values
			ON t1.cout_id = cost_values.cout_id
			GROUP BY t1.cout_id, t1.cout_nom, t1.alt_nom, t1.risque_total)
			SELECT t2.*, (tp-risque_total) as benefice FROM t2;"; 			
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	if ($task == 'delete'){ // happens when a cost-benefit report is deleted
		$workspace = $_POST['ws'];
				
		$temp = $_POST['costBenIDs'];		
		$records = json_decode($temp, true);
		$length = count($records);
		
		for ($i = 0; $i < $length; $i++) {	// for each of the selected records
			$ID = $records[$i];			
			$query .= "DELETE FROM ".$workspace.".cost_benefit WHERE id = $ID;";			
		}
		
		if (!$rs = pg_query($dbconn,$query)){			
			Echo '{success:"false",message:'.json_encode(pg_last_error($dbconn)).'}';						
		}
		else {
			Echo '{success:"true",message: "The selected cost-benefit reports have been deleted!"}';	
		}		
	}
	
	if ($task == 'loadCostBenValues') {
		$id = $_POST['cbID'];
		
		$query = "SELECT cost_benefit_values.cout_id, cost.nom as cout_nom, cost_benefit_values.risque_sce_id, risk_scenarios.nom as risque_sce_nom, alternatives.nom as alt_nom, processus_type, objet_type, risque_total, risk_scenarios.indice
				FROM ".$workspace.".cost_benefit_values, ".$workspace.".alternatives, ".$workspace.".risk_scenarios, ".$workspace.".cost
				WHERE cb_id = $id AND cost_benefit_values.cout_id = cost.id AND cost_benefit_values.risque_sce_id = risk_scenarios.id AND cost.alt_id = alternatives.id;";
				
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	if ($task == 'loadCostBenComValues') { // to retrieve the summary sheet of risk scenarios
		### make sure risque_f function exists under the 'public' schema of the respective database
		### if it doesn't exist, run the following commented SQL code to create the function in the respective database
	  /*-- Function: risque_f(text, character varying, character varying)
		-- DROP FUNCTION risque_f(text, character varying, character varying);

		CREATE OR REPLACE FUNCTION risque_f(
			IN tablename text,
			IN para1 character varying,
			IN para2 character varying,
			OUT result double precision)
		  RETURNS double precision AS
		$BODY$
		BEGIN
		EXECUTE 'SELECT COALESCE(sum('|| para1 ||'),0) FROM ' || tablename || ' WHERE cin_sce_probabilite = ''' || para2 || ''''
		INTO result;
		END
		$BODY$
		  LANGUAGE plpgsql VOLATILE
		  COST 100;
		ALTER FUNCTION risque_f(text, character varying, character varying)
		  OWNER TO admin; */
		  
		$id = $_POST['cbID'];		
		$query = "SELECT cost_benefit_values.cout_id, cost.nom as cout_nom, cost_benefit_values.risque_sce_id, risk_scenarios.nom as risque_sce_nom, alternatives.nom as alt_nom, processus_type, objet_type, risk_scenarios_hazards.id as risque_sce_pro_id, temp_de_retour, risk_scenarios.indice,
				(select risque_f('$workspace.' || risk_scenarios.indice,'risque_obj',temp_de_retour) as risque_obj),
				(select risque_f('$workspace.' || risk_scenarios.indice,'risque_humain',temp_de_retour) as risque_humain),
				(select risque_f('$workspace.' || risk_scenarios.indice,'risque_obj',temp_de_retour) + (select risque_f('$workspace.' || risk_scenarios.indice,'risque_humain',temp_de_retour)) as risque_total),
				(select risque_f('$workspace.' || risk_scenarios.indice,'risque_individuel',temp_de_retour) as risque_individuel)
				FROM ".$workspace.".cost_benefit_values, ".$workspace.".alternatives, ".$workspace.".risk_scenarios, ".$workspace.".risk_scenarios_hazards, ".$workspace.".cost
				WHERE cb_id = $id AND cost_benefit_values.cout_id = cost.id AND cost_benefit_values.risque_sce_id = risk_scenarios.id AND cost.alt_id = alternatives.id AND risk_scenarios_hazards.id_sce = risk_scenarios.id;";				
				
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	pg_close($dbconn);	
?>